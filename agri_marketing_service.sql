-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 02, 2021 at 11:08 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `agri_marketing_service`
--

-- --------------------------------------------------------

--
-- Table structure for table `campaign_information`
--

CREATE TABLE `campaign_information` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `divisional_office_id` bigint(20) UNSIGNED NOT NULL,
  `division_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `document_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `document_name_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `campaign_date` date NOT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cpi_market_commodity_growers_prices`
--

CREATE TABLE `cpi_market_commodity_growers_prices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `price_type_id` text COLLATE utf8mb4_unicode_ci,
  `price_entry_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `select_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `market_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `upazila_id` int(11) NOT NULL,
  `price_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cpi_market_commodity_growers_prices`
--

INSERT INTO `cpi_market_commodity_growers_prices` (`id`, `price_type_id`, `price_entry_type`, `select_type`, `market_id`, `division_id`, `district_id`, `upazila_id`, `price_date`, `created_at`, `updated_at`, `dam_id`) VALUES
(1, '[\"Grower\"]', 'Grower', 'Daily', 2, 1, 1, 11, '2021-06-26', '2021-06-25 23:04:40', '2021-06-25 23:05:48', NULL),
(2, '[\"Grower\"]', 'Grower', 'Daily', 2, 1, 1, 11, '2021-06-27', '2021-06-25 23:06:16', '2021-06-25 23:06:16', NULL),
(3, '[\"Grower\"]', 'Grower', 'Daily', 5, 6, 45, 355, '2021-06-26', '2021-06-26 00:17:02', '2021-06-26 00:17:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cpi_market_commodity_growers_price_details`
--

CREATE TABLE `cpi_market_commodity_growers_price_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `price_table_id` bigint(20) UNSIGNED NOT NULL,
  `price_type` text COLLATE utf8mb4_unicode_ci,
  `com_grp_id` int(11) DEFAULT NULL,
  `com_subgrp_id` int(11) DEFAULT NULL,
  `commodity_id` bigint(20) UNSIGNED NOT NULL,
  `g_lowestPrice` decimal(8,2) DEFAULT NULL,
  `g_highestPrice` decimal(8,2) DEFAULT NULL,
  `g_supply` decimal(8,2) DEFAULT NULL,
  `g_avgPrice` decimal(8,2) DEFAULT NULL,
  `g_percentage_LtoH` decimal(8,2) DEFAULT NULL,
  `g_last_entry` decimal(8,2) DEFAULT NULL,
  `unit_grower` int(11) DEFAULT NULL,
  `submitted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `verified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `verification_date` date DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=inactive, 1=active',
  `comment_id` bigint(20) UNSIGNED DEFAULT NULL,
  `price_date` date DEFAULT NULL COMMENT 'last date for %last entry calculation ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cpi_market_commodity_growers_price_details`
--

INSERT INTO `cpi_market_commodity_growers_price_details` (`id`, `price_table_id`, `price_type`, `com_grp_id`, `com_subgrp_id`, `commodity_id`, `g_lowestPrice`, `g_highestPrice`, `g_supply`, `g_avgPrice`, `g_percentage_LtoH`, `g_last_entry`, `unit_grower`, `submitted_by`, `verified_by`, `verification_date`, `status`, `comment_id`, `price_date`, `created_at`, `updated_at`, `dam_id`) VALUES
(7, 1, '[\"Grower\"]', 2, 39, 20, '1.00', '2.00', NULL, '0.00', '0.00', '0.00', 1, 1, NULL, NULL, 0, NULL, '2021-06-26', '2021-06-25 23:05:48', '2021-06-25 23:05:48', NULL),
(8, 1, '[\"Grower\"]', 5, 37, 21, '3.00', '4.00', NULL, '0.00', '0.00', '0.00', 5, 1, NULL, NULL, 0, NULL, '2021-06-26', '2021-06-25 23:05:48', '2021-06-25 23:05:48', NULL),
(9, 1, '[\"Grower\"]', 5, 37, 22, '5.00', '6.00', NULL, '0.00', '0.00', '0.00', 5, 1, NULL, NULL, 0, NULL, '2021-06-26', '2021-06-25 23:05:48', '2021-06-25 23:05:48', NULL),
(10, 1, '[\"Grower\"]', 1, 38, 23, '7.00', '8.00', NULL, '0.00', '0.00', '0.00', 4, 1, NULL, NULL, 0, NULL, '2021-06-26', '2021-06-25 23:05:48', '2021-06-25 23:05:48', NULL),
(11, 1, '[\"Grower\"]', 5, 37, 24, '90.00', '100.00', NULL, '0.00', '0.00', '0.00', 5, 1, NULL, NULL, 0, NULL, '2021-06-26', '2021-06-25 23:05:48', '2021-06-25 23:05:48', NULL),
(12, 1, '[\"Grower\"]', 4, 36, 25, '110.00', '120.00', NULL, '0.00', '0.00', '0.00', 3, 1, NULL, NULL, 0, NULL, '2021-06-26', '2021-06-25 23:05:48', '2021-06-25 23:05:48', NULL),
(13, 2, '[\"Grower\"]', 2, 39, 20, '1.00', '2.00', NULL, '0.00', '0.00', '0.00', 1, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-25 23:06:16', '2021-06-25 23:06:16', NULL),
(14, 2, '[\"Grower\"]', 5, 37, 21, '3.00', '4.00', NULL, '0.00', '0.00', '0.00', 5, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-25 23:06:16', '2021-06-25 23:06:16', NULL),
(15, 2, '[\"Grower\"]', 5, 37, 22, '5.00', '6.00', NULL, '0.00', '0.00', '0.00', 5, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-25 23:06:16', '2021-06-25 23:06:16', NULL),
(16, 2, '[\"Grower\"]', 1, 38, 23, '7.00', '8.00', NULL, '0.00', '0.00', '0.00', 4, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-25 23:06:16', '2021-06-25 23:06:16', NULL),
(17, 2, '[\"Grower\"]', 5, 37, 24, '90.00', '100.00', NULL, '0.00', '0.00', '0.00', 5, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-25 23:06:16', '2021-06-25 23:06:16', NULL),
(18, 2, '[\"Grower\"]', 4, 36, 25, '110.00', '120.00', NULL, '0.00', '0.00', '0.00', 3, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-25 23:06:16', '2021-06-25 23:06:16', NULL),
(19, 3, '[\"Grower\"]', 2, 39, 20, '65.00', '70.00', NULL, '0.00', '0.00', '0.00', 1, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-26 00:17:02', '2021-06-26 00:17:02', NULL),
(20, 3, '[\"Grower\"]', 5, 37, 21, '65.00', '79.00', NULL, '0.00', '0.00', '0.00', 5, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-26 00:17:02', '2021-06-26 00:17:02', NULL),
(21, 3, '[\"Grower\"]', 5, 37, 22, '55.00', '60.00', NULL, '0.00', '0.00', '0.00', 5, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-26 00:17:02', '2021-06-26 00:17:02', NULL),
(22, 3, '[\"Grower\"]', 1, 38, 23, '45.00', '50.00', NULL, '0.00', '0.00', '0.00', 4, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-26 00:17:02', '2021-06-26 00:17:02', NULL),
(23, 3, '[\"Grower\"]', 5, 37, 24, '35.00', '40.00', NULL, '0.00', '0.00', '0.00', 5, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-26 00:17:02', '2021-06-26 00:17:02', NULL),
(24, 3, '[\"Grower\"]', 4, 36, 25, '1000.00', '1100.00', NULL, '0.00', '0.00', '0.00', 3, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-26 00:17:02', '2021-06-26 00:17:02', NULL),
(25, 3, '[\"Grower\"]', 6, 41, 26, '24.00', '30.00', NULL, '0.00', '0.00', '0.00', 1, NULL, NULL, NULL, 1, NULL, NULL, '2021-06-26 00:17:02', '2021-06-26 00:17:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cpi_market_commodity_prices`
--

CREATE TABLE `cpi_market_commodity_prices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `price_date` date NOT NULL,
  `price_entry_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_type_id` text COLLATE utf8_unicode_ci,
  `select_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `upazila_id` int(11) DEFAULT NULL,
  `market_id` int(11) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cpi_market_commodity_prices`
--

INSERT INTO `cpi_market_commodity_prices` (`id`, `price_date`, `price_entry_type`, `price_type_id`, `select_type`, `division_id`, `district_id`, `upazila_id`, `market_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `dam_id`) VALUES
(1, '2021-06-26', 'Market', '[\"Wholesale\",\"Retail\"]', 'Daily', 1, 1, 11, 2, 1, 1, '2021-06-25 22:59:58', '2021-06-25 22:59:58', NULL),
(2, '2021-06-27', 'Market', '[\"Wholesale\",\"Retail\"]', 'Daily', 1, 1, 11, 2, 1, 1, '2021-06-25 23:02:36', '2021-06-25 23:02:36', NULL),
(3, '2021-06-26', 'Market', '[\"Wholesale\",\"Retail\"]', 'Daily', 6, 45, 355, 5, 1, 1, '2021-06-26 00:14:47', '2021-06-26 00:14:47', NULL),
(4, '2021-06-25', 'Market', '[\"Retail\",\"Wholesale\"]', 'Daily', 6, 45, 355, 5, 1, 1, '2021-06-26 00:17:59', '2021-06-26 00:17:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cpi_market_commodity_price_details`
--

CREATE TABLE `cpi_market_commodity_price_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `price_table_id` int(11) DEFAULT NULL,
  `price_type_id` text COLLATE utf8_unicode_ci,
  `com_grp_id` int(11) DEFAULT NULL,
  `com_subgrp_id` int(11) DEFAULT NULL,
  `commodity_id` int(11) DEFAULT NULL,
  `unit_retail` int(11) DEFAULT NULL,
  `unit_wholesale` int(11) DEFAULT NULL,
  `w_lowestPrice` decimal(10,2) DEFAULT NULL,
  `w_highestPrice` decimal(10,2) DEFAULT NULL,
  `w_supply` decimal(10,2) DEFAULT NULL,
  `r_lowestPrice` decimal(10,2) DEFAULT NULL,
  `r_highestPrice` decimal(10,2) DEFAULT NULL,
  `r_supply` decimal(10,2) DEFAULT NULL,
  `w_avgPrice` decimal(10,2) DEFAULT NULL,
  `r_avgPrice` decimal(10,2) DEFAULT NULL,
  `w_percentage_LtoH` decimal(10,2) DEFAULT NULL,
  `r_percentage_LtoH` decimal(10,2) DEFAULT NULL,
  `w_last_entry` decimal(10,2) DEFAULT NULL,
  `r_last_entry` decimal(10,2) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `verified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `verification_date` date DEFAULT NULL,
  `comment_id` bigint(20) UNSIGNED DEFAULT NULL,
  `comparison_date` date DEFAULT NULL,
  `submit_status` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cpi_market_commodity_price_details`
--

INSERT INTO `cpi_market_commodity_price_details` (`id`, `price_table_id`, `price_type_id`, `com_grp_id`, `com_subgrp_id`, `commodity_id`, `unit_retail`, `unit_wholesale`, `w_lowestPrice`, `w_highestPrice`, `w_supply`, `r_lowestPrice`, `r_highestPrice`, `r_supply`, `w_avgPrice`, `r_avgPrice`, `w_percentage_LtoH`, `r_percentage_LtoH`, `w_last_entry`, `r_last_entry`, `created_by`, `updated_by`, `verified_by`, `verification_date`, `comment_id`, `comparison_date`, `submit_status`, `status`, `created_at`, `updated_at`, `dam_id`) VALUES
(1, 1, '[\"Wholesale\",\"Retail\"]', 2, 39, 20, 5, 3, '50.00', '60.00', NULL, '70.00', '80.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-26', 1, 1, '2021-06-25 22:59:58', '2021-06-25 22:59:58', NULL),
(2, 1, '[\"Wholesale\",\"Retail\"]', 5, 37, 21, 2, 1, '90.00', '100.00', NULL, '110.00', '120.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-26', 1, 1, '2021-06-25 22:59:58', '2021-06-25 22:59:58', NULL),
(3, 1, '[\"Wholesale\",\"Retail\"]', 5, 37, 22, 2, 4, '130.00', '140.00', NULL, '150.00', '160.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-26', 1, 1, '2021-06-25 22:59:58', '2021-06-25 22:59:58', NULL),
(4, 1, '[\"Wholesale\",\"Retail\"]', 1, 38, 23, 3, 3, '170.00', '180.00', NULL, '190.00', '200.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-26', 1, 1, '2021-06-25 22:59:58', '2021-06-25 22:59:58', NULL),
(5, 1, '[\"Wholesale\",\"Retail\"]', 5, 37, 24, 4, 3, '210.00', '220.00', NULL, '230.00', '240.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-26', 1, 1, '2021-06-25 22:59:58', '2021-06-25 22:59:58', NULL),
(6, 2, '[\"Wholesale\",\"Retail\"]', 2, 39, 20, 5, 3, '50.00', '60.00', NULL, '70.00', '80.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-27', 1, 0, '2021-06-25 23:02:36', '2021-06-25 23:02:36', NULL),
(7, 2, '[\"Wholesale\",\"Retail\"]', 5, 37, 21, 2, 1, '90.00', '100.00', NULL, '110.00', '120.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-27', 1, 0, '2021-06-25 23:02:36', '2021-06-25 23:02:36', NULL),
(8, 2, '[\"Wholesale\",\"Retail\"]', 5, 37, 22, 2, 4, '130.00', '140.00', NULL, '150.00', '160.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-27', 1, 0, '2021-06-25 23:02:36', '2021-06-25 23:02:36', NULL),
(9, 2, '[\"Wholesale\",\"Retail\"]', 1, 38, 23, 3, 3, '170.00', '180.00', NULL, '190.00', '200.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-27', 1, 0, '2021-06-25 23:02:36', '2021-06-25 23:02:36', NULL),
(10, 2, '[\"Wholesale\",\"Retail\"]', 5, 37, 24, 4, 3, '210.00', '220.00', NULL, '230.00', '240.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-27', 1, 0, '2021-06-25 23:02:36', '2021-06-25 23:02:36', NULL),
(11, 3, '[\"Wholesale\",\"Retail\"]', 2, 39, 20, 5, 3, '70.00', '80.00', NULL, '60.00', '70.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-26', 1, 1, '2021-06-26 00:14:47', '2021-06-26 00:14:47', NULL),
(12, 3, '[\"Wholesale\",\"Retail\"]', 5, 37, 21, 2, 1, '30.00', '40.00', NULL, '20.00', '30.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-26', 1, 1, '2021-06-26 00:14:47', '2021-06-26 00:14:47', NULL),
(13, 3, '[\"Wholesale\",\"Retail\"]', 5, 37, 22, 2, 4, '55.00', '60.00', NULL, '45.00', '50.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-26', 1, 1, '2021-06-26 00:14:47', '2021-06-26 00:14:47', NULL),
(14, 3, '[\"Wholesale\",\"Retail\"]', 1, 38, 23, 3, 3, '30.00', '35.00', NULL, '35.00', '3.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-26', 1, 1, '2021-06-26 00:14:47', '2021-06-26 00:14:47', NULL),
(15, 3, '[\"Wholesale\",\"Retail\"]', 5, 37, 24, 4, 3, '38.00', '48.00', NULL, '28.00', '35.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-26', 1, 1, '2021-06-26 00:14:47', '2021-06-26 00:14:47', NULL),
(16, 3, '[\"Wholesale\",\"Retail\"]', 4, 36, 25, 5, 1, '1000.00', '1100.00', NULL, '950.00', '980.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-26', 1, 1, '2021-06-26 00:14:48', '2021-06-26 00:14:48', NULL),
(17, 3, '[\"Wholesale\",\"Retail\"]', 6, 41, 26, 3, 2, '199.00', '190.00', NULL, '180.00', '190.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-26', 1, 1, '2021-06-26 00:14:48', '2021-06-26 00:14:48', NULL),
(18, 4, '[\"Retail\",\"Wholesale\"]', 2, 39, 20, 5, 3, '70.00', '80.00', NULL, '60.00', '70.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-25', 1, 1, '2021-06-26 00:17:59', '2021-06-26 00:17:59', NULL),
(19, 4, '[\"Retail\",\"Wholesale\"]', 5, 37, 21, 2, 1, '30.00', '40.00', NULL, '20.00', '30.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-25', 1, 1, '2021-06-26 00:17:59', '2021-06-26 00:17:59', NULL),
(20, 4, '[\"Retail\",\"Wholesale\"]', 5, 37, 22, 2, 4, '55.00', '60.00', NULL, '45.00', '50.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-25', 1, 1, '2021-06-26 00:17:59', '2021-06-26 00:17:59', NULL),
(21, 4, '[\"Retail\",\"Wholesale\"]', 1, 38, 23, 3, 3, '30.00', '35.00', NULL, '35.00', '3.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-25', 1, 1, '2021-06-26 00:17:59', '2021-06-26 00:17:59', NULL),
(22, 4, '[\"Retail\",\"Wholesale\"]', 5, 37, 24, 4, 3, '38.00', '48.00', NULL, '28.00', '35.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-25', 1, 1, '2021-06-26 00:17:59', '2021-06-26 00:17:59', NULL),
(23, 4, '[\"Retail\",\"Wholesale\"]', 4, 36, 25, 5, 1, '1000.00', '1100.00', NULL, '950.00', '980.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-25', 1, 1, '2021-06-26 00:17:59', '2021-06-26 00:17:59', NULL),
(24, 4, '[\"Retail\",\"Wholesale\"]', 6, 41, 26, 3, 2, '199.00', '190.00', NULL, '180.00', '190.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-06-25', 1, 1, '2021-06-26 00:17:59', '2021-06-26 00:17:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cpi_market_commodity_weekly_prices`
--

CREATE TABLE `cpi_market_commodity_weekly_prices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `price_entry_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_type_id` text COLLATE utf8_unicode_ci,
  `select_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `upazila_id` int(11) DEFAULT NULL,
  `market_id` int(11) DEFAULT NULL,
  `year` int(5) DEFAULT NULL,
  `month_id` int(11) DEFAULT NULL,
  `week_id` int(11) DEFAULT NULL,
  `week_table_id` int(11) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cpi_market_commodity_weekly_prices`
--

INSERT INTO `cpi_market_commodity_weekly_prices` (`id`, `price_entry_type`, `price_type_id`, `select_type`, `division_id`, `district_id`, `upazila_id`, `market_id`, `year`, `month_id`, `week_id`, `week_table_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `dam_id`) VALUES
(1, 'Market', '[\"Wholesale\",\"Retail\"]', 'Weekly', 1, 1, 11, 2, 2021, 6, 1, 1, 1, 1, '2021-06-25 23:08:47', '2021-06-25 23:08:47', NULL),
(2, 'Grower', '[\"Wholesale\",\"Retail\",\"Grower\"]', 'Weekly', 1, 1, 11, 2, 2021, 6, 1, 1, 1, 1, '2021-06-25 23:10:13', '2021-06-25 23:10:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cpi_market_commodity_weekly_price_details`
--

CREATE TABLE `cpi_market_commodity_weekly_price_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `price_table_id` bigint(20) UNSIGNED NOT NULL,
  `price_type_id` text COLLATE utf8mb4_unicode_ci,
  `com_grp_id` bigint(20) UNSIGNED DEFAULT NULL,
  `com_subgrp_id` bigint(20) UNSIGNED DEFAULT NULL,
  `commodity_id` bigint(20) UNSIGNED NOT NULL,
  `w_lowestPrice` decimal(8,2) DEFAULT NULL,
  `w_highestPrice` decimal(8,2) DEFAULT NULL,
  `w_supply` decimal(8,2) DEFAULT NULL,
  `r_lowestPrice` decimal(8,2) DEFAULT NULL,
  `r_highestPrice` decimal(8,2) DEFAULT NULL,
  `r_supply` decimal(8,2) DEFAULT NULL,
  `w_avgPrice` decimal(8,2) DEFAULT NULL,
  `r_avgPrice` decimal(8,2) DEFAULT NULL,
  `w_percentage_LtoH` decimal(8,2) DEFAULT NULL,
  `r_percentage_LtoH` decimal(8,2) DEFAULT NULL,
  `w_last_entry` decimal(8,2) DEFAULT NULL,
  `r_last_entry` decimal(8,2) DEFAULT NULL,
  `g_last_entry` decimal(8,2) DEFAULT NULL,
  `g_lowestPrice` decimal(12,2) DEFAULT NULL,
  `g_highestPrice` decimal(12,2) DEFAULT NULL,
  `g_avgPrice` decimal(12,2) DEFAULT NULL,
  `g_percentage_LtoH` decimal(12,2) DEFAULT NULL,
  `unit_retail` int(11) DEFAULT NULL,
  `unit_wholesale` int(11) DEFAULT NULL,
  `unit_grower` int(11) DEFAULT NULL,
  `submitted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `verified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `verification_date` date DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=inactive, 1=active',
  `comment_id` bigint(20) UNSIGNED DEFAULT NULL,
  `comparison_date` date DEFAULT NULL COMMENT 'last date for %last entry calculation ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cpi_market_commodity_weekly_price_details`
--

INSERT INTO `cpi_market_commodity_weekly_price_details` (`id`, `price_table_id`, `price_type_id`, `com_grp_id`, `com_subgrp_id`, `commodity_id`, `w_lowestPrice`, `w_highestPrice`, `w_supply`, `r_lowestPrice`, `r_highestPrice`, `r_supply`, `w_avgPrice`, `r_avgPrice`, `w_percentage_LtoH`, `r_percentage_LtoH`, `w_last_entry`, `r_last_entry`, `g_last_entry`, `g_lowestPrice`, `g_highestPrice`, `g_avgPrice`, `g_percentage_LtoH`, `unit_retail`, `unit_wholesale`, `unit_grower`, `submitted_by`, `verified_by`, `verification_date`, `status`, `comment_id`, `comparison_date`, `created_at`, `updated_at`, `dam_id`) VALUES
(1, 1, '[\"Wholesale\",\"Retail\"]', 2, 39, 20, '5.00', '10.00', NULL, '15.00', '20.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 5, 3, 1, 1, NULL, NULL, 1, NULL, NULL, '2021-06-25 23:08:47', '2021-06-25 23:08:47', NULL),
(2, 1, '[\"Wholesale\",\"Retail\"]', 5, 37, 21, '25.00', '30.00', NULL, '35.00', '40.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 2, 1, 5, 1, NULL, NULL, 1, NULL, NULL, '2021-06-25 23:08:47', '2021-06-25 23:08:47', NULL),
(3, 1, '[\"Wholesale\",\"Retail\"]', 5, 37, 22, '45.00', '50.00', NULL, '55.00', '60.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 2, 4, 5, 1, NULL, NULL, 1, NULL, NULL, '2021-06-25 23:08:47', '2021-06-25 23:08:47', NULL),
(4, 2, '[\"Wholesale\",\"Retail\",\"Grower\"]', 2, 39, 20, '2.00', '4.00', NULL, '6.00', '8.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '10.00', '12.00', '0.00', '0.00', 5, 3, 1, 1, NULL, NULL, 1, NULL, NULL, '2021-06-25 23:10:13', '2021-06-25 23:10:13', NULL),
(5, 2, '[\"Wholesale\",\"Retail\",\"Grower\"]', 5, 37, 21, '14.00', '16.00', NULL, '18.00', '20.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '22.00', '24.00', '0.00', '0.00', 2, 1, 5, 1, NULL, NULL, 1, NULL, NULL, '2021-06-25 23:10:13', '2021-06-25 23:10:13', NULL),
(6, 2, '[\"Wholesale\",\"Retail\",\"Grower\"]', 5, 37, 22, '26.00', '28.00', NULL, '30.00', '32.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '34.00', '36.00', '0.00', '0.00', 2, 4, 5, 1, NULL, NULL, 1, NULL, NULL, '2021-06-25 23:10:13', '2021-06-25 23:10:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cpi_price_collector_profiles`
--

CREATE TABLE `cpi_price_collector_profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `division_id` bigint(20) UNSIGNED DEFAULT NULL,
  `district_id` bigint(20) UNSIGNED DEFAULT NULL,
  `upazilla_id` bigint(20) UNSIGNED DEFAULT NULL,
  `union_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `father_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `father_name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `signature` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_rej_reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_rej_reason_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_approved` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=no, 2=approved,3=reject',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cpi_price_collector_profiles`
--

INSERT INTO `cpi_price_collector_profiles` (`id`, `division_id`, `district_id`, `upazilla_id`, `union_id`, `name`, `name_bn`, `father_name`, `father_name_bn`, `designation`, `designation_bn`, `address`, `address_bn`, `mobile_no`, `nid`, `latitude`, `longitude`, `remarks`, `remarks_bn`, `attachment`, `signature`, `app_rej_reason`, `app_rej_reason_bn`, `is_approved`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 2, 16, 143, 1292, 'Ruhul', 'রুহুল', 'Abdur Rahman', 'আব্দুর রহমান', 'Programmer', 'প্রোগ্রামার', 'Bijoy Sarani', 'বিজয় স্মরণী', '01638584622', '82', '23.7649074', '90.383975', 'Testing Comment', 'Testing Comment bn', '1619090797.png', '1619090797.png', NULL, NULL, 1, 1, 1, 1, '2021-04-22 05:26:39', '2021-04-22 05:51:30'),
(2, 4, 35, 269, 2419, 'Chaim Livingston', 'Cadman Ellison', 'Adara Chan', 'Jamal Petty', 'Aut fugiat culpa off', 'Officia nostrum expe', 'Aut itaque reprehend', 'Et placeat consecte', '878787878787', '82', '23.7566792', '90.3850699', 'Minus commodo aliqui', 'Similique aliquam of', '1619090797.png', '1619090797.png', 'Dolor magni dignissi', 'Adipisci itaque cons', 1, 2, NULL, 1, '2021-04-22 05:27:49', '2021-04-22 05:56:29'),
(3, 8, 63, 481, 4449, 'Chaim Livingston', 'Cadman Ellison', 'Adara Chan', 'Jamal Petty', 'Aut fugiat culpa off', 'Officia nostrum expe', 'Aut itaque reprehend', 'Et placeat consecte', '878787', '60', '40', '49', 'Minus commodo aliqui', 'Similique aliquam of', '1619090797.png', '1619090797.png', NULL, NULL, 1, 3, NULL, 1, '2021-04-22 05:29:12', '2021-04-22 05:57:50'),
(4, 7, 54, 407, 3720, 'Ruhul Amin', 'Ruhul Amin', 'Suki Velasquez', 'Lesley Goodwin', 'Odio consectetur la', 'Placeat alias exped', '125/5, Tejkunipara', 'Deleniti ex nostrum ', '01843867772', '35', '90.24', '90.50', 'Qui est eos omnis q', 'Tempora qui tenetur ', NULL, NULL, NULL, NULL, 1, 1, 1, 1, '2021-04-22 07:15:59', '2021-04-22 07:18:52'),
(5, 8, 64, 485, 4485, 'Colette Medina', 'Colton Taylor', 'Kieran Sparks', 'Jerry Vance', 'Quod occaecat consec', 'Odit quidem quis qui', 'Ipsam ratione dolori', 'Dolorum sit illum ', '46', '32', 'Sint laborum Dolore', 'Enim tempore cumque', 'Voluptatem nostrud v', 'Aut culpa ut a et et', '1619252455.png', '1619252455.png', NULL, NULL, 1, 1, 1, 1, '2021-04-24 02:20:56', '2021-04-24 02:20:56'),
(6, 4, 35, 268, 2411, 'Daphne Benson', 'Lucy Murray', 'Lunea Clements', 'Ruby Harmon', 'Iste repudiandae eu ', 'Consequuntur aut quo', 'Commodo itaque ut co', 'Eius quam omnis temp', '82', '45', 'Totam qui doloremque', 'Itaque perspiciatis', 'Atque et ipsam aute ', 'Facere aut dolor fug', '1619347237.png', '1619347237.png', NULL, NULL, 1, 1, 1, 1, '2021-04-25 04:40:37', '2021-04-25 04:40:37'),
(7, 6, 45, 355, 3186, 'Md. Jahid', 'মোঃ জাহিদ ', 'Azizul Haq', 'Azizul Haq', 'Price Collector', 'Price Collector', 'Austagram, Kishoregonj', 'Austagram, Kishoregonj', '01671404511', '78787878', '743236', '34567', 'Test', 'Test', NULL, NULL, 'Approved', 'Approved', 1, 2, 1, 1, '2021-06-25 23:36:18', '2021-06-25 23:44:06'),
(8, 1, 1, 11, 129, '1233', '123', '', '', '', '', '', '', '01924496005', '', 'qq', 'q', '', '', '1624701815.jpg', '1624694616.jpg', NULL, NULL, 1, 1, 1, 1, '2021-06-26 02:03:36', '2021-06-26 04:03:35'),
(9, 1, 1, 1, 1, '3', '3', '', '', '', '', '', '', '3', '22', '2', '2', '22', '', '1624702170.jpg', '1624702170.jpg', NULL, NULL, 1, 1, 1, 1, '2021-06-26 04:02:26', '2021-06-26 04:09:30'),
(10, 1, 3, 28, 280, '1111', '11111111111111', '1', '11111111111111111', '1111111111111111111', '1', '11111111111111111111', '1', '01717437198', '11111111', '11111111111', '1111', '', '', '1624703324.jpg', '1624703324.jpg', 'Test', 'Test', 1, 2, 1, 1, '2021-06-26 04:24:55', '2021-06-26 04:42:08');

-- --------------------------------------------------------

--
-- Table structure for table `cpi_price_infos`
--

CREATE TABLE `cpi_price_infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `PriceDate` date NOT NULL,
  `LatitudeRecorded` decimal(10,4) DEFAULT NULL,
  `LongitudeRecorded` decimal(10,4) DEFAULT NULL,
  `HighestPrice` decimal(10,2) DEFAULT NULL,
  `LowestPrice` decimal(10,2) DEFAULT NULL,
  `AveragePrice` decimal(10,2) DEFAULT NULL,
  `PriceUSDRate` decimal(10,2) DEFAULT NULL,
  `PriceEuroRate` decimal(10,2) DEFAULT NULL,
  `VerificationDate` date DEFAULT NULL,
  `VerificationTime` time DEFAULT NULL,
  `PriceType_id` int(11) DEFAULT NULL,
  `Market_id` int(11) DEFAULT NULL,
  `Subdistrict_id` int(11) DEFAULT NULL,
  `District_id` int(11) DEFAULT NULL,
  `Division_id` int(11) DEFAULT NULL,
  `Commodity_id` int(11) DEFAULT NULL,
  `BuyerSellerType` int(11) DEFAULT NULL,
  `DailyVolume` decimal(10,2) DEFAULT NULL,
  `MarketPriceEnteredBy` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Verified_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cpi_price_infos`
--

INSERT INTO `cpi_price_infos` (`id`, `PriceDate`, `LatitudeRecorded`, `LongitudeRecorded`, `HighestPrice`, `LowestPrice`, `AveragePrice`, `PriceUSDRate`, `PriceEuroRate`, `VerificationDate`, `VerificationTime`, `PriceType_id`, `Market_id`, `Subdistrict_id`, `District_id`, `Division_id`, `Commodity_id`, `BuyerSellerType`, `DailyVolume`, `MarketPriceEnteredBy`, `Verified_by`, `created_at`, `updated_at`) VALUES
(1, '2021-04-26', '11.0000', '11.0000', '100.00', '88.00', '90.00', '1.00', '0.88', NULL, NULL, 1, 8, 2, 1, 1, 1, 1, '100.00', '1', '1', NULL, '2021-04-26 06:51:25'),
(2, '2021-04-27', '11.0000', '11.0000', '104.00', '80.00', '92.00', '1.00', '0.88', NULL, NULL, 2, 8, 2, 1, 1, 1, 1, '100.00', '1', '1', NULL, '2021-04-27 04:33:09'),
(3, '2021-04-24', '999999.9999', '999999.9999', '204.00', '50.00', '127.00', '458.00', '700.00', NULL, NULL, 2, 7, 143, 16, 2, 3, NULL, '0.00', '1', '1', '2021-04-25 21:45:08', '2021-04-27 04:43:09'),
(4, '2021-04-25', '50.4100', '50.4500', '74.00', '947.00', '510.50', '709.00', '983.00', NULL, NULL, 2, 7, 143, 16, 2, 4, NULL, '44.00', '1', '1', '2021-04-25 22:04:46', '2021-04-27 04:43:41'),
(5, '2021-04-23', '23.7649', '90.3840', '733.00', '133.00', '433.00', '433.00', '433.00', NULL, NULL, 1, 7, 143, 16, 2, 6, NULL, '3.50', '1', '1', '2021-04-25 22:12:29', '2021-04-27 04:44:03'),
(6, '2021-04-27', '11.0000', '11.0000', '100.00', '88.00', '90.00', '1.00', '0.88', NULL, NULL, 1, 8, 2, 1, 1, 1, 1, '100.00', '1', '1', NULL, '2021-04-26 06:51:25'),
(7, '2021-04-28', '11.0000', '11.0000', '107.00', '81.00', '94.00', '1.00', '0.88', NULL, NULL, 2, 8, 2, 1, 1, 1, 1, '100.00', '1', '1', NULL, '2021-04-27 04:36:07'),
(8, '2021-04-29', '14.1200', '14.1900', '108.00', '80.00', '94.00', '982.00', '784.00', NULL, NULL, 2, 7, 143, 16, 2, 2, NULL, '20.00', '1', '1', '2021-04-27 04:45:15', '2021-04-27 04:46:52');

-- --------------------------------------------------------

--
-- Table structure for table `dam_districts`
--

CREATE TABLE `dam_districts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `district_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `district_title_bangla` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `division_id` bigint(20) UNSIGNED NOT NULL,
  `district_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=active, 1=inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dam_districts`
--

INSERT INTO `dam_districts` (`id`, `district_title`, `district_title_bangla`, `division_id`, `district_status`, `created_at`, `updated_at`, `dam_id`) VALUES
(1, 'Comilla', 'কুমিল্লা', 1, 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(2, 'Feni', 'ফেনী', 1, 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(3, 'Brahmanbaria', 'ব্রাহ্মণবাড়িয়া', 1, 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(4, 'Rangamati', 'রাঙ্গামাটি', 1, 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(5, 'Noakhali', 'নোয়াখালী', 1, 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(6, 'Chandpur', 'চাঁদপুর', 1, 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(7, 'Lakshmipur', 'লক্ষ্মীপুর', 1, 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(8, 'Chattogram', 'চট্টগ্রাম', 1, 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(9, 'Coxsbazar', 'কক্সবাজার', 1, 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(10, 'Khagrachhari', 'খাগড়াছড়ি', 1, 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(11, 'Bandarban', 'বান্দরবান', 1, 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(12, 'Sirajganj', 'সিরাজগঞ্জ', 2, 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(13, 'Pabna', 'পাবনা', 2, 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(14, 'Bogura', 'বগুড়া', 2, 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(15, 'Rajshahi', 'রাজশাহী', 2, 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(16, 'Natore', 'নাটোর', 2, 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(17, 'Joypurhat', 'জয়পুরহাট', 2, 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(18, 'Chapainawabganj', 'চাঁপাইনবাবগঞ্জ', 2, 0, '2021-02-14 01:16:26', '2021-02-14 01:16:26', NULL),
(19, 'Naogaon', 'নওগাঁ', 2, 0, '2021-02-14 01:16:26', '2021-02-14 01:16:26', NULL),
(20, 'Jashore', 'যশোর', 3, 0, '2021-02-14 01:16:26', '2021-02-14 01:16:26', NULL),
(21, 'Satkhira', 'সাতক্ষীরা', 3, 0, '2021-02-14 01:16:26', '2021-02-14 01:16:26', NULL),
(22, 'Meherpur', 'মেহেরপুর', 3, 0, '2021-02-14 01:16:26', '2021-02-14 01:16:26', NULL),
(23, 'Narail', 'নড়াইল', 3, 0, '2021-02-14 01:16:26', '2021-02-14 01:16:26', NULL),
(24, 'Chuadanga', 'চুয়াডাঙ্গা', 3, 0, '2021-02-14 01:16:26', '2021-02-14 01:16:26', NULL),
(25, 'Kushtia', 'কুষ্টিয়া', 3, 0, '2021-02-14 01:16:26', '2021-02-14 01:16:26', NULL),
(26, 'Magura', 'মাগুরা', 3, 0, '2021-02-14 01:16:26', '2021-02-14 01:16:26', NULL),
(27, 'Khulna', 'খুলনা', 3, 0, '2021-02-14 01:16:26', '2021-02-14 01:16:26', NULL),
(28, 'Bagerhat', 'বাগেরহাট', 3, 0, '2021-02-14 01:16:26', '2021-02-14 01:16:26', NULL),
(29, 'Jhenaidah', 'ঝিনাইদহ', 3, 0, '2021-02-14 01:16:26', '2021-02-14 01:16:26', NULL),
(30, 'Jhalakathi', 'ঝালকাঠি', 4, 0, '2021-02-14 01:16:26', '2021-02-14 01:16:26', NULL),
(31, 'Patuakhali', 'পটুয়াখালী', 4, 0, '2021-02-14 01:16:27', '2021-02-14 01:16:27', NULL),
(32, 'Pirojpur', 'পিরোজপুর', 4, 0, '2021-02-14 01:16:27', '2021-02-14 01:16:27', NULL),
(33, 'Barisal', 'বরিশাল', 4, 0, '2021-02-14 01:16:27', '2021-02-14 01:16:27', NULL),
(34, 'Bhola', 'ভোলা', 4, 0, '2021-02-14 01:16:27', '2021-02-14 01:16:27', NULL),
(35, 'Barguna', 'বরগুনা', 4, 0, '2021-02-14 01:16:27', '2021-02-14 01:16:27', NULL),
(36, 'Sylhet', 'সিলেট', 5, 0, '2021-02-14 01:16:27', '2021-02-14 01:16:27', NULL),
(37, 'Moulvibazar', 'মৌলভীবাজার', 5, 0, '2021-02-14 01:16:27', '2021-02-14 01:16:27', NULL),
(38, 'Habiganj', 'হবিগঞ্জ', 5, 0, '2021-02-14 01:16:27', '2021-02-14 01:16:27', NULL),
(39, 'Sunamganj', 'সুনামগঞ্জ', 5, 0, '2021-02-14 01:16:27', '2021-02-14 01:16:27', NULL),
(40, 'Narsingdi', 'নরসিংদী', 6, 0, '2021-02-14 01:16:27', '2021-02-14 01:16:27', NULL),
(41, 'Gazipur', 'গাজীপুর', 6, 0, '2021-02-14 01:16:27', '2021-02-14 01:16:27', NULL),
(42, 'Shariatpur', 'শরীয়তপুর', 6, 0, '2021-02-14 01:16:27', '2021-02-14 01:16:27', NULL),
(43, 'Narayanganj', 'নারায়ণগঞ্জ', 6, 0, '2021-02-14 01:16:27', '2021-02-14 01:16:27', NULL),
(44, 'Tangail', 'টাঙ্গাইল', 6, 0, '2021-02-14 01:16:28', '2021-02-14 01:16:28', NULL),
(45, 'Kishoreganj', 'কিশোরগঞ্জ', 6, 0, '2021-02-14 01:16:28', '2021-02-14 01:16:28', NULL),
(46, 'Manikganj', 'মানিকগঞ্জ', 6, 0, '2021-02-14 01:16:28', '2021-02-14 01:16:28', NULL),
(47, 'Dhaka', 'ঢাকা', 6, 0, '2021-02-14 01:16:28', '2021-02-14 01:16:28', NULL),
(48, 'Munshiganj', 'মুন্সিগঞ্জ', 6, 0, '2021-02-14 01:16:28', '2021-02-14 01:16:28', NULL),
(49, 'Rajbari', 'রাজবাড়ী', 6, 0, '2021-02-14 01:16:28', '2021-02-14 01:16:28', NULL),
(50, 'Madaripur', 'মাদারীপুর', 6, 0, '2021-02-14 01:16:28', '2021-02-14 01:16:28', NULL),
(51, 'Gopalganj', 'গোপালগঞ্জ', 6, 0, '2021-02-14 01:16:28', '2021-02-14 01:16:28', NULL),
(52, 'Faridpur', 'ফরিদপুর', 6, 0, '2021-02-14 01:16:28', '2021-02-14 01:16:28', NULL),
(53, 'Panchagarh', 'পঞ্চগড়', 7, 0, '2021-02-14 01:16:28', '2021-02-14 01:16:28', NULL),
(54, 'Dinajpur', 'দিনাজপুর', 7, 0, '2021-02-14 01:16:28', '2021-02-14 01:16:28', NULL),
(55, 'Lalmonirhat', 'লালমনিরহাট', 7, 0, '2021-02-14 01:16:28', '2021-02-14 01:16:28', NULL),
(56, 'Nilphamari', 'নীলফামারী', 7, 0, '2021-02-14 01:16:28', '2021-02-14 01:16:28', NULL),
(57, 'Gaibandha', 'গাইবান্ধা', 7, 0, '2021-02-14 01:16:28', '2021-02-14 01:16:28', NULL),
(58, 'Thakurgaon', 'ঠাকুরগাঁও', 7, 0, '2021-02-14 01:16:28', '2021-02-14 01:16:28', NULL),
(59, 'Rangpur', 'রংপুর', 7, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(60, 'Kurigram', 'কুড়িগ্রাম', 7, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(61, 'Sherpur', 'শেরপুর', 8, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(62, 'Mymensingh', 'ময়মনসিংহ', 8, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(63, 'Jamalpur', 'জামালপুর', 8, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(64, 'Netrokona', 'নেত্রকোণা', 8, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dam_divisions`
--

CREATE TABLE `dam_divisions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `division_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `division_title_bangla` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `division_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=active, 1=inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dam_divisions`
--

INSERT INTO `dam_divisions` (`id`, `division_title`, `division_title_bangla`, `division_status`, `created_at`, `updated_at`, `dam_id`) VALUES
(1, 'Chattagram', 'চট্টগ্রাম', 0, '2021-02-14 01:16:24', '2021-03-11 02:20:26', NULL),
(2, 'Rajshahi', 'রাজশাহী', 0, '2021-02-14 01:16:24', '2021-02-14 01:16:24', NULL),
(3, 'Khulna', 'খুলনা', 0, '2021-02-14 01:16:24', '2021-03-24 03:54:22', NULL),
(4, 'Barisal', 'বরিশাল', 0, '2021-02-14 01:16:24', '2021-03-29 00:08:40', NULL),
(5, 'Sylhet', 'সিলেট', 0, '2021-02-14 01:16:24', '2021-02-14 01:16:24', NULL),
(6, 'Dhaka', 'ঢাকা', 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(7, 'Rangpur', 'রংপুর', 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL),
(8, 'Mymensingh', 'ময়মনসিংহ', 0, '2021-02-14 01:16:25', '2021-02-14 01:16:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dam_upazilas`
--

CREATE TABLE `dam_upazilas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_bangla` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `district_id` bigint(20) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0=active, 1=inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dam_upazilas`
--

INSERT INTO `dam_upazilas` (`id`, `name`, `name_bangla`, `division_id`, `district_id`, `status`, `created_at`, `updated_at`, `dam_id`) VALUES
(1, 'Debidwar', 'দেবিদ্বার', NULL, 1, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(2, 'Barura', 'বরুড়া', NULL, 1, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(3, 'Brahmanpara', 'ব্রাহ্মণপাড়া', NULL, 1, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(4, 'Chandina', 'চান্দিনা', NULL, 1, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(5, 'Chauddagram', 'চৌদ্দগ্রাম', NULL, 1, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(6, 'Daudkandi', 'দাউদকান্দি', NULL, 1, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(7, 'Homna', 'হোমনা', NULL, 1, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(8, 'Laksam', 'লাকসাম', NULL, 1, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(9, 'Muradnagar', 'মুরাদনগর', NULL, 1, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(10, 'Nangalkot', 'নাঙ্গলকোট', NULL, 1, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(11, 'Comilla Sadar', 'কুমিল্লা সদর', NULL, 1, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(12, 'Meghna', 'মেঘনা', NULL, 1, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(13, 'Monohargonj', 'মনোহরগঞ্জ', NULL, 1, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(14, 'Sadarsouth', 'সদর দক্ষিণ', NULL, 1, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(15, 'Titas', 'তিতাস', NULL, 1, 0, '2021-02-14 01:16:29', '2021-02-14 01:16:29', NULL),
(16, 'Burichang', 'বুড়িচং', NULL, 1, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(17, 'Lalmai', 'লালমাই', NULL, 1, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(18, 'Chhagalnaiya', 'ছাগলনাইয়া', NULL, 2, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(19, 'Feni Sadar', 'ফেনী সদর', NULL, 2, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(20, 'Sonagazi', 'সোনাগাজী', NULL, 2, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(21, 'Fulgazi', 'ফুলগাজী', NULL, 2, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(22, 'Parshuram', 'পরশুরাম', NULL, 2, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(23, 'Daganbhuiyan', 'দাগনভূঞা', NULL, 2, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(24, 'Brahmanbaria Sadar', 'ব্রাহ্মণবাড়িয়া সদর', NULL, 3, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(25, 'Kasba', 'কসবা', NULL, 3, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(26, 'Nasirnagar', 'নাসিরনগর', NULL, 3, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(27, 'Sarail', 'সরাইল', NULL, 3, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(28, 'Ashuganj', 'আশুগঞ্জ', NULL, 3, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(29, 'Akhaura', 'আখাউড়া', NULL, 3, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(30, 'Nabinagar', 'নবীনগর', NULL, 3, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(31, 'Bancharampur', 'বাঞ্ছারামপুর', NULL, 3, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(32, 'Bijoynagar', 'বিজয়নগর', NULL, 3, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(33, 'Rangamati Sadar', 'রাঙ্গামাটি সদর', NULL, 4, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(34, 'Kaptai', 'কাপ্তাই', NULL, 4, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(35, 'Kawkhali', 'কাউখালী', NULL, 4, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(36, 'Baghaichari', 'বাঘাইছড়ি', NULL, 4, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(37, 'Barkal', 'বরকল', NULL, 4, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(38, 'Langadu', 'লংগদু', NULL, 4, 0, '2021-02-14 01:16:30', '2021-02-14 01:16:30', NULL),
(39, 'Rajasthali', 'রাজস্থলী', NULL, 4, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(40, 'Belaichari', 'বিলাইছড়ি', NULL, 4, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(41, 'Juraichari', 'জুরাছড়ি', NULL, 4, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(42, 'Naniarchar', 'নানিয়ারচর', NULL, 4, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(43, 'Noakhali Sadar', 'নোয়াখালী সদর', NULL, 5, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(44, 'Companiganj', 'কোম্পানীগঞ্জ', NULL, 5, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(45, 'Begumganj', 'বেগমগঞ্জ', NULL, 5, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(46, 'Hatia', 'হাতিয়া', NULL, 5, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(47, 'Subarnachar', 'সুবর্ণচর', NULL, 5, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(48, 'Kabirhat', 'কবিরহাট', NULL, 5, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(49, 'Senbug', 'সেনবাগ', NULL, 5, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(50, 'Chatkhil', 'চাটখিল', NULL, 5, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(51, 'Sonaimori', 'সোনাইমুড়ী', NULL, 5, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(52, 'Haimchar', 'হাইমচর', NULL, 6, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(53, 'Kachua', 'কচুয়া', NULL, 6, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(54, 'Shahrasti', 'শাহরাস্তি	', NULL, 6, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(55, 'Chandpur Sadar', 'চাঁদপুর সদর', NULL, 6, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(56, 'Matlab South', 'মতলব দক্ষিণ', NULL, 6, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(57, 'Hajiganj', 'হাজীগঞ্জ', NULL, 6, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(58, 'Matlab North', 'মতলব উত্তর', NULL, 6, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(59, 'Faridgonj', 'ফরিদগঞ্জ', NULL, 6, 0, '2021-02-14 01:16:31', '2021-02-14 01:16:31', NULL),
(60, 'Lakshmipur Sadar', 'লক্ষ্মীপুর সদর', NULL, 7, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(61, 'Kamalnagar', 'কমলনগর', NULL, 7, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(62, 'Raipur', 'রায়পুর', NULL, 7, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(63, 'Ramgati', 'রামগতি', NULL, 7, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(64, 'Ramganj', 'রামগঞ্জ', NULL, 7, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(65, 'Rangunia', 'রাঙ্গুনিয়া', NULL, 8, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(66, 'Sitakunda', 'সীতাকুন্ড', NULL, 8, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(67, 'Mirsharai', 'মীরসরাই', NULL, 8, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(68, 'Patiya', 'পটিয়া', NULL, 8, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(69, 'Sandwip', 'সন্দ্বীপ', NULL, 8, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(70, 'Banshkhali', 'বাঁশখালী', NULL, 8, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(71, 'Boalkhali', 'বোয়ালখালী', NULL, 8, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(72, 'Anwara', 'আনোয়ারা', NULL, 8, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(73, 'Chandanaish', 'চন্দনাইশ', NULL, 8, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(74, 'Satkania', 'সাতকানিয়া', NULL, 8, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(75, 'Lohagara', 'লোহাগাড়া', NULL, 8, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(76, 'Hathazari', 'হাটহাজারী', NULL, 8, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(77, 'Fatikchhari', 'ফটিকছড়ি', NULL, 8, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(78, 'Raozan', 'রাউজান', NULL, 8, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(79, 'Karnafuli', 'কর্ণফুলী', NULL, 8, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(80, 'Coxsbazar Sadar', 'কক্সবাজার সদর', NULL, 9, 0, '2021-02-14 01:16:32', '2021-02-14 01:16:32', NULL),
(81, 'Chakaria', 'চকরিয়া', NULL, 9, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(82, 'Kutubdia', 'কুতুবদিয়া', NULL, 9, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(83, 'Ukhiya', 'উখিয়া', NULL, 9, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(84, 'Moheshkhali', 'মহেশখালী', NULL, 9, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(85, 'Pekua', 'পেকুয়া', NULL, 9, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(86, 'Ramu', 'রামু', NULL, 9, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(87, 'Teknaf', 'টেকনাফ', NULL, 9, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(88, 'Khagrachhari Sadar', 'খাগড়াছড়ি সদর', NULL, 10, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(89, 'Dighinala', 'দিঘীনালা', NULL, 10, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(90, 'Panchari', 'পানছড়ি', NULL, 10, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(91, 'Laxmichhari', 'লক্ষীছড়ি', NULL, 10, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(92, 'Mohalchari', 'মহালছড়ি', NULL, 10, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(93, 'Manikchari', 'মানিকছড়ি', NULL, 10, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(94, 'Ramgarh', 'রামগড়', NULL, 10, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(95, 'Matiranga', 'মাটিরাঙ্গা', NULL, 10, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(96, 'Guimara', 'গুইমারা', NULL, 10, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(97, 'Bandarban Sadar', 'বান্দরবান সদর', NULL, 11, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(98, 'Alikadam', 'আলীকদম', NULL, 11, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(99, 'Naikhongchhari', 'নাইক্ষ্যংছড়ি', NULL, 11, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(100, 'Rowangchhari', 'রোয়াংছড়ি', NULL, 11, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(101, 'Lama', 'লামা', NULL, 11, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(102, 'Ruma', 'রুমা', NULL, 11, 0, '2021-02-14 01:16:33', '2021-02-14 01:16:33', NULL),
(103, 'Thanchi', 'থানচি', NULL, 11, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(104, 'Belkuchi', 'বেলকুচি', NULL, 12, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(105, 'Chauhali', 'চৌহালি', NULL, 12, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(106, 'Kamarkhand', 'কামারখন্দ', NULL, 12, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(107, 'Kazipur', 'কাজীপুর', NULL, 12, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(108, 'Raigonj', 'রায়গঞ্জ', NULL, 12, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(109, 'Shahjadpur', 'শাহজাদপুর', NULL, 12, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(110, 'Sirajganj Sadar', 'সিরাজগঞ্জ সদর', NULL, 12, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(111, 'Tarash', 'তাড়াশ', NULL, 12, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(112, 'Ullapara', 'উল্লাপাড়া', NULL, 12, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(113, 'Sujanagar', 'সুজানগর', NULL, 13, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(114, 'Ishurdi', 'ঈশ্বরদী', NULL, 13, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(115, 'Bhangura', 'ভাঙ্গুড়া', NULL, 13, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(116, 'Pabna Sadar', 'পাবনা সদর', NULL, 13, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(117, 'Bera', 'বেড়া', NULL, 13, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(118, 'Atghoria', 'আটঘরিয়া', NULL, 13, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(119, 'Chatmohar', 'চাটমোহর', NULL, 13, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(120, 'Santhia', 'সাঁথিয়া', NULL, 13, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(121, 'Faridpur', 'ফরিদপুর', NULL, 13, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(122, 'Kahaloo', 'কাহালু', NULL, 14, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(123, 'Bogra Sadar', 'বগুড়া সদর', NULL, 14, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(124, 'Shariakandi', 'সারিয়াকান্দি', NULL, 14, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(125, 'Shajahanpur', 'শাজাহানপুর', NULL, 14, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(126, 'Dupchanchia', 'দুপচাচিঁয়া', NULL, 14, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(127, 'Adamdighi', 'আদমদিঘি', NULL, 14, 0, '2021-02-14 01:16:34', '2021-02-14 01:16:34', NULL),
(128, 'Nondigram', 'নন্দিগ্রাম', NULL, 14, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(129, 'Sonatala', 'সোনাতলা', NULL, 14, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(130, 'Dhunot', 'ধুনট', NULL, 14, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(131, 'Gabtali', 'গাবতলী', NULL, 14, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(132, 'Sherpur', 'শেরপুর', NULL, 14, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(133, 'Shibganj', 'শিবগঞ্জ', NULL, 14, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(134, 'Paba', 'পবা', NULL, 15, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(135, 'Durgapur', 'দুর্গাপুর', NULL, 15, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(136, 'Mohonpur', 'মোহনপুর', NULL, 15, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(137, 'Charghat', 'চারঘাট', NULL, 15, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(138, 'Puthia', 'পুঠিয়া', NULL, 15, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(139, 'Bagha', 'বাঘা', NULL, 15, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(140, 'Godagari', 'গোদাগাড়ী', NULL, 15, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(141, 'Tanore', 'তানোর', NULL, 15, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(142, 'Bagmara', 'বাগমারা', NULL, 15, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(143, 'Natore Sadar', 'নাটোর সদর', NULL, 16, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(144, 'Singra', 'সিংড়া', NULL, 16, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(145, 'Baraigram', 'বড়াইগ্রাম', NULL, 16, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(146, 'Bagatipara', 'বাগাতিপাড়া', NULL, 16, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(147, 'Lalpur', 'লালপুর', NULL, 16, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(148, 'Gurudaspur', 'গুরুদাসপুর', NULL, 16, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(149, 'Naldanga', 'নলডাঙ্গা', NULL, 16, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(150, 'Akkelpur', 'আক্কেলপুর', NULL, 17, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(151, 'Kalai', 'কালাই', NULL, 17, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(152, 'Khetlal', 'ক্ষেতলাল', NULL, 17, 0, '2021-02-14 01:16:35', '2021-02-14 01:16:35', NULL),
(153, 'Panchbibi', 'পাঁচবিবি', NULL, 17, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(154, 'Joypurhat Sadar', 'জয়পুরহাট সদর', NULL, 17, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(155, 'Chapainawabganj Sadar', 'চাঁপাইনবাবগঞ্জ সদর', NULL, 18, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(156, 'Gomostapur', 'গোমস্তাপুর', NULL, 18, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(157, 'Nachol', 'নাচোল', NULL, 18, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(158, 'Bholahat', 'ভোলাহাট', NULL, 18, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(159, 'Shibganj', 'শিবগঞ্জ', NULL, 18, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(160, 'Mohadevpur', 'মহাদেবপুর', NULL, 19, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(161, 'Badalgachi', 'বদলগাছী', NULL, 19, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(162, 'Patnitala', 'পত্নিতলা', NULL, 19, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(163, 'Dhamoirhat', 'ধামইরহাট', NULL, 19, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(164, 'Niamatpur', 'নিয়ামতপুর', NULL, 19, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(165, 'Manda', 'মান্দা', NULL, 19, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(166, 'Atrai', 'আত্রাই', NULL, 19, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(167, 'Raninagar', 'রাণীনগর', NULL, 19, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(168, 'Naogaon Sadar', 'নওগাঁ সদর', NULL, 19, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(169, 'Porsha', 'পোরশা', NULL, 19, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(170, 'Sapahar', 'সাপাহার', NULL, 19, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(171, 'Manirampur', 'মণিরামপুর', NULL, 20, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(172, 'Abhaynagar', 'অভয়নগর', NULL, 20, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(173, 'Bagherpara', 'বাঘারপাড়া', NULL, 20, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(174, 'Chougachha', 'চৌগাছা', NULL, 20, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(175, 'Jhikargacha', 'ঝিকরগাছা', NULL, 20, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(176, 'Keshabpur', 'কেশবপুর', NULL, 20, 0, '2021-02-14 01:16:36', '2021-02-14 01:16:36', NULL),
(177, 'Jessore Sadar', 'যশোর সদর', NULL, 20, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(178, 'Sharsha', 'শার্শা', NULL, 20, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(179, 'Assasuni', 'আশাশুনি', NULL, 21, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(180, 'Debhata', 'দেবহাটা', NULL, 21, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(181, 'Kalaroa', 'কলারোয়া', NULL, 21, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(182, 'Satkhira Sadar', 'সাতক্ষীরা সদর', NULL, 21, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(183, 'Shyamnagar', 'শ্যামনগর', NULL, 21, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(184, 'Tala', 'তালা', NULL, 21, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(185, 'Kaliganj', 'কালিগঞ্জ', NULL, 21, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(186, 'Mujibnagar', 'মুজিবনগর', NULL, 22, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(187, 'Meherpur Sadar', 'মেহেরপুর সদর', NULL, 22, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(188, 'Gangni', 'গাংনী', NULL, 22, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(189, 'Narail Sadar', 'নড়াইল সদর', NULL, 23, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(190, 'Lohagara', 'লোহাগড়া', NULL, 23, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(191, 'Kalia', 'কালিয়া', NULL, 23, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(192, 'Chuadanga Sadar', 'চুয়াডাঙ্গা সদর', NULL, 24, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(193, 'Alamdanga', 'আলমডাঙ্গা', NULL, 24, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(194, 'Damurhuda', 'দামুড়হুদা', NULL, 24, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(195, 'Jibannagar', 'জীবননগর', NULL, 24, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(196, 'Kushtia Sadar', 'কুষ্টিয়া সদর', NULL, 25, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(197, 'Kumarkhali', 'কুমারখালী', NULL, 25, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(198, 'Khoksa', 'খোকসা', NULL, 25, 0, '2021-02-14 01:16:37', '2021-02-14 01:16:37', NULL),
(199, 'Mirpur', 'মিরপুর', NULL, 25, 0, '2021-02-14 01:16:38', '2021-02-14 01:16:38', NULL),
(200, 'Daulatpur', 'দৌলতপুর', NULL, 25, 0, '2021-02-14 01:16:38', '2021-02-14 01:16:38', NULL),
(201, 'Bheramara', 'ভেড়ামারা', NULL, 25, 0, '2021-02-14 01:16:38', '2021-02-14 01:16:38', NULL),
(202, 'Shalikha', 'শালিখা', NULL, 26, 0, '2021-02-14 01:16:38', '2021-02-14 01:16:38', NULL),
(203, 'Sreepur', 'শ্রীপুর', NULL, 26, 0, '2021-02-14 01:16:38', '2021-02-14 01:16:38', NULL),
(204, 'Magura Sadar', 'মাগুরা সদর', NULL, 26, 0, '2021-02-14 01:16:38', '2021-02-14 01:16:38', NULL),
(205, 'Mohammadpur', 'মহম্মদপুর', NULL, 26, 0, '2021-02-14 01:16:38', '2021-02-14 01:16:38', NULL),
(206, 'Paikgasa', 'পাইকগাছা', NULL, 27, 0, '2021-02-14 01:16:38', '2021-02-14 01:16:38', NULL),
(207, 'Fultola', 'ফুলতলা', NULL, 27, 0, '2021-02-14 01:16:38', '2021-02-14 01:16:38', NULL),
(208, 'Digholia', 'দিঘলিয়া', NULL, 27, 0, '2021-02-14 01:16:38', '2021-02-14 01:16:38', NULL),
(209, 'Rupsha', 'রূপসা', NULL, 27, 0, '2021-02-14 01:16:38', '2021-02-14 01:16:38', NULL),
(210, 'Terokhada', 'তেরখাদা', NULL, 27, 0, '2021-02-14 01:16:38', '2021-02-14 01:16:38', NULL),
(211, 'Dumuria', 'ডুমুরিয়া', NULL, 27, 0, '2021-02-14 01:16:38', '2021-02-14 01:16:38', NULL),
(212, 'Botiaghata', 'বটিয়াঘাটা', NULL, 27, 0, '2021-02-14 01:16:38', '2021-02-14 01:16:38', NULL),
(213, 'Dakop', 'দাকোপ', NULL, 27, 0, '2021-02-14 01:16:38', '2021-02-14 01:16:38', NULL),
(214, 'Koyra', 'কয়রা', NULL, 27, 0, '2021-02-14 01:16:38', '2021-02-14 01:16:38', NULL),
(215, 'Fakirhat', 'ফকিরহাট', NULL, 28, 0, '2021-02-14 01:16:38', '2021-02-14 01:16:38', NULL),
(216, 'Bagerhat Sadar', 'বাগেরহাট সদর', NULL, 28, 0, '2021-02-14 01:16:38', '2021-02-14 01:16:38', NULL),
(217, 'Mollahat', 'মোল্লাহাট', NULL, 28, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(218, 'Sarankhola', 'শরণখোলা', NULL, 28, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(219, 'Rampal', 'রামপাল', NULL, 28, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(220, 'Morrelganj', 'মোড়েলগঞ্জ', NULL, 28, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(221, 'Kachua', 'কচুয়া', NULL, 28, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(222, 'Mongla', 'মোংলা', NULL, 28, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(223, 'Chitalmari', 'চিতলমারী', NULL, 28, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(224, 'Jhenaidah Sadar', 'ঝিনাইদহ সদর', NULL, 29, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(225, 'Shailkupa', 'শৈলকুপা', NULL, 29, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(226, 'Harinakundu', 'হরিণাকুন্ডু', NULL, 29, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(227, 'Kaliganj', 'কালীগঞ্জ', NULL, 29, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(228, 'Kotchandpur', 'কোটচাঁদপুর', NULL, 29, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(229, 'Moheshpur', 'মহেশপুর', NULL, 29, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(230, 'Jhalakathi Sadar', 'ঝালকাঠি সদর', NULL, 30, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(231, 'Kathalia', 'কাঠালিয়া', NULL, 30, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(232, 'Nalchity', 'নলছিটি', NULL, 30, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(233, 'Rajapur', 'রাজাপুর', NULL, 30, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(234, 'Bauphal', 'বাউফল', NULL, 31, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(235, 'Patuakhali Sadar', 'পটুয়াখালী সদর', NULL, 31, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(236, 'Dumki', 'দুমকি', NULL, 31, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(237, 'Dashmina', 'দশমিনা', NULL, 31, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(238, 'Kalapara', 'কলাপাড়া', NULL, 31, 0, '2021-02-14 01:16:39', '2021-02-14 01:16:39', NULL),
(239, 'Mirzaganj', 'মির্জাগঞ্জ', NULL, 31, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(240, 'Galachipa', 'গলাচিপা', NULL, 31, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(241, 'Rangabali', 'রাঙ্গাবালী', NULL, 31, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(242, 'Pirojpur Sadar', 'পিরোজপুর সদর', NULL, 32, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(243, 'Nazirpur', 'নাজিরপুর', NULL, 32, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(244, 'Kawkhali', 'কাউখালী', NULL, 32, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(245, 'Zianagar', 'জিয়ানগর', NULL, 32, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(246, 'Bhandaria', 'ভান্ডারিয়া', NULL, 32, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(247, 'Mathbaria', 'মঠবাড়ীয়া', NULL, 32, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(248, 'Nesarabad', 'নেছারাবাদ', NULL, 32, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(249, 'Barisal Sadar', 'বরিশাল সদর', NULL, 33, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(250, 'Bakerganj', 'বাকেরগঞ্জ', NULL, 33, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(251, 'Babuganj', 'বাবুগঞ্জ', NULL, 33, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(252, 'Wazirpur', 'উজিরপুর', NULL, 33, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(253, 'Banaripara', 'বানারীপাড়া', NULL, 33, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(254, 'Gournadi', 'গৌরনদী', NULL, 33, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(255, 'Agailjhara', 'আগৈলঝাড়া', NULL, 33, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(256, 'Mehendiganj', 'মেহেন্দিগঞ্জ', NULL, 33, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(257, 'Muladi', 'মুলাদী', NULL, 33, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(258, 'Hizla', 'হিজলা', NULL, 33, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(259, 'Bhola Sadar', 'ভোলা সদর', NULL, 34, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(260, 'Borhan Sddin', 'বোরহান উদ্দিন', NULL, 34, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(261, 'Charfesson', 'চরফ্যাশন', NULL, 34, 0, '2021-02-14 01:16:40', '2021-02-14 01:16:40', NULL),
(262, 'Doulatkhan', 'দৌলতখান', NULL, 34, 0, '2021-02-14 01:16:41', '2021-02-14 01:16:41', NULL),
(263, 'Monpura', 'মনপুরা', NULL, 34, 0, '2021-02-14 01:16:41', '2021-02-14 01:16:41', NULL),
(264, 'Tazumuddin', 'তজুমদ্দিন', NULL, 34, 0, '2021-02-14 01:16:41', '2021-02-14 01:16:41', NULL),
(265, 'Lalmohan', 'লালমোহন', NULL, 34, 0, '2021-02-14 01:16:41', '2021-02-14 01:16:41', NULL),
(266, 'Amtali', 'আমতলী', NULL, 35, 1, '2021-02-14 01:16:41', '2021-04-11 00:10:43', NULL),
(267, 'Barguna Sadar', 'বরগুনা সদর', NULL, 35, 0, '2021-02-14 01:16:41', '2021-02-14 01:16:41', NULL),
(268, 'Betagi', 'বেতাগী', NULL, 35, 0, '2021-02-14 01:16:41', '2021-02-14 01:16:41', NULL),
(269, 'Bamna', 'বামনা', NULL, 35, 0, '2021-02-14 01:16:41', '2021-02-14 01:16:41', NULL),
(270, 'Pathorghata', 'পাথরঘাটা', NULL, 35, 0, '2021-02-14 01:16:41', '2021-02-14 01:16:41', NULL),
(271, 'Taltali', 'তালতলি', NULL, 35, 0, '2021-02-14 01:16:41', '2021-02-14 01:16:41', NULL),
(272, 'Balaganj', 'বালাগঞ্জ', NULL, 36, 0, '2021-02-14 01:16:41', '2021-02-14 01:16:41', NULL),
(273, 'Beanibazar', 'বিয়ানীবাজার', NULL, 36, 0, '2021-02-14 01:16:41', '2021-02-14 01:16:41', NULL),
(274, 'Bishwanath', 'বিশ্বনাথ', NULL, 36, 0, '2021-02-14 01:16:41', '2021-02-14 01:16:41', NULL),
(275, 'Companiganj', 'কোম্পানীগঞ্জ', NULL, 36, 0, '2021-02-14 01:16:41', '2021-02-14 01:16:41', NULL),
(276, 'Fenchuganj', 'ফেঞ্চুগঞ্জ', NULL, 36, 0, '2021-02-14 01:16:41', '2021-02-14 01:16:41', NULL),
(277, 'Golapganj', 'গোলাপগঞ্জ', NULL, 36, 0, '2021-02-14 01:16:41', '2021-02-14 01:16:41', NULL),
(278, 'Gowainghat', 'গোয়াইনঘাট', NULL, 36, 0, '2021-02-14 01:16:41', '2021-02-14 01:16:41', NULL),
(279, 'Jaintiapur', 'জৈন্তাপুর', NULL, 36, 0, '2021-02-14 01:16:42', '2021-02-14 01:16:42', NULL),
(280, 'Kanaighat', 'কানাইঘাট', NULL, 36, 0, '2021-02-14 01:16:42', '2021-02-14 01:16:42', NULL),
(281, 'Sylhet Sadar', 'সিলেট সদর', NULL, 36, 0, '2021-02-14 01:16:42', '2021-02-14 01:16:42', NULL),
(282, 'Zakiganj', 'জকিগঞ্জ', NULL, 36, 0, '2021-02-14 01:16:42', '2021-02-14 01:16:42', NULL),
(283, 'Dakshinsurma', 'দক্ষিণ সুরমা', NULL, 36, 0, '2021-02-14 01:16:42', '2021-02-14 01:16:42', NULL),
(284, 'Osmaninagar', 'ওসমানী নগর', NULL, 36, 0, '2021-02-14 01:16:43', '2021-02-14 01:16:43', NULL),
(285, 'Barlekha', 'বড়লেখা', NULL, 37, 0, '2021-02-14 01:16:43', '2021-02-14 01:16:43', NULL),
(286, 'Kamolganj', 'কমলগঞ্জ', NULL, 37, 0, '2021-02-14 01:16:43', '2021-02-14 01:16:43', NULL),
(287, 'Kulaura', 'কুলাউড়া', NULL, 37, 0, '2021-02-14 01:16:43', '2021-02-14 01:16:43', NULL),
(288, 'Moulvibazar Sadar', 'মৌলভীবাজার সদর', NULL, 37, 0, '2021-02-14 01:16:43', '2021-02-14 01:16:43', NULL),
(289, 'Rajnagar', 'রাজনগর', NULL, 37, 0, '2021-02-14 01:16:43', '2021-02-14 01:16:43', NULL),
(290, 'Sreemangal', 'শ্রীমঙ্গল', NULL, 37, 0, '2021-02-14 01:16:43', '2021-02-14 01:16:43', NULL),
(291, 'Juri', 'জুড়ী', NULL, 37, 0, '2021-02-14 01:16:44', '2021-02-14 01:16:44', NULL),
(292, 'Nabiganj', 'নবীগঞ্জ', NULL, 38, 0, '2021-02-14 01:16:44', '2021-02-14 01:16:44', NULL),
(293, 'Bahubal', 'বাহুবল', NULL, 38, 0, '2021-02-14 01:16:44', '2021-02-14 01:16:44', NULL),
(294, 'Ajmiriganj', 'আজমিরীগঞ্জ', NULL, 38, 0, '2021-02-14 01:16:44', '2021-02-14 01:16:44', NULL),
(295, 'Baniachong', 'বানিয়াচং', NULL, 38, 0, '2021-02-14 01:16:44', '2021-02-14 01:16:44', NULL),
(296, 'Lakhai', 'লাখাই', NULL, 38, 0, '2021-02-14 01:16:44', '2021-02-14 01:16:44', NULL),
(297, 'Chunarughat', 'চুনারুঘাট', NULL, 38, 0, '2021-02-14 01:16:45', '2021-02-14 01:16:45', NULL),
(298, 'Habiganj Sadar', 'হবিগঞ্জ সদর', NULL, 38, 0, '2021-02-14 01:16:45', '2021-02-14 01:16:45', NULL),
(299, 'Madhabpur', 'মাধবপুর', NULL, 38, 0, '2021-02-14 01:16:45', '2021-02-14 01:16:45', NULL),
(300, 'Sunamganj Sadar', 'সুনামগঞ্জ সদর', NULL, 39, 0, '2021-02-14 01:16:46', '2021-02-14 01:16:46', NULL),
(301, 'South Sunamganj', 'দক্ষিণ সুনামগঞ্জ', NULL, 39, 0, '2021-02-14 01:16:46', '2021-02-14 01:16:46', NULL),
(302, 'Bishwambarpur', 'বিশ্বম্ভরপুর', NULL, 39, 0, '2021-02-14 01:16:46', '2021-02-14 01:16:46', NULL),
(303, 'Chhatak', 'ছাতক', NULL, 39, 0, '2021-02-14 01:16:46', '2021-02-14 01:16:46', NULL),
(304, 'Jagannathpur', 'জগন্নাথপুর', NULL, 39, 0, '2021-02-14 01:16:46', '2021-02-14 01:16:46', NULL),
(305, 'Dowarabazar', 'দোয়ারাবাজার', NULL, 39, 0, '2021-02-14 01:16:46', '2021-02-14 01:16:46', NULL),
(306, 'Tahirpur', 'তাহিরপুর', NULL, 39, 0, '2021-02-14 01:16:46', '2021-02-14 01:16:46', NULL),
(307, 'Dharmapasha', 'ধর্মপাশা', NULL, 39, 0, '2021-02-14 01:16:46', '2021-02-14 01:16:46', NULL),
(308, 'Jamalganj', 'জামালগঞ্জ', NULL, 39, 0, '2021-02-14 01:16:47', '2021-02-14 01:16:47', NULL),
(309, 'Shalla', 'শাল্লা', NULL, 39, 0, '2021-02-14 01:16:47', '2021-02-14 01:16:47', NULL),
(310, 'Derai', 'দিরাই', NULL, 39, 0, '2021-02-14 01:16:47', '2021-02-14 01:16:47', NULL),
(311, 'Belabo', 'বেলাবো', NULL, 40, 0, '2021-02-14 01:16:47', '2021-02-14 01:16:47', NULL),
(312, 'Monohardi', 'মনোহরদী', NULL, 40, 0, '2021-02-14 01:16:47', '2021-02-14 01:16:47', NULL),
(313, 'Narsingdi Sadar', 'নরসিংদী সদর', NULL, 40, 0, '2021-02-14 01:16:47', '2021-02-14 01:16:47', NULL),
(314, 'Palash', 'পলাশ', NULL, 40, 0, '2021-02-14 01:16:47', '2021-02-14 01:16:47', NULL),
(315, 'Raipura', 'রায়পুরা', NULL, 40, 0, '2021-02-14 01:16:47', '2021-02-14 01:16:47', NULL),
(316, 'Shibpur', 'শিবপুর', NULL, 40, 0, '2021-02-14 01:16:48', '2021-02-14 01:16:48', NULL),
(317, 'Kaliganj', 'কালীগঞ্জ', NULL, 41, 0, '2021-02-14 01:16:48', '2021-02-14 01:16:48', NULL),
(318, 'Kaliakair', 'কালিয়াকৈর', NULL, 41, 0, '2021-02-14 01:16:48', '2021-02-14 01:16:48', NULL),
(319, 'Kapasia', 'কাপাসিয়া', NULL, 41, 0, '2021-02-14 01:16:48', '2021-02-14 01:16:48', NULL),
(320, 'Gazipur Sadar', 'গাজীপুর সদর', NULL, 41, 0, '2021-02-14 01:16:48', '2021-02-14 01:16:48', NULL),
(321, 'Sreepur', 'শ্রীপুর', NULL, 41, 0, '2021-02-14 01:16:48', '2021-02-14 01:16:48', NULL),
(322, 'Shariatpur Sadar', 'শরিয়তপুর সদর', NULL, 42, 0, '2021-02-14 01:16:48', '2021-02-14 01:16:48', NULL),
(323, 'Naria', 'নড়িয়া', NULL, 42, 0, '2021-02-14 01:16:49', '2021-02-14 01:16:49', NULL),
(324, 'Zajira', 'জাজিরা', NULL, 42, 0, '2021-02-14 01:16:49', '2021-02-14 01:16:49', NULL),
(325, 'Gosairhat', 'গোসাইরহাট', NULL, 42, 0, '2021-02-14 01:16:49', '2021-02-14 01:16:49', NULL),
(326, 'Bhedarganj', 'ভেদরগঞ্জ', NULL, 42, 0, '2021-02-14 01:16:49', '2021-02-14 01:16:49', NULL),
(327, 'Damudya', 'ডামুড্যা', NULL, 42, 0, '2021-02-14 01:16:49', '2021-02-14 01:16:49', NULL),
(328, 'Araihazar', 'আড়াইহাজার', NULL, 43, 0, '2021-02-14 01:16:49', '2021-02-14 01:16:49', NULL),
(329, 'Bandar', 'বন্দর', NULL, 43, 0, '2021-02-14 01:16:49', '2021-02-14 01:16:49', NULL),
(330, 'Narayanganj Sadar', 'নারায়নগঞ্জ সদর', NULL, 43, 0, '2021-02-14 01:16:50', '2021-02-14 01:16:50', NULL),
(331, 'Rupganj', 'রূপগঞ্জ', NULL, 43, 0, '2021-02-14 01:16:51', '2021-02-14 01:16:51', NULL),
(332, 'Sonargaon', 'সোনারগাঁ', NULL, 43, 0, '2021-02-14 01:16:51', '2021-02-14 01:16:51', NULL),
(333, 'Basail', 'বাসাইল', NULL, 44, 0, '2021-02-14 01:16:51', '2021-02-14 01:16:51', NULL),
(334, 'Bhuapur', 'ভুয়াপুর', NULL, 44, 0, '2021-02-14 01:16:51', '2021-02-14 01:16:51', NULL),
(335, 'Delduar', 'দেলদুয়ার', NULL, 44, 0, '2021-02-14 01:16:51', '2021-02-14 01:16:51', NULL),
(336, 'Ghatail', 'ঘাটাইল', NULL, 44, 0, '2021-02-14 01:16:51', '2021-02-14 01:16:51', NULL),
(337, 'Gopalpur', 'গোপালপুর', NULL, 44, 0, '2021-02-14 01:16:51', '2021-02-14 01:16:51', NULL),
(338, 'Madhupur', 'মধুপুর', NULL, 44, 0, '2021-02-14 01:16:52', '2021-02-14 01:16:52', NULL),
(339, 'Mirzapur', 'মির্জাপুর', NULL, 44, 0, '2021-02-14 01:16:52', '2021-02-14 01:16:52', NULL),
(340, 'Nagarpur', 'নাগরপুর', NULL, 44, 0, '2021-02-14 01:16:52', '2021-02-14 01:16:52', NULL),
(341, 'Sakhipur', 'সখিপুর', NULL, 44, 0, '2021-02-14 01:16:52', '2021-02-14 01:16:52', NULL),
(342, 'Tangail Sadar', 'টাঙ্গাইল সদর', NULL, 44, 0, '2021-02-14 01:16:52', '2021-02-14 01:16:52', NULL),
(343, 'Kalihati', 'কালিহাতী', NULL, 44, 0, '2021-02-14 01:16:52', '2021-02-14 01:16:52', NULL),
(344, 'Dhanbari', 'ধনবাড়ী', NULL, 44, 0, '2021-02-14 01:16:52', '2021-02-14 01:16:52', NULL),
(345, 'Itna', 'ইটনা', NULL, 45, 0, '2021-02-14 01:16:53', '2021-02-14 01:16:53', NULL),
(346, 'Katiadi', 'কটিয়াদী', NULL, 45, 0, '2021-02-14 01:16:53', '2021-02-14 01:16:53', NULL),
(347, 'Bhairab', 'ভৈরব', NULL, 45, 0, '2021-02-14 01:16:53', '2021-02-14 01:16:53', NULL),
(348, 'Tarail', 'তাড়াইল', NULL, 45, 0, '2021-02-14 01:16:53', '2021-02-14 01:16:53', NULL),
(349, 'Hossainpur', 'হোসেনপুর', NULL, 45, 0, '2021-02-14 01:16:53', '2021-02-14 01:16:53', NULL),
(350, 'Pakundia', 'পাকুন্দিয়া', NULL, 45, 0, '2021-02-14 01:16:53', '2021-02-14 01:16:53', NULL),
(351, 'Kuliarchar', 'কুলিয়ারচর', NULL, 45, 0, '2021-02-14 01:16:53', '2021-02-14 01:16:53', NULL),
(352, 'Kishoreganj Sadar', 'কিশোরগঞ্জ সদর', NULL, 45, 0, '2021-02-14 01:16:53', '2021-02-14 01:16:53', NULL),
(353, 'Karimgonj', 'করিমগঞ্জ', NULL, 45, 0, '2021-02-14 01:16:53', '2021-02-14 01:16:53', NULL),
(354, 'Bajitpur', 'বাজিতপুর', NULL, 45, 0, '2021-02-14 01:16:53', '2021-02-14 01:16:53', NULL),
(355, 'Austagram', 'অষ্টগ্রাম', NULL, 45, 0, '2021-02-14 01:16:54', '2021-02-14 01:16:54', NULL),
(356, 'Mithamoin', 'মিঠামইন', NULL, 45, 0, '2021-02-14 01:16:54', '2021-02-14 01:16:54', NULL),
(357, 'Nikli', 'নিকলী', NULL, 45, 0, '2021-02-14 01:16:54', '2021-02-14 01:16:54', NULL),
(358, 'Harirampur', 'হরিরামপুর', NULL, 46, 0, '2021-02-14 01:16:54', '2021-02-14 01:16:54', NULL),
(359, 'Saturia', 'সাটুরিয়া', NULL, 46, 0, '2021-02-14 01:16:54', '2021-02-14 01:16:54', NULL),
(360, 'Manikganj Sadar', 'মানিকগঞ্জ সদর', NULL, 46, 0, '2021-02-14 01:16:54', '2021-02-14 01:16:54', NULL),
(361, 'Gior', 'ঘিওর', NULL, 46, 0, '2021-02-14 01:16:54', '2021-02-14 01:16:54', NULL),
(362, 'Shibaloy', 'শিবালয়', NULL, 46, 0, '2021-02-14 01:16:54', '2021-02-14 01:16:54', NULL),
(363, 'Doulatpur', 'দৌলতপুর', NULL, 46, 0, '2021-02-14 01:16:54', '2021-02-14 01:16:54', NULL),
(364, 'Singiar', 'সিংগাইর', NULL, 46, 0, '2021-02-14 01:16:55', '2021-02-14 01:16:55', NULL),
(365, 'Savar', 'সাভার', NULL, 47, 0, '2021-02-14 01:16:55', '2021-02-14 01:16:55', NULL),
(366, 'Dhamrai', 'ধামরাই', NULL, 47, 0, '2021-02-14 01:16:55', '2021-02-14 01:16:55', NULL),
(367, 'Keraniganj', 'কেরাণীগঞ্জ', NULL, 47, 0, '2021-02-14 01:16:55', '2021-02-14 01:16:55', NULL),
(368, 'Nawabganj', 'নবাবগঞ্জ', NULL, 47, 0, '2021-02-14 01:16:55', '2021-02-14 01:16:55', NULL),
(369, 'Dohar', 'দোহার', NULL, 47, 0, '2021-02-14 01:16:55', '2021-02-14 01:16:55', NULL),
(370, 'Munshiganj Sadar', 'মুন্সিগঞ্জ সদর', NULL, 48, 0, '2021-02-14 01:16:55', '2021-02-14 01:16:55', NULL),
(371, 'Sreenagar', 'শ্রীনগর', NULL, 48, 0, '2021-02-14 01:16:55', '2021-02-14 01:16:55', NULL),
(372, 'Sirajdikhan', 'সিরাজদিখান', NULL, 48, 0, '2021-02-14 01:16:55', '2021-02-14 01:16:55', NULL),
(373, 'Louhajanj', 'লৌহজং', NULL, 48, 0, '2021-02-14 01:16:55', '2021-02-14 01:16:55', NULL),
(374, 'Gajaria', 'গজারিয়া', NULL, 48, 0, '2021-02-14 01:16:55', '2021-02-14 01:16:55', NULL),
(375, 'Tongibari', 'টংগীবাড়ি', NULL, 48, 0, '2021-02-14 01:16:56', '2021-02-14 01:16:56', NULL),
(376, 'Rajbari Sadar', 'রাজবাড়ী সদর', NULL, 49, 0, '2021-02-14 01:16:56', '2021-02-14 01:16:56', NULL),
(377, 'Goalanda', 'গোয়ালন্দ', NULL, 49, 0, '2021-02-14 01:16:56', '2021-02-14 01:16:56', NULL),
(378, 'Pangsa', 'পাংশা', NULL, 49, 0, '2021-02-14 01:16:56', '2021-02-14 01:16:56', NULL),
(379, 'Baliakandi', 'বালিয়াকান্দি', NULL, 49, 0, '2021-02-14 01:16:56', '2021-02-14 01:16:56', NULL),
(380, 'Kalukhali', 'কালুখালী', NULL, 49, 0, '2021-02-14 01:16:56', '2021-02-14 01:16:56', NULL),
(381, 'Madaripur Sadar', 'মাদারীপুর সদর', NULL, 50, 0, '2021-02-14 01:16:56', '2021-02-14 01:16:56', NULL),
(382, 'Shibchar', 'শিবচর', NULL, 50, 0, '2021-02-14 01:16:57', '2021-02-14 01:16:57', NULL),
(383, 'Kalkini', 'কালকিনি', NULL, 50, 0, '2021-02-14 01:16:57', '2021-02-14 01:16:57', NULL),
(384, 'Rajoir', 'রাজৈর', NULL, 50, 0, '2021-02-14 01:16:57', '2021-02-14 01:16:57', NULL),
(385, 'Gopalganj Sadar', 'গোপালগঞ্জ সদর', NULL, 51, 0, '2021-02-14 01:16:57', '2021-02-14 01:16:57', NULL),
(386, 'Kashiani', 'কাশিয়ানী', NULL, 51, 0, '2021-02-14 01:16:57', '2021-02-14 01:16:57', NULL),
(387, 'Tungipara', 'টুংগীপাড়া', NULL, 51, 0, '2021-02-14 01:16:57', '2021-02-14 01:16:57', NULL),
(388, 'Kotalipara', 'কোটালীপাড়া', NULL, 51, 0, '2021-02-14 01:16:57', '2021-02-14 01:16:57', NULL),
(389, 'Muksudpur', 'মুকসুদপুর', NULL, 51, 0, '2021-02-14 01:16:58', '2021-02-14 01:16:58', NULL),
(390, 'Faridpur Sadar', 'ফরিদপুর সদর', NULL, 52, 0, '2021-02-14 01:16:58', '2021-02-14 01:16:58', NULL),
(391, 'Alfadanga', 'আলফাডাঙ্গা', NULL, 52, 0, '2021-02-14 01:16:58', '2021-02-14 01:16:58', NULL),
(392, 'Boalmari', 'বোয়ালমারী', NULL, 52, 0, '2021-02-14 01:16:58', '2021-02-14 01:16:58', NULL),
(393, 'Sadarpur', 'সদরপুর', NULL, 52, 0, '2021-02-14 01:16:58', '2021-02-14 01:16:58', NULL),
(394, 'Nagarkanda', 'নগরকান্দা', NULL, 52, 0, '2021-02-14 01:16:58', '2021-02-14 01:16:58', NULL),
(395, 'Bhanga', 'ভাঙ্গা', NULL, 52, 0, '2021-02-14 01:16:58', '2021-02-14 01:16:58', NULL),
(396, 'Charbhadrasan', 'চরভদ্রাসন', NULL, 52, 0, '2021-02-14 01:16:58', '2021-02-14 01:16:58', NULL),
(397, 'Madhukhali', 'মধুখালী', NULL, 52, 0, '2021-02-14 01:16:58', '2021-02-14 01:16:58', NULL),
(398, 'Saltha', 'সালথা', NULL, 52, 0, '2021-02-14 01:16:58', '2021-02-14 01:16:58', NULL),
(399, 'Panchagarh Sadar', 'পঞ্চগড় সদর', NULL, 53, 0, '2021-02-14 01:16:59', '2021-02-14 01:16:59', NULL),
(400, 'Debiganj', 'দেবীগঞ্জ', NULL, 53, 0, '2021-02-14 01:16:59', '2021-02-14 01:16:59', NULL),
(401, 'Boda', 'বোদা', NULL, 53, 0, '2021-02-14 01:16:59', '2021-02-14 01:16:59', NULL),
(402, 'Atwari', 'আটোয়ারী', NULL, 53, 0, '2021-02-14 01:16:59', '2021-02-14 01:16:59', NULL),
(403, 'Tetulia', 'তেতুলিয়া', NULL, 53, 0, '2021-02-14 01:16:59', '2021-02-14 01:16:59', NULL),
(404, 'Nawabganj', 'নবাবগঞ্জ', NULL, 54, 0, '2021-02-14 01:16:59', '2021-02-14 01:16:59', NULL),
(405, 'Birganj', 'বীরগঞ্জ', NULL, 54, 0, '2021-02-14 01:16:59', '2021-02-14 01:16:59', NULL),
(406, 'Ghoraghat', 'ঘোড়াঘাট', NULL, 54, 0, '2021-02-14 01:16:59', '2021-02-14 01:16:59', NULL),
(407, 'Birampur', 'বিরামপুর', NULL, 54, 0, '2021-02-14 01:16:59', '2021-02-14 01:16:59', NULL),
(408, 'Parbatipur', 'পার্বতীপুর', NULL, 54, 0, '2021-02-14 01:16:59', '2021-02-14 01:16:59', NULL),
(409, 'Bochaganj', 'বোচাগঞ্জ', NULL, 54, 0, '2021-02-14 01:16:59', '2021-02-14 01:16:59', NULL),
(410, 'Kaharol', 'কাহারোল', NULL, 54, 0, '2021-02-14 01:16:59', '2021-02-14 01:16:59', NULL),
(411, 'Fulbari', 'ফুলবাড়ী', NULL, 54, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(412, 'Dinajpur Sadar', 'দিনাজপুর সদর', NULL, 54, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(413, 'Hakimpur', 'হাকিমপুর', NULL, 54, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(414, 'Khansama', 'খানসামা', NULL, 54, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(415, 'Birol', 'বিরল', NULL, 54, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(416, 'Chirirbandar', 'চিরিরবন্দর', NULL, 54, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(417, 'Lalmonirhat Sadar', 'লালমনিরহাট সদর', NULL, 55, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(418, 'Kaliganj', 'কালীগঞ্জ', NULL, 55, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(419, 'Hatibandha', 'হাতীবান্ধা', NULL, 55, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(420, 'Patgram', 'পাটগ্রাম', NULL, 55, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(421, 'Aditmari', 'আদিতমারী', NULL, 55, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(422, 'Syedpur', 'সৈয়দপুর', NULL, 56, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(423, 'Domar', 'ডোমার', NULL, 56, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(424, 'Dimla', 'ডিমলা', NULL, 56, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(425, 'Jaldhaka', 'জলঢাকা', NULL, 56, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(426, 'Kishorganj', 'কিশোরগঞ্জ', NULL, 56, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(427, 'Nilphamari Sadar', 'নীলফামারী সদর', NULL, 56, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(428, 'Sadullapur', 'সাদুল্লাপুর', NULL, 57, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(429, 'Gaibandha Sadar', 'গাইবান্ধা সদর', NULL, 57, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(430, 'Palashbari', 'পলাশবাড়ী', NULL, 57, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(431, 'Saghata', 'সাঘাটা', NULL, 57, 0, '2021-02-14 01:17:00', '2021-02-14 01:17:00', NULL),
(432, 'Gobindaganj', 'গোবিন্দগঞ্জ', NULL, 57, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(433, 'Sundarganj', 'সুন্দরগঞ্জ', NULL, 57, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(434, 'Phulchari', 'ফুলছড়ি', NULL, 57, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(435, 'Thakurgaon Sadar', 'ঠাকুরগাঁও সদর', NULL, 58, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(436, 'Pirganj', 'পীরগঞ্জ', NULL, 58, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(437, 'Ranisankail', 'রাণীশংকৈল', NULL, 58, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(438, 'Haripur', 'হরিপুর', NULL, 58, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(439, 'Baliadangi', 'বালিয়াডাঙ্গী', NULL, 58, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(440, 'Rangpur Sadar', 'রংপুর সদর', NULL, 59, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(441, 'Gangachara', 'গংগাচড়া', NULL, 59, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(442, 'Taragonj', 'তারাগঞ্জ', NULL, 59, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(443, 'Badargonj', 'বদরগঞ্জ', NULL, 59, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(444, 'Mithapukur', 'মিঠাপুকুর', NULL, 59, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(445, 'Pirgonj', 'পীরগঞ্জ', NULL, 59, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(446, 'Kaunia', 'কাউনিয়া', NULL, 59, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(447, 'Pirgacha', 'পীরগাছা', NULL, 59, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(448, 'Kurigram Sadar', 'কুড়িগ্রাম সদর', NULL, 60, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(449, 'Nageshwari', 'নাগেশ্বরী', NULL, 60, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(450, 'Bhurungamari', 'ভুরুঙ্গামারী', NULL, 60, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(451, 'Phulbari', 'ফুলবাড়ী', NULL, 60, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(452, 'Rajarhat', 'রাজারহাট', NULL, 60, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(453, 'Ulipur', 'উলিপুর', NULL, 60, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(454, 'Chilmari', 'চিলমারী', NULL, 60, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(455, 'Rowmari', 'রৌমারী', NULL, 60, 0, '2021-02-14 01:17:01', '2021-02-14 01:17:01', NULL),
(456, 'Charrajibpur', 'চর রাজিবপুর', NULL, 60, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(457, 'Sherpur Sadar', 'শেরপুর সদর', NULL, 61, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(458, 'Nalitabari', 'নালিতাবাড়ী', NULL, 61, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(459, 'Sreebordi', 'শ্রীবরদী', NULL, 61, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(460, 'Nokla', 'নকলা', NULL, 61, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(461, 'Jhenaigati', 'ঝিনাইগাতী', NULL, 61, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(462, 'Fulbaria', 'ফুলবাড়ীয়া', NULL, 62, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(463, 'Trishal', 'ত্রিশাল', NULL, 62, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(464, 'Bhaluka', 'ভালুকা', NULL, 62, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(465, 'Muktagacha', 'মুক্তাগাছা', NULL, 62, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(466, 'Mymensingh Sadar', 'ময়মনসিংহ সদর', NULL, 62, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(467, 'Dhobaura', 'ধোবাউড়া', NULL, 62, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(468, 'Phulpur', 'ফুলপুর', NULL, 62, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(469, 'Haluaghat', 'হালুয়াঘাট', NULL, 62, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(470, 'Gouripur', 'গৌরীপুর', NULL, 62, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(471, 'Gafargaon', 'গফরগাঁও', NULL, 62, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(472, 'Iswarganj', 'ঈশ্বরগঞ্জ', NULL, 62, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(473, 'Nandail', 'নান্দাইল', NULL, 62, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(474, 'Tarakanda', 'তারাকান্দা', NULL, 62, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(475, 'Jamalpur Sadar', 'জামালপুর সদর', NULL, 63, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(476, 'Melandah', 'মেলান্দহ', NULL, 63, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(477, 'Islampur', 'ইসলামপুর', NULL, 63, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(478, 'Dewangonj', 'দেওয়ানগঞ্জ', NULL, 63, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(479, 'Sarishabari', 'সরিষাবাড়ী', NULL, 63, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(480, 'Madarganj', 'মাদারগঞ্জ', NULL, 63, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(481, 'Bokshiganj', 'বকশীগঞ্জ', NULL, 63, 0, '2021-02-14 01:17:02', '2021-02-14 01:17:02', NULL),
(482, 'Barhatta', 'বারহাট্টা', NULL, 64, 0, '2021-02-14 01:17:03', '2021-02-14 01:17:03', NULL),
(483, 'Durgapur', 'দুর্গাপুর', NULL, 64, 0, '2021-02-14 01:17:03', '2021-02-14 01:17:03', NULL),
(484, 'Kendua', 'কেন্দুয়া', NULL, 64, 0, '2021-02-14 01:17:03', '2021-02-14 01:17:03', NULL),
(485, 'Atpara', 'আটপাড়া', NULL, 64, 0, '2021-02-14 01:17:03', '2021-02-14 01:17:03', NULL),
(486, 'Madan', 'মদন', NULL, 64, 0, '2021-02-14 01:17:03', '2021-02-14 01:17:03', NULL),
(487, 'Khaliajuri', 'খালিয়াজুরী', NULL, 64, 0, '2021-02-14 01:17:03', '2021-02-14 01:17:03', NULL),
(488, 'Kalmakanda', 'কলমাকান্দা', NULL, 64, 0, '2021-02-14 01:17:03', '2021-02-14 01:17:03', NULL),
(489, 'Mohongonj', 'মোহনগঞ্জ', NULL, 64, 0, '2021-02-14 01:17:03', '2021-02-14 01:17:03', NULL),
(490, 'Purbadhala', 'পূর্বধলা', NULL, 64, 0, '2021-02-14 01:17:03', '2021-02-14 01:17:03', NULL),
(491, 'Netrokona Sadar', 'নেত্রকোণা সদর', NULL, 64, 0, '2021-02-14 01:17:03', '2021-02-14 01:17:03', NULL),
(492, 'QA Test Upazila', 'QA Test Upazila Bn', NULL, 65, 0, '2021-03-01 23:04:52', '2021-03-24 03:55:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ginner_grower_profiles`
--

CREATE TABLE `ginner_grower_profiles` (
  `id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1=Ginner, 2=Grower',
  `applicant_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `org_id` int(11) DEFAULT NULL,
  `region_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `upazilla_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `father_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `father_name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `land_area` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_draft` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Draft, 2=Final Save',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive, 3=Draft, 4=Final Save',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ginner_grower_profiles`
--

INSERT INTO `ginner_grower_profiles` (`id`, `type`, `applicant_id`, `org_id`, `region_id`, `district_id`, `upazilla_id`, `unit_id`, `zone_id`, `name`, `name_bn`, `father_name`, `father_name_bn`, `land_area`, `address`, `address_bn`, `mobile_no`, `nid`, `password`, `is_draft`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(37, 2, '8250854', 3, 5, 14, 127, 3, 21, '5trtyrty', 'tyytr5', 'ttttdfgfd', 'test', '5555555', 'dHAKA', 'ddd', '01635115644', '4625529864', '$2y$10$JgjDca56KZkA2bUVmONQ6e7nTFSG8IucTO0opSnvi4oe7LA2z3zxi', 2, 1, 1, 1, '2021-04-06 01:05:50', '2021-04-11 02:17:20'),
(38, 2, '12485335', 5, 8, 33, 249, 7, 22, 'Casey Osborne', 'Pearl Randolph', 'Orlando Miller', 'Abbot Burke', '54645', 'Culpa adipisci adipi', 'Dignissimos quae aut', '01635115618', '546456456', '$2y$10$YhWPBhCMX/Luf35XJap/rONomIqbhuaK81/bwF7.UWJJLT9KOrddq', 1, 1, 1, 1, '2021-04-06 01:06:02', '2021-04-06 01:12:12'),
(39, 1, '81027447', 3, 5, 14, 131, 4, 21, 'Edan Christian', 'Charlotte Short', 'Tate Alexander', 'Pearl Grant', '34534', 'Ex est ullam sed la', 'Error officiis praes', '01635115614', '345345353', '$2y$10$nOHkMEn.b/CMrYTauTsR4OaleUC7gEgoYhVLhjII/.2su5xhGXsYW', 1, 1, 1, 1, '2021-04-06 01:07:34', '2021-04-06 01:12:14'),
(40, 1, '33136830', 3, 4, 11, 98, 5, 20, 'habib', 'rtert', 'Kevyn Roberson', 'Geraldine Schwartz', '4444', 'fdg dfg454 dfgd45', 'Voluptatum irure off', '01916875877', '4534545', '', 2, 3, 1, 1, '2021-04-06 01:14:37', '2021-04-11 02:04:30'),
(41, 1, '37515193', 3, 4, 28, 216, 5, 20, 'Velma Workman', 'Hilda Glass', 'Kasper Barker', 'Eden Hardin', '23', 'Enim lorem culpa sus', 'Ipsum cillum tempore', '01635115616', '459521451', '$2y$10$kMjlHRheHGdG003hEN2Uq.GCvasw8D4uLHyqqf3Pn.7eV39/eIPV.', 1, 1, 1, 1, '2021-04-06 01:27:03', '2021-04-06 01:27:03'),
(42, 2, '70950691', 3, 1, 1, 2, 9, 18, 'Roth Clemons', 'Roth Clemons bn', 'Sloane Townsend', 'Geoffrey Haney', '45', 'Eaque sunt iure dolo', 'Ipsum sunt quia culp', '01916875875', '8056481322', '$2y$10$5Ot7OfV3YjlpFnU1CoAp7.b9tvI1V177IFOOEGO/HV4XVvjRjhf4m', 1, 1, 1, 1, '2021-04-06 01:27:35', '2021-04-06 01:27:35'),
(43, 1, '10113596', 3, 1, 1, 2, 9, 18, 'Nazmuzzaman Redoy', 'হৃদয়', 'Monir', 'মনির', '50', 'Bepari Para, Khilkhet, Dhaka', 'Kamaler Mor', '01635115613', '3545644544', '', 1, 1, 1, 1, '2021-04-06 18:25:22', '2021-06-01 01:23:09'),
(44, 2, '22294878', 1, 1, 28, 216, 9, 18, 'Arif', 'Arif Bn', 'Asadul haque', 'আসাদুল হক', '1245', 'dHAKA', 'ঢাকা ', '01635115617', '4625529864', '', 1, 1, 1, 1, '2021-04-08 03:55:35', '2021-06-01 02:11:31'),
(45, 2, '25626468', NULL, 5, 1, 4, 4, 21, 'dd', 'dd', 'dd', 'dd', '20', NULL, NULL, '01916852852', '19946119461000365', '$2y$10$nCy3cbfPtq6pRrmcmPA8mOJhF4OqqvTDo7L.C6Z2d3PM8aFqwptdG', 1, 1, 1, 1, '2021-05-05 06:28:26', '2021-05-05 06:28:26'),
(46, 2, '67700121', NULL, 13, 45, 355, 13, 29, 'Md. Jahid', 'মোঃ জাহিদ ', 'Azizul Haq', 'Azizul Haq', '25', NULL, NULL, '01717437198', '78787878', '$2y$10$GRzF.zj2Xlq20E.cslilY.xtz/h2FzbV/2xyRm5oVOCkXmVpNfffC', 1, 1, 1, 1, '2021-05-05 22:19:48', '2021-05-05 22:19:48'),
(47, 2, '28523610', NULL, 13, 45, 355, 13, 29, 'Md. Rohim', 'Md. Rohim', 'Md. Korim Uddin', 'Md. Korim Uddin', '12', NULL, NULL, '01913064670', '98234715', '$2y$10$.AmtHymoYBGsoaoP5OlqZuARqxIDz/dtYvNMo1izFK85CDgNXXrtK', 1, 1, 1, 1, '2021-05-08 23:10:53', '2021-05-08 23:10:53'),
(48, 1, '47305340', NULL, 5, 1, 4, 4, 21, 'Suman', 'সুমন', 'Cassidy Warner', 'Paki Lindsay', '10', 'Quibusdam voluptatem', 'Voluptatum adipisci ', '01638584611', '5489713561', '$2y$10$8QB8iJBVMPMqGPHNBRgSg.9F9U.D8UEJ7RosC3csulsklns.9PZZy', 1, 1, 1, 1, '2021-05-08 23:28:44', '2021-05-08 23:28:44'),
(49, 2, '54339535', NULL, 2, 3, 1, 11, 27, 'QA Test Grower User', 'QA Test Grower User Bn', 'QA Test Grower User Father', 'QA Test Grower User Father Bn', '100', 'QA Test Ad', 'QA Test Address Bn', '01521445003', '19946119', '', 1, 1, 1, 1, '2021-05-31 04:30:22', '2021-06-02 01:00:42'),
(50, 1, '52746278', NULL, 1, 36, 272, 9, 18, 'Ginner User', 'Ginner User Bn', 'Ginner User Father', 'Ginner User Father Bn', '100', 'Sylhet Hobigang', 'Sylhet Hobigang Bn', '01521445004', '19946119461000364', '$2y$10$YetJIi38Jipcjvox9dp3deogqqswVez6C4RqO8bZ2wDKiozLQGDEW', 1, 1, 1, 1, '2021-05-31 04:42:27', '2021-06-01 02:57:24'),
(51, 2, '10440998', NULL, 5, 1, 4, 4, 21, 'suman pro', 'suman pro bn', 'MD.Rafiqul Islam', 'মোঃ রফিকুল ইসলাম', '55', 'Gulshan-1,house-7.road-7,dhaka-1212', 'Gulshan-1,house-7.road-7,dhaka-1212', '01620515151', '23454623', '$2y$10$xg.5FSROU4aM5yE3yxWVde1Vgto3rmFpmKWV1ZTWam47IDPS0XylS', 1, 1, 1, 1, '2021-06-01 00:41:46', '2021-06-01 00:41:46'),
(52, 2, '10836633', NULL, 1, 36, 281, 9, 18, 'Sylhet Khan', 'Sylhet Khan Bn', 'Hasib Khan', 'Hasib Khan Bn', '500', 'Sylhet Sadar, Road#5', 'Sylhet Sadar, Road#5 Bn', '01521445005', '19946119461000333', '$2y$10$Yu3WdkNLqI4UyH6MwGcgFOTyozuhFaj1FtEp2XV6S7yD4cX055OQ.', 1, 1, 1, 1, '2021-06-01 23:08:20', '2021-06-01 23:08:20'),
(53, 1, '14514269', NULL, 1, 36, 281, 9, 18, 'Sylhet Ginner User', 'Sylhet Ginner User Bn', 'Jamal Khan', 'Jamal Khan Bn', '1000', 'Sylhet', 'Sylhet Add Bn', '01521445007', '199461194610003666', '$2y$10$hdcBBBkLnwhBKUvnL4i5nePoFxK7S5NhqmdXSU4CDKttxFZV.6TY2', 1, 1, 1, 1, '2021-06-02 03:00:31', '2021-06-03 02:54:00'),
(54, 2, '87564758', NULL, 5, 1, 4, 4, 21, 'Jihan', 'jamil bn', 'Suman Ahmed', 'Suman Ahmed Bn', '50', 'ss', 'ss bn', '01916876876', '19946119461000366', '', 1, 1, 1, 1, '2021-06-03 04:58:26', '2021-06-03 05:11:40'),
(55, 2, '52465666', NULL, 1, 36, 281, 9, 18, 'Jisan', 'Jisan Bn', 'MD. Younus', 'MD. Younus Bn', '10', 'Sreemontapur, Chandina, Comilla', 'Sreemontapur, Chandina, Comilla', '01720143107', '', '$2y$10$8IyDZJH1Sv6qkzc1fcXr9uiiIAJnNOl2jTU.uZfC2yBace5P62Vki', 1, 1, 1, 1, '2021-06-05 03:32:27', '2021-06-05 03:43:34'),
(56, 1, '66516928', NULL, 1, 36, 272, 9, 18, 'Mahbub', 'Mahbub bn', 'Rashed', 'Alec Madden', 'Ex minima sed alias ', 'Distinctio Vel et q', 'In velit ad eveniet', '01711130480', '199461194610003664', '$2y$10$aHkUUt5CzI0z5g3ETm0.G.jswU8RU2FEtpVNZCgro9cKxnUXuSANG', 1, 1, 1, 1, '2021-06-08 06:15:43', '2021-06-08 23:01:15');

-- --------------------------------------------------------

--
-- Table structure for table `ginner_schedules`
--

CREATE TABLE `ginner_schedules` (
  `id` int(11) NOT NULL,
  `fiscal_year_id` int(11) NOT NULL,
  `schedule_date` date NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `cotton_variety_id` int(11) NOT NULL,
  `hatt_id` int(11) NOT NULL,
  `seasons_id` int(11) NOT NULL,
  `cotton_id` int(11) NOT NULL,
  `quantity` double(8,2) NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=not approved, 2=approved, 3=reject',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ginner_schedules`
--

INSERT INTO `ginner_schedules` (`id`, `fiscal_year_id`, `schedule_date`, `applicant_id`, `cotton_variety_id`, `hatt_id`, `seasons_id`, `cotton_id`, `quantity`, `remarks`, `remarks_bn`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(16, 6, '2021-05-03', 41, 3, 3, 4, 2, 324.00, 'Magna non distinctio', 'Necessitatibus rerum', 1, 1, 1, '2021-05-06 00:32:31', '2021-05-06 00:32:31'),
(17, 6, '2021-05-05', 41, 1, 2, 1, 3, 387.00, 'Ullam aliquam molest', 'Cillum do itaque dol', 3, 1, 1, '2021-05-06 00:32:58', '2021-06-01 23:21:02'),
(18, 5, '2021-05-06', 41, 2, 2, 3, 2, 276.00, 'Dolore pariatur Rep', 'Maiores soluta qui l', 2, 1, 1, '2021-05-06 00:36:27', '2021-05-17 22:44:30'),
(19, 1, '2021-05-19', 47, 2, 7, 3, 2, 10.00, '', '', 3, 1, 1, '2021-05-17 22:46:13', '2021-06-01 03:24:58'),
(20, 5, '2021-05-03', 38, 2, 3, 2, 2, 122.00, '', '', 2, 1, 1, '2021-05-19 23:01:26', '2021-05-31 06:02:48'),
(21, 1, '2021-06-01', 50, 5, 8, 5, 4, 100.00, '', '', 2, 1, 1, '2021-06-01 03:18:37', '2021-06-01 03:25:04'),
(22, 1, '2021-06-02', 52, 6, 9, 1, 5, 100.00, 'NA', 'NA Bn', 2, 1, 1, '2021-06-01 23:17:03', '2021-06-01 23:25:15');

-- --------------------------------------------------------

--
-- Table structure for table `grower_central_stocks`
--

CREATE TABLE `grower_central_stocks` (
  `id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1=Ginner, 2=Grower',
  `applicant_id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `fiscal_year_id` int(11) NOT NULL,
  `seasons_id` int(11) NOT NULL,
  `cotton_variety_id` int(11) NOT NULL,
  `cotton_id` int(11) NOT NULL,
  `quantity` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `grower_central_stocks`
--

INSERT INTO `grower_central_stocks` (`id`, `type`, `applicant_id`, `org_id`, `fiscal_year_id`, `seasons_id`, `cotton_variety_id`, `cotton_id`, `quantity`, `created_at`, `updated_at`) VALUES
(3, 2, 37, 1, 1, 1, 1, 1, 1129.00, '2021-04-06 17:48:09', '2021-04-06 23:56:46'),
(4, 2, 37, 3, 5, 3, 1, 1, 10.00, '2021-04-06 18:45:24', '2021-04-06 18:45:24'),
(5, 2, 40, 3, 6, 2, 1, 1, 454.00, '2021-04-06 22:55:03', '2021-04-06 22:55:03'),
(6, 2, 40, 3, 7, 3, 2, 2, 677.00, '2021-04-06 22:56:20', '2021-04-06 22:56:20'),
(7, 2, 41, 3, 7, 3, 2, 2, 1.00, '2021-04-06 23:23:39', '2021-04-06 23:23:39'),
(8, 2, 41, 3, 7, 3, 2, 2, 0.00, '2021-04-06 23:29:48', '2021-04-06 23:29:48'),
(9, 2, 42, 3, 7, 3, 2, 2, 550.00, '2021-04-06 23:30:45', '2021-04-06 23:30:45'),
(10, 2, 38, 5, 6, 1, 1, 2, 790.00, '2021-04-07 00:08:43', '2021-04-07 00:08:43'),
(11, 2, 46, 0, 1, 3, 1, 2, 67000.00, '2021-05-18 00:55:55', '2021-06-02 02:30:40'),
(12, 2, 49, 0, 1, 5, 5, 4, 15000.00, '2021-05-31 04:46:38', '2021-06-02 02:21:46'),
(13, 2, 50, 0, 1, 1, 1, 1, 400.00, '2021-06-01 00:03:28', '2021-06-01 00:03:28'),
(14, 2, 42, 3, 1, 1, 1, 1, 1000.00, '2021-06-01 22:54:20', '2021-06-02 03:54:15'),
(15, 2, 52, 0, 1, 1, 6, 5, 6000.00, '2021-06-01 23:11:43', '2021-06-02 23:41:40'),
(16, 2, 37, 3, 1, 1, 4, 3, 200099.00, '2021-06-03 00:23:09', '2021-06-06 23:59:27'),
(17, 2, 42, 3, 3, 2, 6, 5, 1000.00, '2021-06-03 00:25:52', '2021-06-03 00:26:40'),
(18, 2, 52, 0, 1, 3, 6, 5, 7000.00, '2021-06-03 00:44:53', '2021-06-03 00:44:53'),
(19, 2, 42, 3, 3, 3, 6, 5, 1000.00, '2021-06-03 01:25:50', '2021-06-03 01:25:50'),
(20, 2, 52, 0, 1, 2, 6, 5, 7000.00, '2021-06-03 01:26:09', '2021-06-03 01:26:09'),
(21, 2, 51, 0, 1, 1, 6, 5, 1000.00, '2021-06-03 02:36:01', '2021-06-03 02:36:01'),
(22, 2, 51, 0, 1, 2, 6, 5, 999999.99, '2021-06-03 03:48:11', '2021-06-03 03:48:11'),
(23, 2, 37, 0, 6, 2, 4, 3, 454.00, '2021-06-05 03:03:46', '2021-06-05 03:03:46'),
(24, 2, 37, 0, 1, 3, 4, 3, 670000.00, '2021-06-05 03:05:18', '2021-06-05 03:05:18'),
(25, 2, 51, 0, 1, 1, 4, 3, 200099.00, '2021-06-07 00:00:04', '2021-06-07 00:00:04');

-- --------------------------------------------------------

--
-- Table structure for table `grower_hatt_manages`
--

CREATE TABLE `grower_hatt_manages` (
  `id` int(11) NOT NULL,
  `fiscal_year_id` int(11) NOT NULL,
  `seasons_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `upazilla_id` int(11) NOT NULL,
  `hatt_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `hatt_date` date NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=not close, 2=closed',
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `grower_hatt_manages`
--

INSERT INTO `grower_hatt_manages` (`id`, `fiscal_year_id`, `seasons_id`, `division_id`, `district_id`, `upazilla_id`, `hatt_id`, `region_id`, `zone_id`, `unit_id`, `hatt_date`, `remarks`, `remarks_bn`, `status`, `address`, `address_bn`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(8, 3, 1, 1, 1, 2, 2, 1, 18, 9, '2021-04-11', 'test ', 'test bn', 1, 'Mohakhali DOHS', 'Mohakhali DOHS bn', NULL, NULL, '2021-04-11 00:13:13', '2021-04-11 00:13:13'),
(9, 1, 3, 6, 45, 355, 7, 13, 29, 13, '2021-05-05', '', '', 1, 'Kishoregonj', '', NULL, NULL, '2021-05-04 22:30:00', '2021-05-04 22:30:00'),
(10, 1, 5, 4, 35, 269, 8, 14, 30, 14, '2021-05-31', 'No Remarks', 'No Remarks Bn', 1, 'QA Test Address', 'QA Test Address Bn', NULL, NULL, '2021-05-31 04:51:55', '2021-05-31 04:52:19'),
(11, 1, 5, 5, 38, 293, 9, 1, 18, 9, '2021-06-01', 'Remarksssiii', 'Remarks Bn', 1, 'Sylhet Adddd', 'Sylhet Add Bnn', NULL, NULL, '2021-06-01 03:32:26', '2021-06-03 04:44:06');

-- --------------------------------------------------------

--
-- Table structure for table `grower_prod_achievements`
--

CREATE TABLE `grower_prod_achievements` (
  `id` int(11) NOT NULL,
  `fiscal_year_id` int(11) NOT NULL,
  `achievement_date` date NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `cotton_variety_id` int(11) NOT NULL,
  `cotton_id` int(11) NOT NULL,
  `seasons_id` int(11) DEFAULT NULL,
  `quantity` double(8,2) NOT NULL DEFAULT '0.00',
  `remarks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=not close, 2=closed',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `grower_prod_achievements`
--

INSERT INTO `grower_prod_achievements` (`id`, `fiscal_year_id`, `achievement_date`, `applicant_id`, `cotton_variety_id`, `cotton_id`, `seasons_id`, `quantity`, `remarks`, `remarks_bn`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(4, 1, '2021-12-12', 37, 1, 1, 1, 60.00, 'remarks', ' remarks bn', 2, 1, 1, '2021-04-06 17:48:09', '2021-04-06 22:04:36'),
(5, 1, '2021-12-12', 37, 1, 1, 1, 60.00, 'remarks', ' remarks bn', 2, 1, 1, '2021-04-06 17:56:47', '2021-04-06 22:05:53'),
(6, 1, '2021-12-12', 37, 1, 1, 1, 60.00, 'remarks', ' remarks bn', 2, 1, 1, '2021-04-06 17:59:17', '2021-04-06 22:05:58'),
(7, 5, '2021-04-15', 37, 1, 1, 3, 10.00, 'rtyrty', ' remarks bn', 2, 1, 1, '2021-04-06 18:45:24', '2021-04-06 18:45:24'),
(8, 1, '2021-12-12', 37, 1, 1, 1, 60.00, 'remarks', ' remarks bn', 1, 1, 1, '2021-04-06 19:28:11', '2021-04-06 19:28:11'),
(9, 1, '2021-12-12', 37, 1, 1, 1, 60.00, 'remarks', ' remarks bn', 1, 1, 1, '2021-04-06 19:42:42', '2021-04-06 19:42:42'),
(11, 6, '2021-04-14', 37, 4, 3, 2, 454.00, 'ghfh', 'fgh gfhfg', 1, 1, 1, '2021-04-06 22:55:03', '2021-06-05 03:03:46'),
(12, 7, '2021-04-14', 42, 2, 2, 3, 550.00, 'gelloooo', 'againb tesdt', 1, 1, 1, '2021-04-06 22:56:20', '2021-04-06 23:30:45'),
(13, 1, '2021-12-12', 37, 1, 1, 1, 400.00, 'bn testtttt', 'testinggggggg', 1, 1, 1, '2021-04-06 23:51:23', '2021-04-06 23:53:34'),
(14, 1, '2021-12-12', 37, 1, 1, 1, 609.00, 'remarks', ' remarks bn', 1, 1, 1, '2021-04-06 23:56:46', '2021-04-06 23:56:46'),
(15, 6, '2021-04-20', 38, 1, 2, 1, 790.00, 'gfgfh g', 'fgh fghfg', 2, 1, 1, '2021-04-07 00:08:42', '2021-05-18 02:14:12'),
(16, 1, '2021-05-18', 37, 4, 3, 3, 670000.00, 'null', 'null', 1, 1, 1, '2021-05-18 00:55:55', '2021-06-05 03:05:18'),
(17, 1, '2021-05-31', 49, 5, 4, 5, 15000.00, 'Remarks ', 'Remarks Bn', 1, 1, 1, '2021-05-31 04:46:38', '2021-06-02 02:21:46'),
(18, 1, '2021-06-01', 50, 1, 1, 1, 400.00, NULL, NULL, 1, 1, 1, '2021-06-01 00:03:28', '2021-06-01 00:03:28'),
(19, 1, '2021-06-02', 42, 1, 1, 1, 1000.00, 'NA', 'NA Bn', 1, 1, 1, '2021-06-01 22:54:20', '2021-06-02 03:54:15'),
(20, 1, '2021-06-02', 52, 6, 5, 1, 6000.00, 'NA sssssss', 'NA Bn test', 1, 1, 1, '2021-06-01 23:11:43', '2021-06-02 23:41:40'),
(21, 1, '0000-00-00', 51, 4, 3, 1, 200099.00, 'yy', 'oipouoiuo', 1, 1, 1, '2021-06-03 00:23:09', '2021-06-07 00:00:04'),
(22, 3, '2021-06-03', 42, 6, 5, 3, 1000.00, 'test remarks', 'test remarks bn', 1, 1, 1, '2021-06-03 00:25:52', '2021-06-03 01:25:50'),
(23, 1, '2012-04-21', 52, 6, 5, 2, 7000.00, 'sss', 'sss bn', 1, 1, 1, '2021-06-03 00:44:53', '2021-06-03 01:26:09'),
(24, 1, '2021-06-04', 51, 6, 5, 2, 999999.99, 'test ', 'test bn', 1, 1, 1, '2021-06-03 02:36:01', '2021-06-03 03:48:11');

-- --------------------------------------------------------

--
-- Table structure for table `grower_sell_entry`
--

CREATE TABLE `grower_sell_entry` (
  `id` int(11) NOT NULL,
  `fiscal_year_id` int(11) NOT NULL,
  `seasons_id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `hatt_id` int(11) NOT NULL,
  `cotton_variety_id` int(11) NOT NULL,
  `cotton_id` int(11) NOT NULL,
  `hatt_date` date NOT NULL,
  `quantity` double(8,2) NOT NULL DEFAULT '0.00',
  `unit_id` int(11) NOT NULL,
  `price` double(8,2) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=not approved, 2=approved, 3=reject',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `grower_sell_entry`
--

INSERT INTO `grower_sell_entry` (`id`, `fiscal_year_id`, `seasons_id`, `applicant_id`, `hatt_id`, `cotton_variety_id`, `cotton_id`, `hatt_date`, `quantity`, `unit_id`, `price`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(22, 1, 2, 37, 5, 1, 1, '2020-12-10', 150.00, 10, 10.00, 1, NULL, NULL, NULL, NULL),
(23, 1, 1, 37, 5, 1, 1, '2021-04-09', 12.00, 10, 123.00, 1, NULL, NULL, NULL, NULL),
(24, 1, 2, 37, 5, 1, 1, '2021-04-09', 12.00, 10, 123.00, 1, NULL, NULL, NULL, NULL),
(25, 1, 1, 52, 9, 6, 5, '2021-06-02', 100.00, 9, 1000.00, 1, NULL, NULL, '2021-06-01 23:14:40', '2021-06-01 23:14:40'),
(26, 1, 1, 52, 9, 6, 5, '2021-06-02', 200.00, 9, 2000.00, 1, NULL, NULL, '2021-06-01 23:36:18', '2021-06-01 23:36:18'),
(27, 1, 1, 52, 9, 6, 5, '2021-06-03', 1000.00, 9, 5000.00, 1, NULL, NULL, '2021-06-02 23:28:15', '2021-06-02 23:28:15'),
(28, 1, 1, 37, 5, 4, 3, '2021-06-03', 400.00, 3, 1000.00, 1, NULL, NULL, '2021-06-03 00:24:27', '2021-06-03 00:24:27');

-- --------------------------------------------------------

--
-- Table structure for table `hatt_notifications`
--

CREATE TABLE `hatt_notifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hatt_id` int(11) NOT NULL,
  `fiscal_year_id` int(11) NOT NULL,
  `seasons_id` int(11) NOT NULL,
  `ginner_grower_id` int(11) NOT NULL,
  `seen_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=unseen, 2=seen',
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hatt_notifications`
--

INSERT INTO `hatt_notifications` (`id`, `hatt_id`, `fiscal_year_id`, `seasons_id`, `ginner_grower_id`, `seen_status`, `message`, `created_at`, `updated_at`) VALUES
(1, 2, 3, 1, 42, 1, 'Dear Roth Clemons, you can sell cotton to Mohakhali DOHS in 11 Apr, 2021', '2021-04-11 00:13:14', '2021-04-11 00:13:14'),
(2, 2, 3, 1, 43, 1, 'Dear Nazmuzzaman Redoy, you can purchase cotton from Mohakhali DOHS in 11 Apr, 2021', '2021-04-11 00:13:14', '2021-04-11 00:13:14');

-- --------------------------------------------------------

--
-- Table structure for table `linkage_gro_buy_profiles`
--

CREATE TABLE `linkage_gro_buy_profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `applicant_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Grower, 2=Buyer',
  `commodity_type_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `father_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `father_name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `land_area` double(8,2) DEFAULT NULL,
  `division_id` bigint(20) UNSIGNED DEFAULT NULL,
  `district_id` bigint(20) UNSIGNED DEFAULT NULL,
  `upazilla_id` bigint(20) UNSIGNED DEFAULT NULL,
  `union_id` bigint(20) UNSIGNED DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_rej_reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_rej_reason_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_approved` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=no, 2=approved,3=reject',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `linkage_gro_buy_profiles`
--

INSERT INTO `linkage_gro_buy_profiles` (`id`, `applicant_id`, `type`, `commodity_type_id`, `name`, `name_bn`, `father_name`, `father_name_bn`, `land_area`, `division_id`, `district_id`, `upazilla_id`, `union_id`, `address`, `address_bn`, `mobile_no`, `nid`, `remarks`, `remarks_bn`, `app_rej_reason`, `app_rej_reason_bn`, `is_approved`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(6, '1', 1, 1, 'hanif', ' hanif bn', 'abc', 'abc bn', 10.00, 1, 1, 3, 36, 'Shahebabad', 'shahebabad bn', '01850953185', '12354896', 'xyz', 'xyz bn', NULL, NULL, 1, 1, 1, 1, '2021-04-25 23:51:49', '2021-04-25 23:51:49'),
(7, '2', 1, 1, 'suman', 'suman bn', 'abc', 'abc bn', 10.00, 1, 1, 4, 50, 'Shahebabad', 'shahebabad bn', '01302598912', '.02365132', 'xyz', 'xyz bn', NULL, NULL, 1, 1, 1, 1, '2021-04-27 02:50:09', '2021-04-27 02:50:09'),
(8, '3', 1, 1, 'Ujjol', 'Ujjol bn', 'abcd', 'abcd bn', 10.00, 1, 6, 55, 503, 'Shahebabad, chadpur', 'Shahebabad, chadpur bn', '01850953185', '0213548752', 'remarks en', 'remarks bn', NULL, NULL, 1, 1, 1, 1, '2021-04-27 04:44:04', '2021-04-27 04:44:04'),
(9, '4', 2, 1, 'Ruhul', 'Ruhul bn', 'abcd', 'abcd bn', 10.00, 2, 16, 143, 1292, 'Shahebabad, chadpur', 'Shahebabad, chadpur bn', '01850953185', '0126453', 'remarks en', 'remarks bn', NULL, NULL, 1, 1, 1, 1, '2021-04-27 23:17:09', '2021-04-27 23:17:09');

-- --------------------------------------------------------

--
-- Table structure for table `linkage_gro_buy_pro_details`
--

CREATE TABLE `linkage_gro_buy_pro_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gro_buy_profiles_id` bigint(20) UNSIGNED NOT NULL,
  `commodity_group_id` int(11) NOT NULL,
  `commodity_sub_group_id` int(11) NOT NULL,
  `commodity_id` int(11) NOT NULL,
  `price_type_id` int(11) DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `quantity` decimal(10,2) NOT NULL DEFAULT '0.00',
  `attachment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `origin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `origin_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `linkage_gro_buy_pro_details`
--

INSERT INTO `linkage_gro_buy_pro_details` (`id`, `gro_buy_profiles_id`, `commodity_group_id`, `commodity_sub_group_id`, `commodity_id`, `price_type_id`, `unit_id`, `price`, `quantity`, `attachment`, `origin`, `origin_bn`, `remarks`, `remarks_bn`, `created_at`, `updated_at`) VALUES
(1, 6, 3, 4, 2, 1, 3, '200.00', '10.00', NULL, 'origin en', 'origin bn', 'remarks en', 'remarks bn', '2021-04-25 23:51:49', '2021-04-27 05:44:14'),
(2, 6, 5, 24, 3, 2, 2, '250.00', '10.00', NULL, 'dfgg', 'vcncvn bn', 'gvxhncb en', ' esgfd bn', '2021-04-25 23:51:49', '2021-04-27 05:44:14'),
(3, 7, 3, 4, 2, 1, 2, '200.00', '5.00', NULL, 'hgfxjh', 'jgj bn', 'reyh ', 'gbfjhc bn', '2021-04-27 02:50:09', '2021-04-27 04:13:16'),
(4, 7, 5, 24, 4, 2, 3, '300.00', '6.00', NULL, 'gfhcgj', 'trgdgy  bn', 'rtgut', ' ffgdhh bn', '2021-04-27 02:50:09', '2021-04-27 04:13:17'),
(5, 7, 5, 24, 5, 3, 7, '400.00', '7.00', NULL, 'trfesyrs', 'gdfxh bn', 'trgy', ' fgdhd bn', '2021-04-27 02:50:09', '2021-04-27 04:13:17'),
(6, 8, 5, 24, 3, 1, 2, '40.00', '8.00', NULL, 'origin en', 'origin bn', 'remarks en', 'remarks bn', '2021-04-27 04:44:04', '2021-04-27 22:53:06'),
(7, 8, 5, 24, 4, 2, 3, '60.00', '3.00', NULL, 'origin en', 'jgj bn', 'reyh ', 'gbfjhc bn', '2021-04-27 04:44:04', '2021-04-27 22:53:07'),
(8, 8, 5, 24, 5, 1, 6, '20.00', '9.00', NULL, 'origin en', 'origin bn', 'remarks en', 'remarks bn', '2021-04-27 04:44:04', '2021-04-27 22:53:07'),
(10, 9, 5, 24, 5, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-04-27 23:17:09', '2021-04-27 23:17:09'),
(11, 9, 5, 24, 6, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-04-27 23:17:09', '2021-04-27 23:17:09'),
(12, 9, 3, 4, 2, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, '2021-04-27 23:17:09', '2021-04-27 23:17:09');

-- --------------------------------------------------------

--
-- Table structure for table `master_alert_percentages`
--

CREATE TABLE `master_alert_percentages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fiscal_year_id` bigint(20) UNSIGNED DEFAULT NULL,
  `alert_percentage` double(8,2) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_alert_percentages`
--

INSERT INTO `master_alert_percentages` (`id`, `fiscal_year_id`, `alert_percentage`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(4, 1, 50.00, 2, 1, 1, '2021-06-14 00:48:17', '2021-06-25 23:25:51');

-- --------------------------------------------------------

--
-- Table structure for table `master_campaigns`
--

CREATE TABLE `master_campaigns` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `campaign_name_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=dmin, 2=request, 3=dev head',
  `fiscal_year_id` int(11) DEFAULT '0',
  `divisional_office_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_campaigns`
--

INSERT INTO `master_campaigns` (`id`, `campaign_name`, `campaign_name_bn`, `type_id`, `fiscal_year_id`, `divisional_office_id`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'One en', 'এক', 1, 1, NULL, 1, NULL, 1, NULL, '2021-04-19 00:26:38'),
(2, 'Two', 'দুই', 1, 1, NULL, 1, NULL, NULL, NULL, NULL),
(3, 'Three', 'তিন', 2, 1, NULL, 1, NULL, NULL, NULL, NULL),
(4, 'Four', 'চার', 3, 1, NULL, 1, NULL, NULL, NULL, NULL),
(5, 'one', 'one bn', 2, 1, NULL, 1, 1, 1, '2021-04-17 03:40:33', '2021-06-05 23:46:16'),
(6, 'Adtisement', 'প্রচার', 1, 1, NULL, 1, 1, 1, '2021-04-17 12:34:55', '2021-04-17 12:34:55'),
(7, 'test ', 'test bn', 1, 1, NULL, 1, NULL, NULL, '2021-04-18 09:47:40', '2021-04-18 09:47:40'),
(8, 'pusti campaign', 'pusti campaign bn', 2, 1, NULL, 2, 1, 1, '2021-04-18 21:43:12', '2021-06-08 03:51:16'),
(9, '19 April', '19 April', 1, 1, NULL, 1, NULL, NULL, '2021-04-19 09:38:04', '2021-04-19 09:38:04'),
(10, '19 April', '19 April', 1, 1, NULL, 1, NULL, NULL, '2021-04-19 09:43:43', '2021-04-19 09:43:43'),
(11, 'testCampaign 11', 'testCampaign 11 bn', 2, 6, NULL, 1, 1, 1, '2021-04-19 22:19:01', '2021-04-20 21:09:03'),
(12, 'Twelve', 'Twelve bn', 1, 6, NULL, 1, 1, 1, '2021-05-06 02:55:59', '2021-05-06 02:55:59'),
(13, 'Thirteennn', 'Thirteennn bn', 1, 1, NULL, 1, 1, 1, '2021-05-20 00:53:19', '2021-06-08 03:43:31'),
(14, 'Narsindi E-Pusti Campaign', 'Narsindi E-Pusti Campaign Bn', 1, 1, NULL, 1, 1, 1, '2021-06-05 00:06:45', '2021-06-08 03:56:21'),
(15, 'Narsindi E-School-Pusti Campaign', 'Narsindi E-School-Pusti Campaign Bn', 2, 1, NULL, 1, 1, 1, '2021-06-05 06:02:16', '2021-06-11 22:39:31'),
(16, 'Cassady Sawyer', 'Dennis Hebert', 2, 8, NULL, 2, 1, 1, '2021-06-08 01:44:32', '2021-06-11 23:58:37'),
(17, 'Madeson Mcdonald', 'Destiny Pennington', 2, 3, NULL, 1, 1, 1, '2021-06-08 01:46:27', '2021-06-11 23:42:58'),
(18, 'Armando Wynn', 'Fletcher Morin', 2, 7, NULL, 1, 1, 1, '2021-06-08 01:50:08', '2021-06-11 23:48:08');

-- --------------------------------------------------------

--
-- Table structure for table `master_commodity_groups`
--

CREATE TABLE `master_commodity_groups` (
  `id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `group_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_name_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_commodity_groups`
--

INSERT INTO `master_commodity_groups` (`id`, `org_id`, `group_name`, `group_name_bn`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `dam_id`) VALUES
(1, 13, 'Vegetable EN', 'শাকসবজি বাংলা', 1, 0, 1, '2021-06-12 03:10:32', '2021-06-23 04:18:26', 2),
(2, 13, 'Milk', 'দুধ', 1, 0, 1, '2021-06-12 03:10:32', '2021-06-23 03:46:27', 3),
(3, 13, 'Lentils Dal', 'ডাল', 1, 0, 1, '2021-06-12 03:10:32', '2021-06-23 03:16:35', 4),
(4, 13, 'Water  Fish - 1', 'পানির মাছ', 1, 0, 1, '2021-06-12 03:10:32', '2021-06-23 03:15:10', 5),
(5, 13, 'Foodgrain', 'খাদ্যশস্য', 1, 0, 1, '2021-06-12 03:10:33', '2021-06-23 03:22:55', 6),
(6, 13, 'test', 'test', 1, 1, 1, '2021-06-23 22:29:25', '2021-06-23 22:29:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_commodity_names`
--

CREATE TABLE `master_commodity_names` (
  `id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `commodity_group_id` int(11) NOT NULL,
  `commodity_sub_group_id` int(11) NOT NULL,
  `commodity_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commodity_name_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `unit_grower` int(11) DEFAULT NULL,
  `unit_whole_sale` int(11) DEFAULT NULL,
  `unit_retail` int(11) DEFAULT NULL,
  `price_type_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_commodity_names`
--

INSERT INTO `master_commodity_names` (`id`, `org_id`, `commodity_group_id`, `commodity_sub_group_id`, `commodity_name`, `commodity_name_bn`, `unit_id`, `unit_grower`, `unit_whole_sale`, `unit_retail`, `price_type_id`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `dam_id`) VALUES
(20, 13, 2, 39, 'Cow Milk', 'গরুর দুধ', NULL, 1, 3, 5, 1, 1, 0, 1, '2021-06-12 06:06:50', '2021-06-23 02:14:49', 8),
(21, 13, 5, 37, 'Beans', 'মটরশুটি', NULL, 5, 1, 2, 1, 1, 0, 1, '2021-06-12 06:06:50', '2021-06-21 03:50:52', 1),
(22, 13, 5, 37, 'Aus-Fine', 'আউশ চাল - সরু', NULL, 5, 4, 2, 2, 1, 0, 1, '2021-06-12 06:06:51', '2021-06-21 03:29:37', 2),
(23, 13, 1, 38, 'Brinjal', 'বেগুন', NULL, 4, 3, 3, 1, 1, 0, 1, '2021-06-12 06:06:51', '2021-06-21 03:30:01', 5),
(24, 13, 5, 37, 'Aus-Coarse', 'আউশ চাল - মোটা', NULL, 5, 3, 4, 2, 1, 0, 1, '2021-06-12 06:06:51', '2021-06-21 03:29:49', 3),
(25, 13, 4, 36, 'Hilsha', 'ইলিশ', NULL, 3, 1, 5, 1, 1, 0, 1, '2021-06-12 06:06:51', '2021-06-21 03:29:43', 6),
(26, 13, 6, 41, 'check', 'check', NULL, 1, 2, 3, 1, 1, 1, 1, '2021-06-23 22:30:26', '2021-06-23 22:30:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_commodity_sub_groups`
--

CREATE TABLE `master_commodity_sub_groups` (
  `id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `commodity_group_id` int(11) NOT NULL,
  `sub_group_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_group_name_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_commodity_sub_groups`
--

INSERT INTO `master_commodity_sub_groups` (`id`, `org_id`, `commodity_group_id`, `sub_group_name`, `sub_group_name_bn`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `dam_id`) VALUES
(35, 13, 1, 'Beans', 'মটরশুটি', 1, 0, 1, '2021-06-12 03:57:52', '2021-06-23 02:14:38', 1),
(36, 13, 4, 'Hilsha', 'ইলিশ মাছ', 1, 0, 0, '2021-06-12 03:57:52', '2021-06-12 03:57:52', 2),
(37, 13, 5, 'Rice', 'চাল', 1, 0, 0, '2021-06-12 03:57:52', '2021-06-12 03:57:52', 3),
(38, 13, 1, 'Brinjal', 'বেগুন', 1, 0, 1, '2021-06-12 03:57:52', '2021-06-23 03:43:17', 4),
(39, 13, 2, 'Cow Milk', 'গরুর দুধ', 1, 0, 0, '2021-06-12 03:57:52', '2021-06-13 06:17:24', 5),
(41, 0, 6, 'test sub group', 'test sub group', 1, 1, 1, '2021-06-23 22:29:38', '2021-06-23 22:29:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_commodity_types`
--

CREATE TABLE `master_commodity_types` (
  `id` int(11) NOT NULL,
  `type_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_commodity_types`
--

INSERT INTO `master_commodity_types` (`id`, `type_name`, `type_name_bn`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Commodity 1', 'পণ্য 1', 1, NULL, NULL, NULL, '2021-04-22 01:35:43'),
(2, 'Commodity 2', 'পণ্য 23', 1, NULL, 1, NULL, '2021-04-21 23:40:55'),
(3, 'commodity type ', 'commodity type bn u', 2, 1, 1, '2021-04-21 23:34:10', '2021-05-03 03:12:35'),
(4, 'New 1', 'নতুন 1', 1, 1, 1, '2021-05-03 03:13:25', '2021-05-03 03:13:25'),
(5, 'Test', 'Test', 1, 1, 1, '2021-05-03 03:44:28', '2021-05-03 03:44:28');

-- --------------------------------------------------------

--
-- Table structure for table `master_communication_linkages`
--

CREATE TABLE `master_communication_linkages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_communication_linkages`
--

INSERT INTO `master_communication_linkages` (`id`, `name`, `name_bn`, `created_by`, `updated_by`, `status`, `created_at`, `updated_at`, `dam_id`) VALUES
(1, 'Highway Road', 'Highway Road', NULL, NULL, 1, '2021-06-14 02:07:00', '2021-06-14 02:07:00', 1),
(2, 'Trolly', 'Trolly', NULL, NULL, 1, '2021-06-14 02:07:00', '2021-06-14 02:07:00', 9),
(3, 'Tomtom', 'Tomtom', NULL, NULL, 1, '2021-06-14 02:07:00', '2021-06-14 02:07:00', 8),
(4, 'Semi Pucca Road', 'Semi Pucca Road', NULL, NULL, 1, '2021-06-14 02:07:01', '2021-06-14 02:07:01', 7),
(5, 'Railway', 'Railway', NULL, NULL, 1, '2021-06-14 02:07:01', '2021-06-14 02:07:01', 6),
(6, 'Pucca Road', 'Pucca Road', NULL, NULL, 1, '2021-06-14 02:07:01', '2021-06-14 02:07:01', 5),
(7, 'Paved Way', 'Paved Way', NULL, NULL, 1, '2021-06-14 02:07:01', '2021-06-14 02:07:01', 4),
(8, 'Launch', 'Launch', NULL, NULL, 1, '2021-06-14 02:07:01', '2021-06-14 02:07:01', 3),
(9, 'Kancha/Earthen Road', 'Kancha/Earthen Road', NULL, NULL, 1, '2021-06-14 02:07:01', '2021-06-14 02:07:01', 2),
(10, 'Waterway', 'Waterway', NULL, NULL, 1, '2021-06-14 02:07:01', '2021-06-14 02:07:01', 10);

-- --------------------------------------------------------

--
-- Table structure for table `master_cotton_names`
--

CREATE TABLE `master_cotton_names` (
  `id` int(11) NOT NULL,
  `org_id` int(11) DEFAULT NULL,
  `cotton_variety_id` int(11) NOT NULL,
  `cotton_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cotton_name_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_cotton_names`
--

INSERT INTO `master_cotton_names` (`id`, `org_id`, `cotton_variety_id`, `cotton_name`, `cotton_name_bn`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'cotton_name en', 'cotton_name bn', 1, 1, 1, '2021-03-31 23:00:25', '2021-05-31 00:42:11'),
(2, 5, 3, 'test Cotton', 'test Cotton bn', 2, 1, 1, '2021-03-31 23:51:24', '2021-06-01 00:49:34'),
(3, 1, 4, 'test Cotton one en', 'test Cotton one bn', 1, 1, 1, '2021-03-31 23:55:13', '2021-05-31 00:50:07'),
(4, NULL, 5, 'QA Test Cotton Name', 'QA Test Cotton Name Bn', 1, 1, 1, '2021-05-31 00:47:15', '2021-05-31 01:02:40'),
(5, NULL, 6, 'Sylhet Cotton Name', 'Sylhet Cotton Name Bn', 1, 1, 1, '2021-06-01 23:03:49', '2021-06-01 23:09:29');

-- --------------------------------------------------------

--
-- Table structure for table `master_cotton_varities`
--

CREATE TABLE `master_cotton_varities` (
  `id` int(11) NOT NULL,
  `org_id` int(11) DEFAULT NULL,
  `cotton_variety` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cotton_variety_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_cotton_varities`
--

INSERT INTO `master_cotton_varities` (`id`, `org_id`, `cotton_variety`, `cotton_variety_bn`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'cotton variety', 'cotton variety bn', 2, 1, 1, '2021-03-31 20:12:08', '2021-05-31 00:40:53'),
(2, 5, 'good cotton', 'good cotton bn', 2, 1, 1, '2021-03-31 20:35:51', '2021-05-31 00:40:55'),
(3, 5, 'variety one en', 'variety one bn', 1, 1, 1, '2021-03-31 23:30:07', '2021-03-31 23:32:34'),
(4, 1, 'cotton variety two', 'cotton variety two bn', 1, 1, 1, '2021-03-31 23:31:56', '2021-03-31 23:31:56'),
(5, NULL, 'QA Test Cotton Variety', 'QA Test Cotton Variety Bn', 1, 1, 1, '2021-05-31 00:27:50', '2021-05-31 00:27:50'),
(6, NULL, 'Sylhet Cotton Variety', 'Sylhet Cotton Variety Bn', 1, 1, 1, '2021-06-01 23:03:00', '2021-06-01 23:09:49');

-- --------------------------------------------------------

--
-- Table structure for table `master_destination_of_produces`
--

CREATE TABLE `master_destination_of_produces` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_destination_of_produces`
--

INSERT INTO `master_destination_of_produces` (`id`, `name`, `name_bn`, `created_by`, `updated_by`, `status`, `created_at`, `updated_at`, `dam_id`) VALUES
(3, 'Dhaka Terminal Market', 'Dhaka Terminal Market', 0, 0, 1, '2021-06-15 04:43:48', '2021-06-15 04:43:48', 3),
(1, 'Surrounding Areas', 'Surrounding Areas', 0, 0, 1, '2021-06-15 04:43:48', '2021-06-15 04:43:48', 1),
(2, 'District Market', 'District Market', 0, 0, 1, '2021-06-15 04:43:48', '2021-06-15 04:43:48', 2),
(NULL, 'Others', 'অন্যান্য', NULL, NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_divisional_offices`
--

CREATE TABLE `master_divisional_offices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `office_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `office_name_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `division_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_no` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_divisional_offices`
--

INSERT INTO `master_divisional_offices` (`id`, `office_name`, `office_name_bn`, `division_id`, `district_id`, `name`, `name_bn`, `email`, `mobile_no`, `address`, `address_bn`, `designation`, `designation_bn`, `remarks`, `remarks_bn`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Office New', 'Office New Bn', 1, 1, 'ss', 'ss bn', 'ss@gmail.com', '01723019011', 'dhk', 'dhk bn', 'Prog', 'Prog bn', 'remarks', 'remarks bn', 1, 1, 1, '2021-04-11 22:54:07', '2021-04-12 01:04:01'),
(2, 'Camilla Keller', 'Judith Gill', 4, 35, 'Benedict Buchanan', 'Jameson French', 'gaxizosewu@mailinator.com', '055454545421', 'Ea consectetur qui ', 'Sint in id ut ipsam ', 'Illo eu ut iusto con', 'Rerum quia enim culp', 'Minus consequatur R', 'Minima voluptatem D', 1, 1, 1, '2021-04-12 01:10:01', '2021-04-12 01:11:11'),
(3, 'Stephanie Anthony', 'Logan Wilkerson', 8, 63, 'Jael Whitley', 'Juliet Avila', 'pipap@mailinator.com', '69', 'Nisi sapiente eos b', 'Asperiores autem ut ', 'Amet ullamco illum', 'Dicta explicabo Sun', 'Quis accusantium nem', 'Rerum ut quam incidu', 1, 1, 1, '2021-04-12 01:10:10', '2021-04-12 01:10:10'),
(4, 'Kiayada Christian', 'Colton Middleton', 6, 47, 'Myra Bright', 'Brandon Mcpherson', 'nijolikiv@mailinator.com', '34', 'Ratione rerum omnis ', 'Veniam consequatur ', 'Iure nostrum ea sapi', 'Sit recusandae Rer', 'Eius excepteur dicta', 'Eos deserunt quisqua', 1, 1, 1, '2021-04-12 01:10:23', '2021-04-12 01:10:23'),
(5, 'Natore Office', 'নাটোর অফিস', 2, 16, 'Ruhul', 'রুহুল', 'ruhulcse@gmail.com', '01638584622', 'Natore Sadar', 'নাটোর সদর', 'Programmer', 'প্রোগ্রামার', 'Nothing to say', 'বলার কিছু নেই', 1, 1, 1, '2021-04-12 01:10:29', '2021-04-12 02:39:20'),
(6, 'Norman Dunlap', 'Jacob Vance', 4, 34, 'Jana Oneil', 'Byron Perkins', 'bedo@mailinator.com', '2', 'Neque excepteur offi', 'Consequatur Eum et ', 'Ut odit culpa eiusmo', 'Incidunt iusto ipsa', 'At dolores ipsa mol', 'Eum neque dolorum ir', 1, 1, 1, '2021-04-12 01:10:40', '2021-04-12 01:10:40'),
(7, 'Kylee Joseph', 'Keegan Gilbert', 1, 11, 'Debra Cooley', 'Dorothy Cole', 'xugitije@mailinator.com', '1', 'Illo perspiciatis e', 'Et reprehenderit ali', 'Fugiat dignissimos n', 'Nemo error aut adipi', 'Do quia aute sit el', 'Eos atque ut enim is', 1, 1, 1, '2021-04-12 01:15:27', '2021-04-12 01:15:27'),
(8, 'Belle Anthony', 'Irma Zamora', 3, 28, 'Colton Gilbert', 'Francis Heath', 'pevybu@mailinator.com', '83', 'Do est pariatur Ut ', 'Dolor sit veritatis', 'Numquam sunt adipisc', 'Quae voluptates susc', 'Omnis possimus dese', 'Soluta incidunt dol', 1, 1, 1, '2021-04-12 01:15:34', '2021-04-12 01:15:34'),
(9, 'Alden Horton', 'Chaim Espinoza', 4, 33, 'Ezra Sullivan', 'Bert Ramsey', 'lysi@mailinator.com', '5', 'Dolor consectetur id', 'Quis suscipit quae n', 'Dolor architecto odi', 'Culpa eius rem eum ', 'Nesciunt veniam ma', 'Et sit exercitation', 2, 1, 1, '2021-04-12 01:15:47', '2021-06-05 03:35:13'),
(10, 'Glenna Knowles', 'Zelenia Whitney', 1, 3, 'Riley Curry', 'Marcia Ewing', 'cudu@mailinator.com', '16', 'Velit maiores error', 'Ut mollit voluptate ', 'Aliqua Repellendus', 'Ut duis vitae aspern', 'Nulla quia dolore do', 'Excepteur velit aper', 1, 1, 1, '2021-04-12 01:15:55', '2021-04-12 01:15:55'),
(11, 'Blaine Wise', 'Amity Mcintosh', 7, 57, 'Amity Everett', 'Cain Foreman', 'vilyvyl@mailinator.com', '100', 'Dolores non esse vol', 'Id voluptatum molest', 'Maiores distinctio ', 'Blanditiis vel id o', 'Nihil qui pariatur ', 'Rerum fuga Veniam ', 2, 1, 1, '2021-04-12 01:16:02', '2021-06-05 03:35:26'),
(12, 'Karyn Bridges', 'Gannon Bauer', 5, 38, 'Mallory Finch', 'Amaya Stephenson', 'lotalugeh@mailinator.com', '22', 'Quis officiis eiusmo', 'Placeat aut velit q', 'Deserunt ex inventor', 'Ipsum aliquam ut vol', 'Accusamus dolores ut', 'Rerum sit ea volupta', 1, 1, 1, '2021-04-12 01:16:08', '2021-04-12 01:16:08'),
(13, 'Eve Brennan', 'Kaseem Leach', 6, 52, 'Emerald Nelson', 'Macaulay Beach', 'zowiz@mailinator.com', '38', 'Est voluptates dolor', 'Earum omnis placeat', 'Laudantium aliqua ', 'Aliquid repudiandae ', 'Cumque et elit dese', 'Non optio ut dicta ', 2, 1, 1, '2021-04-12 01:16:15', '2021-04-12 02:37:53'),
(14, 'Merritt Puckett', 'Riley Serrano', 6, 47, 'Rigel Wolf', 'Lillian Reese', 'fumoqov@mailinator.com', '20', 'Ex harum qui dolore ', 'Culpa fugiat sapien', 'Quidem reiciendis ad', 'Et excepturi dolor a', 'Error veritatis mole', 'Facilis excepteur nu', 1, 1, 1, '2021-04-12 01:16:49', '2021-04-12 02:39:31'),
(15, 'Divisional Office', 'বিভাগীয় অফিস', 1, 1, 'Suman Syntech', 'Suman Syntech', 'ssuiu@gmail.com', '01991652700', 'Dhaka', 'ঢাকা', 'Web Developer', 'Web Developer', 'test remark -2', 'test remark bn2', 1, 1, 1, '2021-04-15 05:17:03', '2021-05-06 02:32:38'),
(16, 'Zahir Reeves', 'Inez Summers', 4, 33, 'Brynne Pruitt', 'Arthur Burgess', 'loby@mailinator.com', '01811121212444', 'Dignissimos omnis cu', 'Temporibus nisi blan', 'Sed alias consequunt', 'Quis officia non sit', 'Sint dolorem ut qui en ttt', 'Consequat Saepe esttttt', 1, 1, 1, '2021-05-06 00:49:19', '2021-05-06 02:33:03'),
(17, 'Kishoregonj Office', 'Kishoregonj Office', 6, 45, 'Md. Jahid', 'মোঃ জাহিদ ', 'probal_02@yahoo.com', '01811121212', 'Savar, Manikgonj, ', 'সাভার, মানিকগঞ্জ ', 'Director', 'Director', 'nothing en', 'nothing bn 2', 1, 1, 1, '2021-05-06 00:55:31', '2021-05-06 01:36:13'),
(18, 'Gannon Monroe', 'Lunea Norris', 6, 41, 'Hedy Crosby', 'Imelda Mason', 'qyvepoca@mailinator.com', '128', 'Dolorum ea alias qui', 'Vel ut molestias nes', 'Quis dolor quos ab i', 'Qui necessitatibus f', 'Excepteur et delectu', 'Sit aut dolor minima', 1, 1, 1, '2021-05-06 01:41:10', '2021-05-06 01:41:10'),
(19, 'Melodie Pollard', 'Ryder Glover', 2, 19, 'Yeo Hatfield', 'Eaton Haynes', 'xiloh@mailinator.com', '702', 'Obcaecati incidunt ', 'Dicta occaecat nihil', 'Est quo possimus ve', 'Adipisci laboris sin', 'Odio unde distinctio', 'Id cumque ut aut as', 1, 1, 1, '2021-05-06 02:33:34', '2021-05-06 02:33:34'),
(20, 'Manikganj Office', 'Manikganj Office', 6, 46, 'Md. Jahid', 'মোঃ জাহিদ ', '', '01671404511', 'Savar, Manikgonj, ', 'সাভার, মানিকগঞ্জ ', '', '', '', '', 1, 1, 1, '2021-05-06 02:40:56', '2021-05-08 23:53:51'),
(21, 'Bogura Divisional Office', 'বগুড়া বিভাগীয় অফিস', 2, 14, 'Ruhul', 'Ruhul bn', 'ruhul@gmail.com', '01843867772', 'bogura', 'bogura bn', '', '', '', '', 1, 1, 1, '2021-05-08 22:39:17', '2021-05-08 22:43:04'),
(22, 'Pabna Div', 'Pabna Div bn', 2, 13, 'Zisan', 'Zisan bn', 'tabum@mailinator.com', '044545454', 'Pabna', 'Pabna bn', 'Eligendi exercitatio', 'Quis consequuntur vo', 'Dignissimos et dolor', 'Nemo autem et dolori', 1, 1, 1, '2021-05-08 22:45:29', '2021-06-11 23:47:24'),
(23, 'Narsingdi Div Office', 'Narsingdi Div  Office bn', 6, 40, 'Emerald Knowles', 'Chaim Graham', 'zuzakowyq@mailinator.com', '64545545', 'Velit duis nostrud l', 'Ea quis pariatur Ei', 'Quia qui nulla labor', 'Nulla libero Nam per', 'Qui pariatur Sint ', 'Est quia earum sint', 2, 1, 1, '2021-05-08 22:51:32', '2021-06-08 03:51:04'),
(24, 'Narsingdi E-Pusti Office', 'Narsingdi E-Pusti Office Bn', 6, 40, 'Nasir Shah', 'Nasir Shah Bn', 'nasir@yopmail.com', '01521445008', 'Narsindhi Sadar', 'Narsindhi Sadar Bn', 'Co-Ordinator', 'Co-Ordinator Bn', 'none none', 'NA Bn', 1, 1, 1, '2021-06-04 23:21:48', '2021-06-11 23:47:14'),
(25, 'Jack Gutierrez', 'Zelenia Peters', 6, 40, 'Solid Engineering', 'Solid Engineering', 'amir@gmail.com', '01711130480', 'House-21, Road-2, Block-A, Dhaka Uddan, Mohammadpur', 'Consequuntur est eum', 'Voluptas et delectus', 'Dolore nisi tenetur ', 'Magna deleniti cumqu', 'Nostrum sunt veniam', 1, 1, 1, '2021-06-08 03:27:42', '2021-06-08 03:27:42'),
(26, 'Office-1', 'Office-1', 1, 1, 'Test', 'Test', '', '01711887877', '', '', '', '', '', '', 1, 1, 1, '2021-06-12 04:07:16', '2021-06-12 04:07:16');

-- --------------------------------------------------------

--
-- Table structure for table `master_hatts`
--

CREATE TABLE `master_hatts` (
  `id` int(11) NOT NULL,
  `org_id` int(11) DEFAULT NULL,
  `division_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `upazilla_id` int(11) NOT NULL,
  `hatt_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hatt_name_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_hatts`
--

INSERT INTO `master_hatts` (`id`, `org_id`, `division_id`, `district_id`, `upazilla_id`, `hatt_name`, `hatt_name_bn`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 3, 1, 1, 2, 'Two', 'Two bn', 1, 1, 1, '2021-04-03 19:54:38', '2021-04-06 18:18:22'),
(3, 5, 4, 33, 255, 'Two 2', 'Two 2 bn', 2, 1, 1, '2021-04-03 19:59:30', '2021-06-02 06:12:49'),
(4, 5, 4, 34, 259, 'Three -3', 'Three bn -3', 2, 1, 1, '2021-04-03 20:00:42', '2021-06-01 00:51:23'),
(5, 1, 2, 16, 143, 'Dighapatia', 'দিঘাপতিয়া', 1, 1, 1, '2021-04-05 01:48:53', '2021-04-05 01:48:53'),
(7, NULL, 6, 45, 355, 'Austagram Boro Bazar Hat', 'Austagram Boro Bazar Hat', 1, 1, 1, '2021-05-04 03:27:41', '2021-05-04 03:27:41'),
(8, NULL, 4, 35, 269, 'QA Test Hatt Name', 'QA Test Hatt Name Bn', 1, 1, 1, '2021-05-31 03:05:00', '2021-05-31 03:05:00'),
(9, NULL, 5, 36, 272, 'Sylhet Hatt', 'Sylhet Hatt Bn', 1, 1, 1, '2021-06-01 03:30:58', '2021-06-01 23:01:41');

-- --------------------------------------------------------

--
-- Table structure for table `master_infrastructures`
--

CREATE TABLE `master_infrastructures` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_infrastructures`
--

INSERT INTO `master_infrastructures` (`id`, `name`, `name_bn`, `created_by`, `updated_by`, `status`, `created_at`, `updated_at`, `dam_id`) VALUES
(1, 'Bank', 'Bank', 0, 0, 1, '2021-06-14 05:13:28', '2021-06-14 05:13:28', 1),
(2, 'Drainage', 'Drainage', 0, 0, 1, '2021-06-14 05:13:29', '2021-06-14 05:13:29', 2),
(3, 'Electricity', 'Electricity', 0, 0, 1, '2021-06-14 05:13:29', '2021-06-14 05:13:29', 3),
(4, 'Govt. Godown', 'Govt. Godown', 0, 0, 1, '2021-06-14 05:13:29', '2021-06-14 05:13:29', 4),
(5, 'Govt. Wherehouse', 'Govt. Wherehouse', 0, 0, 1, '2021-06-14 05:13:29', '2021-06-14 05:13:29', 5),
(6, 'Health Center', 'Health Center', 0, 0, 1, '2021-06-14 05:13:29', '2021-06-14 05:13:29', 6),
(7, 'Matrishadan', 'Matrishadan', 0, 0, 1, '2021-06-14 05:13:29', '2021-06-14 05:13:29', 7),
(8, 'Mosque', 'Mosque', 0, 0, 1, '2021-06-14 05:13:29', '2021-06-14 05:13:29', 8),
(9, 'NGO Office', 'NGO Office', 0, 0, 1, '2021-06-14 05:13:29', '2021-06-14 05:13:29', 9);

-- --------------------------------------------------------

--
-- Table structure for table `master_lease_value_years`
--

CREATE TABLE `master_lease_value_years` (
  `id` int(11) DEFAULT NULL,
  `name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` year(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_lease_value_years`
--

INSERT INTO `master_lease_value_years` (`id`, `name_bn`, `name`, `created_at`, `updated_at`, `status`, `dam_id`) VALUES
(1, '১৯৯৮ ইং / ১৪০৫ বাংলা', 1998, '2021-06-13 23:14:00', '2021-06-13 23:53:03', 1, 1),
(2, '১৯৯৯ ইং / ১৪০৬ বাংলা', 1999, '2021-06-13 23:14:00', '2021-06-13 23:53:03', 1, 2),
(3, '২০১৬ ইং / ১৪২৩ বাংলা', 2016, '2021-06-13 23:14:00', '2021-06-13 23:53:03', 1, 3),
(4, '২০১৭ ইং / ১৪২৪ বাংলা', 2017, '2021-06-13 23:14:00', '2021-06-13 23:53:03', 1, 4),
(5, '২০১৮ ইং / ১৪২৫ বাংলা', 2018, '2021-06-13 23:14:00', '2021-06-13 23:53:03', 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `master_mapping_parameters`
--

CREATE TABLE `master_mapping_parameters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `month` int(11) NOT NULL,
  `gender` int(11) NOT NULL COMMENT '1 = Male, 2 = Female',
  `weight_range_from` double(5,2) NOT NULL,
  `weight_range_to` double(5,2) DEFAULT NULL,
  `height_range_from` double(5,2) NOT NULL,
  `height_range_to` double(5,2) DEFAULT NULL,
  `z_score` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 = active, 2 = inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_mapping_parameters`
--

INSERT INTO `master_mapping_parameters` (`id`, `month`, `gender`, `weight_range_from`, `weight_range_to`, `height_range_from`, `height_range_to`, `z_score`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(103, 1, 1, 1.00, 1.50, 2.00, 2.50, '1', 1, NULL, 1, '2021-05-02 04:23:12', '2021-05-05 00:12:23'),
(104, 1, 1, 1.60, 2.50, 2.60, 2.90, '2', 1, NULL, NULL, '2021-05-02 04:23:25', '2021-05-02 04:23:25'),
(105, 2, 1, 1.00, 1.50, 2.00, 2.50, '1', 1, 1, 1, '2021-05-02 05:07:08', '2021-05-02 05:07:08'),
(106, 2, 1, 3.00, 4.00, 5.00, 6.00, '4', 1, 1, 1, '2021-05-02 05:07:19', '2021-05-02 05:07:19'),
(107, 1, 2, 1.00, 1.50, 2.00, 2.50, '1', 1, 1, 1, '2021-05-02 23:22:34', '2021-05-02 23:22:34'),
(108, 1, 2, 2.00, 3.00, 4.00, 5.00, '2', 1, 1, 1, '2021-05-02 23:22:46', '2021-05-02 23:22:46'),
(112, 6, 2, 4.00, 4.00, 4.00, 4.00, '3', 1, 1, 1, '2021-05-05 00:14:57', '2021-05-05 01:35:44'),
(113, 6, 2, 5.00, 5.00, 5.00, 5.00, '3', 1, 1, 1, '2021-05-05 00:14:57', '2021-05-05 00:14:57'),
(114, 7, 2, 1.00, 1.50, 2.00, 2.50, '1', 1, 1, 1, '2021-05-05 01:22:36', '2021-05-05 01:22:36'),
(115, 7, 2, 2.00, 3.00, 3.00, 3.00, '2', 1, 1, 1, '2021-05-05 01:23:00', '2021-05-05 01:23:00'),
(116, 2, 2, 1.00, 1.50, 2.00, 2.50, '1', 1, 1, 1, '2021-05-25 05:59:14', '2021-05-25 05:59:14'),
(117, 2, 2, 4.00, 4.00, 4.00, 4.00, '2', 1, 1, 1, '2021-05-25 05:59:14', '2021-05-25 05:59:14'),
(118, 1, 1, 1.00, 1.50, 2.00, 2.50, '1', 1, 1, 1, '2021-05-30 23:07:49', '2021-05-30 23:07:49'),
(119, 1, 1, 1.00, 1.50, 2.00, 2.50, '3', 1, 1, 1, '2021-05-30 23:10:41', '2021-05-30 23:10:41'),
(120, 7, 1, 3.00, 8.00, 3.00, 7.00, '1', 1, 1, 1, '2021-05-30 23:28:20', '2021-05-30 23:28:20'),
(121, 1, 1, 1.00, 1.50, 2.00, 2.50, '9', 1, 1, 1, '2021-05-30 23:36:46', '2021-05-30 23:36:46'),
(122, 11, 1, 1.00, 1.50, 2.00, 2.50, '1', 1, 1, 1, '2021-05-31 00:10:16', '2021-05-31 00:10:16');

-- --------------------------------------------------------

--
-- Table structure for table `master_markets`
--

CREATE TABLE `master_markets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `division_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `upazila_id` int(11) DEFAULT NULL,
  `market_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `market_name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foundation_year` year(4) DEFAULT NULL,
  `village` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_office` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `union` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `market_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `govt_covered` bigint(20) UNSIGNED DEFAULT NULL,
  `govt_open` bigint(20) UNSIGNED DEFAULT NULL,
  `private_covered` bigint(20) UNSIGNED DEFAULT NULL,
  `private_open` bigint(20) UNSIGNED DEFAULT NULL,
  `total_market_area` bigint(20) UNSIGNED DEFAULT NULL,
  `shed_no` bigint(20) UNSIGNED DEFAULT NULL,
  `shed_area` bigint(20) UNSIGNED DEFAULT NULL,
  `stall_no_agri` bigint(20) UNSIGNED DEFAULT NULL,
  `stall_no_nonagri` bigint(20) UNSIGNED DEFAULT NULL,
  `hat_days` text COLLATE utf8_unicode_ci,
  `market_time_from` time DEFAULT NULL,
  `market_time_to` time DEFAULT NULL,
  `infrastructure_id` text COLLATE utf8_unicode_ci,
  `communication_linkage_id` text COLLATE utf8_unicode_ci,
  `number_of_buyers` tinyint(4) DEFAULT NULL,
  `number_of_sellers` tinyint(4) DEFAULT NULL,
  `farmer_share` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trader_share` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `other_product_destination` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_destination` text COLLATE utf8_unicode_ci,
  `vehicle_id` text COLLATE utf8_unicode_ci,
  `avg_distance` bigint(20) UNSIGNED DEFAULT NULL,
  `data_collection_year` year(4) DEFAULT NULL,
  `mobile_market_committee` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `market_representative_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `market_representative_mobile` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` decimal(10,2) DEFAULT NULL,
  `longitude` decimal(10,2) DEFAULT NULL,
  `approval_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=pending, 2=approved, 2=rejected',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `approved_date` date DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_markets`
--

INSERT INTO `master_markets` (`id`, `division_id`, `district_id`, `upazila_id`, `market_name`, `market_name_bn`, `foundation_year`, `village`, `post_office`, `union`, `market_type`, `govt_covered`, `govt_open`, `private_covered`, `private_open`, `total_market_area`, `shed_no`, `shed_area`, `stall_no_agri`, `stall_no_nonagri`, `hat_days`, `market_time_from`, `market_time_to`, `infrastructure_id`, `communication_linkage_id`, `number_of_buyers`, `number_of_sellers`, `farmer_share`, `trader_share`, `other_product_destination`, `product_destination`, `vehicle_id`, `avg_distance`, `data_collection_year`, `mobile_market_committee`, `market_representative_name`, `market_representative_mobile`, `latitude`, `longitude`, `approval_status`, `status`, `approved_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `dam_id`) VALUES
(1, 1, 1, 11, '1234', '1234', 1950, 'বড় বাজার', 'Comilla-3500', 'N/A', '2', 100, 45, 40, 30, 0, 4, 400, 400, 300, '[1,2,3]', '07:00:00', '20:00:00', '[1,2,3]', '[2,3,10]', 100, 127, '15', '75', '', '[3]', '[1,3,4,5]', 100, 2020, '01711378181', '1234', '01711567899', '100.00', '100.00', 1, 1, NULL, NULL, NULL, '2021-06-22 05:16:43', '2021-06-23 02:45:33', 0),
(2, 1, 1, 11, 'Market name', 'বাজারের নাম (বাংলা) ', 2005, 'Dolores voluptatibus', 'Est dolor aperiam qu', 'Impedit non quod re', '1', 89, 15, 22, 74, 0, 32, 59, 76, 66, '[1,3,5,6,7]', '23:45:00', '07:46:00', '[1,2,4,7,8,9]', '[1,5,8]', 107, 127, '34', '44', '', '[3,1,2,null]', '[1,2]', 41, 2025, '+1 (294) 768-7575', 'Gray Walls', '91642215211', '0.00', '0.00', 1, 1, NULL, NULL, NULL, '2021-06-22 05:59:43', '2021-06-24 01:45:17', 0),
(3, 1, 1, 11, 'sunny', 'sunny', 2002, '2', '23', 'Impedit non quod re', '2', 12, 2, 3, 2, 0, 5, 5, 4, 5, '[3,4,5]', '22:25:00', '16:30:00', '[4,5]', '[4,5]', 12, 4, '11', '12', '', '[1]', '[1,2]', 10, 2020, '10', '1234567', '01924496004', '20.00', '30.00', 1, 1, NULL, NULL, NULL, '2021-06-23 04:26:59', '2021-06-23 04:26:59', 0),
(4, 1, 1, 11, 'ismail market', 'ismail market bn', 2021, 'konpara', 'konapra', 'demra', '4', 10, 20, 30, 40, 0, 50, 60, 40, 80, '[2,3,4]', '03:36:00', '03:36:00', '[2,3,4]', '[4]', 12, 3, '10', '13', '', '[3,1]', '[2]', 30, 2020, '01711378181', 'ismail', '01924496004', '0.00', '0.00', 1, 1, NULL, NULL, NULL, '2021-06-23 22:32:35', '2021-06-23 22:32:35', 0),
(5, 6, 45, 355, 'Austagram Boro Bazar', 'অষ্টগ্রাম বড় বাজার', 1980, 'Austagram', 'Austagram', 'Austagram Sador', '5', 70, 30, 30, 0, 0, 3456, 45678, 300, 200, '[1]', '06:30:00', '22:30:00', '[1,2,3,8]', '[1,2,3,4,6,9,8,10]', 127, 127, '50', '50', '', '[1,null]', '[1,2,4,5]', 12, 2020, '01711121212', 'Jahid Hasan', '01671404511', '24.28', '24.28', 1, 1, NULL, NULL, NULL, '2021-06-25 23:50:01', '2021-06-25 23:50:01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_market_directory_commodities`
--

CREATE TABLE `master_market_directory_commodities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `market_directory_id` bigint(20) UNSIGNED DEFAULT NULL,
  `commodity` text COLLATE utf8_unicode_ci,
  `unit` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_market_directory_commodities`
--

INSERT INTO `master_market_directory_commodities` (`id`, `market_directory_id`, `commodity`, `unit`, `quantity`, `created_at`, `updated_at`, `dam_id`) VALUES
(5, 1, '20', '1', 100, '2021-06-23 02:45:33', '2021-06-23 02:45:33', NULL),
(6, 1, '21', '1', 25000, '2021-06-23 02:45:33', '2021-06-23 02:45:33', NULL),
(10, 3, '20', '1', 4, '2021-06-23 06:26:24', '2021-06-23 06:26:24', NULL),
(11, 3, '20', '3', 200, '2021-06-23 06:26:24', '2021-06-23 06:26:24', NULL),
(12, 4, '20', '1', 10, '2021-06-23 22:32:35', '2021-06-23 22:32:35', NULL),
(13, 2, '21', '1', 829, '2021-06-24 01:45:17', '2021-06-24 01:45:17', NULL),
(14, 5, '20', '4', 1000, '2021-06-25 23:50:01', '2021-06-25 23:50:01', NULL),
(15, 5, '21', '5', 2000, '2021-06-25 23:50:01', '2021-06-25 23:50:01', NULL),
(16, 5, '23', '5', 3000, '2021-06-25 23:50:01', '2021-06-25 23:50:01', NULL),
(17, 5, '25', '5', 1000, '2021-06-25 23:50:01', '2021-06-25 23:50:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_market_directory_lease_value_years`
--

CREATE TABLE `master_market_directory_lease_value_years` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `market_directory_id` bigint(20) UNSIGNED DEFAULT NULL,
  `lease_year_id` bigint(20) UNSIGNED DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_market_directory_lease_value_years`
--

INSERT INTO `master_market_directory_lease_value_years` (`id`, `market_directory_id`, `lease_year_id`, `value`, `created_at`, `updated_at`, `dam_id`) VALUES
(19, 1, 1, 500, '2021-06-23 02:45:33', '2021-06-23 02:45:33', NULL),
(20, 1, 2, 500, '2021-06-23 02:45:33', '2021-06-23 02:45:33', NULL),
(21, 1, 3, 500, '2021-06-23 02:45:33', '2021-06-23 02:45:33', NULL),
(22, 1, 4, 500, '2021-06-23 02:45:33', '2021-06-23 02:45:33', NULL),
(23, 1, 5, 500, '2021-06-23 02:45:33', '2021-06-23 02:45:33', NULL),
(24, 1, 6, 500, '2021-06-23 02:45:33', '2021-06-23 02:45:33', NULL),
(42, 3, 1, 4, '2021-06-23 06:26:24', '2021-06-23 06:26:24', NULL),
(43, 3, 2, 44, '2021-06-23 06:26:24', '2021-06-23 06:26:24', NULL),
(44, 3, 3, 4, '2021-06-23 06:26:24', '2021-06-23 06:26:24', NULL),
(45, 3, 4, 4, '2021-06-23 06:26:24', '2021-06-23 06:26:24', NULL),
(46, 3, 5, 4, '2021-06-23 06:26:24', '2021-06-23 06:26:24', NULL),
(47, 4, 1, 10, '2021-06-23 22:32:35', '2021-06-23 22:32:35', NULL),
(48, 4, 2, 20, '2021-06-23 22:32:36', '2021-06-23 22:32:36', NULL),
(49, 4, 3, 30, '2021-06-23 22:32:36', '2021-06-23 22:32:36', NULL),
(50, 4, 4, 40, '2021-06-23 22:32:36', '2021-06-23 22:32:36', NULL),
(51, 4, 5, 50, '2021-06-23 22:32:36', '2021-06-23 22:32:36', NULL),
(52, 2, 1, 0, '2021-06-24 01:45:17', '2021-06-24 01:45:17', NULL),
(53, 2, 2, 0, '2021-06-24 01:45:17', '2021-06-24 01:45:17', NULL),
(54, 2, 3, 0, '2021-06-24 01:45:17', '2021-06-24 01:45:17', NULL),
(55, 2, 4, 0, '2021-06-24 01:45:17', '2021-06-24 01:45:17', NULL),
(56, 2, 5, 0, '2021-06-24 01:45:17', '2021-06-24 01:45:17', NULL),
(57, 2, 6, 0, '2021-06-24 01:45:17', '2021-06-24 01:45:17', NULL),
(58, 5, 1, 2, '2021-06-25 23:50:01', '2021-06-25 23:50:01', NULL),
(59, 5, 2, 2, '2021-06-25 23:50:01', '2021-06-25 23:50:01', NULL),
(60, 5, 3, 2, '2021-06-25 23:50:01', '2021-06-25 23:50:01', NULL),
(61, 5, 4, 2, '2021-06-25 23:50:01', '2021-06-25 23:50:01', NULL),
(62, 5, 5, 2, '2021-06-25 23:50:01', '2021-06-25 23:50:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_measurement_units`
--

CREATE TABLE `master_measurement_units` (
  `id` int(11) NOT NULL,
  `unit_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_measurement_units`
--

INSERT INTO `master_measurement_units` (`id`, `unit_name`, `unit_name_bn`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `dam_id`) VALUES
(1, 'Quintal', 'কুইন্টাল', 1, 0, 0, '2021-06-15 04:49:55', '2021-06-15 04:49:55', 6),
(2, 'gm', 'গ্রাম', 1, 0, 0, '2021-06-15 04:49:55', '2021-06-15 04:49:55', 5),
(3, 'Milligram', 'মিলিগ্রাম', 1, 0, 0, '2021-06-15 04:49:56', '2021-06-15 04:49:56', 4),
(4, 'Liter', 'লিটার', 1, 0, 0, '2021-06-15 04:49:56', '2021-06-15 04:49:56', 3),
(5, 'kg', 'কেজি', 1, 0, 0, '2021-06-15 04:49:56', '2021-06-15 04:49:56', 1),
(6, 'Bag', 'থলে', 1, 0, 1, '2021-06-15 04:49:56', '2021-06-25 23:31:24', 2);

-- --------------------------------------------------------

--
-- Table structure for table `master_price_types`
--

CREATE TABLE `master_price_types` (
  `id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `type_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_name_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_price_types`
--

INSERT INTO `master_price_types` (`id`, `org_id`, `type_name`, `type_name_bn`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `dam_id`) VALUES
(1, 13, 'Wholesale', 'পাইকারি', 1, 1, NULL, '2021-04-06 22:00:00', NULL, NULL),
(2, 13, 'Retail', 'খুচরা', 1, 1, NULL, '2021-04-08 00:20:41', NULL, NULL),
(3, 13, 'Growers', 'প্রযোজক', 1, NULL, 0, '2021-06-12 04:53:41', '2021-06-13 06:23:37', 3),
(7, 16, 'Whole Sales', 'খুচরা ', 1, 1, 1, '2021-06-25 23:54:02', '2021-06-25 23:54:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_regions`
--

CREATE TABLE `master_regions` (
  `id` int(11) NOT NULL,
  `org_id` int(11) DEFAULT NULL,
  `region_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_regions`
--

INSERT INTO `master_regions` (`id`, `org_id`, `region_name`, `region_name_bn`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'Sylhet Region', 'সিলেট অঞ্চল', 1, 1, 1, '2021-03-31 18:04:55', '2021-04-06 18:18:45'),
(2, 3, 'Barishal', 'বরিশাল', 1, 1, 1, '2021-03-31 18:13:27', '2021-04-06 18:19:35'),
(3, 5, 'Mymensing Region', 'ময়মনসিংহ অঞ্চল', 1, 1, 1, '2021-03-31 18:14:07', '2021-05-31 00:18:59'),
(4, 3, 'Rajshahi Region', 'রাজশাহী অঞ্চল', 1, 1, 1, '2021-03-31 18:15:53', '2021-04-06 01:39:26'),
(5, 3, 'Chattogram Region', 'চট্টগ্রাম অঞ্চল', 1, 1, 1, '2021-03-31 18:17:01', '2021-04-06 01:39:14'),
(6, 13, 'test region 1', 'test region 1 bn', 2, NULL, NULL, '2021-03-31 19:02:09', '2021-05-31 01:05:09'),
(7, 3, 'region name 111', 'region_name bn 111', 1, NULL, NULL, '2021-03-31 19:02:31', '2021-03-31 19:02:31'),
(8, 5, 'Jashore Region', 'যশোর অঞ্চল', 1, NULL, 1, '2021-03-31 23:08:11', '2021-04-06 01:55:00'),
(9, 5, 'Khulna Region', 'খুলনা অঞ্চল', 1, 1, 1, '2021-04-06 01:37:31', '2021-04-06 01:38:58'),
(10, 5, 'Rangpur Region', 'রংপুর অঞ্চল', 1, 1, 1, '2021-04-06 01:38:33', '2021-04-06 01:38:33'),
(11, 3, 'One', 'এক', 2, 1, 1, '2021-04-11 22:24:59', '2021-06-03 03:40:42'),
(13, NULL, 'Dhaka Region', 'ঢাকা অঞ্চল ', 1, 1, 1, '2021-05-04 22:27:29', '2021-05-04 22:27:29'),
(14, NULL, 'QA Test Region', 'QA Test Region Bn', 1, 1, 1, '2021-05-31 00:13:33', '2021-05-31 00:14:33'),
(15, NULL, 'Tangail Region', 'Tangail  Region Bn', 2, 1, 1, '2021-05-31 00:16:24', '2021-05-31 01:05:42');

-- --------------------------------------------------------

--
-- Table structure for table `master_report_headers`
--

CREATE TABLE `master_report_headers` (
  `id` int(11) NOT NULL,
  `heading` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `heading_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `left_logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `right_logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `org_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0=active, 1=inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_report_headers`
--

INSERT INTO `master_report_headers` (`id`, `heading`, `heading_bn`, `left_logo`, `right_logo`, `address`, `address_bn`, `org_id`, `created_by`, `updated_by`, `status`, `created_at`, `updated_at`) VALUES
(3, 'secound', 'secound bn', '1617776471.jpg', '1617775830.png', 'test 2', 'test 2', 3, 1, 1, 0, '2021-04-06 18:10:30', '2021-05-04 06:46:24'),
(4, 'Man is mortal', 'মানুষ মরণশীল', '1617875956.jpg', '1617875956.png', 'khilkhet', 'খিলক্ষেত', 5, 1, 1, 0, '2021-04-08 03:59:16', '2021-04-08 03:59:16'),
(5, 'report heading', 'report heading bn', '1619607490.png', '1619607490.png', 'Dhaka', 'dhaka bn', 13, 1, 1, 0, '2021-04-28 04:58:10', '2021-04-28 04:58:10');

-- --------------------------------------------------------

--
-- Table structure for table `master_seasons`
--

CREATE TABLE `master_seasons` (
  `id` int(11) NOT NULL,
  `org_id` int(11) DEFAULT NULL,
  `season_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `season_name_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_seasons`
--

INSERT INTO `master_seasons` (`id`, `org_id`, `season_name`, `season_name_bn`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 3, 'Summer', 'গ্রীষ্ম', 1, NULL, 1, NULL, '2021-05-31 03:45:00'),
(2, 3, 'Fall', 'বর্ষা', 1, NULL, NULL, NULL, NULL),
(3, 5, 'Winter Season', 'শীত', 1, NULL, 1, NULL, '2021-05-04 03:26:09'),
(4, 3, 'summer En', 'semmer bn', 2, 1, 1, '2021-04-05 21:29:18', '2021-06-01 01:05:08'),
(5, NULL, 'QA Test Season', 'QA Test Season Bn', 1, 1, 1, '2021-05-31 03:41:54', '2021-05-31 03:42:16');

-- --------------------------------------------------------

--
-- Table structure for table `master_units`
--

CREATE TABLE `master_units` (
  `id` int(11) NOT NULL,
  `org_id` int(11) DEFAULT NULL,
  `region_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `upazilla_id` int(11) DEFAULT NULL,
  `unit_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_units`
--

INSERT INTO `master_units` (`id`, `org_id`, `region_id`, `zone_id`, `district_id`, `upazilla_id`, `unit_name`, `unit_name_bn`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `dam_id`) VALUES
(1, NULL, 1, 18, 1, 1, 'Kg', 'kg bn', 1, 1, 1, '2021-06-20 00:14:42', '2021-06-20 00:14:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_vehicles`
--

CREATE TABLE `master_vehicles` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_vehicles`
--

INSERT INTO `master_vehicles` (`id`, `name`, `name_bn`, `created_by`, `updated_by`, `status`, `created_at`, `updated_at`, `dam_id`) VALUES
(1, 'AutoRickshaw', 'AutoRickshaw', 0, 0, 1, '2021-06-14 05:15:12', '2021-06-14 05:15:12', 1),
(2, 'Boat', 'Boat', 0, 0, 1, '2021-06-14 05:15:12', '2021-06-14 05:15:12', 2),
(3, 'Cart', 'Cart', 0, 0, 1, '2021-06-14 05:15:12', '2021-06-14 05:15:12', 3),
(4, 'CNG', 'CNG', 0, 0, 1, '2021-06-14 05:15:12', '2021-06-14 05:15:12', 4),
(5, 'Cycle', 'Cycle', 0, 0, 1, '2021-06-14 05:15:12', '2021-06-14 05:15:12', 5);

-- --------------------------------------------------------

--
-- Table structure for table `master_weeks`
--

CREATE TABLE `master_weeks` (
  `id` int(11) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `week` int(11) DEFAULT NULL,
  `month` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `starting_date` date DEFAULT NULL,
  `ending_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dam_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_weeks`
--

INSERT INTO `master_weeks` (`id`, `year`, `week`, `month`, `starting_date`, `ending_date`, `created_at`, `updated_at`, `dam_id`) VALUES
(1, 2020, 1, 'November', '2020-11-01', '2020-11-07', '2021-06-14 05:15:48', '2021-06-14 05:15:48', 1),
(2, 2020, 2, 'November', '2020-11-08', '2020-11-14', '2021-06-14 05:15:48', '2021-06-14 05:15:48', 2),
(5, 2020, 3, 'November', '2020-11-15', '2020-11-21', '2021-06-14 05:15:48', '2021-06-14 05:15:48', 5),
(7, 2020, 4, 'Novembers', '2020-11-22', '2020-11-28', '2021-06-14 05:15:48', '2021-06-15 04:20:03', 7);

-- --------------------------------------------------------

--
-- Table structure for table `master_zones`
--

CREATE TABLE `master_zones` (
  `id` int(11) NOT NULL,
  `org_id` int(11) DEFAULT NULL,
  `zone_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zone_name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_zones`
--

INSERT INTO `master_zones` (`id`, `org_id`, `zone_name`, `zone_name_bn`, `region_id`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(17, 3, 'Rajshahi Zone', 'রাজশাহী জোন', 4, 1, 1, 1, '2021-03-31 22:15:40', '2021-05-31 01:03:52'),
(18, 1, 'Sylhet Zone', 'সিলেট জোন', 1, 1, 1, 1, '2021-03-31 22:15:54', '2021-04-06 01:46:34'),
(19, 1, 'Moulvibazar Zone', 'মৌলভীবাজার জোন', 1, 1, 1, 1, '2021-03-31 22:42:22', '2021-04-06 01:47:01'),
(20, 3, 'Natore Zone', 'নাটোর জোন', 4, 1, 1, 1, '2021-03-31 23:35:03', '2021-05-31 02:39:53'),
(21, 3, 'Chattogram Zone', 'চট্টগ্রাম জোন', 5, 1, 1, 1, '2021-04-03 16:10:44', '2021-04-06 01:40:19'),
(22, 3, 'Pabna Zone', 'পাবনা জোন', 4, 1, 1, 1, '2021-04-03 17:40:32', '2021-04-06 01:52:51'),
(23, 5, 'Rangpur Zone', 'রংপুর জোন', 10, 1, 1, 1, '2021-04-03 17:40:43', '2021-04-06 01:52:00'),
(24, 1, 'Habiganj Zone', 'হাবিগঞ্জ জোন', 1, 1, 1, 1, '2021-04-03 17:40:52', '2021-04-06 01:51:24'),
(25, 5, 'Khulna Zone', 'খুলনা জোন', 9, 1, 1, 1, '2021-04-03 17:41:07', '2021-04-06 01:50:44'),
(26, 3, 'Bandarban Zone', 'বান্দরবান জোন', 5, 1, 1, 1, '2021-04-03 17:41:15', '2021-04-06 01:41:02'),
(27, 3, 'Bhola Zone', 'ভোলা', 2, 1, 1, 1, '2021-04-06 16:38:10', '2021-04-06 16:38:10'),
(29, NULL, 'Kishoregonj Zone', 'Kishoregonj Zone', 13, 1, 1, 1, '2021-05-04 22:28:11', '2021-05-04 22:28:11'),
(30, NULL, 'QA Test Zone', 'QA Test Zone Bn', 14, 1, 1, 1, '2021-05-31 01:21:57', '2021-05-31 01:24:56');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2021_03_31_072715_create_master_regions_table', 1),
(2, '2021_03_31_072836_create_master_zones_table', 1),
(3, '2021_03_31_073021_create_master_units_table', 1),
(4, '2021_03_31_073231_create_master_cotton_varities_table', 1),
(5, '2021_03_31_073941_create_master_cotton_names_table', 1),
(6, '2021_03_31_074209_create_master_hatts_table', 1),
(7, '2021_03_31_091735_create_ginner_grower_profiles_table', 1),
(8, '2021_03_31_091736_create_master_seasons_table', 1),
(9, '2021_03_31_093313_create_grower_prod_achievements_table', 1),
(10, '2021_04_01_054521_create_grower_central_stocks_table', 1),
(11, '2021_04_01_060348_create_ginner_schedules_table', 1),
(12, '2021_04_01_060831_create_user_logs_table', 1),
(14, '2021_04_05_035330_create_grower_sell_entry_table', 1),
(15, '2021_04_05_084702_create_master_commodity_groups_table', 1),
(16, '2021_04_05_084834_create_master_commodity_sub_groups_table', 1),
(17, '2021_04_05_085354_create_master_price_types_table', 1),
(18, '2021_04_05_085355_create_master_commodity_names_table', 1),
(19, '2021_04_07_035553_create_master_report_headers_table', 1),
(20, '2021_04_08_143327_create_hatt_notifications_table', 1),
(21, '2021_04_01_061041_create_grower_hatt_manages_table', 2),
(24, '2021_04_12_040223_create_master_divisional_offices_table', 3),
(28, '2021_04_12_082502_create_master_mapping_parameters_table', 5),
(32, '2021_04_12_075631_create_pusti_campaign_events_table', 7),
(33, '2021_04_13_040436_create_pusti_campaign_events_details_table', 7),
(34, '2021_04_12_080805_create_pusti_campaign_schedules_table', 8),
(36, '2021_04_12_080830_create_pusti_campaign_requests_table', 9),
(37, '2021_04_14_060304_create_pusti_campaign_calendars_table', 10),
(40, '2021_04_14_061634_create_pusti_campaign_attendances_table', 10),
(41, '2021_04_14_062226_create_pusti_campaign_feedbacks_table', 10),
(42, '2021_04_16_095557_create_pm_socio_economic_information_table', 11),
(43, '2021_04_16_095737_create_pm_physical_measurement_table', 12),
(44, '2021_04_16_095823_create_pm_health_measurement_table', 12),
(47, '2021_04_14_060732_create_pusti_campaign_materials_table', 13),
(49, '2021_04_12_075307_create_master_campaigns_table', 14),
(51, '2021_04_14_060947_create_pusti_campaign_mat_details_table', 15),
(52, '2021_04_18_120034_create_campaign_information_table', 16),
(53, '2021_04_18_125748_create_pusti_campaign_informations_table', 17),
(54, '2021_04_21_062252_create_master_measurement_units_table', 18),
(55, '2021_04_21_062710_create_master_commodity_types_table', 19),
(56, '2021_04_21_062954_create_linkage_gro_buy_profiles_table', 20),
(58, '2021_04_21_101724_create_cpi_price_collector_profiles_table', 22),
(60, '2021_04_22_091227_create_cpi_price_infos_table', 23),
(61, '2021_04_24_083443_create_master_alert_percentages_table', 24),
(62, '2021_04_21_063013_create_linkage_gro_buy_pro_details_table', 25),
(68, '2021_06_13_051630_create_communication_linkages_table', 27),
(69, '2021_06_13_052516_create_destination_of_produces_table', 27),
(70, '2021_06_13_053325_create_master_destination_of_produces_table', 28),
(71, '2021_06_13_053409_create_master_communication_linkages_table', 28),
(73, '2021_06_13_053633_create_cpi_district_daily_price_averages_table', 29),
(75, '2021_06_13_054104_create_district_growers_price_averages_table', 30),
(76, '2021_06_13_054324_create_district_weekly_price_averages_table', 31),
(77, '2021_06_13_055100_create_division_wise_daily_prices_table', 32),
(78, '2021_06_13_055328_create_master_infrastructures_table', 33),
(79, '2021_06_13_055634_create_master_lease_value_years_table', 34),
(81, '2021_06_13_055919_create_market_commodity_growers_prices_table', 35),
(82, '2021_06_13_060514_create_market_commodity_growers_price_details_table', 36),
(83, '2021_06_13_061350_create_market_commodity_prices_table', 37),
(84, '2021_06_13_061358_create_market_commodity_price_details_table', 37),
(85, '2021_06_13_062328_create_market_commodity_weekly_prices_table', 38),
(86, '2021_06_13_062334_create_market_commodity_weekly_price_details_table', 38),
(88, '2021_04_22_084130_create_master_markets_table', 39),
(90, '2021_06_13_075250_create_market_market_directory_lease_value_years_table', 41),
(96, '2021_06_13_081919_create_master_vehicles', 45),
(97, '2021_06_13_102950_create_master_weeks_table', 46),
(98, '2021_06_13_081148_create_market_national_daily_price_a_v_g_s_table', 47),
(99, '2021_06_13_080910_create_market_market_weekly_price_a_v_g_calculations_table', 48),
(100, '2021_06_13_073328_create_market_directory_commodities_table', 49),
(101, '2021_06_14_102622_create_dam_districts_table', 50),
(102, '2021_06_14_102824_create_dam_divisions_table', 50),
(103, '2021_06_14_102838_create_dam_upazilas_table', 50);

-- --------------------------------------------------------

--
-- Table structure for table `pm_health_measurement`
--

CREATE TABLE `pm_health_measurement` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `socio_economic_info_id` bigint(20) UNSIGNED NOT NULL,
  `question_no_1` tinyint(4) NOT NULL COMMENT '12 - 15 years = 1, 16 - 19 years = 2, 20 -23 years = 3, 24 - 27 years = 4, 28 - 31 years = 5, 32 - 35 years = 6, >36 years = 7',
  `question_no_2` tinyint(4) NOT NULL COMMENT '1 = 1, 2 = 2, 3 = 3, 4 = 4, 5 = 5, 6 = 6',
  `question_no_3` tinyint(4) NOT NULL,
  `question_no_4` tinyint(4) NOT NULL COMMENT '<2 years = 1, 2 - 3 years = 2, 3 - 4 years = 3, >4 years = 4',
  `question_no_5` tinyint(4) NOT NULL COMMENT '2.5 kv = 1, 2.5kg = 2',
  `question_no_6` tinyint(4) NOT NULL COMMENT '1 = Yes, 2 = No',
  `question_no_7` tinyint(4) DEFAULT NULL COMMENT 'Superstition = 1, Illness = 2, Others = 3',
  `question_no_8` tinyint(4) NOT NULL COMMENT '1 = Yes, 2 = No',
  `question_no_9` tinyint(4) NOT NULL COMMENT '1 = Yes, 2 = No',
  `question_no_10` tinyint(4) NOT NULL COMMENT '1 = Yes, 2 = No',
  `question_no_11` tinyint(4) NOT NULL COMMENT ' 1 Items = 1, 2 Items = 2, 3 Items = 3, 4 Items = 4, 5 Items = 5, 6 Items = 6',
  `question_no_12` tinyint(4) NOT NULL COMMENT '1 = Yes, 2 = No',
  `question_no_13` tinyint(4) NOT NULL COMMENT '1 = Yes, 2 = No',
  `question_no_14` tinyint(4) NOT NULL COMMENT '1 = Yes, 2 = No',
  `question_no_15` tinyint(4) NOT NULL COMMENT '1 = Yes, 2 = No',
  `question_no_16` tinyint(4) NOT NULL COMMENT '1 = Yes, 2 = No',
  `question_no_17` tinyint(4) NOT NULL COMMENT '1 = Yes, 2 = No',
  `question_no_18` tinyint(4) NOT NULL COMMENT '1 = Yes, 2 = No',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pm_health_measurement`
--

INSERT INTO `pm_health_measurement` (`id`, `socio_economic_info_id`, `question_no_1`, `question_no_2`, `question_no_3`, `question_no_4`, `question_no_5`, `question_no_6`, `question_no_7`, `question_no_8`, `question_no_9`, `question_no_10`, `question_no_11`, `question_no_12`, `question_no_13`, `question_no_14`, `question_no_15`, `question_no_16`, `question_no_17`, `question_no_18`, `created_at`, `updated_at`) VALUES
(62, 54, 2, 2, 2, 1, 1, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 2, 1, 1, '2021-05-23 04:30:57', '2021-05-23 04:30:57'),
(71, 56, 3, 2, 3, 1, 1, 2, 3, 1, 1, 1, 2, 2, 1, 1, 2, 2, 1, 1, '2021-05-23 05:14:06', '2021-05-23 05:14:06'),
(77, 59, 4, 1, 1, 1, 1, 1, 3, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, '2021-05-31 00:26:20', '2021-05-31 00:26:20'),
(78, 58, 1, 2, 1, 1, 1, 1, 3, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, '2021-05-31 00:30:30', '2021-05-31 00:30:30'),
(79, 60, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1, 2, 2, 1, 1, 1, 2, '2021-05-31 00:37:20', '2021-05-31 00:37:20'),
(80, 57, 2, 2, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2021-05-31 00:40:58', '2021-05-31 00:40:58'),
(81, 61, 2, 2, 2, 2, 1, 2, 1, 1, 1, 1, 3, 2, 2, 1, 1, 1, 1, 1, '2021-06-07 06:27:32', '2021-06-07 06:27:32'),
(82, 55, 1, 1, 1, 1, 1, 1, 2, 2, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, '2021-06-18 23:18:39', '2021-06-18 23:18:39'),
(83, 53, 1, 2, 1, 1, 1, 2, 3, 1, 1, 1, 2, 2, 1, 1, 2, 1, 2, 1, '2021-06-18 23:46:23', '2021-06-18 23:46:23');

-- --------------------------------------------------------

--
-- Table structure for table `pm_physical_measurement`
--

CREATE TABLE `pm_physical_measurement` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `socio_economic_info_id` bigint(20) UNSIGNED NOT NULL,
  `height` double(5,2) NOT NULL COMMENT 'Unit is inch',
  `weight` double(5,2) NOT NULL COMMENT 'Unit is Kg',
  `head_circumference` double(5,2) NOT NULL COMMENT 'Unit is cm',
  `chest_circumference` double(5,2) NOT NULL COMMENT 'Unit is cm',
  `muac` tinyint(4) NOT NULL COMMENT 'SAM (11.5cm) = 1, MAM (11.5 - 12.5cm) = 2, Normal (>12.5cm) = 3',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pm_physical_measurement`
--

INSERT INTO `pm_physical_measurement` (`id`, `socio_economic_info_id`, `height`, `weight`, `head_circumference`, `chest_circumference`, `muac`, `created_at`, `updated_at`) VALUES
(66, 54, 17.00, 20.00, 15.00, 36.00, 2, '2021-05-23 04:30:35', '2021-05-23 04:30:35'),
(75, 56, 17.00, 20.00, 15.00, 36.00, 1, '2021-05-23 05:14:04', '2021-05-23 05:14:04'),
(81, 59, 10.00, 25.00, 15.00, 36.00, 1, '2021-05-31 00:26:16', '2021-05-31 00:26:16'),
(82, 58, 10.00, 65.00, 15.00, 36.00, 1, '2021-05-31 00:30:07', '2021-05-31 00:30:07'),
(83, 60, 25.00, 65.00, 15.00, 36.00, 2, '2021-05-31 00:37:00', '2021-05-31 00:37:00'),
(84, 57, 100.00, 1.20, 11.00, 11.00, 1, '2021-05-31 00:40:55', '2021-05-31 00:40:55'),
(85, 61, 255.00, 10.00, 15.00, 36.00, 2, '2021-06-07 06:27:08', '2021-06-07 06:27:08'),
(86, 55, 30.00, 65.00, 15.00, 36.00, 1, '2021-06-18 23:18:36', '2021-06-18 23:18:36'),
(87, 53, 10.00, 15.00, 15.00, 36.00, 1, '2021-06-18 23:46:15', '2021-06-18 23:46:15');

-- --------------------------------------------------------

--
-- Table structure for table `pm_socio_economic_info`
--

CREATE TABLE `pm_socio_economic_info` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pusti_mapping_id` int(11) NOT NULL,
  `pusti_mapping_date` date NOT NULL,
  `name_en` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `father_name_en` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `father_name_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mother_name_en` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mother_name_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `age` date NOT NULL COMMENT 'Date of birth will be taken as input',
  `gender` tinyint(4) NOT NULL COMMENT '1 = Boy, 2 = Girl',
  `father_education_status` tinyint(4) NOT NULL COMMENT '1 = Never Goes to School, 2 = Primary, 3 = Secondary or Hiegher Secondary , 4 = Honours',
  `mother_education_status` tinyint(4) NOT NULL COMMENT '1 = Never Goes to School, 2 = Primary, 3 = Secondary/Hiegher Secondary , 4 = Honours',
  `earning_member` tinyint(4) NOT NULL COMMENT '1 = Father, 2 = Mother, 3 = Father/Mother , 4 = Others',
  `monthly_family_income` tinyint(4) NOT NULL COMMENT '1 = <5000, 2 = 5000 - 10000, 3 = 10000 - 20000, 4 = >20000',
  `income_source` tinyint(4) NOT NULL COMMENT '1 = Business/Hervest, 2 = Service, 3 = Teacher, 4 = Others',
  `family_category` tinyint(4) NOT NULL COMMENT '1 = Single, 2 = Join',
  `no_of_family_member` tinyint(4) NOT NULL,
  `religion` tinyint(4) NOT NULL COMMENT '1 = Muslim, 2 = Hindu, 3 = Christian, 4  = Buddhist',
  `area_type_id` tinyint(4) NOT NULL,
  `division_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `city_corporation_id` tinyint(4) DEFAULT NULL,
  `ward_id` int(11) DEFAULT NULL,
  `pauroshoba_id` int(11) DEFAULT NULL,
  `upazila_id` int(11) DEFAULT NULL,
  `union_id` int(11) DEFAULT NULL,
  `village_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `village_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 = active, 2 = inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pm_socio_economic_info`
--

INSERT INTO `pm_socio_economic_info` (`id`, `pusti_mapping_id`, `pusti_mapping_date`, `name_en`, `name_bn`, `father_name_en`, `father_name_bn`, `mother_name_en`, `mother_name_bn`, `age`, `gender`, `father_education_status`, `mother_education_status`, `earning_member`, `monthly_family_income`, `income_source`, `family_category`, `no_of_family_member`, `religion`, `area_type_id`, `division_id`, `district_id`, `city_corporation_id`, `ward_id`, `pauroshoba_id`, `upazila_id`, `union_id`, `village_en`, `village_bn`, `mobile_no`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(53, 101, '2021-05-23', 'Sheikh Md Sabbir Hossain', 'Sheikh Md Sabbir Hossain', 'Saiful Islam', 'সাইফুল', 'Laky', 'নিলুফা', '2021-01-17', 1, 4, 4, 1, 4, 4, 1, 3, 1, 1, 6, 52, 4, 20, 0, 0, 0, 'Thana Road', 'থানা রাস্তা', '01234567891', 1, 1, 1, '2021-05-23 04:05:21', '2021-05-27 06:11:11'),
(54, 102, '2021-05-23', 'Nasim Ahmed', 'Nasim Ahmed', 'kaif ahmed', 'আলিম সরকার', 'Mitali Sarkar', 'মিতালী সরকার', '2021-02-05', 2, 4, 4, 1, 4, 1, 1, 4, 1, 1, 6, 47, 1, 20, 0, 0, 0, 'Thana', 'থানা রাস্তা', '01234567890', 1, 1, 1, '2021-05-23 04:30:25', '2021-05-23 04:30:25'),
(55, 103, '2021-05-23', 'Sajid', 'Sajid', 'Demo', 'সাব্বির হোসেন', 'Mitali Sarkar', 'নিলুফা', '2019-01-23', 1, 4, 4, 1, 4, 1, 1, 3, 1, 3, 6, 47, 0, 26, 0, 369, 3332, 'Thana', 'থানা রাস্তা', '01234567890', 1, 1, 1, '2021-05-23 04:32:43', '2021-05-23 05:15:56'),
(56, 104, '2021-05-23', 'Md Riyaj Uddin', 'Md Riyaj Uddin', 'kaif ahmed', 'কাইফ আহমদ', 'Laky', 'জেমি', '2020-01-14', 1, 4, 4, 1, 4, 3, 1, 2, 1, 1, 1, 1, 2, 38, 0, 0, 0, 'Thana Road', 'থানা রাস্তা', '01234567890', 1, 1, 1, '2021-05-23 04:47:48', '2021-05-23 05:14:10'),
(57, 212, '2021-05-31', 'Rabeya', 'রাবেয়া', 'Rohim Uddin', 'রহিম উদ্দিন', 'Rohima Begum', 'রহিমা  বেগম', '2021-04-01', 2, 2, 2, 1, 3, 1, 1, 4, 1, 3, 6, 45, 0, 0, 0, 355, 3186, 'Austagram', 'Austagram', '01671404511', 1, 1, 1, '2021-05-30 23:30:23', '2021-05-30 23:33:50'),
(58, 187, '2021-05-31', 'bdtask', 'bdtask', 'Saiful Islam', 'সাব্বির হোসেন', 'Jame', 'জেমি', '2020-01-08', 1, 4, 3, 1, 4, 1, 1, 2, 1, 1, 1, 1, 2, 39, 0, 0, 0, 'Thana Road', '', '12345678912', 1, 1, 1, '2021-05-30 23:58:26', '2021-05-31 00:29:56'),
(59, 188, '2021-05-31', 'Yousuf Sarkar', 'Yousuf Sarkar', 'kaif ahmed', 'সাইফুল', 'Laky', 'নিলুফা', '2020-02-12', 1, 4, 3, 3, 3, 2, 1, 3, 1, 1, 1, 1, 2, 38, 0, 77, 0, 'Thana Road', 'থানা রাস্তা', '01234545678', 1, 1, 1, '2021-05-31 00:07:09', '2021-05-31 00:25:51'),
(60, 186, '2021-05-31', 'Nasim Ahmed', 'Nasim Ahmed', 'Alim Sarkar', 'কাইফ আহমদ', 'Jame', 'জেমি', '2020-02-25', 1, 4, 3, 1, 4, 1, 1, 3, 1, 1, 1, 1, 2, 39, 0, 0, 0, 'Thana Road', 'থানা রাস্তা', '12345678912', 1, 1, 1, '2021-05-31 00:35:52', '2021-05-31 00:37:42'),
(61, 241, '2021-06-07', 'Md Riyaj Uddin', 'Md Riyaj Uddin', 'Sabbir Hossain', 'সাইফুল', 'Lak', 'মিতালী সরকার', '2021-06-07', 1, 3, 3, 4, 4, 1, 2, 5, 1, 1, 6, 47, 1, 20, 0, 0, 0, 'Thana Road', 'থানা রাস্তা', '01234567891', 1, 1, 1, '2021-06-07 06:26:47', '2021-06-07 06:28:02');

-- --------------------------------------------------------

--
-- Table structure for table `pusti_campaign_attendances`
--

CREATE TABLE `pusti_campaign_attendances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_type_id` tinyint(4) NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `divisional_office_id` bigint(20) UNSIGNED NOT NULL,
  `school_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `school_name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `upload_date` date NOT NULL,
  `boy` int(11) DEFAULT NULL,
  `girls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pusti_campaign_attendances`
--

INSERT INTO `pusti_campaign_attendances` (`id`, `campaign_type_id`, `campaign_id`, `divisional_office_id`, `school_name`, `school_name_bn`, `attachment`, `upload_date`, `boy`, `girls`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(7, 2, 6, 7, 'Len Schwartz', 'Wyoming Rowe', '1618733519.pdf', '2021-04-06', 43, 36, '2021-04-18 02:12:00', '2021-04-18 02:12:00', 1, 1),
(11, 3, 13, 1, 'Mirpur School', 'Mirpur School', '1618824380.jpeg', '2021-04-25', 20, 20, '2021-04-19 09:26:20', '2021-06-11 23:00:08', 249, 1),
(12, 3, 8, 17, 'Pilot School', 'Pilot School', '1620540328.PDF', '2021-05-09', 20, 20, '2021-05-09 00:05:28', '2021-05-09 00:05:28', 1, 1),
(13, 3, 15, 24, 'Narsingdi Pilot High School', 'Narsingdi Pilot High School Bn', '1622960554.pdf', '2021-06-06', 10, 11, '2021-06-06 00:22:34', '2021-06-06 00:29:45', 1, 1),
(14, 1, 1, 2, 'Narsingdi Pilot High Schoolll', 'Narsingdi Pilot High School Bnll', '1622961802.pdf', '2021-06-08', 343, 3434, '2021-06-06 00:43:22', '2021-06-06 00:43:22', 1, 1),
(15, 1, 17, 3, 'Narsingdi Pilot High Schoolll', 'Narsingdi Pilot High School Bnll', '1623325885.pdf', '2021-06-10', 3, 4, '2021-06-10 05:51:25', '2021-06-10 05:51:25', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pusti_campaign_calendars`
--

CREATE TABLE `pusti_campaign_calendars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fiscal_year_id` int(11) NOT NULL,
  `campaign_type_id` tinyint(4) DEFAULT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `divisional_office_id` bigint(20) UNSIGNED DEFAULT NULL,
  `division_id` bigint(20) UNSIGNED DEFAULT NULL,
  `district_id` bigint(20) UNSIGNED DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `budget` double(8,2) NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pusti_campaign_calendars`
--

INSERT INTO `pusti_campaign_calendars` (`id`, `fiscal_year_id`, `campaign_type_id`, `campaign_id`, `divisional_office_id`, `division_id`, `district_id`, `quantity`, `start_date`, `end_date`, `budget`, `remarks`, `remarks_bn`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 3, 2, 6, 23, 6, 40, 40, '2021-04-01', '2021-04-02', 999999.99, 'Elit placeat exerc', '05y5', 1, 1, '2021-04-17 09:30:56', '2021-06-05 01:00:59'),
(2, 7, 2, 3, 1, 1, 1, 846, '2021-04-06', '2021-04-07', 80.00, 'Dicta nisi corrupti', '0', 1, 1, '2021-04-17 09:36:25', '2021-04-17 09:36:25'),
(3, 8, 3, 4, 1, 1, 1, 153, '2021-04-07', '2021-04-08', 60.00, 'Repellendus Fugit ', '0', 1, 1, '2021-04-17 09:41:30', '2021-04-17 09:41:30'),
(4, 3, 3, 5, 1, 1, 1, 209, '2021-04-07', '2021-04-12', 105.00, 'Dolorem quas tempor ', 'Dolorem quas tempor bn', 1, 1, '2021-04-17 09:58:48', '2021-04-17 10:01:05'),
(5, 3, 2, 2, 1, 1, 1, 281, '2021-04-01', '2021-04-02', 62.00, 'Ut ex aliqua Eos c', 'Dolor repellendus A', 1, 1, '2021-04-17 22:36:50', '2021-04-17 22:36:50'),
(6, 3, 2, 2, 1, 1, 1, 5685656, '2021-04-06', '2021-04-07', 565656.00, 'sdfsdfsdf', 'update bn', 1, 1, '2021-04-17 22:37:43', '2021-04-17 22:38:44'),
(7, 6, 1, 6, 1, 1, 1, 534, '2021-04-07', '2021-04-08', 21.00, 'In ipsum cumque moll', 'Fuga Saepe fugit e', 1, 1, '2021-04-17 23:11:16', '2021-04-17 23:11:16'),
(8, 3, 2, 3, 3, 1, 3, 10, '2021-04-01', '2021-04-02', 50000.00, 'remarks', 'remarks bn', 1, 1, '2021-04-18 04:11:40', '2021-04-18 04:11:40'),
(9, 3, 2, 3, 3, 1, 3, 10, '2021-04-02', '2021-04-03', 60000.00, 'remarks-2', 'remarks bn-2', 1, 1, '2021-04-18 04:11:40', '2021-04-18 04:12:25'),
(10, 1, 3, 8, 17, 6, 45, 30, '2021-05-13', '2021-05-20', 345555.00, 'Test', '', 1, 1, '2021-05-09 00:04:40', '2021-05-09 00:04:40'),
(11, 1, 3, 14, 24, 6, 40, 10, '2021-06-01', '2021-06-30', 999999.99, 'NA4', 'NA Bn4', 1, 1, '2021-06-05 00:57:46', '2021-06-06 04:36:16'),
(12, 8, 1, 3, 5, 2, 16, 473, '2021-06-01', '2021-06-09', 999999.99, 'Dolore est ea moles', 'Ipsam sint aute aut ', 1, 1, '2021-06-08 00:58:17', '2021-06-08 00:58:17'),
(13, 8, 1, 5, 1, 1, 1, 766, '2021-06-01', '2021-06-01', 999999.99, 'Aperiam qui sunt ve', 'Rerum assumenda expe', 1, 1, '2021-06-08 01:06:14', '2021-06-10 02:56:49'),
(14, 1, 1, 17, 3, 8, 63, 100, '2021-06-10', '2021-06-12', 999999.99, '', '', 1, 1, '2021-06-10 02:58:36', '2021-06-10 02:58:36');

-- --------------------------------------------------------

--
-- Table structure for table `pusti_campaign_events`
--

CREATE TABLE `pusti_campaign_events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fiscal_year_id` int(11) NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pusti_campaign_events`
--

INSERT INTO `pusti_campaign_events` (`id`, `fiscal_year_id`, `campaign_id`, `quantity`, `start_date`, `end_date`, `attachment`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 100, '2021-04-01', '2021-04-09', NULL, 2, NULL, 1, NULL, '2021-06-08 03:52:53'),
(2, 5, 1, 20, '2021-04-05', '2021-04-05', NULL, 1, NULL, 1, NULL, '2021-06-11 22:52:06'),
(12, 6, 4, 31, '2021-05-10', '2021-04-14', NULL, 1, 1, 1, '2021-05-08 00:35:08', '2021-05-08 00:35:08'),
(13, 5, 3, 45, '2021-05-03', '2021-04-06', NULL, 1, 1, 1, '2021-05-08 00:36:59', '2021-05-08 00:36:59'),
(15, 5, 5, 444, '2021-05-03', '2021-05-04', NULL, 1, 1, 1, '2021-05-08 00:43:15', '2021-05-08 00:43:15'),
(16, 1, 2, 89, '2021-05-15', '2021-05-25', NULL, 1, 1, 1, '2021-05-08 00:53:10', '2021-05-08 00:56:41'),
(17, 5, 8, 14, '2021-05-04', '2021-05-05', NULL, 1, 1, 1, '2021-05-08 01:00:14', '2021-05-08 01:00:14'),
(18, 3, 3, 4545, '2021-05-04', '2021-05-07', NULL, 1, 1, 1, '2021-05-08 03:27:24', '2021-05-08 03:27:24'),
(19, 1, 3, 44, '2021-05-02', '2021-05-13', NULL, 1, 1, 1, '2021-05-08 03:59:49', '2021-05-08 03:59:49'),
(30, 5, 4, 45000, '2021-05-01', '2021-05-02', NULL, 1, 1, 1, '2021-05-08 04:48:24', '2021-05-08 22:26:38'),
(31, 3, 2, 44, '2021-05-04', '2021-05-04', '1620471765.png', 1, 1, 1, '2021-05-08 04:53:39', '2021-05-08 04:53:39'),
(32, 5, 1, 44, '2021-05-03', '2021-05-06', '1620471765.png', 1, 1, 1, '2021-05-08 05:02:45', '2021-05-08 05:02:45'),
(33, 5, 4, 14, '2021-05-04', '2021-05-05', NULL, 1, 1, 1, '2021-05-08 21:59:00', '2021-05-08 21:59:00'),
(34, 1, 8, 30, '2021-05-01', '2021-05-09', NULL, 1, 1, 1, '2021-05-08 22:21:09', '2021-05-08 22:21:09'),
(35, 6, 4, 444, '2021-05-03', '2021-05-05', '1620534071.png', 1, 1, 1, '2021-05-08 22:21:11', '2021-05-08 22:21:11'),
(36, 5, 6, 44, '2021-05-04', '2021-05-04', NULL, 2, 1, 1, '2021-05-08 22:27:14', '2021-06-05 04:37:10'),
(37, 3, 2, 1212, '2021-05-02', '2021-05-17', NULL, 2, 1, 1, '2021-05-31 03:36:44', '2021-06-05 04:36:36'),
(38, 1, 14, 5, '2021-06-01', '2021-06-30', NULL, 1, 1, 1, '2021-06-05 04:30:55', '2021-06-05 04:36:29'),
(39, 1, 17, 10, '2021-06-10', '2021-06-10', NULL, 1, 1, 1, '2021-06-10 03:01:44', '2021-06-10 03:04:36');

-- --------------------------------------------------------

--
-- Table structure for table `pusti_campaign_events_details`
--

CREATE TABLE `pusti_campaign_events_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pusti_campaign_event_id` bigint(20) UNSIGNED NOT NULL,
  `division_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pusti_campaign_events_details`
--

INSERT INTO `pusti_campaign_events_details` (`id`, `pusti_campaign_event_id`, `division_id`, `district_id`, `created_at`, `updated_at`) VALUES
(9, 12, 4, 34, '2021-05-08 00:35:08', '2021-05-08 00:35:08'),
(12, 15, 2, 14, '2021-05-08 00:43:15', '2021-05-08 00:43:15'),
(13, 15, 8, 62, '2021-05-08 00:43:15', '2021-05-08 00:43:15'),
(17, 17, 6, 52, '2021-05-08 01:00:14', '2021-05-08 01:00:14'),
(22, 16, 1, 1, '2021-05-08 01:18:06', '2021-05-08 01:18:07'),
(27, 19, 1, 11, '2021-05-08 03:59:49', '2021-05-08 03:59:49'),
(28, 19, 8, 62, '2021-05-08 03:59:49', '2021-05-08 03:59:49'),
(29, 18, 6, 47, '2021-05-08 04:00:09', '2021-05-08 04:00:09'),
(34, 31, 1, 6, '2021-05-08 04:53:39', '2021-05-08 04:53:39'),
(35, 31, 3, 20, '2021-05-08 04:53:39', '2021-05-08 04:53:39'),
(36, 32, 1, 3, '2021-05-08 05:02:45', '2021-05-08 05:02:45'),
(37, 33, 1, 8, '2021-05-08 21:59:00', '2021-05-08 21:59:00'),
(38, 33, 3, 24, '2021-05-08 21:59:00', '2021-05-08 21:59:00'),
(39, 34, 6, 45, '2021-05-08 22:21:09', '2021-05-08 22:21:09'),
(40, 35, 1, 3, '2021-05-08 22:21:11', '2021-05-08 22:21:11'),
(41, 35, 1, 3, '2021-05-08 22:21:11', '2021-05-08 22:21:11'),
(42, 30, 4, 35, '2021-05-08 22:26:38', '2021-05-08 22:26:38'),
(43, 30, 4, 34, '2021-05-08 22:26:39', '2021-05-08 22:26:39'),
(44, 30, 3, 29, '2021-05-08 22:26:39', '2021-05-08 22:26:39'),
(45, 30, 6, 46, '2021-05-08 22:26:39', '2021-05-08 22:26:39'),
(48, 37, 4, 35, '2021-05-31 03:36:44', '2021-05-31 03:36:44'),
(49, 37, 1, 6, '2021-05-31 03:36:44', '2021-05-31 03:36:44'),
(52, 1, 6, 41, '2021-06-05 04:06:41', '2021-06-05 04:06:41'),
(53, 1, 6, 40, '2021-06-05 04:06:41', '2021-06-05 04:06:41'),
(54, 1, 1, 1, '2021-06-05 04:06:41', '2021-06-05 04:06:41'),
(62, 38, 6, 40, '2021-06-05 04:39:34', '2021-06-05 04:39:34'),
(63, 38, 1, 3, '2021-06-05 04:39:34', '2021-06-05 04:39:34'),
(64, 36, 4, 33, '2021-06-05 04:41:33', '2021-06-05 04:41:33'),
(68, 39, 8, 61, '2021-06-10 03:04:27', '2021-06-10 03:04:27'),
(69, 39, 8, 63, '2021-06-10 03:04:27', '2021-06-10 03:04:27'),
(70, 39, 8, 64, '2021-06-10 03:04:27', '2021-06-10 03:04:27'),
(71, 2, 1, 1, '2021-06-11 22:52:07', '2021-06-11 22:52:07'),
(72, 13, 4, 31, '2021-06-11 22:52:53', '2021-06-11 22:52:53');

-- --------------------------------------------------------

--
-- Table structure for table `pusti_campaign_feedbacks`
--

CREATE TABLE `pusti_campaign_feedbacks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_type_id` tinyint(4) NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `divisional_office_id` bigint(20) UNSIGNED NOT NULL,
  `school_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `school_name_bn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `upload_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pusti_campaign_feedbacks`
--

INSERT INTO `pusti_campaign_feedbacks` (`id`, `campaign_type_id`, `campaign_id`, `divisional_office_id`, `school_name`, `school_name_bn`, `attachment`, `upload_date`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(3, 3, 10, 1, 'Steven Suarez', 'Cyrus Cunningham', '1618741805.png', '2021-04-02', '2021-04-18 04:30:05', '2021-04-20 05:00:37', 1, 249),
(4, 2, 7, 1, 'Mirpur School', 'Mirpur School', '1618894868.jpeg', '2021-04-20', '2021-04-20 05:01:08', '2021-04-20 05:01:08', 249, 249),
(5, 3, 15, 24, 'Narsingdi Pilot High School', 'Narsingdi Pilot High School Bn', '1622972970.jpeg', '2021-06-05', '2021-06-06 03:49:30', '2021-06-11 23:03:37', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pusti_campaign_informations`
--

CREATE TABLE `pusti_campaign_informations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `divisional_office_id` bigint(20) UNSIGNED NOT NULL,
  `division_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `document_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `document_name_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `campaign_date` date NOT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pusti_campaign_informations`
--

INSERT INTO `pusti_campaign_informations` (`id`, `campaign_id`, `divisional_office_id`, `division_id`, `district_id`, `document_name`, `document_name_bn`, `campaign_date`, `attachment`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, 10, 1, 1, 1, 'test ', 'asdf', '2021-04-28', '1618825423.jpeg', NULL, NULL, '2021-04-19 09:43:43', '2021-04-19 09:43:43');

-- --------------------------------------------------------

--
-- Table structure for table `pusti_campaign_materials`
--

CREATE TABLE `pusti_campaign_materials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fiscal_year_id` int(11) NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `campaign_type_id` tinyint(4) NOT NULL,
  `divisional_office_id` bigint(20) UNSIGNED DEFAULT NULL,
  `campaign_date` date NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pusti_campaign_materials`
--

INSERT INTO `pusti_campaign_materials` (`id`, `fiscal_year_id`, `campaign_id`, `campaign_type_id`, `divisional_office_id`, `campaign_date`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 3, 2, 1, 1, '2021-04-20', 1, 1, '2021-04-19 04:53:31', '2021-06-09 00:31:29'),
(4, 3, 2, 1, 1, '2021-04-06', 1, 1, '2021-04-18 23:23:28', '2021-04-18 23:23:28'),
(5, 5, 1, 1, 1, '2021-04-25', 1, 1, '2021-04-18 23:31:02', '2021-04-18 23:31:02'),
(7, 8, 3, 2, 15, '2021-04-22', 1, 1, '2021-04-19 10:30:25', '2021-04-19 10:38:08'),
(8, 1, 17, 2, 24, '2021-06-01', 1, 1, '2021-06-06 02:46:31', '2021-06-10 05:56:31'),
(9, 1, 15, 2, 24, '2021-06-06', 1, 1, '2021-06-06 02:51:24', '2021-06-06 02:51:24'),
(10, 3, 6, 1, 5, '2021-06-07', 1, 1, '2021-06-06 03:05:51', '2021-06-08 02:48:37'),
(11, 5, 15, 2, 18, '2021-06-17', 1, 1, '2021-06-08 02:45:08', '2021-06-08 02:46:02'),
(12, 3, 14, 1, 2, '2021-06-09', 1, 1, '2021-06-08 23:53:03', '2021-06-08 23:53:03'),
(13, 3, 1, 1, 1, '2021-06-01', 1, 1, '2021-06-09 01:31:59', '2021-06-09 01:31:59'),
(14, 3, 1, 1, 1, '2021-06-09', 1, 1, '2021-06-09 01:50:05', '2021-06-09 01:50:05'),
(34, 1, 17, 2, 2, '2021-06-10', 1, 1, '2021-06-10 05:57:29', '2021-06-10 05:57:29'),
(35, 3, 1, 1, 1, '2021-06-07', 1, 1, '2021-06-10 06:07:16', '2021-06-10 06:07:16'),
(36, 7, 17, 2, 16, '2021-06-16', 1, 1, '2021-06-10 06:08:23', '2021-06-10 06:08:23');

-- --------------------------------------------------------

--
-- Table structure for table `pusti_campaign_mat_details`
--

CREATE TABLE `pusti_campaign_mat_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_materials_id` bigint(20) UNSIGNED DEFAULT NULL,
  `material_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `material_name_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pusti_campaign_mat_details`
--

INSERT INTO `pusti_campaign_mat_details` (`id`, `campaign_materials_id`, `material_name`, `material_name_bn`, `attachment`, `created_by`, `created_at`, `updated_at`) VALUES
(9, 5, 'test 12', 'test bn 12 bn', '1618810259.jpg', 1, '2021-04-18 23:30:59', '2021-04-18 23:31:02'),
(16, 1, 'Book ', 'Book Bn', NULL, 1, '2021-06-06 02:41:13', '2021-06-06 02:57:50'),
(17, 1, 'Book ', 'Book Bn', NULL, 1, '2021-06-06 02:41:17', '2021-06-06 02:57:50'),
(19, 1, 'Pen', 'Pen Bn', '1622968918.pdf', 1, '2021-06-06 02:41:58', '2021-06-06 02:57:50'),
(20, 1, 'Book', 'Book Bn', '1622969440.pdf', 1, '2021-06-06 02:50:40', '2021-06-06 02:57:50'),
(21, 1, 'Pen', 'Pen Bn', '1622969462.pdf', 1, '2021-06-06 02:51:02', '2021-06-06 02:57:50'),
(22, 1, 'Bag', 'Bag Bn', '1622969478.pdf', 1, '2021-06-06 02:51:18', '2021-06-06 02:57:50'),
(52, 14, 'dgf', 'fgdgfdg', '1623224020.jpg', 1, '2021-06-09 01:33:40', '2021-06-09 01:50:05'),
(54, 14, 'dd', 'dd', '1623224981.jpg', 1, '2021-06-09 01:49:42', '2021-06-09 01:50:06'),
(55, 14, 'ee', 'ee', '1623224989.jpg', 1, '2021-06-09 01:49:49', '2021-06-09 01:50:06'),
(56, 35, '', '', NULL, 1, '2021-06-10 05:55:52', '2021-06-10 06:07:16'),
(57, 35, 'a', 'a', '1623326175.pdf', 1, '2021-06-10 05:56:15', '2021-06-10 06:07:16'),
(58, 35, 'f', 'd', '1623326188.pdf', 1, '2021-06-10 05:56:28', '2021-06-10 06:07:16'),
(59, 35, 'rrtret', 'retret', '1623326826.pdf', 1, '2021-06-10 06:07:06', '2021-06-10 06:07:16'),
(61, 1, 'sd', 'da bn', '1623474378.jpeg', 1, '2021-06-11 23:06:18', '2021-06-11 23:08:18'),
(62, 1, 'Need Noney', 'No Money', '1623474495.pdf', 1, '2021-06-11 23:08:15', '2021-06-11 23:08:18'),
(63, 1, '', '', NULL, 1, '2021-06-11 23:25:51', '2021-06-11 23:29:56'),
(64, 1, 'Shoe', 'Shoe Bn', NULL, 1, '2021-06-11 23:26:15', '2021-06-11 23:29:56'),
(65, 1, 'Shoe', 'Shoe Bn', NULL, 1, '2021-06-11 23:26:23', '2021-06-11 23:29:56'),
(66, 1, 'Shoe', 'Shoe Bn', '1623475593.jpeg', 1, '2021-06-11 23:26:33', '2021-06-11 23:29:56');

-- --------------------------------------------------------

--
-- Table structure for table `pusti_campaign_requests`
--

CREATE TABLE `pusti_campaign_requests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `division_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `school_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `school_name_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designation_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `campaign_reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `campaign_reason_bn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pusti_campaign_requests`
--

INSERT INTO `pusti_campaign_requests` (`id`, `campaign_id`, `division_id`, `district_id`, `school_name`, `school_name_bn`, `name`, `name_bn`, `designation`, `designation_bn`, `email`, `mobile_no`, `start_date`, `end_date`, `campaign_reason`, `campaign_reason_bn`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'Zila School', 'Zila School Bn', 'Zila en', 'Zila bn', 'Head Master', 'Head Master bn', 'zilaschool@gmail.com', '01620506565', '2021-04-15', '2021-04-25', 'progress test', 'progress test bn', 1, 1, 1, '2021-04-16 18:00:00', '2021-04-19 00:26:38'),
(2, 5, 4, 35, 'test School', 'test School bn', 'hanif', 'hanif bn', 'teacher', 'teacher bn', 'hanif@gmail.com', '01701898339', '2021-04-20', '2021-04-25', 'asdfhh', 'zcvbcv bn', 2, 1, 1, '2021-04-17 03:40:34', '2021-04-17 22:15:02'),
(3, 8, 4, 35, 'shahebabad school', 'shebebabad  school bn', 'hanif', 'hanif bn', 'teacher', 'teacher bn', 'unseen.hanif@gmail.com', '1701898338', '2021-04-20', '2021-04-24', 'reason', 'reason bn', 1, 1, 1, '2021-04-18 21:43:12', '2021-04-18 21:43:12'),
(4, 11, 4, 35, 'shahebabad school', 'shebebabad  school bn', 'Zila en', 'Zila bn', 'teacher', 'teacher bn', 'unseen.hanif@gmail.com', '01254875', '2021-04-21', '2021-04-30', 'dghfff', 'fhjgghh bn', 1, 1, 1, '2021-04-19 22:19:01', '2021-04-19 22:19:01'),
(5, 15, 6, 40, 'Narsingdi Pilot High School', 'Narsingdi Pilot High School Bn', 'Sawkat Khan', 'Sawkat Khan Bn', 'CEO', 'CEO Bn', 'ceo@yopmail.com', '015214450045', '2021-06-05', '2021-06-30', 'NA', 'NA Bn', 1, 1, 1, '2021-06-05 06:02:16', '2021-06-05 23:46:16'),
(6, 16, 8, 61, 'Merrill Wilkins', 'Freya Gates', 'Aristotle Hicks', 'Nola Chandler', 'Voluptatem deleniti ', 'Nihil explicabo Est', 'nimaqiter@mailinator.com', '11', '2021-06-07', '2021-06-07', 'Incididunt ea eu rep', 'Laborum enim praesen', 1, 1, 1, '2021-06-08 01:44:32', '2021-06-08 01:44:32'),
(7, 17, 2, 13, 'Bo Bell', 'Charissa Harper', 'Amal Williams', 'Curran Schroeder', 'Labore proident arc', 'Rerum pariatur Aut ', 'xilobyf@mailinator.com', '11', '2021-06-01', '2021-06-07', 'Eius ex omnis iste v', 'Ad dolor fuga Asper', 1, 1, 1, '2021-06-08 01:46:27', '2021-06-08 01:46:27'),
(8, 18, 8, 63, 'Sonya David', 'Ahmed Keller', 'Solid Engineering', 'Solid Engineering', 'Qui ut inventore off', 'Ad culpa animi cons', 'soliddesignc@gmail.com', '01711130480', '2021-06-01', '2021-06-01', 'Quia voluptatibus et', 'Ipsum autem sed cons', 2, 1, 1, '2021-06-08 01:50:08', '2021-06-08 03:51:16');

-- --------------------------------------------------------

--
-- Table structure for table `pusti_campaign_schedules`
--

CREATE TABLE `pusti_campaign_schedules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fiscal_year_id` int(11) NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `divisional_office_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `facilator_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facilator_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pusti_campaign_schedules`
--

INSERT INTO `pusti_campaign_schedules` (`id`, `fiscal_year_id`, `campaign_id`, `divisional_office_id`, `quantity`, `start_date`, `end_date`, `facilator_1`, `facilator_2`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 1, 2, 1, 5, '2021-04-13', '2021-04-16', 'one', 'two', 1, 1, 1, '2021-04-13 00:08:35', '2021-06-05 05:32:31'),
(5, 3, 1, 1, 12, '2021-04-15', '2021-04-30', 'test 1', 'test 2', 2, 1, 1, '2021-04-13 02:44:14', '2021-06-05 05:05:30'),
(6, 1, 6, 2, 22, '2021-05-10', '2021-05-25', 'Test1', 'Test2', 1, 1, 1, '2021-05-08 22:21:56', '2021-06-10 05:07:54'),
(7, 7, 8, 8, 48, '2021-05-04', '2021-05-05', '', '', 1, 1, 1, '2021-05-08 23:09:14', '2021-06-08 01:25:45'),
(8, 1, 14, 24, 5, '2021-06-01', '2021-06-30', 'Facilator 1 Narsingdi', 'Facilator 2 Narsingdi', 1, 1, 1, '2021-06-05 04:59:59', '2021-06-06 04:28:02');

-- --------------------------------------------------------

--
-- Table structure for table `user_logs`
--

CREATE TABLE `user_logs` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_id` bigint(20) UNSIGNED NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `execution_type` int(11) NOT NULL COMMENT '0=insert, 1=update,2=delete',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_logs`
--

INSERT INTO `user_logs` (`id`, `user_id`, `username`, `menu_name`, `table_name`, `data_id`, `ip`, `execution_type`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'test', 'master_divisional_offices', 1, '', 0, '2021-04-11 22:54:10', '2021-04-11 22:54:10'),
(2, 1, 'admin', 'test', 'master_divisional_offices', 1, '', 1, '2021-04-11 23:30:42', '2021-04-11 23:30:42'),
(3, 1, 'admin', 'test', 'master_divisional_offices', 1, '', 2, '2021-04-11 23:31:49', '2021-04-11 23:31:49'),
(4, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 1, '', 2, '2021-04-12 01:04:01', '2021-04-12 01:04:01'),
(5, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 2, '', 0, '2021-04-12 01:10:01', '2021-04-12 01:10:01'),
(6, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 3, '', 0, '2021-04-12 01:10:10', '2021-04-12 01:10:10'),
(7, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 4, '', 0, '2021-04-12 01:10:23', '2021-04-12 01:10:23'),
(8, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 5, '', 0, '2021-04-12 01:10:30', '2021-04-12 01:10:30'),
(9, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 6, '', 0, '2021-04-12 01:10:40', '2021-04-12 01:10:40'),
(10, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 2, '', 1, '2021-04-12 01:11:11', '2021-04-12 01:11:11'),
(11, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 5, '', 1, '2021-04-12 01:13:19', '2021-04-12 01:13:19'),
(12, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 5, '', 1, '2021-04-12 01:13:46', '2021-04-12 01:13:46'),
(13, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 7, '', 0, '2021-04-12 01:15:27', '2021-04-12 01:15:27'),
(14, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 8, '', 0, '2021-04-12 01:15:34', '2021-04-12 01:15:34'),
(15, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 9, '', 0, '2021-04-12 01:15:47', '2021-04-12 01:15:47'),
(16, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 10, '', 0, '2021-04-12 01:15:55', '2021-04-12 01:15:55'),
(17, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 11, '', 0, '2021-04-12 01:16:02', '2021-04-12 01:16:02'),
(18, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 12, '', 0, '2021-04-12 01:16:08', '2021-04-12 01:16:08'),
(19, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 13, '', 0, '2021-04-12 01:16:15', '2021-04-12 01:16:15'),
(20, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 14, '', 0, '2021-04-12 01:16:49', '2021-04-12 01:16:49'),
(21, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office', 'master_divisional_offices', 5, '', 2, '2021-04-12 02:37:37', '2021-04-12 02:37:37'),
(22, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office', 'master_divisional_offices', 13, '', 2, '2021-04-12 02:37:45', '2021-04-12 02:37:45'),
(23, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office', 'master_divisional_offices', 13, '', 2, '2021-04-12 02:37:49', '2021-04-12 02:37:49'),
(24, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office', 'master_divisional_offices', 13, '', 2, '2021-04-12 02:37:53', '2021-04-12 02:37:53'),
(25, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office', 'master_divisional_offices', 5, '', 2, '2021-04-12 02:39:20', '2021-04-12 02:39:20'),
(26, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office', 'master_divisional_offices', 14, '', 2, '2021-04-12 02:39:27', '2021-04-12 02:39:27'),
(27, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office', 'master_divisional_offices', 14, '', 2, '2021-04-12 02:39:31', '2021-04-12 02:39:31'),
(28, 1, 'admin', 'test', 'pusti_campaign_schedules', 2, '', 0, '2021-04-12 22:04:01', '2021-04-12 22:04:01'),
(29, 1, 'admin', 'test', 'pusti_campaign_schedules', 2, '', 1, '2021-04-12 22:20:09', '2021-04-12 22:20:09'),
(30, 1, 'admin', 'test', 'pusti_campaign_schedules', 2, '', 2, '2021-04-12 22:21:45', '2021-04-12 22:21:45'),
(31, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/campaign', 'master_campaigns', 2, '', 0, '2021-04-12 23:38:41', '2021-04-12 23:38:41'),
(32, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/campaign', 'master_campaigns', 3, '', 0, '2021-04-12 23:38:51', '2021-04-12 23:38:51'),
(33, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/campaign', 'master_campaigns', 3, '', 2, '2021-04-12 23:38:57', '2021-04-12 23:38:57'),
(34, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/campaign', 'master_campaigns', 3, '', 2, '2021-04-12 23:39:06', '2021-04-12 23:39:06'),
(35, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/campaign', 'master_campaigns', 2, '', 2, '2021-04-12 23:39:09', '2021-04-12 23:39:09'),
(36, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/campaign', 'master_campaigns', 3, '', 1, '2021-04-12 23:39:38', '2021-04-12 23:39:38'),
(37, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/campaign#', 'master_campaigns', 4, '', 0, '2021-04-12 23:46:59', '2021-04-12 23:46:59'),
(38, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/campaign#', 'master_campaigns', 5, '', 0, '2021-04-12 23:48:24', '2021-04-12 23:48:24'),
(39, 1, 'admin', 'test', 'pusti_campaign_schedules', 2, '', 0, '2021-04-13 00:08:35', '2021-04-13 00:08:35'),
(40, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/campaign', 'master_campaigns', 6, '', 0, '2021-04-13 00:37:59', '2021-04-13 00:37:59'),
(41, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 5, '', 0, '2021-04-13 02:44:14', '2021-04-13 02:44:14'),
(42, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 5, '', 1, '2021-04-13 02:50:30', '2021-04-13 02:50:30'),
(43, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 5, '', 2, '2021-04-13 02:50:35', '2021-04-13 02:50:35'),
(44, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/birtan/campaign-schedule-assign#', 'pusti_campaign_schedules', 5, '', 1, '2021-04-13 02:59:50', '2021-04-13 02:59:50'),
(45, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/campaign', 'master_campaigns', 3, '', 0, '2021-04-13 04:20:31', '2021-04-13 04:20:31'),
(46, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 2, '', 1, '2021-04-13 04:20:43', '2021-04-13 04:20:43'),
(47, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/campaign', 'master_campaigns', 4, '', 0, '2021-04-13 04:20:50', '2021-04-13 04:20:50'),
(48, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/campaign', 'master_campaigns', 2, '', 1, '2021-04-13 04:21:03', '2021-04-13 04:21:03'),
(49, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/campaign', 'master_campaigns', 1, '', 1, '2021-04-13 04:21:23', '2021-04-13 04:21:23'),
(50, 1, 'admin', 'http://localhost:8080/agri-marketing/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 15, '', 0, '2021-04-15 05:17:03', '2021-04-15 05:17:03'),
(51, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 2, '', 2, '2021-04-15 08:42:15', '2021-04-15 08:42:15'),
(52, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 1, '', 2, '2021-04-15 08:47:02', '2021-04-15 08:47:02'),
(53, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 1, '', 2, '2021-04-15 08:47:10', '2021-04-15 08:47:10'),
(54, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage#', 'pusti_campaign_requests', 2, '', 0, '2021-04-17 03:40:34', '2021-04-17 03:40:34'),
(55, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage#', 'master_campaigns', 5, '', 0, '2021-04-17 03:40:34', '2021-04-17 03:40:34'),
(56, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material', 'pusti_campaign_mat_details', 2, '', 0, '2021-04-17 11:03:02', '2021-04-17 11:03:02'),
(57, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material', 'pusti_campaign_mat_details', 3, '', 0, '2021-04-17 11:03:16', '2021-04-17 11:03:16'),
(58, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material', 'pusti_campaign_mat_details', 1, '', 0, '2021-04-17 11:05:19', '2021-04-17 11:05:19'),
(59, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event#', 'pusti_campaign_events', 2, '', 2, '2021-04-17 05:08:26', '2021-04-17 05:08:26'),
(60, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event#', 'pusti_campaign_events', 2, '', 2, '2021-04-17 05:08:35', '2021-04-17 05:08:35'),
(61, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 1, '', 0, '2021-04-17 09:30:59', '2021-04-17 09:30:59'),
(62, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 15, '', 2, '2021-04-17 09:32:40', '2021-04-17 09:32:40'),
(63, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 15, '', 2, '2021-04-17 09:33:22', '2021-04-17 09:33:22'),
(64, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 2, '', 0, '2021-04-17 09:36:25', '2021-04-17 09:36:25'),
(65, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 3, '', 0, '2021-04-17 09:41:34', '2021-04-17 09:41:34'),
(66, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 4, '', 0, '2021-04-17 09:58:58', '2021-04-17 09:58:58'),
(67, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 4, '', 1, '2021-04-17 09:59:48', '2021-04-17 09:59:48'),
(68, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 4, '', 1, '2021-04-17 10:01:05', '2021-04-17 10:01:05'),
(69, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 6, '', 0, '2021-04-17 12:34:58', '2021-04-17 12:34:58'),
(70, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 2, '', 2, '2021-04-17 22:15:02', '2021-04-17 22:15:02'),
(71, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material', 'pusti_campaign_mat_details', 1, '', 0, '2021-04-18 04:25:59', '2021-04-18 04:25:59'),
(72, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material', 'pusti_campaign_mat_details', 2, '', 0, '2021-04-18 04:26:09', '2021-04-18 04:26:09'),
(73, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material', 'pusti_campaign_materials', 2, '', 0, '2021-04-18 04:26:48', '2021-04-18 04:26:48'),
(74, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material', 'pusti_campaign_mat_details', 1, '', 0, '2021-04-18 04:31:08', '2021-04-18 04:31:08'),
(75, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material', 'pusti_campaign_mat_details', 2, '', 0, '2021-04-18 04:31:16', '2021-04-18 04:31:16'),
(76, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material', 'pusti_campaign_materials', 4, '', 0, '2021-04-18 04:32:30', '2021-04-18 04:32:30'),
(77, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 5, '', 0, '2021-04-17 22:36:50', '2021-04-17 22:36:50'),
(78, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 6, '', 0, '2021-04-17 22:37:43', '2021-04-17 22:37:43'),
(79, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 6, '', 1, '2021-04-17 22:38:44', '2021-04-17 22:38:44'),
(80, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material#', 'pusti_campaign_mat_details', 2, '', 2, '2021-04-18 04:42:25', '2021-04-18 04:42:25'),
(81, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material#', 'pusti_campaign_mat_details', 3, '', 0, '2021-04-18 04:42:45', '2021-04-18 04:42:45'),
(82, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material#', 'pusti_campaign_mat_details', 4, '', 0, '2021-04-18 04:47:35', '2021-04-18 04:47:35'),
(83, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material#', 'pusti_campaign_materials', 4, '', 0, '2021-04-18 04:47:38', '2021-04-18 04:47:38'),
(84, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 7, '', 0, '2021-04-17 23:11:16', '2021-04-17 23:11:16'),
(85, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material', 'pusti_campaign_mat_details', 5, '', 0, '2021-04-18 05:25:47', '2021-04-18 05:25:47'),
(86, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material', 'pusti_campaign_materials', 5, '', 0, '2021-04-18 05:25:50', '2021-04-18 05:25:50'),
(87, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 1, '', 0, '2021-04-18 01:26:31', '2021-04-18 01:26:31'),
(88, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 2, '', 0, '2021-04-18 01:27:41', '2021-04-18 01:27:41'),
(89, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 3, '', 0, '2021-04-18 01:30:07', '2021-04-18 01:30:07'),
(90, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 4, '', 0, '2021-04-18 01:31:20', '2021-04-18 01:31:20'),
(91, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 5, '', 0, '2021-04-18 01:37:11', '2021-04-18 01:37:11'),
(92, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 3, '', 1, '2021-04-18 01:54:56', '2021-04-18 01:54:56'),
(93, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 4, '', 1, '2021-04-18 01:55:18', '2021-04-18 01:55:18'),
(94, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 5, '', 1, '2021-04-18 01:55:50', '2021-04-18 01:55:50'),
(95, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 6, '', 0, '2021-04-18 01:59:02', '2021-04-18 01:59:02'),
(96, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 6, '', 1, '2021-04-18 01:59:35', '2021-04-18 01:59:35'),
(97, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 7, '', 0, '2021-04-18 02:12:00', '2021-04-18 02:12:00'),
(98, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-information', 'pusti_campaign_informations', 1, '', 0, '2021-04-18 09:47:41', '2021-04-18 09:47:41'),
(99, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-feedback', 'pusti_campaign_feedbacks', 1, '', 0, '2021-04-18 03:52:17', '2021-04-18 03:52:17'),
(100, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-feedback', 'pusti_campaign_feedbacks', 2, '', 0, '2021-04-18 03:52:47', '2021-04-18 03:52:47'),
(101, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 8, '', 0, '2021-04-18 04:11:41', '2021-04-18 04:11:41'),
(102, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 9, '', 0, '2021-04-18 04:11:41', '2021-04-18 04:11:41'),
(103, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 9, '', 1, '2021-04-18 04:12:26', '2021-04-18 04:12:26'),
(104, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 2, '', 2, '2021-04-18 04:15:28', '2021-04-18 04:15:28'),
(105, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-feedback', 'pusti_campaign_feedbacks', 2, '', 1, '2021-04-18 04:21:03', '2021-04-18 04:21:03'),
(106, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-feedback', 'pusti_campaign_feedbacks', 3, '', 0, '2021-04-18 04:30:05', '2021-04-18 04:30:05'),
(107, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 8, '', 0, '2021-04-18 04:33:59', '2021-04-18 04:33:59'),
(108, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material#', 'pusti_campaign_mat_details', 6, '', 0, '2021-04-18 21:33:28', '2021-04-18 21:33:28'),
(109, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material#', 'pusti_campaign_mat_details', 7, '', 0, '2021-04-18 21:33:50', '2021-04-18 21:33:50'),
(110, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material#', 'pusti_campaign_mat_details', 8, '', 0, '2021-04-18 21:37:26', '2021-04-18 21:37:26'),
(111, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material#', 'pusti_campaign_mat_details', 9, '', 0, '2021-04-18 21:38:21', '2021-04-18 21:38:21'),
(112, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 3, '', 0, '2021-04-18 21:43:12', '2021-04-18 21:43:12'),
(113, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 8, '', 0, '2021-04-18 21:43:12', '2021-04-18 21:43:12'),
(114, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 10, '', 0, '2021-04-18 21:51:18', '2021-04-18 21:51:18'),
(115, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material', 'pusti_campaign_materials', 12, '', 0, '2021-04-19 04:40:47', '2021-04-19 04:40:47'),
(116, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material', 'pusti_campaign_materials', 13, '', 0, '2021-04-19 04:42:39', '2021-04-19 04:42:39'),
(117, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material', 'pusti_campaign_materials', 5, '', 0, '2021-04-19 04:46:22', '2021-04-19 04:46:22'),
(118, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material', 'pusti_campaign_mat_details', 1, '', 0, '2021-04-19 04:53:10', '2021-04-19 04:53:10'),
(119, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material', 'pusti_campaign_mat_details', 2, '', 0, '2021-04-19 04:53:28', '2021-04-19 04:53:28'),
(120, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-material', 'pusti_campaign_materials', 1, '', 0, '2021-04-19 04:53:31', '2021-04-19 04:53:31'),
(121, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 2, '', 0, '2021-04-18 23:13:43', '2021-04-18 23:13:43'),
(122, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 3, '', 0, '2021-04-18 23:14:37', '2021-04-18 23:14:37'),
(123, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 4, '', 0, '2021-04-18 23:14:53', '2021-04-18 23:14:53'),
(124, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 5, '', 0, '2021-04-18 23:15:28', '2021-04-18 23:15:28'),
(125, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 6, '', 0, '2021-04-18 23:15:36', '2021-04-18 23:15:36'),
(126, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 3, '', 0, '2021-04-18 23:15:38', '2021-04-18 23:15:38'),
(127, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 7, '', 0, '2021-04-18 23:23:14', '2021-04-18 23:23:14'),
(128, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 4, '', 0, '2021-04-18 23:23:29', '2021-04-18 23:23:29'),
(129, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 8, '', 0, '2021-04-18 23:30:42', '2021-04-18 23:30:42'),
(130, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 9, '', 0, '2021-04-18 23:30:59', '2021-04-18 23:30:59'),
(131, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 5, '', 0, '2021-04-18 23:31:02', '2021-04-18 23:31:02'),
(132, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 8, '', 2, '2021-04-18 23:31:27', '2021-04-18 23:31:27'),
(133, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 1, '', 1, '2021-04-19 00:26:38', '2021-04-19 00:26:38'),
(134, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 1, '', 1, '2021-04-19 00:26:38', '2021-04-19 00:26:38'),
(135, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 9, '', 0, '2021-04-19 06:41:47', '2021-04-19 06:41:47'),
(136, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-information', 'pusti_campaign_informations', 1, '', 0, '2021-04-19 08:17:35', '2021-04-19 08:17:35'),
(137, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 9, '', 1, '2021-04-19 08:20:16', '2021-04-19 08:20:16'),
(138, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 9, '', 1, '2021-04-19 08:41:28', '2021-04-19 08:41:28'),
(139, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 9, '', 1, '2021-04-19 08:57:00', '2021-04-19 08:57:00'),
(140, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 9, '', 1, '2021-04-19 08:57:09', '2021-04-19 08:57:09'),
(141, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 9, '', 2, '2021-04-19 03:10:19', '2021-04-19 03:10:19'),
(142, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 9, '', 2, '2021-04-19 09:12:22', '2021-04-19 09:12:22'),
(143, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 9, '', 2, '2021-04-19 09:13:45', '2021-04-19 09:13:45'),
(144, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 9, '', 2, '2021-04-19 09:15:10', '2021-04-19 09:15:10'),
(145, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 10, '', 0, '2021-04-19 09:24:47', '2021-04-19 09:24:47'),
(146, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 10, '', 2, '2021-04-19 09:25:27', '2021-04-19 09:25:27'),
(147, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 11, '', 0, '2021-04-19 09:26:21', '2021-04-19 09:26:21'),
(148, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 11, '', 1, '2021-04-19 09:27:16', '2021-04-19 09:27:16'),
(149, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 11, '', 1, '2021-04-19 09:27:26', '2021-04-19 09:27:26'),
(150, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 11, '', 1, '2021-04-19 09:29:04', '2021-04-19 09:29:04'),
(151, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 11, '', 1, '2021-04-19 09:30:27', '2021-04-19 09:30:27'),
(152, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 11, '', 1, '2021-04-19 09:30:44', '2021-04-19 09:30:44'),
(153, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-information', 'pusti_campaign_informations', 1, '', 2, '2021-04-19 09:34:12', '2021-04-19 09:34:12'),
(154, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-information', 'pusti_campaign_informations', 2, '', 0, '2021-04-19 09:38:05', '2021-04-19 09:38:05'),
(155, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 11, '', 1, '2021-04-19 09:42:32', '2021-04-19 09:42:32'),
(156, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-attendance', 'pusti_campaign_attendances', 11, '', 1, '2021-04-19 09:42:41', '2021-04-19 09:42:41'),
(157, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-information', 'pusti_campaign_informations', 2, '', 2, '2021-04-19 09:43:23', '2021-04-19 09:43:23'),
(158, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-information', 'pusti_campaign_informations', 3, '', 0, '2021-04-19 09:43:43', '2021-04-19 09:43:43'),
(159, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 10, '', 0, '2021-04-19 10:23:32', '2021-04-19 10:23:32'),
(160, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 7, '', 0, '2021-04-19 10:30:25', '2021-04-19 10:30:25'),
(161, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material#', 'pusti_campaign_materials', 7, '', 0, '2021-04-19 10:38:08', '2021-04-19 10:38:08'),
(162, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material#', 'pusti_campaign_materials', 7, '', 0, '2021-04-19 10:44:04', '2021-04-19 10:44:04'),
(163, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 1, '', 0, '2021-04-19 22:17:26', '2021-04-19 22:17:26'),
(164, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 4, '', 0, '2021-04-19 22:19:01', '2021-04-19 22:19:01'),
(165, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 11, '', 0, '2021-04-19 22:19:01', '2021-04-19 22:19:01'),
(166, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 11, '', 1, '2021-04-19 22:19:51', '2021-04-19 22:19:51'),
(167, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 4, '', 1, '2021-04-19 22:19:51', '2021-04-19 22:19:51'),
(168, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-feedback', 'pusti_campaign_feedbacks', 3, '', 1, '2021-04-20 05:00:38', '2021-04-20 05:00:38'),
(169, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-feedback', 'pusti_campaign_feedbacks', 4, '', 0, '2021-04-20 05:01:08', '2021-04-20 05:01:08'),
(170, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-feedback', 'pusti_campaign_feedbacks', 4, '', 2, '2021-04-20 05:03:55', '2021-04-20 05:03:55'),
(171, 249, 'ss@gmail.com', 'http://localhost:8080/divisional-head/campaign-feedback', 'pusti_campaign_feedbacks', 4, '', 2, '2021-04-20 05:05:56', '2021-04-20 05:05:56'),
(172, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 11, '', 1, '2021-04-20 21:09:03', '2021-04-20 21:09:03'),
(173, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 4, '', 1, '2021-04-20 21:09:03', '2021-04-20 21:09:03'),
(174, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/grower-buyer-pofile#', 'linkage_gro_buy_profiles', 5, '', 0, '2021-04-22 10:44:26', '2021-04-22 10:44:26'),
(175, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/grower-buyer-pofile', 'linkage_gro_buy_profiles', 5, '', 1, '2021-04-24 05:03:59', '2021-04-24 05:03:59'),
(176, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/registration-list#', 'linkage_gro_buy_profiles', 5, '', 1, '2021-04-25 05:30:23', '2021-04-25 05:30:23'),
(177, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/grower-buyer-pofile', 'linkage_gro_buy_profiles', 6, '', 0, '2021-04-25 23:51:49', '2021-04-25 23:51:49'),
(178, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/price-information/price-collection-details?Division_id=1&District_id=1&PriceDate=2021-04-26', 'cpi_price_infos', 1, '', 1, '2021-04-26 06:49:08', '2021-04-26 06:49:08'),
(179, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/price-information/price-collection-details?Division_id=1&District_id=1&PriceDate=2021-04-26', 'cpi_price_infos', 1, '', 1, '2021-04-26 06:51:25', '2021-04-26 06:51:25'),
(180, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/grower-buyer-pofile', 'linkage_gro_buy_profiles', 7, '', 0, '2021-04-27 02:50:09', '2021-04-27 02:50:09'),
(181, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/product-information#', 'linkage_gro_buy_pro_details', 1, '', 0, '2021-04-27 04:10:44', '2021-04-27 04:10:44'),
(182, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/product-information#', 'linkage_gro_buy_pro_details', 2, '', 0, '2021-04-27 04:10:44', '2021-04-27 04:10:44'),
(183, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/product-information#', 'linkage_gro_buy_pro_details', 3, '', 0, '2021-04-27 04:13:17', '2021-04-27 04:13:17'),
(184, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/product-information#', 'linkage_gro_buy_pro_details', 4, '', 0, '2021-04-27 04:13:17', '2021-04-27 04:13:17'),
(185, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/product-information#', 'linkage_gro_buy_pro_details', 5, '', 0, '2021-04-27 04:13:17', '2021-04-27 04:13:17'),
(186, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/grower-buyer-pofile', 'linkage_gro_buy_profiles', 8, '', 0, '2021-04-27 04:44:04', '2021-04-27 04:44:04'),
(187, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/product-information', 'linkage_gro_buy_pro_details', 1, '', 0, '2021-04-27 05:44:14', '2021-04-27 05:44:14'),
(188, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/product-information', 'linkage_gro_buy_pro_details', 2, '', 0, '2021-04-27 05:44:14', '2021-04-27 05:44:14'),
(189, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/product-information-form#', 'linkage_gro_buy_pro_details', 6, '', 0, '2021-04-27 22:53:06', '2021-04-27 22:53:06'),
(190, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/product-information-form#', 'linkage_gro_buy_pro_details', 7, '', 0, '2021-04-27 22:53:07', '2021-04-27 22:53:07'),
(191, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/product-information-form#', 'linkage_gro_buy_pro_details', 8, '', 0, '2021-04-27 22:53:07', '2021-04-27 22:53:07'),
(192, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/grower-buyer-pofile', 'linkage_gro_buy_profiles', 9, '', 0, '2021-04-27 23:17:09', '2021-04-27 23:17:09'),
(193, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 11, '', 0, '2021-04-27 23:24:21', '2021-04-27 23:24:21'),
(194, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 12, '', 0, '2021-04-27 23:24:22', '2021-04-27 23:24:22'),
(195, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/report-header#', 'master_report_headers', 5, '', 0, '2021-04-28 04:58:10', '2021-04-28 04:58:10'),
(196, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/product-information-form#', 'linkage_gro_buy_pro_details', 1, '', 0, '2021-04-28 22:15:10', '2021-04-28 22:15:10'),
(197, 1, 'admin', 'http://localhost:8080/agri-marketing-service/market-linkage/linkage/product-information-form#', 'linkage_gro_buy_pro_details', 2, '', 0, '2021-04-28 22:15:10', '2021-04-28 22:15:10'),
(198, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/region#', 'master_regions', 12, '', 1, '2021-05-03 08:41:34', '2021-05-03 08:41:34'),
(199, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/region#', 'master_regions', 12, '', 2, '2021-05-03 08:42:27', '2021-05-03 08:42:27'),
(200, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-variety#', 'master_cotton_varities', 5, '', 1, '2021-05-03 09:31:56', '2021-05-03 09:31:56'),
(201, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-name#', 'master_cotton_names', 4, '', 1, '2021-05-03 10:02:27', '2021-05-03 10:02:27'),
(202, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-name#', 'master_cotton_names', 4, '', 1, '2021-05-03 10:02:40', '2021-05-03 10:02:40'),
(203, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/zone', 'master_zones', 28, '', 1, '2021-05-03 10:48:54', '2021-05-03 10:48:54'),
(204, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/season#', 'master_seasons', 5, '', 1, '2021-05-04 06:19:20', '2021-05-04 06:19:20'),
(205, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/report-header', 'master_report_headers', 6, '', 0, '2021-05-04 06:30:57', '2021-05-04 06:30:57'),
(206, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/report-header', 'master_report_headers', 3, '', 1, '2021-05-04 06:46:25', '2021-05-04 06:46:25'),
(207, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/season', 'master_seasons', 3, '', 1, '2021-05-04 03:26:09', '2021-05-04 03:26:09'),
(208, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/ginner-grower/hat-manage', 'grower_hatt_manages', 9, '', 0, '2021-05-04 22:30:00', '2021-05-04 22:30:00'),
(209, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 16, '', 0, '2021-05-06 00:49:19', '2021-05-06 00:49:19'),
(210, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office#', 'master_divisional_offices', 17, '', 0, '2021-05-06 00:55:31', '2021-05-06 00:55:31'),
(211, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 17, '', 1, '2021-05-06 01:07:38', '2021-05-06 01:07:38'),
(212, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 16, '', 1, '2021-05-06 01:08:06', '2021-05-06 01:08:06'),
(213, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 17, '', 1, '2021-05-06 01:34:28', '2021-05-06 01:34:28'),
(214, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 17, '', 1, '2021-05-06 01:35:59', '2021-05-06 01:35:59'),
(215, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 17, '', 1, '2021-05-06 01:36:13', '2021-05-06 01:36:13'),
(216, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 18, '', 0, '2021-05-06 01:41:10', '2021-05-06 01:41:10'),
(217, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 16, '', 1, '2021-05-06 02:01:12', '2021-05-06 02:01:12'),
(218, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 15, '', 1, '2021-05-06 02:32:38', '2021-05-06 02:32:38'),
(219, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 16, '', 1, '2021-05-06 02:33:03', '2021-05-06 02:33:03'),
(220, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 19, '', 0, '2021-05-06 02:33:34', '2021-05-06 02:33:34'),
(221, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 20, '', 0, '2021-05-06 02:40:56', '2021-05-06 02:40:56'),
(222, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 12, '', 0, '2021-05-06 02:56:01', '2021-05-06 02:56:01'),
(228, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 9, '', 0, '2021-05-08 00:35:09', '2021-05-08 00:35:09'),
(229, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 12, '', 0, '2021-05-08 00:35:09', '2021-05-08 00:35:09'),
(230, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 10, '', 0, '2021-05-08 00:36:59', '2021-05-08 00:36:59'),
(231, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 13, '', 0, '2021-05-08 00:36:59', '2021-05-08 00:36:59'),
(232, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 11, '', 0, '2021-05-08 00:40:29', '2021-05-08 00:40:29'),
(233, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 14, '', 0, '2021-05-08 00:40:29', '2021-05-08 00:40:29'),
(234, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 12, '', 0, '2021-05-08 00:43:15', '2021-05-08 00:43:15'),
(235, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 13, '', 0, '2021-05-08 00:43:15', '2021-05-08 00:43:15'),
(236, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 15, '', 0, '2021-05-08 00:43:16', '2021-05-08 00:43:16'),
(237, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 14, '', 0, '2021-05-08 00:53:10', '2021-05-08 00:53:10'),
(238, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 14, '', 0, '2021-05-08 00:53:11', '2021-05-08 00:53:11'),
(239, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 16, '', 0, '2021-05-08 00:53:11', '2021-05-08 00:53:11'),
(240, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 15, '', 0, '2021-05-08 00:56:41', '2021-05-08 00:56:41'),
(241, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 15, '', 0, '2021-05-08 00:56:41', '2021-05-08 00:56:41'),
(242, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 16, '', 0, '2021-05-08 00:56:41', '2021-05-08 00:56:41'),
(243, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 16, '', 0, '2021-05-08 00:57:15', '2021-05-08 00:57:15'),
(244, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 16, '', 0, '2021-05-08 00:57:16', '2021-05-08 00:57:16'),
(245, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 16, '', 0, '2021-05-08 00:57:16', '2021-05-08 00:57:16'),
(246, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 17, '', 0, '2021-05-08 01:00:14', '2021-05-08 01:00:14'),
(247, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 18, '', 0, '2021-05-08 01:00:14', '2021-05-08 01:00:14'),
(248, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 19, '', 0, '2021-05-08 01:00:15', '2021-05-08 01:00:15'),
(249, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 17, '', 0, '2021-05-08 01:00:15', '2021-05-08 01:00:15'),
(250, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 20, '', 0, '2021-05-08 01:04:50', '2021-05-08 01:04:50'),
(251, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 20, '', 0, '2021-05-08 01:04:50', '2021-05-08 01:04:50'),
(252, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 16, '', 0, '2021-05-08 01:04:50', '2021-05-08 01:04:50'),
(253, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 21, '', 0, '2021-05-08 01:15:49', '2021-05-08 01:15:49'),
(254, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 21, '', 0, '2021-05-08 01:15:49', '2021-05-08 01:15:49'),
(255, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 16, '', 0, '2021-05-08 01:15:49', '2021-05-08 01:15:49'),
(256, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 22, '', 0, '2021-05-08 01:18:07', '2021-05-08 01:18:07'),
(257, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 22, '', 0, '2021-05-08 01:18:07', '2021-05-08 01:18:07'),
(258, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 16, '', 0, '2021-05-08 01:18:07', '2021-05-08 01:18:07'),
(259, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 23, '', 0, '2021-05-08 03:27:24', '2021-05-08 03:27:24'),
(260, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 24, '', 0, '2021-05-08 03:27:24', '2021-05-08 03:27:24'),
(261, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 25, '', 0, '2021-05-08 03:27:24', '2021-05-08 03:27:24'),
(262, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 18, '', 0, '2021-05-08 03:27:24', '2021-05-08 03:27:24'),
(263, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 26, '', 0, '2021-05-08 03:59:21', '2021-05-08 03:59:21'),
(264, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 26, '', 0, '2021-05-08 03:59:21', '2021-05-08 03:59:21'),
(265, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 26, '', 0, '2021-05-08 03:59:21', '2021-05-08 03:59:21'),
(266, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 1, '', 0, '2021-05-08 03:59:21', '2021-05-08 03:59:21'),
(267, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 27, '', 0, '2021-05-08 03:59:49', '2021-05-08 03:59:49'),
(268, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 28, '', 0, '2021-05-08 03:59:49', '2021-05-08 03:59:49'),
(269, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 19, '', 0, '2021-05-08 03:59:50', '2021-05-08 03:59:50'),
(270, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 29, '', 0, '2021-05-08 04:00:09', '2021-05-08 04:00:09'),
(271, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 29, '', 0, '2021-05-08 04:00:09', '2021-05-08 04:00:09'),
(272, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 18, '', 0, '2021-05-08 04:00:09', '2021-05-08 04:00:09'),
(273, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 30, '', 0, '2021-05-08 04:48:24', '2021-05-08 04:48:24'),
(274, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 31, '', 0, '2021-05-08 04:48:24', '2021-05-08 04:48:24'),
(275, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 32, '', 0, '2021-05-08 04:48:24', '2021-05-08 04:48:24'),
(276, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 33, '', 0, '2021-05-08 04:48:24', '2021-05-08 04:48:24'),
(277, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 30, '', 0, '2021-05-08 04:48:24', '2021-05-08 04:48:24'),
(278, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 34, '', 0, '2021-05-08 04:53:39', '2021-05-08 04:53:39'),
(279, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 35, '', 0, '2021-05-08 04:53:39', '2021-05-08 04:53:39'),
(280, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 31, '', 0, '2021-05-08 04:53:39', '2021-05-08 04:53:39'),
(281, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 36, '', 0, '2021-05-08 05:02:45', '2021-05-08 05:02:45');
INSERT INTO `user_logs` (`id`, `user_id`, `username`, `menu_name`, `table_name`, `data_id`, `ip`, `execution_type`, `created_at`, `updated_at`) VALUES
(282, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 32, '', 0, '2021-05-08 05:02:45', '2021-05-08 05:02:45'),
(283, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 37, '', 0, '2021-05-08 21:59:00', '2021-05-08 21:59:00'),
(284, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 38, '', 0, '2021-05-08 21:59:00', '2021-05-08 21:59:00'),
(285, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 33, '', 0, '2021-05-08 21:59:00', '2021-05-08 21:59:00'),
(286, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 39, '', 0, '2021-05-08 22:21:09', '2021-05-08 22:21:09'),
(287, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 34, '', 0, '2021-05-08 22:21:09', '2021-05-08 22:21:09'),
(288, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 40, '', 0, '2021-05-08 22:21:11', '2021-05-08 22:21:11'),
(289, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 41, '', 0, '2021-05-08 22:21:11', '2021-05-08 22:21:11'),
(290, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 35, '', 0, '2021-05-08 22:21:11', '2021-05-08 22:21:11'),
(291, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 6, '', 0, '2021-05-08 22:21:56', '2021-05-08 22:21:56'),
(292, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 42, '', 0, '2021-05-08 22:26:39', '2021-05-08 22:26:39'),
(293, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 43, '', 0, '2021-05-08 22:26:39', '2021-05-08 22:26:39'),
(294, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 44, '', 0, '2021-05-08 22:26:39', '2021-05-08 22:26:39'),
(295, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 45, '', 0, '2021-05-08 22:26:39', '2021-05-08 22:26:39'),
(296, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 30, '', 0, '2021-05-08 22:26:39', '2021-05-08 22:26:39'),
(297, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 46, '', 0, '2021-05-08 22:27:14', '2021-05-08 22:27:14'),
(298, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 36, '', 0, '2021-05-08 22:27:15', '2021-05-08 22:27:15'),
(299, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 47, '', 0, '2021-05-08 22:27:50', '2021-05-08 22:27:50'),
(300, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 36, '', 0, '2021-05-08 22:27:50', '2021-05-08 22:27:50'),
(301, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 21, '', 0, '2021-05-08 22:39:17', '2021-05-08 22:39:17'),
(302, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 21, '', 1, '2021-05-08 22:43:04', '2021-05-08 22:43:04'),
(303, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 22, '', 0, '2021-05-08 22:45:29', '2021-05-08 22:45:29'),
(304, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 22, '', 1, '2021-05-08 22:47:22', '2021-05-08 22:47:22'),
(305, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 23, '', 0, '2021-05-08 22:51:32', '2021-05-08 22:51:32'),
(306, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 7, '', 0, '2021-05-08 23:09:14', '2021-05-08 23:09:14'),
(307, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 20, '', 1, '2021-05-08 23:53:51', '2021-05-08 23:53:51'),
(308, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 10, '', 0, '2021-05-09 00:04:40', '2021-05-09 00:04:40'),
(309, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 12, '', 0, '2021-05-09 00:05:28', '2021-05-09 00:05:28'),
(310, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 13, '', 0, '2021-05-20 00:53:19', '2021-05-20 00:53:19'),
(311, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market#', 'master_markets', 2, '', 2, '2021-05-23 23:00:04', '2021-05-23 23:00:04'),
(312, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market#', 'master_markets', 2, '', 2, '2021-05-23 23:00:18', '2021-05-23 23:00:18'),
(313, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market', 'master_markets', 1, '', 2, '2021-05-24 02:43:48', '2021-05-24 02:43:48'),
(314, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/region', 'master_regions', 14, '', 1, '2021-05-31 00:14:21', '2021-05-31 00:14:21'),
(315, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/region', 'master_regions', 14, '', 2, '2021-05-31 00:14:26', '2021-05-31 00:14:26'),
(316, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/region', 'master_regions', 14, '', 2, '2021-05-31 00:14:33', '2021-05-31 00:14:33'),
(317, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/region', 'master_regions', 15, '', 2, '2021-05-31 00:16:37', '2021-05-31 00:16:37'),
(318, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/region', 'master_regions', 15, '', 1, '2021-05-31 00:17:06', '2021-05-31 00:17:06'),
(319, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/region', 'master_regions', 15, '', 2, '2021-05-31 00:17:19', '2021-05-31 00:17:19'),
(320, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/region', 'master_regions', 3, '', 1, '2021-05-31 00:18:48', '2021-05-31 00:18:48'),
(321, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/region', 'master_regions', 3, '', 2, '2021-05-31 00:18:56', '2021-05-31 00:18:56'),
(322, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/region', 'master_regions', 3, '', 2, '2021-05-31 00:18:59', '2021-05-31 00:18:59'),
(323, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-variety', 'master_cotton_varities', 2, '', 2, '2021-05-31 00:27:05', '2021-05-31 00:27:05'),
(324, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-variety', 'master_cotton_varities', 2, '', 2, '2021-05-31 00:27:10', '2021-05-31 00:27:10'),
(325, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-variety', 'master_cotton_varities', 2, '', 2, '2021-05-31 00:27:14', '2021-05-31 00:27:14'),
(326, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-variety', 'master_cotton_varities', 5, '', 1, '2021-05-31 00:27:59', '2021-05-31 00:27:59'),
(327, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-variety', 'master_cotton_varities', 2, '', 2, '2021-05-31 00:40:33', '2021-05-31 00:40:33'),
(328, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-variety', 'master_cotton_varities', 1, '', 2, '2021-05-31 00:40:53', '2021-05-31 00:40:53'),
(329, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-variety', 'master_cotton_varities', 2, '', 2, '2021-05-31 00:40:55', '2021-05-31 00:40:55'),
(330, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-name', 'master_cotton_names', 1, '', 2, '2021-05-31 00:42:11', '2021-05-31 00:42:11'),
(331, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-name', 'master_cotton_names', 4, '', 1, '2021-05-31 00:47:31', '2021-05-31 00:47:31'),
(332, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-name', 'master_cotton_names', 4, '', 1, '2021-05-31 00:47:50', '2021-05-31 00:47:50'),
(333, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-name#', 'master_cotton_names', 4, '', 1, '2021-05-31 00:48:46', '2021-05-31 00:48:46'),
(334, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-name', 'master_cotton_names', 2, '', 1, '2021-05-31 00:49:50', '2021-05-31 00:49:50'),
(335, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-name', 'master_cotton_names', 2, '', 1, '2021-05-31 00:49:57', '2021-05-31 00:49:57'),
(336, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-name', 'master_cotton_names', 3, '', 1, '2021-05-31 00:50:07', '2021-05-31 00:50:07'),
(337, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-name', 'master_cotton_names', 1, '', 1, '2021-05-31 01:01:58', '2021-05-31 01:01:58'),
(338, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-name', 'master_cotton_names', 3, '', 1, '2021-05-31 01:02:04', '2021-05-31 01:02:04'),
(339, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-name', 'master_cotton_names', 4, '', 1, '2021-05-31 01:02:11', '2021-05-31 01:02:11'),
(340, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-name', 'master_cotton_names', 4, '', 2, '2021-05-31 01:02:35', '2021-05-31 01:02:35'),
(341, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-name', 'master_cotton_names', 4, '', 2, '2021-05-31 01:02:40', '2021-05-31 01:02:40'),
(342, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/zone', 'master_zones', 17, '', 1, '2021-05-31 01:03:42', '2021-05-31 01:03:42'),
(343, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/zone', 'master_zones', 17, '', 2, '2021-05-31 01:03:46', '2021-05-31 01:03:46'),
(344, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/zone', 'master_zones', 17, '', 2, '2021-05-31 01:03:52', '2021-05-31 01:03:52'),
(345, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/region', 'master_regions', 6, '', 2, '2021-05-31 01:05:09', '2021-05-31 01:05:09'),
(346, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/region', 'master_regions', 15, '', 2, '2021-05-31 01:05:42', '2021-05-31 01:05:42'),
(347, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/zone', 'master_zones', 30, '', 1, '2021-05-31 01:24:34', '2021-05-31 01:24:34'),
(348, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/zone', 'master_zones', 30, '', 1, '2021-05-31 01:24:40', '2021-05-31 01:24:40'),
(349, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/zone', 'master_zones', 30, '', 1, '2021-05-31 01:24:56', '2021-05-31 01:24:56'),
(350, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/zone#', 'master_zones', 18, '', 1, '2021-05-31 02:39:39', '2021-05-31 02:39:39'),
(351, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/zone#', 'master_zones', 20, '', 2, '2021-05-31 02:39:49', '2021-05-31 02:39:49'),
(352, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/zone#', 'master_zones', 20, '', 2, '2021-05-31 02:39:53', '2021-05-31 02:39:53'),
(353, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event#', 'pusti_campaign_events_details', 48, '', 0, '2021-05-31 03:36:44', '2021-05-31 03:36:44'),
(354, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event#', 'pusti_campaign_events_details', 49, '', 0, '2021-05-31 03:36:44', '2021-05-31 03:36:44'),
(355, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event#', 'pusti_campaign_events', 37, '', 0, '2021-05-31 03:36:44', '2021-05-31 03:36:44'),
(356, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/season', 'master_seasons', 5, '', 2, '2021-05-31 03:42:11', '2021-05-31 03:42:11'),
(357, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/season', 'master_seasons', 5, '', 2, '2021-05-31 03:42:16', '2021-05-31 03:42:16'),
(358, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/season#', 'master_seasons', 1, '', 1, '2021-05-31 03:45:00', '2021-05-31 03:45:00'),
(359, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/season#', 'master_seasons', 5, '', 1, '2021-05-31 03:45:50', '2021-05-31 03:45:50'),
(360, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/report-header', 'master_report_headers', 3, '', 1, '2021-05-31 03:56:02', '2021-05-31 03:56:02'),
(361, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/ginner-grower/hat-manage', 'grower_hatt_manages', 10, '', 0, '2021-05-31 04:51:55', '2021-05-31 04:51:55'),
(362, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/ginner-grower/hat-manage', 'grower_hatt_manages', 10, '', 2, '2021-05-31 04:52:15', '2021-05-31 04:52:15'),
(363, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/ginner-grower/hat-manage', 'grower_hatt_manages', 10, '', 2, '2021-05-31 04:52:19', '2021-05-31 04:52:19'),
(364, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-name', 'master_cotton_names', 2, '', 2, '2021-06-01 00:49:34', '2021-06-01 00:49:34'),
(365, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/season', 'master_seasons', 4, '', 2, '2021-06-01 01:05:08', '2021-06-01 01:05:08'),
(366, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/ginner-grower/hat-manage', 'grower_hatt_manages', 11, '', 0, '2021-06-01 03:32:27', '2021-06-01 03:32:27'),
(367, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/ginner-grower/hat-manage', 'grower_hatt_manages', 11, '', 1, '2021-06-01 03:32:39', '2021-06-01 03:32:39'),
(368, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/ginner-grower/hat-manage', 'grower_hatt_manages', 11, '', 2, '2021-06-01 03:33:15', '2021-06-01 03:33:15'),
(369, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/ginner-grower/hat-manage', 'grower_hatt_manages', 11, '', 2, '2021-06-01 03:33:21', '2021-06-01 03:33:21'),
(370, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/ginner-grower/hat-manage', 'grower_hatt_manages', 11, '', 1, '2021-06-01 03:56:26', '2021-06-01 03:56:26'),
(371, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/zone', 'master_zones', 18, '', 1, '2021-06-01 23:02:27', '2021-06-01 23:02:27'),
(372, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-variety', 'master_cotton_varities', 6, '', 1, '2021-06-01 23:04:55', '2021-06-01 23:04:55'),
(373, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-name', 'master_cotton_names', 5, '', 1, '2021-06-01 23:09:29', '2021-06-01 23:09:29'),
(374, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-variety', 'master_cotton_varities', 6, '', 1, '2021-06-01 23:09:49', '2021-06-01 23:09:49'),
(375, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/ginner-grower/production-achievement', 'grower_prod_achievements', 21, '', 0, '2021-06-03 02:36:01', '2021-06-03 02:36:01'),
(376, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/region', 'master_regions', 11, '', 2, '2021-06-03 03:40:42', '2021-06-03 03:40:42'),
(377, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/ginner-grower/hat-manage#', 'grower_hatt_manages', 11, '', 2, '2021-06-03 04:43:39', '2021-06-03 04:43:39'),
(378, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/ginner-grower/hat-manage#', 'grower_hatt_manages', 11, '', 2, '2021-06-03 04:43:45', '2021-06-03 04:43:45'),
(379, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/ginner-grower/hat-manage#', 'grower_hatt_manages', 11, '', 1, '2021-06-03 04:44:06', '2021-06-03 04:44:06'),
(380, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 24, '', 0, '2021-06-04 23:21:48', '2021-06-04 23:21:48'),
(381, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 24, '', 1, '2021-06-04 23:22:13', '2021-06-04 23:22:13'),
(382, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 24, '', 1, '2021-06-04 23:23:04', '2021-06-04 23:23:04'),
(383, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 24, '', 1, '2021-06-04 23:23:48', '2021-06-04 23:23:48'),
(384, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 24, '', 1, '2021-06-04 23:41:22', '2021-06-04 23:41:22'),
(385, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 24, '', 2, '2021-06-04 23:42:34', '2021-06-04 23:42:34'),
(386, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 24, '', 2, '2021-06-04 23:42:39', '2021-06-04 23:42:39'),
(387, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 24, '', 1, '2021-06-04 23:43:37', '2021-06-04 23:43:37'),
(388, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 24, '', 2, '2021-06-04 23:43:48', '2021-06-04 23:43:48'),
(389, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 24, '', 2, '2021-06-04 23:43:52', '2021-06-04 23:43:52'),
(390, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 13, '', 1, '2021-06-05 00:00:46', '2021-06-05 00:00:46'),
(391, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 13, '', 1, '2021-06-05 00:00:53', '2021-06-05 00:00:53'),
(392, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 12, '', 1, '2021-06-05 00:01:16', '2021-06-05 00:01:16'),
(393, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 13, '', 1, '2021-06-05 00:01:29', '2021-06-05 00:01:29'),
(394, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 13, '', 2, '2021-06-05 00:02:29', '2021-06-05 00:02:29'),
(395, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 14, '', 0, '2021-06-05 00:06:46', '2021-06-05 00:06:46'),
(396, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 14, '', 1, '2021-06-05 00:07:01', '2021-06-05 00:07:01'),
(397, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 14, '', 1, '2021-06-05 00:07:14', '2021-06-05 00:07:14'),
(398, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 14, '', 1, '2021-06-05 00:07:26', '2021-06-05 00:07:26'),
(399, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 14, '', 2, '2021-06-05 00:07:55', '2021-06-05 00:07:55'),
(400, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 24, '', 2, '2021-06-05 00:09:24', '2021-06-05 00:09:24'),
(401, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 24, '', 2, '2021-06-05 00:09:28', '2021-06-05 00:09:28'),
(402, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 14, '', 2, '2021-06-05 00:09:39', '2021-06-05 00:09:39'),
(403, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 14, '', 1, '2021-06-05 00:10:24', '2021-06-05 00:10:24'),
(404, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 14, '', 2, '2021-06-05 00:10:29', '2021-06-05 00:10:29'),
(405, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 14, '', 2, '2021-06-05 00:27:29', '2021-06-05 00:27:29'),
(406, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 10, '', 1, '2021-06-05 00:34:19', '2021-06-05 00:34:19'),
(407, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 9, '', 1, '2021-06-05 00:34:24', '2021-06-05 00:34:24'),
(408, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 11, '', 0, '2021-06-05 00:57:46', '2021-06-05 00:57:46'),
(409, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 11, '', 1, '2021-06-05 00:58:43', '2021-06-05 00:58:43'),
(410, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 1, '', 1, '2021-06-05 00:59:29', '2021-06-05 00:59:29'),
(411, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 1, '', 1, '2021-06-05 01:01:00', '2021-06-05 01:01:00'),
(412, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 11, '', 1, '2021-06-05 01:02:56', '2021-06-05 01:02:56'),
(413, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 11, '', 1, '2021-06-05 01:03:46', '2021-06-05 01:03:46'),
(414, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 11, '', 1, '2021-06-05 01:03:57', '2021-06-05 01:03:57'),
(415, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/cotton-variety', 'master_cotton_varities', 1, '', 1, '2021-06-05 02:46:28', '2021-06-05 02:46:28'),
(416, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 9, '', 2, '2021-06-05 03:35:13', '2021-06-05 03:35:13'),
(417, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 11, '', 2, '2021-06-05 03:35:26', '2021-06-05 03:35:26'),
(418, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 50, '', 0, '2021-06-05 04:06:19', '2021-06-05 04:06:19'),
(419, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 51, '', 0, '2021-06-05 04:06:19', '2021-06-05 04:06:19'),
(420, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 1, '', 0, '2021-06-05 04:06:19', '2021-06-05 04:06:19'),
(421, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 52, '', 0, '2021-06-05 04:06:41', '2021-06-05 04:06:41'),
(422, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 53, '', 0, '2021-06-05 04:06:41', '2021-06-05 04:06:41'),
(423, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 54, '', 0, '2021-06-05 04:06:41', '2021-06-05 04:06:41'),
(424, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 1, '', 0, '2021-06-05 04:06:41', '2021-06-05 04:06:41'),
(425, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 55, '', 0, '2021-06-05 04:30:55', '2021-06-05 04:30:55'),
(426, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 56, '', 0, '2021-06-05 04:30:56', '2021-06-05 04:30:56'),
(427, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 38, '', 0, '2021-06-05 04:30:56', '2021-06-05 04:30:56'),
(428, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 57, '', 0, '2021-06-05 04:32:11', '2021-06-05 04:32:11'),
(429, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 58, '', 0, '2021-06-05 04:32:11', '2021-06-05 04:32:11'),
(430, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 38, '', 0, '2021-06-05 04:32:11', '2021-06-05 04:32:11'),
(431, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 59, '', 0, '2021-06-05 04:33:53', '2021-06-05 04:33:53'),
(432, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 60, '', 0, '2021-06-05 04:33:53', '2021-06-05 04:33:53'),
(433, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 38, '', 0, '2021-06-05 04:33:53', '2021-06-05 04:33:53'),
(434, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 38, '', 2, '2021-06-05 04:36:25', '2021-06-05 04:36:25'),
(435, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 38, '', 2, '2021-06-05 04:36:29', '2021-06-05 04:36:29'),
(436, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 37, '', 2, '2021-06-05 04:36:36', '2021-06-05 04:36:36'),
(437, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 36, '', 2, '2021-06-05 04:36:44', '2021-06-05 04:36:44'),
(438, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 61, '', 0, '2021-06-05 04:37:10', '2021-06-05 04:37:10'),
(439, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 36, '', 0, '2021-06-05 04:37:10', '2021-06-05 04:37:10'),
(440, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 62, '', 0, '2021-06-05 04:39:34', '2021-06-05 04:39:34'),
(441, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 63, '', 0, '2021-06-05 04:39:34', '2021-06-05 04:39:34'),
(442, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 38, '', 0, '2021-06-05 04:39:34', '2021-06-05 04:39:34'),
(443, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 64, '', 0, '2021-06-05 04:41:33', '2021-06-05 04:41:33'),
(444, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 36, '', 0, '2021-06-05 04:41:33', '2021-06-05 04:41:33'),
(445, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 7, '', 1, '2021-06-05 04:53:59', '2021-06-05 04:53:59'),
(446, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 7, '', 1, '2021-06-05 04:54:28', '2021-06-05 04:54:28'),
(447, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 8, '', 0, '2021-06-05 04:59:59', '2021-06-05 04:59:59'),
(448, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 6, '', 1, '2021-06-05 05:01:55', '2021-06-05 05:01:55'),
(449, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 8, '', 1, '2021-06-05 05:04:10', '2021-06-05 05:04:10'),
(450, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 5, '', 2, '2021-06-05 05:05:26', '2021-06-05 05:05:26'),
(451, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 5, '', 2, '2021-06-05 05:05:31', '2021-06-05 05:05:31'),
(452, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 5, '', 1, '2021-06-05 05:05:37', '2021-06-05 05:05:37'),
(453, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 2, '', 2, '2021-06-05 05:32:00', '2021-06-05 05:32:00'),
(454, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 2, '', 2, '2021-06-05 05:32:22', '2021-06-05 05:32:22'),
(455, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 2, '', 2, '2021-06-05 05:32:27', '2021-06-05 05:32:27'),
(456, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 2, '', 2, '2021-06-05 05:32:31', '2021-06-05 05:32:31'),
(457, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 2, '', 1, '2021-06-05 05:35:08', '2021-06-05 05:35:08'),
(458, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 11, '', 1, '2021-06-05 05:38:12', '2021-06-05 05:38:12'),
(459, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 4, '', 1, '2021-06-05 05:38:12', '2021-06-05 05:38:12'),
(460, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 5, '', 0, '2021-06-05 06:02:16', '2021-06-05 06:02:16'),
(461, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 15, '', 0, '2021-06-05 06:02:16', '2021-06-05 06:02:16'),
(462, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 15, '', 1, '2021-06-05 06:02:30', '2021-06-05 06:02:30'),
(463, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 5, '', 1, '2021-06-05 06:02:30', '2021-06-05 06:02:30'),
(464, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 15, '', 1, '2021-06-05 06:04:05', '2021-06-05 06:04:05'),
(465, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 5, '', 1, '2021-06-05 06:04:05', '2021-06-05 06:04:05'),
(466, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 15, '', 1, '2021-06-05 06:05:48', '2021-06-05 06:05:48'),
(467, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 5, '', 1, '2021-06-05 06:05:48', '2021-06-05 06:05:48'),
(468, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 15, '', 1, '2021-06-05 06:08:48', '2021-06-05 06:08:48'),
(469, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 5, '', 1, '2021-06-05 06:08:48', '2021-06-05 06:08:48'),
(470, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 15, '', 1, '2021-06-05 23:05:29', '2021-06-05 23:05:29'),
(471, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 5, '', 1, '2021-06-05 23:05:29', '2021-06-05 23:05:29'),
(472, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 8, '', 2, '2021-06-05 23:07:19', '2021-06-05 23:07:19'),
(473, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 8, '', 2, '2021-06-05 23:07:26', '2021-06-05 23:07:26'),
(474, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 8, '', 2, '2021-06-05 23:07:36', '2021-06-05 23:07:36'),
(475, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 8, '', 2, '2021-06-05 23:07:45', '2021-06-05 23:07:45'),
(476, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 8, '', 2, '2021-06-05 23:07:58', '2021-06-05 23:07:58'),
(477, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 15, '', 1, '2021-06-05 23:19:31', '2021-06-05 23:19:31'),
(478, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 5, '', 1, '2021-06-05 23:19:31', '2021-06-05 23:19:31'),
(479, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 5, '', 2, '2021-06-05 23:19:38', '2021-06-05 23:19:38'),
(480, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 5, '', 2, '2021-06-05 23:19:38', '2021-06-05 23:19:38'),
(481, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 5, '', 2, '2021-06-05 23:19:43', '2021-06-05 23:19:43'),
(482, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 5, '', 2, '2021-06-05 23:19:43', '2021-06-05 23:19:43'),
(483, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 15, '', 1, '2021-06-05 23:46:04', '2021-06-05 23:46:04'),
(484, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 5, '', 1, '2021-06-05 23:46:04', '2021-06-05 23:46:04'),
(485, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 5, '', 2, '2021-06-05 23:46:11', '2021-06-05 23:46:11'),
(486, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 5, '', 2, '2021-06-05 23:46:11', '2021-06-05 23:46:11'),
(487, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 5, '', 2, '2021-06-05 23:46:16', '2021-06-05 23:46:16'),
(488, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 5, '', 2, '2021-06-05 23:46:16', '2021-06-05 23:46:16'),
(489, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 12, '', 1, '2021-06-06 00:05:36', '2021-06-06 00:05:36'),
(490, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 13, '', 0, '2021-06-06 00:22:34', '2021-06-06 00:22:34'),
(491, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 13, '', 1, '2021-06-06 00:26:50', '2021-06-06 00:26:50'),
(492, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 13, '', 1, '2021-06-06 00:27:20', '2021-06-06 00:27:20'),
(493, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 13, '', 1, '2021-06-06 00:29:08', '2021-06-06 00:29:08'),
(494, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 13, '', 1, '2021-06-06 00:29:27', '2021-06-06 00:29:27'),
(495, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 13, '', 1, '2021-06-06 00:29:45', '2021-06-06 00:29:45'),
(496, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 13, '', 1, '2021-06-06 00:40:29', '2021-06-06 00:40:29'),
(497, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 14, '', 0, '2021-06-06 00:43:22', '2021-06-06 00:43:22'),
(498, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 18, '', 0, '2021-06-06 02:41:28', '2021-06-06 02:41:28'),
(499, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 19, '', 0, '2021-06-06 02:41:58', '2021-06-06 02:41:58'),
(500, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 8, '', 0, '2021-06-06 02:46:31', '2021-06-06 02:46:31'),
(501, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 20, '', 0, '2021-06-06 02:50:40', '2021-06-06 02:50:40'),
(502, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 21, '', 0, '2021-06-06 02:51:02', '2021-06-06 02:51:02'),
(503, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 22, '', 0, '2021-06-06 02:51:18', '2021-06-06 02:51:18'),
(504, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 9, '', 0, '2021-06-06 02:51:24', '2021-06-06 02:51:24'),
(505, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 10, '', 2, '2021-06-06 02:52:40', '2021-06-06 02:52:40'),
(506, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 7, '', 0, '2021-06-06 02:52:45', '2021-06-06 02:52:45'),
(507, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 7, '', 0, '2021-06-06 02:53:05', '2021-06-06 02:53:05'),
(508, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 31, '', 0, '2021-06-06 02:53:30', '2021-06-06 02:53:30'),
(509, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 7, '', 0, '2021-06-06 02:53:33', '2021-06-06 02:53:33'),
(510, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 33, '', 0, '2021-06-06 02:57:45', '2021-06-06 02:57:45'),
(511, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 1, '', 0, '2021-06-06 02:57:50', '2021-06-06 02:57:50'),
(512, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 1, '', 0, '2021-06-06 02:59:11', '2021-06-06 02:59:11'),
(513, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 34, '', 0, '2021-06-06 03:00:41', '2021-06-06 03:00:41'),
(514, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 7, '', 0, '2021-06-06 03:00:45', '2021-06-06 03:00:45'),
(515, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 18, '', 2, '2021-06-06 03:01:32', '2021-06-06 03:01:32'),
(516, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 1, '', 0, '2021-06-06 03:01:54', '2021-06-06 03:01:54'),
(517, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 45, '', 0, '2021-06-06 03:05:20', '2021-06-06 03:05:20'),
(518, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 46, '', 0, '2021-06-06 03:05:37', '2021-06-06 03:05:37'),
(519, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 10, '', 0, '2021-06-06 03:05:51', '2021-06-06 03:05:51'),
(520, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 47, '', 0, '2021-06-06 03:07:36', '2021-06-06 03:07:36'),
(521, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 1, '', 0, '2021-06-06 03:07:43', '2021-06-06 03:07:43'),
(522, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-feedback', 'pusti_campaign_feedbacks', 5, '', 0, '2021-06-06 03:49:30', '2021-06-06 03:49:30'),
(523, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-feedback', 'pusti_campaign_feedbacks', 5, '', 1, '2021-06-06 03:56:02', '2021-06-06 03:56:02'),
(524, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-feedback', 'pusti_campaign_feedbacks', 5, '', 1, '2021-06-06 03:56:25', '2021-06-06 03:56:25'),
(525, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 8, '', 2, '2021-06-06 04:28:02', '2021-06-06 04:28:02'),
(526, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 11, '', 1, '2021-06-06 04:35:55', '2021-06-06 04:35:55'),
(527, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 11, '', 1, '2021-06-06 04:36:16', '2021-06-06 04:36:16'),
(528, 1, 'admin', 'http://localhost:8080/agri-marketing/cotton/configuration/region', 'master_regions', 1, '', 1, '2021-06-07 00:48:34', '2021-06-07 00:48:34'),
(529, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 12, '', 0, '2021-06-08 00:58:18', '2021-06-08 00:58:18'),
(530, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 13, '', 0, '2021-06-08 01:06:14', '2021-06-08 01:06:14'),
(531, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 6, '', 2, '2021-06-08 01:22:58', '2021-06-08 01:22:58'),
(532, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 7, '', 2, '2021-06-08 01:25:31', '2021-06-08 01:25:31'),
(533, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 6, '', 2, '2021-06-08 01:25:41', '2021-06-08 01:25:41'),
(534, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 7, '', 2, '2021-06-08 01:25:45', '2021-06-08 01:25:45'),
(535, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 6, '', 0, '2021-06-08 01:44:32', '2021-06-08 01:44:32'),
(536, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 16, '', 0, '2021-06-08 01:44:32', '2021-06-08 01:44:32'),
(537, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 7, '', 0, '2021-06-08 01:46:27', '2021-06-08 01:46:27'),
(538, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 17, '', 0, '2021-06-08 01:46:27', '2021-06-08 01:46:27'),
(539, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 8, '', 0, '2021-06-08 01:50:08', '2021-06-08 01:50:08'),
(540, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 18, '', 0, '2021-06-08 01:50:08', '2021-06-08 01:50:08'),
(541, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 8, '', 0, '2021-06-08 02:41:40', '2021-06-08 02:41:40'),
(542, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 11, '', 0, '2021-06-08 02:45:08', '2021-06-08 02:45:08'),
(543, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 11, '', 0, '2021-06-08 02:46:02', '2021-06-08 02:46:02'),
(544, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 10, '', 0, '2021-06-08 02:48:37', '2021-06-08 02:48:37'),
(545, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 25, '', 0, '2021-06-08 03:27:42', '2021-06-08 03:27:42'),
(546, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 14, '', 2, '2021-06-08 03:36:02', '2021-06-08 03:36:02'),
(547, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 15, '', 2, '2021-06-08 03:36:14', '2021-06-08 03:36:14'),
(548, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 16, '', 2, '2021-06-08 03:37:11', '2021-06-08 03:37:11'),
(549, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 13, '', 2, '2021-06-08 03:43:32', '2021-06-08 03:43:32'),
(550, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 17, '', 2, '2021-06-08 03:50:47', '2021-06-08 03:50:47'),
(551, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 23, '', 2, '2021-06-08 03:51:04', '2021-06-08 03:51:04'),
(552, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 8, '', 2, '2021-06-08 03:51:16', '2021-06-08 03:51:16'),
(553, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 8, '', 2, '2021-06-08 03:51:16', '2021-06-08 03:51:16'),
(554, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 1, '', 2, '2021-06-08 03:52:53', '2021-06-08 03:52:53'),
(555, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 17, '', 2, '2021-06-08 03:56:11', '2021-06-08 03:56:11'),
(556, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 16, '', 2, '2021-06-08 03:56:17', '2021-06-08 03:56:17'),
(557, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 14, '', 2, '2021-06-08 03:56:21', '2021-06-08 03:56:21');
INSERT INTO `user_logs` (`id`, `user_id`, `username`, `menu_name`, `table_name`, `data_id`, `ip`, `execution_type`, `created_at`, `updated_at`) VALUES
(558, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 12, '', 0, '2021-06-08 23:53:03', '2021-06-08 23:53:03'),
(559, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 12, '', 0, '2021-06-08 23:54:01', '2021-06-08 23:54:01'),
(560, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 47, '', 2, '2021-06-09 00:15:22', '2021-06-09 00:15:22'),
(561, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 46, '', 2, '2021-06-09 00:15:23', '2021-06-09 00:15:23'),
(562, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 45, '', 2, '2021-06-09 00:15:24', '2021-06-09 00:15:24'),
(563, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 34, '', 2, '2021-06-09 00:15:34', '2021-06-09 00:15:34'),
(564, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 33, '', 2, '2021-06-09 00:15:36', '2021-06-09 00:15:36'),
(565, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 31, '', 2, '2021-06-09 00:15:38', '2021-06-09 00:15:38'),
(566, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 2, '', 2, '2021-06-09 00:15:50', '2021-06-09 00:15:50'),
(567, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 1, '', 2, '2021-06-09 00:15:51', '2021-06-09 00:15:51'),
(568, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 1, '', 0, '2021-06-09 00:31:30', '2021-06-09 00:31:30'),
(569, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 48, '', 0, '2021-06-09 00:33:20', '2021-06-09 00:33:20'),
(570, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 49, '', 0, '2021-06-09 00:40:20', '2021-06-09 00:40:20'),
(571, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 50, '', 0, '2021-06-09 01:14:06', '2021-06-09 01:14:06'),
(572, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 13, '', 0, '2021-06-09 01:31:59', '2021-06-09 01:31:59'),
(573, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 52, '', 0, '2021-06-09 01:33:40', '2021-06-09 01:33:40'),
(574, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 53, '', 0, '2021-06-09 01:35:03', '2021-06-09 01:35:03'),
(575, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 54, '', 0, '2021-06-09 01:49:42', '2021-06-09 01:49:42'),
(576, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 55, '', 0, '2021-06-09 01:49:49', '2021-06-09 01:49:49'),
(577, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 14, '', 0, '2021-06-09 01:50:06', '2021-06-09 01:50:06'),
(578, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 13, '', 0, '2021-06-09 02:25:31', '2021-06-09 02:25:31'),
(579, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 53, '', 2, '2021-06-09 06:15:16', '2021-06-09 06:15:16'),
(580, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 18, '', 1, '2021-06-10 02:41:30', '2021-06-10 02:41:30'),
(581, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 18, '', 2, '2021-06-10 02:41:38', '2021-06-10 02:41:38'),
(582, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 13, '', 1, '2021-06-10 02:56:49', '2021-06-10 02:56:49'),
(583, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 14, '', 0, '2021-06-10 02:58:36', '2021-06-10 02:58:36'),
(584, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 65, '', 0, '2021-06-10 03:01:44', '2021-06-10 03:01:44'),
(585, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 66, '', 0, '2021-06-10 03:01:44', '2021-06-10 03:01:44'),
(586, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 67, '', 0, '2021-06-10 03:01:44', '2021-06-10 03:01:44'),
(587, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 39, '', 0, '2021-06-10 03:01:44', '2021-06-10 03:01:44'),
(588, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 68, '', 0, '2021-06-10 03:04:27', '2021-06-10 03:04:27'),
(589, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 69, '', 0, '2021-06-10 03:04:27', '2021-06-10 03:04:27'),
(590, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 70, '', 0, '2021-06-10 03:04:27', '2021-06-10 03:04:27'),
(591, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 39, '', 0, '2021-06-10 03:04:27', '2021-06-10 03:04:27'),
(592, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 39, '', 2, '2021-06-10 03:04:32', '2021-06-10 03:04:32'),
(593, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 39, '', 2, '2021-06-10 03:04:36', '2021-06-10 03:04:36'),
(594, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 8, '', 1, '2021-06-10 04:57:45', '2021-06-10 04:57:45'),
(595, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 6, '', 2, '2021-06-10 05:07:50', '2021-06-10 05:07:50'),
(596, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 6, '', 2, '2021-06-10 05:07:54', '2021-06-10 05:07:54'),
(597, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 14, '', 1, '2021-06-10 05:42:26', '2021-06-10 05:42:26'),
(598, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 15, '', 0, '2021-06-10 05:51:25', '2021-06-10 05:51:25'),
(599, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 57, '', 0, '2021-06-10 05:56:15', '2021-06-10 05:56:15'),
(600, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 58, '', 0, '2021-06-10 05:56:28', '2021-06-10 05:56:28'),
(601, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 8, '', 0, '2021-06-10 05:56:31', '2021-06-10 05:56:31'),
(602, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 34, '', 0, '2021-06-10 05:57:29', '2021-06-10 05:57:29'),
(603, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 59, '', 0, '2021-06-10 06:07:06', '2021-06-10 06:07:06'),
(604, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 35, '', 0, '2021-06-10 06:07:16', '2021-06-10 06:07:16'),
(605, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 60, '', 0, '2021-06-10 06:08:21', '2021-06-10 06:08:21'),
(606, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 36, '', 0, '2021-06-10 06:08:23', '2021-06-10 06:08:23'),
(607, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 15, '', 2, '2021-06-11 22:39:31', '2021-06-11 22:39:31'),
(608, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'master_campaigns', 18, '', 1, '2021-06-11 22:43:15', '2021-06-11 22:43:15'),
(609, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-request-manage', 'pusti_campaign_requests', 8, '', 1, '2021-06-11 22:43:15', '2021-06-11 22:43:15'),
(610, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 71, '', 0, '2021-06-11 22:52:07', '2021-06-11 22:52:07'),
(611, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 2, '', 0, '2021-06-11 22:52:07', '2021-06-11 22:52:07'),
(612, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events_details', 72, '', 0, '2021-06-11 22:52:53', '2021-06-11 22:52:53'),
(613, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/manage-campaign-event', 'pusti_campaign_events', 13, '', 0, '2021-06-11 22:52:53', '2021-06-11 22:52:53'),
(614, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-schedule-assign', 'pusti_campaign_schedules', 8, '', 1, '2021-06-11 22:54:42', '2021-06-11 22:54:42'),
(615, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-calendar', 'pusti_campaign_calendars', 14, '', 1, '2021-06-11 22:56:52', '2021-06-11 22:56:52'),
(616, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 11, '', 1, '2021-06-11 22:59:15', '2021-06-11 22:59:15'),
(617, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 11, '', 1, '2021-06-11 22:59:47', '2021-06-11 22:59:47'),
(618, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-attendance', 'pusti_campaign_attendances', 11, '', 1, '2021-06-11 23:00:08', '2021-06-11 23:00:08'),
(619, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-feedback', 'pusti_campaign_feedbacks', 5, '', 1, '2021-06-11 23:02:51', '2021-06-11 23:02:51'),
(620, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-feedback', 'pusti_campaign_feedbacks', 5, '', 1, '2021-06-11 23:03:02', '2021-06-11 23:03:02'),
(621, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-feedback', 'pusti_campaign_feedbacks', 5, '', 1, '2021-06-11 23:03:18', '2021-06-11 23:03:18'),
(622, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-feedback', 'pusti_campaign_feedbacks', 5, '', 1, '2021-06-11 23:03:30', '2021-06-11 23:03:30'),
(623, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-feedback', 'pusti_campaign_feedbacks', 5, '', 1, '2021-06-11 23:03:37', '2021-06-11 23:03:37'),
(624, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 61, '', 0, '2021-06-11 23:06:19', '2021-06-11 23:06:19'),
(625, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 7, '', 0, '2021-06-11 23:06:30', '2021-06-11 23:06:30'),
(626, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 62, '', 0, '2021-06-11 23:08:15', '2021-06-11 23:08:15'),
(627, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 1, '', 0, '2021-06-11 23:08:19', '2021-06-11 23:08:19'),
(628, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_mat_details', 66, '', 0, '2021-06-11 23:26:34', '2021-06-11 23:26:34'),
(629, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-material', 'pusti_campaign_materials', 1, '', 0, '2021-06-11 23:29:56', '2021-06-11 23:29:56'),
(630, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/birtan/campaign-feedback', 'pusti_campaign_feedbacks', 5, '', 1, '2021-06-11 23:31:02', '2021-06-11 23:31:02'),
(631, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 17, '', 2, '2021-06-11 23:40:56', '2021-06-11 23:40:56'),
(632, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 17, '', 2, '2021-06-11 23:42:58', '2021-06-11 23:42:58'),
(633, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 24, '', 2, '2021-06-11 23:47:10', '2021-06-11 23:47:10'),
(634, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 24, '', 2, '2021-06-11 23:47:14', '2021-06-11 23:47:14'),
(635, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 22, '', 2, '2021-06-11 23:47:20', '2021-06-11 23:47:20'),
(636, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 22, '', 2, '2021-06-11 23:47:24', '2021-06-11 23:47:24'),
(637, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 18, '', 2, '2021-06-11 23:48:08', '2021-06-11 23:48:08'),
(638, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 18, '', 1, '2021-06-11 23:58:24', '2021-06-11 23:58:24'),
(639, 1, 'admin', 'http://localhost:8080/agri-marketing-service/e-pusti/configuration/campaign', 'master_campaigns', 16, '', 2, '2021-06-11 23:58:37', '2021-06-11 23:58:37'),
(640, 1, 'admin', 'http://localhost:8081/agri-marketing-service/e-pusti/configuration/divisional-office', 'master_divisional_offices', 26, '', 0, '2021-06-12 04:07:16', '2021-06-12 04:07:16'),
(649, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_commodities', 24, '', 0, '2021-06-16 04:56:14', '2021-06-16 04:56:14'),
(650, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_commodities', 25, '', 0, '2021-06-16 04:56:15', '2021-06-16 04:56:15'),
(651, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_lease_value_years', 11, '', 0, '2021-06-16 04:56:15', '2021-06-16 04:56:15'),
(652, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_lease_value_years', 12, '', 0, '2021-06-16 04:56:15', '2021-06-16 04:56:15'),
(653, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_lease_value_years', 13, '', 0, '2021-06-16 04:56:15', '2021-06-16 04:56:15'),
(654, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_lease_value_years', 14, '', 0, '2021-06-16 04:56:15', '2021-06-16 04:56:15'),
(655, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_lease_value_years', 15, '', 0, '2021-06-16 04:56:15', '2021-06-16 04:56:15'),
(656, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_lease_value_years', 16, '', 0, '2021-06-16 04:56:15', '2021-06-16 04:56:15'),
(657, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 21, '', 0, '2021-06-16 04:56:15', '2021-06-16 04:56:15'),
(658, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_commodities', 26, '', 0, '2021-06-16 04:56:19', '2021-06-16 04:56:19'),
(659, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_commodities', 27, '', 0, '2021-06-16 04:56:19', '2021-06-16 04:56:19'),
(660, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_lease_value_years', 17, '', 0, '2021-06-16 04:56:19', '2021-06-16 04:56:19'),
(661, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_lease_value_years', 18, '', 0, '2021-06-16 04:56:19', '2021-06-16 04:56:19'),
(662, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_lease_value_years', 19, '', 0, '2021-06-16 04:56:19', '2021-06-16 04:56:19'),
(663, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_lease_value_years', 20, '', 0, '2021-06-16 04:56:19', '2021-06-16 04:56:19'),
(664, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_lease_value_years', 21, '', 0, '2021-06-16 04:56:19', '2021-06-16 04:56:19'),
(665, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_lease_value_years', 22, '', 0, '2021-06-16 04:56:19', '2021-06-16 04:56:19'),
(666, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 22, '', 0, '2021-06-16 04:56:19', '2021-06-16 04:56:19'),
(667, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 23, '', 0, '2021-06-16 04:56:48', '2021-06-16 04:56:48'),
(668, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 24, '', 0, '2021-06-16 04:56:55', '2021-06-16 04:56:55'),
(669, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 26, '', 0, '2021-06-16 04:58:08', '2021-06-16 04:58:08'),
(670, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 27, '', 0, '2021-06-16 04:58:41', '2021-06-16 04:58:41'),
(671, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_commodities', 28, '', 0, '2021-06-16 05:04:10', '2021-06-16 05:04:10'),
(672, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_commodities', 29, '', 0, '2021-06-16 05:04:10', '2021-06-16 05:04:10'),
(673, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_lease_value_years', 23, '', 0, '2021-06-16 05:04:10', '2021-06-16 05:04:10'),
(674, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_lease_value_years', 24, '', 0, '2021-06-16 05:04:10', '2021-06-16 05:04:10'),
(675, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_lease_value_years', 25, '', 0, '2021-06-16 05:04:10', '2021-06-16 05:04:10'),
(676, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_lease_value_years', 26, '', 0, '2021-06-16 05:04:10', '2021-06-16 05:04:10'),
(677, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_lease_value_years', 27, '', 0, '2021-06-16 05:04:10', '2021-06-16 05:04:10'),
(678, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_market_directory_lease_value_years', 28, '', 0, '2021-06-16 05:04:10', '2021-06-16 05:04:10'),
(679, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 31, '', 0, '2021-06-16 05:04:10', '2021-06-16 05:04:10'),
(680, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 32, '', 0, '2021-06-16 05:04:46', '2021-06-16 05:04:46'),
(681, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 33, '', 0, '2021-06-16 05:05:00', '2021-06-16 05:05:00'),
(682, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 34, '', 0, '2021-06-16 05:05:05', '2021-06-16 05:05:05'),
(683, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 43, '', 0, '2021-06-16 05:16:31', '2021-06-16 05:16:31'),
(684, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 49, '', 0, '2021-06-16 05:32:13', '2021-06-16 05:32:13'),
(685, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 50, '', 0, '2021-06-16 05:34:17', '2021-06-16 05:34:17'),
(686, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 51, '', 0, '2021-06-16 05:38:07', '2021-06-16 05:38:07'),
(687, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 52, '', 0, '2021-06-16 05:38:53', '2021-06-16 05:38:53'),
(688, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 53, '', 0, '2021-06-16 05:44:07', '2021-06-16 05:44:07'),
(689, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 54, '', 0, '2021-06-16 05:48:02', '2021-06-16 05:48:02'),
(690, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 55, '', 0, '2021-06-16 05:53:59', '2021-06-16 05:53:59'),
(691, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 56, '', 0, '2021-06-16 05:55:27', '2021-06-16 05:55:27'),
(692, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 57, '', 0, '2021-06-16 06:05:00', '2021-06-16 06:05:00'),
(693, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 58, '', 0, '2021-06-16 06:07:58', '2021-06-16 06:07:58'),
(694, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 59, '', 0, '2021-06-16 22:31:03', '2021-06-16 22:31:03'),
(695, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 60, '', 0, '2021-06-16 22:45:07', '2021-06-16 22:45:07'),
(696, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market', 'master_markets', 60, '', 2, '2021-06-16 22:59:13', '2021-06-16 22:59:13'),
(697, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market', 'master_markets', 60, '', 2, '2021-06-16 22:59:36', '2021-06-16 22:59:36'),
(698, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory?id=60', 'master_markets', 61, '', 0, '2021-06-16 23:13:25', '2021-06-16 23:13:25'),
(699, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory?id=61', 'master_markets', 62, '', 0, '2021-06-16 23:13:32', '2021-06-16 23:13:32'),
(700, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 63, '', 0, '2021-06-17 01:05:58', '2021-06-17 01:05:58'),
(701, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 64, '', 0, '2021-06-17 05:06:06', '2021-06-17 05:06:06'),
(702, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 65, '', 0, '2021-06-17 05:19:04', '2021-06-17 05:19:04'),
(703, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory?id=65', 'master_markets', 65, '', 1, '2021-06-17 05:22:38', '2021-06-17 05:22:38'),
(704, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory?id=65', 'master_markets', 65, '', 1, '2021-06-17 05:23:01', '2021-06-17 05:23:01'),
(705, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory?id=65', 'master_markets', 65, '', 1, '2021-06-17 05:23:19', '2021-06-17 05:23:19'),
(706, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory?id=65', 'master_markets', 65, '', 1, '2021-06-17 05:23:42', '2021-06-17 05:23:42'),
(707, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 66, '', 0, '2021-06-17 05:33:37', '2021-06-17 05:33:37'),
(708, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 67, '', 0, '2021-06-17 05:34:36', '2021-06-17 05:34:36'),
(709, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory?id=65', 'master_markets', 65, '', 1, '2021-06-17 05:35:22', '2021-06-17 05:35:22'),
(710, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory?id=67', 'master_markets', 67, '', 1, '2021-06-17 06:05:09', '2021-06-17 06:05:09'),
(711, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory?id=67', 'master_markets', 67, '', 1, '2021-06-17 06:07:15', '2021-06-17 06:07:15'),
(712, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 68, '', 0, '2021-06-20 00:36:31', '2021-06-20 00:36:31'),
(713, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 69, '', 0, '2021-06-20 22:48:39', '2021-06-20 22:48:39'),
(714, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 1, '', 0, '2021-06-22 05:06:24', '2021-06-22 05:06:24'),
(715, 1, 'admin', 'http://localhost:8082/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 1, '', 0, '2021-06-22 05:16:43', '2021-06-22 05:16:43'),
(716, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 2, '', 0, '2021-06-22 05:59:43', '2021-06-22 05:59:43'),
(717, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory?id=2', 'master_markets', 2, '', 1, '2021-06-23 02:08:51', '2021-06-23 02:08:51'),
(718, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory?id=1', 'master_markets', 1, '', 1, '2021-06-23 02:45:33', '2021-06-23 02:45:33'),
(719, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory?id=2', 'master_markets', 2, '', 1, '2021-06-23 03:42:22', '2021-06-23 03:42:22'),
(720, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 3, '', 0, '2021-06-23 04:26:59', '2021-06-23 04:26:59'),
(721, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory?id=2', 'master_markets', 2, '', 1, '2021-06-23 05:00:55', '2021-06-23 05:00:55'),
(722, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory?id=3', 'master_markets', 3, '', 1, '2021-06-23 06:26:24', '2021-06-23 06:26:24'),
(723, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 4, '', 0, '2021-06-23 22:32:36', '2021-06-23 22:32:36'),
(724, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory?id=2', 'master_markets', 2, '', 1, '2021-06-24 01:45:17', '2021-06-24 01:45:17'),
(725, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/measurement-unit', 'master_measurement_units', 6, '', 1, '2021-06-25 23:31:24', '2021-06-25 23:31:24'),
(726, 1, 'admin', 'http://localhost:8080/agri-marketing/crop-price-info/configuration/market-directory', 'master_markets', 5, '', 0, '2021-06-25 23:50:01', '2021-06-25 23:50:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `campaign_information`
--
ALTER TABLE `campaign_information`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campaign_information_campaign_id_foreign` (`campaign_id`),
  ADD KEY `campaign_information_divisional_office_id_foreign` (`divisional_office_id`);

--
-- Indexes for table `cpi_market_commodity_growers_prices`
--
ALTER TABLE `cpi_market_commodity_growers_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cpi_market_commodity_growers_price_details`
--
ALTER TABLE `cpi_market_commodity_growers_price_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cpi_market_commodity_prices`
--
ALTER TABLE `cpi_market_commodity_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cpi_market_commodity_price_details`
--
ALTER TABLE `cpi_market_commodity_price_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cpi_market_commodity_weekly_prices`
--
ALTER TABLE `cpi_market_commodity_weekly_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cpi_market_commodity_weekly_price_details`
--
ALTER TABLE `cpi_market_commodity_weekly_price_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cpi_price_collector_profiles`
--
ALTER TABLE `cpi_price_collector_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cpi_price_infos`
--
ALTER TABLE `cpi_price_infos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cpi_price_infos_pricedate_index` (`PriceDate`);

--
-- Indexes for table `dam_districts`
--
ALTER TABLE `dam_districts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `master_districts_division_id_foreign` (`division_id`);

--
-- Indexes for table `dam_divisions`
--
ALTER TABLE `dam_divisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dam_upazilas`
--
ALTER TABLE `dam_upazilas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `master_upazillas_district_id_foreign` (`district_id`);

--
-- Indexes for table `ginner_grower_profiles`
--
ALTER TABLE `ginner_grower_profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ginner_grower_profiles_applicant_id_unique` (`applicant_id`),
  ADD KEY `ginner_grower_profiles_region_id_foreign` (`region_id`),
  ADD KEY `ginner_grower_profiles_zone_id_foreign` (`zone_id`),
  ADD KEY `ginner_grower_profiles_unit_id_foreign` (`unit_id`);

--
-- Indexes for table `ginner_schedules`
--
ALTER TABLE `ginner_schedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ginner_schedules_cotton_variety_id_foreign` (`cotton_variety_id`),
  ADD KEY `ginner_schedules_cotton_id_foreign` (`cotton_id`),
  ADD KEY `ginner_schedules_hatt_id_foreign` (`hatt_id`),
  ADD KEY `ginner_schedules_seasons_id_foreign` (`seasons_id`),
  ADD KEY `ginner_schedules_applicant_id_foreign` (`applicant_id`);

--
-- Indexes for table `grower_central_stocks`
--
ALTER TABLE `grower_central_stocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grower_central_stocks_cotton_variety_id_foreign` (`cotton_variety_id`),
  ADD KEY `grower_central_stocks_cotton_id_foreign` (`cotton_id`),
  ADD KEY `grower_central_stocks_seasons_id_foreign` (`seasons_id`),
  ADD KEY `grower_central_stocks_applicant_id_foreign` (`applicant_id`);

--
-- Indexes for table `grower_hatt_manages`
--
ALTER TABLE `grower_hatt_manages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grower_hatt_manages_seasons_id_foreign` (`seasons_id`),
  ADD KEY `grower_hatt_manages_hatt_id_foreign` (`hatt_id`),
  ADD KEY `grower_hatt_manages_region_id_foreign` (`region_id`),
  ADD KEY `grower_hatt_manages_zone_id_foreign` (`zone_id`),
  ADD KEY `grower_hatt_manages_unit_id_foreign` (`unit_id`);

--
-- Indexes for table `grower_prod_achievements`
--
ALTER TABLE `grower_prod_achievements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grower_prod_achievements_cotton_variety_id_foreign` (`cotton_variety_id`),
  ADD KEY `grower_prod_achievements_cotton_id_foreign` (`cotton_id`),
  ADD KEY `grower_prod_achievements_seasons_id_foreign` (`seasons_id`),
  ADD KEY `grower_prod_achievements_applicant_id_foreign` (`applicant_id`);

--
-- Indexes for table `grower_sell_entry`
--
ALTER TABLE `grower_sell_entry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grower_sell_entry_cotton_variety_id_foreign` (`cotton_variety_id`),
  ADD KEY `grower_sell_entry_cotton_id_foreign` (`cotton_id`),
  ADD KEY `grower_sell_entry_hatt_id_foreign` (`hatt_id`),
  ADD KEY `grower_sell_entry_unit_id_foreign` (`unit_id`),
  ADD KEY `grower_sell_entry_seasons_id_foreign` (`seasons_id`),
  ADD KEY `grower_sell_entry_applicant_id_foreign` (`applicant_id`);

--
-- Indexes for table `hatt_notifications`
--
ALTER TABLE `hatt_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hatt_notifications_hatt_id_foreign` (`hatt_id`),
  ADD KEY `hatt_notifications_seasons_id_foreign` (`seasons_id`),
  ADD KEY `hatt_notifications_ginner_grower_id_foreign` (`ginner_grower_id`);

--
-- Indexes for table `linkage_gro_buy_profiles`
--
ALTER TABLE `linkage_gro_buy_profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `linkage_gro_buy_profiles_commodity_type_id_foreign` (`commodity_type_id`);

--
-- Indexes for table `linkage_gro_buy_pro_details`
--
ALTER TABLE `linkage_gro_buy_pro_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `linkage_gro_buy_pro_details_gro_buy_profiles_id_foreign` (`gro_buy_profiles_id`),
  ADD KEY `linkage_gro_buy_pro_details_commodity_group_id_foreign` (`commodity_group_id`),
  ADD KEY `linkage_gro_buy_pro_details_commodity_sub_group_id_foreign` (`commodity_sub_group_id`),
  ADD KEY `linkage_gro_buy_pro_details_commodity_id_foreign` (`commodity_id`);

--
-- Indexes for table `master_alert_percentages`
--
ALTER TABLE `master_alert_percentages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_campaigns`
--
ALTER TABLE `master_campaigns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_commodity_groups`
--
ALTER TABLE `master_commodity_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_commodity_names`
--
ALTER TABLE `master_commodity_names`
  ADD PRIMARY KEY (`id`),
  ADD KEY `master_commodity_names_commodity_group_id_foreign` (`commodity_group_id`),
  ADD KEY `master_commodity_names_commodity_sub_group_id_foreign` (`commodity_sub_group_id`),
  ADD KEY `master_commodity_names_unit_id_foreign` (`unit_id`),
  ADD KEY `master_commodity_names_price_type_id_foreign` (`price_type_id`);

--
-- Indexes for table `master_commodity_sub_groups`
--
ALTER TABLE `master_commodity_sub_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `master_commodity_sub_groups_commodity_group_id_foreign` (`commodity_group_id`);

--
-- Indexes for table `master_commodity_types`
--
ALTER TABLE `master_commodity_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_communication_linkages`
--
ALTER TABLE `master_communication_linkages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_cotton_names`
--
ALTER TABLE `master_cotton_names`
  ADD PRIMARY KEY (`id`),
  ADD KEY `master_cotton_names_cotton_variety_id_foreign` (`cotton_variety_id`);

--
-- Indexes for table `master_cotton_varities`
--
ALTER TABLE `master_cotton_varities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_divisional_offices`
--
ALTER TABLE `master_divisional_offices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_hatts`
--
ALTER TABLE `master_hatts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_mapping_parameters`
--
ALTER TABLE `master_mapping_parameters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_markets`
--
ALTER TABLE `master_markets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_market_directory_commodities`
--
ALTER TABLE `master_market_directory_commodities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `master_market_directory_commodities_market_directory_id_foreign` (`market_directory_id`);

--
-- Indexes for table `master_market_directory_lease_value_years`
--
ALTER TABLE `master_market_directory_lease_value_years`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_measurement_units`
--
ALTER TABLE `master_measurement_units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_price_types`
--
ALTER TABLE `master_price_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_regions`
--
ALTER TABLE `master_regions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_report_headers`
--
ALTER TABLE `master_report_headers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_seasons`
--
ALTER TABLE `master_seasons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_units`
--
ALTER TABLE `master_units`
  ADD PRIMARY KEY (`id`),
  ADD KEY `master_units_region_id_foreign` (`region_id`),
  ADD KEY `master_units_zone_id_foreign` (`zone_id`);

--
-- Indexes for table `master_zones`
--
ALTER TABLE `master_zones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `master_zones_region_id_foreign` (`region_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pm_health_measurement`
--
ALTER TABLE `pm_health_measurement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pm_health_measurement_socio_economic_info_id_foreign` (`socio_economic_info_id`);

--
-- Indexes for table `pm_physical_measurement`
--
ALTER TABLE `pm_physical_measurement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pm_physical_measurement_socio_economic_info_id_foreign` (`socio_economic_info_id`);

--
-- Indexes for table `pm_socio_economic_info`
--
ALTER TABLE `pm_socio_economic_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pusti_mapping_id` (`pusti_mapping_id`);

--
-- Indexes for table `pusti_campaign_attendances`
--
ALTER TABLE `pusti_campaign_attendances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pusti_campaign_attendances_campaign_id_foreign` (`campaign_id`),
  ADD KEY `pusti_campaign_attendances_divisional_office_id_foreign` (`divisional_office_id`);

--
-- Indexes for table `pusti_campaign_calendars`
--
ALTER TABLE `pusti_campaign_calendars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pusti_campaign_calendars_campaign_id_foreign` (`campaign_id`),
  ADD KEY `pusti_campaign_calendars_divisional_office_id_foreign` (`divisional_office_id`);

--
-- Indexes for table `pusti_campaign_events`
--
ALTER TABLE `pusti_campaign_events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pusti_campaign_events_campaign_id_foreign` (`campaign_id`);

--
-- Indexes for table `pusti_campaign_events_details`
--
ALTER TABLE `pusti_campaign_events_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pusti_campaign_events_details_pusti_campaign_event_id_foreign` (`pusti_campaign_event_id`);

--
-- Indexes for table `pusti_campaign_feedbacks`
--
ALTER TABLE `pusti_campaign_feedbacks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pusti_campaign_feedbacks_campaign_id_foreign` (`campaign_id`),
  ADD KEY `pusti_campaign_feedbacks_divisional_office_id_foreign` (`divisional_office_id`);

--
-- Indexes for table `pusti_campaign_informations`
--
ALTER TABLE `pusti_campaign_informations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pusti_campaign_informations_campaign_id_foreign` (`campaign_id`),
  ADD KEY `pusti_campaign_informations_divisional_office_id_foreign` (`divisional_office_id`);

--
-- Indexes for table `pusti_campaign_materials`
--
ALTER TABLE `pusti_campaign_materials`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pusti_campaign_materials_campaign_id_foreign` (`campaign_id`),
  ADD KEY `pusti_campaign_materials_divisional_office_id_foreign` (`divisional_office_id`);

--
-- Indexes for table `pusti_campaign_mat_details`
--
ALTER TABLE `pusti_campaign_mat_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pusti_campaign_mat_details_campaign_materials_id_foreign` (`campaign_materials_id`);

--
-- Indexes for table `pusti_campaign_requests`
--
ALTER TABLE `pusti_campaign_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pusti_campaign_requests_campaign_id_foreign` (`campaign_id`);

--
-- Indexes for table `pusti_campaign_schedules`
--
ALTER TABLE `pusti_campaign_schedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pusti_campaign_schedules_campaign_id_foreign` (`campaign_id`),
  ADD KEY `pusti_campaign_schedules_divisional_office_id_foreign` (`divisional_office_id`);

--
-- Indexes for table `user_logs`
--
ALTER TABLE `user_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username_index` (`username`),
  ADD KEY `uid_index` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `campaign_information`
--
ALTER TABLE `campaign_information`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cpi_market_commodity_growers_prices`
--
ALTER TABLE `cpi_market_commodity_growers_prices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cpi_market_commodity_growers_price_details`
--
ALTER TABLE `cpi_market_commodity_growers_price_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `cpi_market_commodity_prices`
--
ALTER TABLE `cpi_market_commodity_prices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cpi_market_commodity_price_details`
--
ALTER TABLE `cpi_market_commodity_price_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `cpi_market_commodity_weekly_prices`
--
ALTER TABLE `cpi_market_commodity_weekly_prices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cpi_market_commodity_weekly_price_details`
--
ALTER TABLE `cpi_market_commodity_weekly_price_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cpi_price_collector_profiles`
--
ALTER TABLE `cpi_price_collector_profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cpi_price_infos`
--
ALTER TABLE `cpi_price_infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `dam_districts`
--
ALTER TABLE `dam_districts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `dam_divisions`
--
ALTER TABLE `dam_divisions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `dam_upazilas`
--
ALTER TABLE `dam_upazilas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=493;

--
-- AUTO_INCREMENT for table `ginner_grower_profiles`
--
ALTER TABLE `ginner_grower_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `ginner_schedules`
--
ALTER TABLE `ginner_schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `grower_central_stocks`
--
ALTER TABLE `grower_central_stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `grower_hatt_manages`
--
ALTER TABLE `grower_hatt_manages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `grower_prod_achievements`
--
ALTER TABLE `grower_prod_achievements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `grower_sell_entry`
--
ALTER TABLE `grower_sell_entry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `hatt_notifications`
--
ALTER TABLE `hatt_notifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `linkage_gro_buy_profiles`
--
ALTER TABLE `linkage_gro_buy_profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `linkage_gro_buy_pro_details`
--
ALTER TABLE `linkage_gro_buy_pro_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `master_alert_percentages`
--
ALTER TABLE `master_alert_percentages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `master_campaigns`
--
ALTER TABLE `master_campaigns`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `master_commodity_groups`
--
ALTER TABLE `master_commodity_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `master_commodity_names`
--
ALTER TABLE `master_commodity_names`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `master_commodity_sub_groups`
--
ALTER TABLE `master_commodity_sub_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `master_commodity_types`
--
ALTER TABLE `master_commodity_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `master_communication_linkages`
--
ALTER TABLE `master_communication_linkages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `master_cotton_names`
--
ALTER TABLE `master_cotton_names`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `master_cotton_varities`
--
ALTER TABLE `master_cotton_varities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `master_divisional_offices`
--
ALTER TABLE `master_divisional_offices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `master_hatts`
--
ALTER TABLE `master_hatts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `master_mapping_parameters`
--
ALTER TABLE `master_mapping_parameters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `master_markets`
--
ALTER TABLE `master_markets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `master_market_directory_commodities`
--
ALTER TABLE `master_market_directory_commodities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `master_market_directory_lease_value_years`
--
ALTER TABLE `master_market_directory_lease_value_years`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `master_measurement_units`
--
ALTER TABLE `master_measurement_units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `master_price_types`
--
ALTER TABLE `master_price_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `master_regions`
--
ALTER TABLE `master_regions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `master_report_headers`
--
ALTER TABLE `master_report_headers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `master_seasons`
--
ALTER TABLE `master_seasons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `master_units`
--
ALTER TABLE `master_units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `master_zones`
--
ALTER TABLE `master_zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `pm_health_measurement`
--
ALTER TABLE `pm_health_measurement`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `pm_physical_measurement`
--
ALTER TABLE `pm_physical_measurement`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `pm_socio_economic_info`
--
ALTER TABLE `pm_socio_economic_info`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `pusti_campaign_attendances`
--
ALTER TABLE `pusti_campaign_attendances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `pusti_campaign_calendars`
--
ALTER TABLE `pusti_campaign_calendars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `pusti_campaign_events`
--
ALTER TABLE `pusti_campaign_events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `pusti_campaign_events_details`
--
ALTER TABLE `pusti_campaign_events_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `pusti_campaign_feedbacks`
--
ALTER TABLE `pusti_campaign_feedbacks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pusti_campaign_informations`
--
ALTER TABLE `pusti_campaign_informations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pusti_campaign_materials`
--
ALTER TABLE `pusti_campaign_materials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `pusti_campaign_mat_details`
--
ALTER TABLE `pusti_campaign_mat_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `pusti_campaign_requests`
--
ALTER TABLE `pusti_campaign_requests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pusti_campaign_schedules`
--
ALTER TABLE `pusti_campaign_schedules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_logs`
--
ALTER TABLE `user_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=727;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `campaign_information`
--
ALTER TABLE `campaign_information`
  ADD CONSTRAINT `campaign_information_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `master_campaigns` (`id`),
  ADD CONSTRAINT `campaign_information_divisional_office_id_foreign` FOREIGN KEY (`divisional_office_id`) REFERENCES `master_divisional_offices` (`id`);

--
-- Constraints for table `ginner_grower_profiles`
--
ALTER TABLE `ginner_grower_profiles`
  ADD CONSTRAINT `ginner_grower_profiles_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `master_regions` (`id`),
  ADD CONSTRAINT `ginner_grower_profiles_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `master_units` (`id`),
  ADD CONSTRAINT `ginner_grower_profiles_zone_id_foreign` FOREIGN KEY (`zone_id`) REFERENCES `master_zones` (`id`);

--
-- Constraints for table `ginner_schedules`
--
ALTER TABLE `ginner_schedules`
  ADD CONSTRAINT `ginner_schedules_applicant_id_foreign` FOREIGN KEY (`applicant_id`) REFERENCES `ginner_grower_profiles` (`id`),
  ADD CONSTRAINT `ginner_schedules_cotton_id_foreign` FOREIGN KEY (`cotton_id`) REFERENCES `master_cotton_names` (`id`),
  ADD CONSTRAINT `ginner_schedules_cotton_variety_id_foreign` FOREIGN KEY (`cotton_variety_id`) REFERENCES `master_cotton_varities` (`id`),
  ADD CONSTRAINT `ginner_schedules_hatt_id_foreign` FOREIGN KEY (`hatt_id`) REFERENCES `master_hatts` (`id`),
  ADD CONSTRAINT `ginner_schedules_seasons_id_foreign` FOREIGN KEY (`seasons_id`) REFERENCES `master_seasons` (`id`);

--
-- Constraints for table `grower_central_stocks`
--
ALTER TABLE `grower_central_stocks`
  ADD CONSTRAINT `grower_central_stocks_applicant_id_foreign` FOREIGN KEY (`applicant_id`) REFERENCES `ginner_grower_profiles` (`id`),
  ADD CONSTRAINT `grower_central_stocks_cotton_id_foreign` FOREIGN KEY (`cotton_id`) REFERENCES `master_cotton_names` (`id`),
  ADD CONSTRAINT `grower_central_stocks_cotton_variety_id_foreign` FOREIGN KEY (`cotton_variety_id`) REFERENCES `master_cotton_varities` (`id`),
  ADD CONSTRAINT `grower_central_stocks_seasons_id_foreign` FOREIGN KEY (`seasons_id`) REFERENCES `master_seasons` (`id`);

--
-- Constraints for table `grower_hatt_manages`
--
ALTER TABLE `grower_hatt_manages`
  ADD CONSTRAINT `grower_hatt_manages_hatt_id_foreign` FOREIGN KEY (`hatt_id`) REFERENCES `master_hatts` (`id`),
  ADD CONSTRAINT `grower_hatt_manages_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `master_regions` (`id`),
  ADD CONSTRAINT `grower_hatt_manages_seasons_id_foreign` FOREIGN KEY (`seasons_id`) REFERENCES `master_seasons` (`id`),
  ADD CONSTRAINT `grower_hatt_manages_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `master_units` (`id`),
  ADD CONSTRAINT `grower_hatt_manages_zone_id_foreign` FOREIGN KEY (`zone_id`) REFERENCES `master_zones` (`id`);

--
-- Constraints for table `grower_prod_achievements`
--
ALTER TABLE `grower_prod_achievements`
  ADD CONSTRAINT `grower_prod_achievements_applicant_id_foreign` FOREIGN KEY (`applicant_id`) REFERENCES `ginner_grower_profiles` (`id`),
  ADD CONSTRAINT `grower_prod_achievements_cotton_id_foreign` FOREIGN KEY (`cotton_id`) REFERENCES `master_cotton_names` (`id`),
  ADD CONSTRAINT `grower_prod_achievements_cotton_variety_id_foreign` FOREIGN KEY (`cotton_variety_id`) REFERENCES `master_cotton_varities` (`id`),
  ADD CONSTRAINT `grower_prod_achievements_seasons_id_foreign` FOREIGN KEY (`seasons_id`) REFERENCES `master_seasons` (`id`);

--
-- Constraints for table `grower_sell_entry`
--
ALTER TABLE `grower_sell_entry`
  ADD CONSTRAINT `grower_sell_entry_applicant_id_foreign` FOREIGN KEY (`applicant_id`) REFERENCES `ginner_grower_profiles` (`id`),
  ADD CONSTRAINT `grower_sell_entry_cotton_id_foreign` FOREIGN KEY (`cotton_id`) REFERENCES `master_cotton_names` (`id`),
  ADD CONSTRAINT `grower_sell_entry_cotton_variety_id_foreign` FOREIGN KEY (`cotton_variety_id`) REFERENCES `master_cotton_varities` (`id`),
  ADD CONSTRAINT `grower_sell_entry_hatt_id_foreign` FOREIGN KEY (`hatt_id`) REFERENCES `master_hatts` (`id`),
  ADD CONSTRAINT `grower_sell_entry_seasons_id_foreign` FOREIGN KEY (`seasons_id`) REFERENCES `master_seasons` (`id`),
  ADD CONSTRAINT `grower_sell_entry_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `master_units` (`id`);

--
-- Constraints for table `hatt_notifications`
--
ALTER TABLE `hatt_notifications`
  ADD CONSTRAINT `hatt_notifications_ginner_grower_id_foreign` FOREIGN KEY (`ginner_grower_id`) REFERENCES `ginner_grower_profiles` (`id`),
  ADD CONSTRAINT `hatt_notifications_hatt_id_foreign` FOREIGN KEY (`hatt_id`) REFERENCES `master_hatts` (`id`),
  ADD CONSTRAINT `hatt_notifications_seasons_id_foreign` FOREIGN KEY (`seasons_id`) REFERENCES `master_seasons` (`id`);

--
-- Constraints for table `linkage_gro_buy_profiles`
--
ALTER TABLE `linkage_gro_buy_profiles`
  ADD CONSTRAINT `linkage_gro_buy_profiles_commodity_type_id_foreign` FOREIGN KEY (`commodity_type_id`) REFERENCES `master_commodity_types` (`id`);

--
-- Constraints for table `linkage_gro_buy_pro_details`
--
ALTER TABLE `linkage_gro_buy_pro_details`
  ADD CONSTRAINT `linkage_gro_buy_pro_details_commodity_group_id_foreign` FOREIGN KEY (`commodity_group_id`) REFERENCES `master_commodity_groups` (`id`),
  ADD CONSTRAINT `linkage_gro_buy_pro_details_commodity_id_foreign` FOREIGN KEY (`commodity_id`) REFERENCES `master_commodity_names` (`id`),
  ADD CONSTRAINT `linkage_gro_buy_pro_details_commodity_sub_group_id_foreign` FOREIGN KEY (`commodity_sub_group_id`) REFERENCES `master_commodity_sub_groups` (`id`),
  ADD CONSTRAINT `linkage_gro_buy_pro_details_gro_buy_profiles_id_foreign` FOREIGN KEY (`gro_buy_profiles_id`) REFERENCES `linkage_gro_buy_profiles` (`id`);

--
-- Constraints for table `master_commodity_names`
--
ALTER TABLE `master_commodity_names`
  ADD CONSTRAINT `master_commodity_names_commodity_group_id_foreign` FOREIGN KEY (`commodity_group_id`) REFERENCES `master_commodity_groups` (`id`),
  ADD CONSTRAINT `master_commodity_names_commodity_sub_group_id_foreign` FOREIGN KEY (`commodity_sub_group_id`) REFERENCES `master_commodity_sub_groups` (`id`),
  ADD CONSTRAINT `master_commodity_names_price_type_id_foreign` FOREIGN KEY (`price_type_id`) REFERENCES `master_price_types` (`id`),
  ADD CONSTRAINT `master_commodity_names_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `master_units` (`id`);

--
-- Constraints for table `master_commodity_sub_groups`
--
ALTER TABLE `master_commodity_sub_groups`
  ADD CONSTRAINT `master_commodity_sub_groups_commodity_group_id_foreign` FOREIGN KEY (`commodity_group_id`) REFERENCES `master_commodity_groups` (`id`);

--
-- Constraints for table `master_cotton_names`
--
ALTER TABLE `master_cotton_names`
  ADD CONSTRAINT `master_cotton_names_cotton_variety_id_foreign` FOREIGN KEY (`cotton_variety_id`) REFERENCES `master_cotton_varities` (`id`);

--
-- Constraints for table `master_market_directory_commodities`
--
ALTER TABLE `master_market_directory_commodities`
  ADD CONSTRAINT `master_market_directory_commodities_market_directory_id_foreign` FOREIGN KEY (`market_directory_id`) REFERENCES `master_markets` (`id`);

--
-- Constraints for table `master_units`
--
ALTER TABLE `master_units`
  ADD CONSTRAINT `master_units_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `master_regions` (`id`),
  ADD CONSTRAINT `master_units_zone_id_foreign` FOREIGN KEY (`zone_id`) REFERENCES `master_zones` (`id`);

--
-- Constraints for table `master_zones`
--
ALTER TABLE `master_zones`
  ADD CONSTRAINT `master_zones_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `master_regions` (`id`);

--
-- Constraints for table `pm_health_measurement`
--
ALTER TABLE `pm_health_measurement`
  ADD CONSTRAINT `pm_health_measurement_socio_economic_info_id_foreign` FOREIGN KEY (`socio_economic_info_id`) REFERENCES `pm_socio_economic_info` (`id`);

--
-- Constraints for table `pm_physical_measurement`
--
ALTER TABLE `pm_physical_measurement`
  ADD CONSTRAINT `pm_physical_measurement_socio_economic_info_id_foreign` FOREIGN KEY (`socio_economic_info_id`) REFERENCES `pm_socio_economic_info` (`id`);

--
-- Constraints for table `pusti_campaign_attendances`
--
ALTER TABLE `pusti_campaign_attendances`
  ADD CONSTRAINT `pusti_campaign_attendances_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `master_campaigns` (`id`),
  ADD CONSTRAINT `pusti_campaign_attendances_divisional_office_id_foreign` FOREIGN KEY (`divisional_office_id`) REFERENCES `master_divisional_offices` (`id`);

--
-- Constraints for table `pusti_campaign_calendars`
--
ALTER TABLE `pusti_campaign_calendars`
  ADD CONSTRAINT `pusti_campaign_calendars_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `master_campaigns` (`id`),
  ADD CONSTRAINT `pusti_campaign_calendars_divisional_office_id_foreign` FOREIGN KEY (`divisional_office_id`) REFERENCES `master_divisional_offices` (`id`);

--
-- Constraints for table `pusti_campaign_events`
--
ALTER TABLE `pusti_campaign_events`
  ADD CONSTRAINT `pusti_campaign_events_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `master_campaigns` (`id`);

--
-- Constraints for table `pusti_campaign_events_details`
--
ALTER TABLE `pusti_campaign_events_details`
  ADD CONSTRAINT `pusti_campaign_events_details_pusti_campaign_event_id_foreign` FOREIGN KEY (`pusti_campaign_event_id`) REFERENCES `pusti_campaign_events` (`id`);

--
-- Constraints for table `pusti_campaign_feedbacks`
--
ALTER TABLE `pusti_campaign_feedbacks`
  ADD CONSTRAINT `pusti_campaign_feedbacks_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `master_campaigns` (`id`),
  ADD CONSTRAINT `pusti_campaign_feedbacks_divisional_office_id_foreign` FOREIGN KEY (`divisional_office_id`) REFERENCES `master_divisional_offices` (`id`);

--
-- Constraints for table `pusti_campaign_informations`
--
ALTER TABLE `pusti_campaign_informations`
  ADD CONSTRAINT `pusti_campaign_informations_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `master_campaigns` (`id`),
  ADD CONSTRAINT `pusti_campaign_informations_divisional_office_id_foreign` FOREIGN KEY (`divisional_office_id`) REFERENCES `master_divisional_offices` (`id`);

--
-- Constraints for table `pusti_campaign_materials`
--
ALTER TABLE `pusti_campaign_materials`
  ADD CONSTRAINT `pusti_campaign_materials_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `master_campaigns` (`id`),
  ADD CONSTRAINT `pusti_campaign_materials_divisional_office_id_foreign` FOREIGN KEY (`divisional_office_id`) REFERENCES `master_divisional_offices` (`id`);

--
-- Constraints for table `pusti_campaign_mat_details`
--
ALTER TABLE `pusti_campaign_mat_details`
  ADD CONSTRAINT `pusti_campaign_mat_details_campaign_materials_id_foreign` FOREIGN KEY (`campaign_materials_id`) REFERENCES `pusti_campaign_materials` (`id`);

--
-- Constraints for table `pusti_campaign_requests`
--
ALTER TABLE `pusti_campaign_requests`
  ADD CONSTRAINT `pusti_campaign_requests_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `master_campaigns` (`id`);

--
-- Constraints for table `pusti_campaign_schedules`
--
ALTER TABLE `pusti_campaign_schedules`
  ADD CONSTRAINT `pusti_campaign_schedules_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `master_campaigns` (`id`),
  ADD CONSTRAINT `pusti_campaign_schedules_divisional_office_id_foreign` FOREIGN KEY (`divisional_office_id`) REFERENCES `master_divisional_offices` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
