<?php

namespace App\Library;
use Illuminate\Support\Facades\DB;

class CommonDropdown 
{
    /* get all regions */
    public static function regionsList() {
        return DB::table('master_regions')->select('id as value','org_id','region_name as text','region_name as text_en','region_name_bn as text_bn', 'status')->get();
    }

    /* get all zones */
    public static function zonesList() {
        return DB::table('master_zones')->select('id as value','org_id','zone_name as text','zone_name as text_en','zone_name_bn as text_bn','region_id', 'status')->get();         
    }

    /* get all cotton variety */
    public static function cottonVaritiesList() {
        return DB::table('master_cotton_varities')->select('id as value','org_id','cotton_variety as text','cotton_variety as text_en','cotton_variety_bn as text_bn','status')->get();         
    }

    /* get all cotton names */
    public static function cottonNamesList() {
        return DB::table('master_cotton_names')->select('id  as value','org_id','cotton_variety_id','cotton_name as text','cotton_name as text_en','cotton_name_bn as text_bn','status')->get();         
    }

    /* get all hat names */
    public static function hatList() {
        return DB::table('master_hatts')->select('id  as value','org_id', 'division_id', 'district_id', 'upazilla_id','hatt_name as text','hatt_name as text_en','hatt_name_bn as text_bn','status')->get();         
    }

    /* get all master units */
    public static function unitList() {
        return DB::table('master_units')
        ->select('id as value','org_id', 'region_id', 'zone_id', 'district_id', 'upazilla_id','unit_name as text','unit_name as text_en','unit_name_bn as text_bn','status')
        ->get();         
    }

    /* get all master seasons */
    public static function seasonsList() {
        return DB::table('master_seasons')->select('id as value','org_id', 'season_name as text','season_name as text_en','season_name_bn as text_bn','status')->get();         
    }  

    /* get all master campaigns */
    public static function campaignNameList() {
        return DB::table('master_campaigns')->select('id as value', 'campaign_name as text', 'campaign_name as text_en','campaign_name_bn as text_bn', 'type_id', 'status')->get();        
    }  
    
    /* get all master commodity groups */
    public static function commodityGroupList()
    {
        return DB::table('master_commodity_groups')->select('id as value','org_id','group_name as text','group_name as text_en','group_name_bn as text_bn','status')->get();
    }
    /* get all master commodity sub group */
    public static function commoditySubGroupList()
    {
        return DB::table('master_commodity_sub_groups')->select('id as value','org_id', 'commodity_group_id','sub_group_name as text','sub_group_name as text_en','sub_group_name_bn as text_bn','status')->get();
    }  

    /* get all master price type list*/
    public static function priceTypeList()
    {
        return DB::table('master_price_types')->select('id as value','org_id','type_name as text','type_name as text_en','type_name_bn as text_bn','status')->get();
    }

    /* get all master divisional office list*/
    public static function divisionalOfficeList()
    {
        return DB::table('master_divisional_offices')->select('id as value', 'division_id','district_id', 'mobile_no', 'email', 'office_name as text','office_name as text_en','office_name_bn as text_bn','status')->get();
    }

    /* get all master commodity types */
    public static function commodityTypeList()
    {
        return DB::table('master_commodity_types')->select('id as value', 'type_name as text','type_name as text_en','type_name_bn as text_bn','status')->get();
    }

    /* get all master commodity list types */
    public static function commodityNameList()
    {
        return DB::table('master_commodity_names')->select('id as value', 'commodity_group_id', 'commodity_sub_group_id', 'commodity_name as text','commodity_name as text_en','commodity_name_bn as text_bn','status')->get();
    }

    /* get all master market list types */
    public static function marketList()
    {
        return DB::table('master_markets')->select('id as value', 'market_name as text','market_name as text_en','market_name_bn as text_bn', 'upazila_id')->where('status', 1)->get();
    }

    /* show alert percentage */
    public static function alertPercentageList()
    {
        return DB::table('master_alert_percentages')->select('id as value', 'alert_percentage as text','alert_percentage as text_en','alert_percentage as text_bn', 'fiscal_year_id')->get();
    }

    /* show ginner grower list*/
    public static function ginnerGrowerList()
    {
        return DB::table('ginner_grower_profiles')->select('id as value', 'name as text','name as text_en','name_bn as text_bn', 'father_name', 'father_name_bn', 'district_id', 'upazilla_id', 'unit_id', 'status', 'type', 'address', 'address_bn', 'mobile_no')->get();
    }
    /* show ginner grower list*/
    public static function leaseYearList()
    {
        return DB::table('master_lease_value_years')
            ->select('id as value', 'name as text','name as text_en','name_bn as text_bn', 'status')
            ->get();
    }

    /* show ginner grower list*/
    public static function measurementUnitList()
    {
        return DB::table('master_measurement_units')
            ->select('id as value', 'unit_name as text','unit_name as text_en','unit_name_bn as text_bn', 'status')
            ->get();
    }

    /**
     * Infrastructure List
     */
    public static function infrastructureList()
    {
        return DB::table('master_infrastructures')
            ->select('id as value', 'name as text','name as text_en','name_bn as text_bn', 'status')
            ->get();
    }

    /**
     * communication Linkage List 
     */
    public static function communicationLinkageList()
    {
        return DB::table('master_communication_linkages')
            ->select('id as value', 'name as text','name as text_en','name_bn as text_bn', 'status')
            ->get();
    }

    /**
     * Designation Of Product List 
     */
    public static function designationOfProductList()
    {
        return DB::table('master_destination_of_produces')
            ->select('id as value', 'name as text','name as text_en','name_bn as text_bn', 'status')
            ->get();
    }

    /**
     * Vehicle List
     */
    public static function vehicleList()
    {
        return DB::table('master_vehicles')
            ->select('id as value', 'name as text','name as text_en','name_bn as text_bn', 'status')
            ->get();
    }
}



