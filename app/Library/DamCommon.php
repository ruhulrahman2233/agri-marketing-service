<?php

namespace App\Library;
use Illuminate\Support\Facades\DB;

class DamCommon 
{
    public static function divisionList()
    {
      return DB::table('dam_divisions')
              ->select('id as value', 'division_title as text_en', 'division_title_bangla as text_bn', 'division_status', 'dam_id')
              ->get();
    }
  
    public static function districtList($divisionId = null)
    {
      $query = DB::table('dam_districts')
                ->select('id as value', 'district_title as text_en', 'district_title_bangla as text_bn', 'division_id', 'dam_id', 'district_status');
  
      if ($divisionId) {
        $query = $query->where('division_id', $divisionId);
      }
  
      return $query->get();
    }
  
    public static function upazilaList($districtId = null)
    {
      $query = DB::table('dam_upazilas')
                ->select('id as value', 'name as text_en', 'name_bangla as text_bn', 'district_id', 'dam_id', 'division_id', 'status');
  
      if ($districtId) {
        $query = $query->where('district_id', $districtId);
      }
  
      return $query->get();
    }
  
  
}



