<?php

namespace App\Models\MarketLinkage\Linkage;

use Illuminate\Database\Eloquent\Model;

class GrowerBuyerProfileDetails extends Model
{
    protected $table ="linkage_gro_buy_pro_details";
    
	protected $fillable = [
		'gro_buy_profiles_id',
		'commodity_group_id',
		'commodity_sub_group_id',
		'commodity_id',
		'price_type_id',	
		'unit_id',	
		'price',	
		'quantity',	
		'attachment',	
		'origin',	
		'origin_bn',	
		'remarks',	
		'remarks_bn'
    ];
}



