<?php

namespace App\Models\MarketLinkage\Linkage;

use Illuminate\Database\Eloquent\Model;

class GrowerBuyerProfile extends Model
{
    protected $table ="linkage_gro_buy_profiles";
    
	protected $fillable = [
		'applicant_id',
		'type',
		'commodity_type_id',
		'name',
		'name_bn',
		'father_name',
		'father_name_bn',
		'land_area',
		'division_id',
		'district_id',
		'upazilla_id',
		'union_id',
		'address',
		'address_bn',
		'mobile_no',
		'nid',
		'remarks',
		'remarks_bn',
		'app_rej_reason',
		'app_rej_reason_bn',
		'is_approved',
        'status',
		'created_by',
		'updated_by'
    ];

	public function details () {
		return $this->hasMany(GrowerBuyerProfileDetails::class, 'gro_buy_profiles_id');
	}
}
