<?php

namespace App\Models\MarketLinkage\Config;

use Illuminate\Database\Eloquent\Model;

class CommodityType extends Model
{
    protected $table ="master_commodity_types";
    
	protected $fillable = [
		'type_name',
		'type_name_bn',
        'status',
		'created_by',
		'updated_by'
    ];
}
