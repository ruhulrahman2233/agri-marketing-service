<?php

namespace App\Models\CropPriceInfo\Config;

use Illuminate\Database\Eloquent\Model;

class MasterMarkets extends Model
{
    protected $table="master_markets";

    protected $fillable =[
        'division_id',  
		'district_id',    
		'upazila_id',  
		'market_name',    
		'market_name_bn',  
		'foundation_year',     
		'village',     
		'post_office',  
		'union',  
		'market_type',  
		'govt_covered',  
		'govt_open',  
		'private_covered',     
		'private_open',    
		'total_market_area',  
		'shed_no',     
		'shed_area',   
		'stall_no_agri',    
		'stall_no_nonagri',   
		'hat_days',    
		'market_time_from',     
		'market_time_to',  
		'infrastructure_id',     
		'communication_linkage_id', 
		'number_of_buyers',   
		'number_of_sellers',     
		'farmer_share',     
		'trader_share',  
		'other_product_destination',  
		'product_destination',  
		'vehicle_id',    
		'avg_distance',     
		'data_collection_year',     
		'mobile_market_committee',
		'market_representative_name',
		'market_representative_mobile',
		'latitude',
		'longitude',
		'approval_status',
		'approved_date',
		'dam_id',
		'created_by',	
		'updated_by',   
		'status'
    ];
	protected $casts = [
        'hat_days' => 'array',
        'infrastructure_id' => 'array',
        'communication_linkage_id' => 'array',
        'vehicle_id' => 'array',
        'product_destination' => 'array',
    ];

	public function setHattDaysAttribute ($value)
    {
        $properties = [];

        if ($value) {
            foreach ($value as $array_item) {
                $properties[] = (int) $array_item;
            }
        }

        $this->attributes['hat_days'] = json_encode($properties);
    }

	public function setInfrastructureAttribute ($value)
    {
        $properties = [];

        if ($value) {
            foreach ($value as $array_item) {
                $properties[] = (int) $array_item;
            }
        }

        $this->attributes['infrastructure_id'] = json_encode($properties);
    }

	public function setCommounicationLinkageAttribute($value)
    {
        $properties = [];

        if ($value) {
            foreach ($value as $array_item) {
                $properties[] = (int) $array_item;
            }
        }

        $this->attributes['communication_linkage_id'] = json_encode($properties);
    }
	public function setvehicleAttribute ($value)
    {
        $properties = [];

        if ($value) {
            foreach ($value as $array_item) {
                $properties[] = (int) $array_item;
            }
        }

        $this->attributes['vehicle_id'] = json_encode($properties);
    }

	public function setProductionDesignationAttribute ($value)
    {
        $properties = [];

        if ($value) {
            foreach ($value as $array_item) {
                $properties[] = (int) $array_item;
            }
        }

        $this->attributes['product_destination'] = json_encode($properties);
    }

	public static function boot()
    {
        parent::boot();
    }
	public function market_directory_commodities()
    {
        return $this->hasMany('App\Models\CropPriceInfo\Config\MasterMarketDirectoryCommodities', 'market_directory_id');
    }
	public function market_lease_year()
    {
        return $this->hasMany('App\Models\CropPriceInfo\Config\MasterMarketDirectoryLeaseValueYear', 'market_directory_id');
    }
    public function marketing_items()
    {
        return $this->hasMany('App\Models\CropPriceInfo\Config\MasterMarketDirectoryCommodities', 'market_directory_id', 'id');
    }
    public function lease_value()
    {
        return $this->hasMany('App\Models\CropPriceInfo\Config\MasterMarketDirectoryLeaseValueYear', 'market_directory_id', 'id');
    }
}
