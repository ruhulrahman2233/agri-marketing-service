<?php

namespace App\Models\CropPriceInfo\Config;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelSetup;

class MasterCommunicationLinkage extends Model
{
    protected $table ="master_communication_linkages";

	protected $fillable = [
		'name',
		'name_bn',
        'created_by',
        'updated_by',
        'status',
        'dam_id',
    ];
}
