<?php

namespace App\Models\CropPriceInfo\Config;

use Illuminate\Database\Eloquent\Model;

class DamDivision extends Model
{
    protected $table ="dam_divisions";

    protected $fillable = [
		'id',
		'division_title',
        'division_title_bangla',
		'division_status',
        'dam_id'
    ];
}
