<?php

namespace App\Models\CropPriceInfo\Config;

use Illuminate\Database\Eloquent\Model;

class CpiPriceInfos extends Model
{
    protected $table="cpi_price_infos";

    protected $fillable =[
        'PriceDate', 	
		'LatitudeRecorded', 	
		'LongitudeRecorded',	
		'HighestPrice', 	
		'LowestPrice', 	
		'AveragePrice', 	
		'PriceUSDRate', 	
		'PriceEuroRate', 	
		'VerificationDate', 	
		'VerificationTime', 	
		'PriceType_id', 	
		'Market_id', 	
		'Subdistrict_id', 	
		'District_id', 	
		'Division_id', 	
		'Commodity_id', 	
		'BuyerSellerType', 	
		'DailyVolume', 	
		'MarketPriceEnteredBy', 	
		'Verified_by' 	
    ];
}
