<?php

namespace App\Models\CropPriceInfo\Config;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelSetup;

class MasterCommodityName extends Model
{
    use ModelSetup;
    protected $table="master_commodity_names";

    protected $fillable =[
        'org_id',
        'commodity_group_id',
        'commodity_sub_group_id',
        'commodity_name',  
		'commodity_name_bn',
        'unit_grower',
        'unit_whole_sale',
        'unit_retail',
        'price_type_id',
		'created_by',	
		'updated_by',   
		'status',
		'dam_id',   
    ];
    public function unit_grower()
    {
        return $this->belongsTo('App\Models\CropPriceInfo\Config\MasterMeasurementUnit','unit_grower');
    }
    public function unit_whole_sale()
    {
        return $this->belongsTo('App\Models\CropPriceInfo\Config\MasterMeasurementUnit','unit_whole_sale');
    }
    public function unit_retail()
    {
        return $this->belongsTo('App\Models\CropPriceInfo\Config\MasterMeasurementUnit','unit_retail');
    }
}
