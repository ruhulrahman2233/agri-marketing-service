<?php

namespace App\Models\CropPriceInfo\Config;

use Illuminate\Database\Eloquent\Model;

class MasterMarketDirectoryCommodities extends Model
{
    protected $table = "master_market_directory_commodities";

    protected $fillable = [
        'market_directory_id','commodity','unit','quantity', 'dam_id'
    ];
}
