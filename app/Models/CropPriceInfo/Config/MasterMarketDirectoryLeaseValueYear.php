<?php

namespace App\Models\CropPriceInfo\Config;

use Illuminate\Database\Eloquent\Model;

class MasterMarketDirectoryLeaseValueYear extends Model
{
    protected $table = "master_market_directory_lease_value_years";

    protected $fillable = [
        'market_directory_id', 'lease_year_id','value', 'dam_id'
    ];
}
