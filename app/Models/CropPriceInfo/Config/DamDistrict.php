<?php

namespace App\Models\CropPriceInfo\Config;

use Illuminate\Database\Eloquent\Model;

class DamDistrict extends Model
{
    protected $table ="dam_districts";
    protected $fillable = [
		'id',
		'division_id',
		'district_title',
        'district_title_bangla',
		'district_status',
        'dam_id'
    ];

}
