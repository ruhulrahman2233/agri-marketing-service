<?php

namespace App\Models\CropPriceInfo\Config;

use Illuminate\Database\Eloquent\Model;

class MasterWeek extends Model
{
    protected $table ="master_weeks";

	protected $fillable = [
        'id',
		'year',
		'week',
        'month',
        'starting_date',
        'ending_date',
        'dam_id',
    ];
}
