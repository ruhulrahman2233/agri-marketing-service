<?php

namespace App\Models\CropPriceInfo\Config;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelSetup;

class MasterPriceType extends Model
{
	use ModelSetup;
    protected $table ="master_price_types";

    protected $fillable = [
    	'org_id', 	
		'type_name', 	
		'type_name_bn',	
		'status', 
		'created_by', 	
		'updated_by' ,
		'dam_id' 
    ];
}
