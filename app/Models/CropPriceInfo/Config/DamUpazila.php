<?php

namespace App\Models\CropPriceInfo\Config;

use Illuminate\Database\Eloquent\Model;

class DamUpazila extends Model
{
    protected $table ="dam_upazilas";

    protected $fillable = [
		'id',
		'division_id',
		'district_id',
        'name',
		'name_bangla',
		'status',
        'dam_id'
    ];
}
