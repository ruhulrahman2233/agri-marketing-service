<?php

namespace App\Models\CropPriceInfo\Config;

use Illuminate\Database\Eloquent\Model;

class MasterAlertPercentages extends Model
{
    protected $table="master_alert_percentages";

    protected $fillable =[
        'fiscal_year_id', 	
		'alert_percentage',
		'created_by',	
		'updated_by',   
		'status'
    ];
}
