<?php

namespace App\Models\CropPriceInfo\Config;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelSetup;

class MasterMeasurementUnit extends Model
{
	use ModelSetup;
    protected $table ="master_measurement_units";
 	
	protected $fillable = [ 	
		'unit_name', 	
		'unit_name_bn',
		'status',
		'created_by',
		'updated_by',
		'dam_id'
    ];
}
