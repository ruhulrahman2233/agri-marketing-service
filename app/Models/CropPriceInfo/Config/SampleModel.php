<?php
namespace App\Models\CropPriceInfo\Config;

use Illuminate\Database\Eloquent\Model;

class SampleModel extends Model
{
	protected $table ="sample_table";
	
	protected $fillable = [
		'fiscal_year_id', 	
		'applicant_id', 	
		'hatt_id', 	
		'cotton_variety_id', 	
		'cotton_id', 	
		'hatt_date', 	
		'quantity', 	
		'unit_id',	
		'price',
		'status',
		'created_by',
		'updated_by'
    ];
}
