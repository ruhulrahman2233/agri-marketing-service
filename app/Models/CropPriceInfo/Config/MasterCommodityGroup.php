<?php

namespace App\Models\CropPriceInfo\Config;

use Illuminate\Database\Eloquent\Model;

class MasterCommodityGroup extends Model
{
 	protected $table ="master_commodity_groups";
	protected $fillable = [
		'org_id',
		'group_name',
		'group_name_bn',
        'status',
		'created_by',
		'updated_by'
    ];

	
}
