<?php

namespace App\Models\CropPriceInfo\Config;

use Illuminate\Database\Eloquent\Model;

class MasterleaseValueYear extends Model
{
    protected $table ="master_lease_value_years";

	protected $fillable = [
		'id',
		'name',
		'name_bn',
        'status',
        'dam_id',
    ];
}
