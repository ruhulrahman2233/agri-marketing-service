<?php

namespace App\Models\CropPriceInfo\Config;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelSetup;

class MasterCommoditySubGroup extends Model
{
	use ModelSetup;

 	protected $table ="master_commodity_sub_groups";
 	
	protected $fillable = [
		'org_id', 	
		'commodity_group_id', 	
		'sub_group_name', 	
		'sub_group_name_bn',
		'status',
		'dam_id',
		'created_by',
		'updated_by'
    ];

}

