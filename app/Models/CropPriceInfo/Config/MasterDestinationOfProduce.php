<?php

namespace App\Models\CropPriceInfo\Config;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelSetup;

class MasterDestinationOfProduce extends Model
{
    use ModelSetup;
    protected $table ="master_destination_of_produces";

	protected $fillable = [
		'id',
		'name',
		'name_bn',
        'status',
		'created_by',
		'updated_by',
        'dam_id',
    ];
}
