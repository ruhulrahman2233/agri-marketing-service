<?php

namespace App\Models\CropPriceInfo\Cpi;

use Illuminate\Database\Eloquent\Model;

class MarketCommodityGrowerPriceDetail extends Model
{
    protected $table="cpi_market_commodity_growers_price_details";
    protected $fillable =[
        'price_table_id', 	
		'price_type', 	
		'com_grp_id', 	
		'com_subgrp_id', 	
		'commodity_id', 	
		'g_lowestPrice', 	
		'g_highestPrice', 	
		'g_avgPrice', 	
		'g_supply', 	
		'g_percentage_LtoH', 	
		'g_last_entry', 	
		'created_by', 	
		'updated_by', 	
		'verified_by', 	
		'verification_date', 	
		'comment_id', 	
		'comparison_date', 	
		'submit_status', 	
		'status', 		
		'dam_id',
		'unit_grower',
		'submitted_by'
    ];
	public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

	public function commodity_price()
    {
        return $this->belongsTo('App\Models\CropPriceInfo\Cpi\MarketCommodityPrice','price_table_id');
    }
	public function group()
    {
        return $this->belongsTo('App\Models\CropPriceInfo\Config\MasterCommodityGroup','com_grp_id');
    }
	public function sub_group()
    {
        return $this->belongsTo('App\Models\CropPriceInfo\Config\MasterCommoditySubGroup','com_subgrp_id');
    }
	public function commodity()
    {
        return $this->belongsTo('App\Models\CropPriceInfo\Config\MasterCommodityName','commodity_id');
    }
	public function division_id()
    {
        return $this->belongsTo('App\Models\CropPriceInfo\Config\MarketCommodityGrowerPrice','division_id');
    }
}
