<?php

namespace App\Models\CropPriceInfo\Cpi;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelSetup;

class MarketCommodityPrice extends Model
{
    use ModelSetup;

    protected $table="cpi_market_commodity_prices";

    protected $fillable =[
        'price_entry_type',
        'price_type_id',
        'select_type',
        'market_id', 	
		'division_id', 	
		'district_id',	
		'upazila_id', 	
		'dam_id', 	
		'price_date',
        'year',
        'month_id',
        'week_id'
    ];

	public function commodity_list()
    {
        return $this->hasMany('App\Models\CropPriceInfo\Cpi\MarketCommodityPriceDetail','price_table_id');
    }
}
