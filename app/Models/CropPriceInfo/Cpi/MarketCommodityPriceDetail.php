<?php

namespace App\Models\CropPriceInfo\Cpi;

use Illuminate\Database\Eloquent\Model;

class MarketCommodityPriceDetail extends Model
{
    protected $table="cpi_market_commodity_price_details";
	protected $casts = [
		'price_type_id' => 'array'
	 ];
    protected $fillable =[
        'price_table_id', 	
		'price_type_id', 	
		'com_grp_id', 	
		'com_subgrp_id', 	
		'commodity_id', 	
		'w_lowestPrice', 	
		'w_highestPrice', 	
		'w_supply', 	
		'r_lowestPrice', 	
		'r_highestPrice', 	
		'r_supply', 	
		'w_avgPrice', 	
		'r_avgPrice', 	
		'w_percentage_LtoH', 	
		'r_percentage_LtoH', 	
		'w_last_entry', 	
		'r_last_entry', 	
		'unit_retail', 	
		'unit_wholesale', 	
		'created_by', 	
		'updated_by', 	
		'verified_by', 	
		'verification_date', 	
		'comment_id', 	
		'comparison_date', 	
		'submit_status', 	
		'status', 		
		'dam_id'
    ];
	public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
	public function commodity_price()
    {
        return $this->belongsTo('App\Models\CropPriceInfo\Cpi\MarketCommodityPrice','price_table_id');
    }
	public function group()
    {
        return $this->belongsTo('App\Models\CropPriceInfo\Config\MasterCommodityGroup','com_grp_id')->withDefault();
    }
	public function sub_group()
    {
        return $this->belongsTo('App\Models\CropPriceInfo\Config\MasterCommoditySubGroup','com_subgrp_id')->withDefault();
    }
	public function commodity()
    {
        return $this->belongsTo('App\Models\CropPriceInfo\Config\MasterCommodityName','commodity_id')->withDefault();
    }
}
