<?php

namespace App\Models\CropPriceInfo\Cpi;

use Illuminate\Database\Eloquent\Model;

class MarketCommodityGrowerPrice extends Model
{

    protected $table="cpi_market_commodity_growers_prices";

    protected $fillable =[
        'price_entry_type',
        'price_type_id',
        'select_type',
        'market_id', 	
		'division_id', 	
		'district_id',	
		'upazila_id', 	
		'dam_id', 	
		'price_date'
    ];
	public function commodity_list()
    {
        return $this->hasMany('App\Models\CropPriceInfo\Cpi\MarketCommodityGrowerPriceDetail','price_table_id');
    }
}
