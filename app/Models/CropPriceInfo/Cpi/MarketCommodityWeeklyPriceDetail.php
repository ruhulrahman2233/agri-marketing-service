<?php

namespace App\Models\CropPriceInfo\Cpi;

use Illuminate\Database\Eloquent\Model;

class MarketCommodityWeeklyPriceDetail extends Model
{
    protected $table="cpi_market_commodity_weekly_price_details";
	protected $casts = [
		'price_type_id' => 'array'
	];
    protected $fillable =[
        'price_table_id', 	
		'price_type_id', 	
		'com_grp_id', 	
		'com_subgrp_id', 	
		'commodity_id', 	
		'w_lowestPrice', 	
		'w_highestPrice', 	
		'w_supply', 	
		'r_lowestPrice', 	
		'r_highestPrice', 	
		'r_supply', 	
		'w_avgPrice', 	
		'r_avgPrice', 	
		'w_percentage_LtoH', 	
		'r_percentage_LtoH', 	
		'w_last_entry', 	
		'r_last_entry', 	
		'g_lowestPrice', 	
		'g_highestPrice', 	
		'g_avgPrice', 	
		'g_supply', 	
		'g_percentage_LtoH', 	
		'g_last_entry',	
		'unit_retail',	
		'unit_wholesale',	
		'unit_grower',	
		'created_by', 	
		'updated_by', 	
		'verified_by', 	
		'verification_date', 	
		'comment_id', 	
		'comparison_date', 	
		'submit_status', 	
		'status', 		
		'dam_id'
    ];
	public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
	public function weekly_price()
    {
        return $this->belongsTo('App\Models\CropPriceInfo\Cpi\MarketCommodityWeeklyPrice','price_table_id');
    }
	public function group()
    {
        return $this->belongsTo('App\Models\CropPriceInfo\Config\MasterCommodityGroup','com_grp_id');
    }
	public function sub_group()
    {
        return $this->belongsTo('App\Models\CropPriceInfo\Config\MasterCommoditySubGroup','com_subgrp_id');
    }
	public function commodity()
    {
        return $this->belongsTo('App\Models\CropPriceInfo\Config\MasterCommodityName','commodity_id');
    }
}
