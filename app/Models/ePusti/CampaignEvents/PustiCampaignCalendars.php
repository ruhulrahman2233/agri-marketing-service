<?php

namespace App\Models\EPusti\CampaignEvents;

use Illuminate\Database\Eloquent\Model;

class PustiCampaignCalendars extends Model
{
    
    protected $table="pusti_campaign_calendars";

    protected $fillable =[
        'fiscal_year_id', 	
	 	// 'campaign_type_id', 	
	 	'campaign_id', 	
	 	'divisional_office_id', 	
	 	'division_id',	
	 	'district_id', 	
	 	'quantity', 	
	 	'start_date', 	
	 	'end_date', 	
	 	'budget', 	
	 	'remarks', 	
	 	'remarks_bn', 
		'created_by',	
		'updated_by' 
    ];
}
