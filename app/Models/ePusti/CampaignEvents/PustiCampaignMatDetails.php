<?php

namespace App\Models\EPusti\CampaignEvents;

use Illuminate\Database\Eloquent\Model;

class PustiCampaignMatDetails extends Model
{
    protected $table="pusti_campaign_mat_details";

    protected $fillable =[
        'campaign_materials_id', 	
        'material_name', 	
        'material_name_bn', 	
		'attachment'
    ];
}
