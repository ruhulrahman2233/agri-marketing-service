<?php

namespace App\Models\EPusti\CampaignEvents;

use Illuminate\Database\Eloquent\Model;

class PustiCampaignFeedbacks extends Model
{
    protected $table="pusti_campaign_feedbacks";

    protected $fillable =[
        'campaign_type_id', 	
		'campaign_id', 	
		'divisional_office_id', 	
		'school_name', 	
		'school_name_bn', 	
		'attachment', 	
		'upload_date', 
		'created_by',
		'updated_by'  
    ];
}
