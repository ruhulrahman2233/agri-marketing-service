<?php

namespace App\Models\EPusti\CampaignEvents;

use Illuminate\Database\Eloquent\Model;

class PustiCampaignEvents extends Model
{
    protected $table ="pusti_campaign_events";

    protected $fillable = [
		'fiscal_year_id',
		'campaign_id',
		'quantity',
		'start_date',
		'end_date',
		'attachment',
		'status',
		'created_by',
		'updated_by' 
    ];

    //Pusti Campaign Events Details 
    public function details()
    {
    	return $this->hasMany('App\Models\EPusti\CampaignEvents\PustiCampaignEventsDetails', 'pusti_campaign_event_id', 'id');
}	}
