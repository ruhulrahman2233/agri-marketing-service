<?php

namespace App\Models\EPusti\CampaignEvents;

use Illuminate\Database\Eloquent\Model;

class PustiCampaignEventsDetails extends Model
{
    protected $table ="pusti_campaign_events_details";

    protected $fillable = [
		'pusti_campaign_event_id',
		'division_id',
		'district_id'
    ];

    //Pusti Campaign Events 
    public function pusti_cmpign_evnt()
    {
        return $this->belongsTo('App\Models\EPusti\CampaignEvents\PustiCampaignEvents', 'pusti_campaign_event_id', 'id');
    }
}
