<?php

namespace App\Models\EPusti\CampaignEvents;

use Illuminate\Database\Eloquent\Model;

class PustiCampaignMaterials extends Model
{
    protected $table="pusti_campaign_materials";

    protected $fillable =[
        'fiscal_year_id', 	
        'campaign_id', 	
        'campaign_type_id', 	
		'divisional_office_id', 	
		'campaign_date', 
		'created_by',	
		'updated_by'
    ];
}
