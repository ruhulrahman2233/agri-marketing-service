<?php

namespace App\Models\EPusti\CampaignEvents;

use Illuminate\Database\Eloquent\Model;

class PustiCampaignRequests extends Model
{
    protected $table="pusti_campaign_requests";

    protected $fillable =[
        'campaign_id', 	
        'division_id', 	
		'district_id', 	
		'school_name', 	
		'school_name_bn', 	
		'name', 	
		'name_bn', 	
		'designation', 	
		'designation_bn', 	
		'email', 	
		'mobile_no', 	
		'start_date', 	
		'end_date', 	
		'campaign_reason', 	
		'campaign_reason_bn',
		'created_by',	
		'updated_by',   
		'status'
    ];
}
