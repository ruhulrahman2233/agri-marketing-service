<?php

namespace App\Models\EPusti\Birtan;

use Illuminate\Database\Eloquent\Model;

class CampaignSchedule extends Model
{
    protected $table ="pusti_campaign_schedules";
 	
	protected $fillable = [
        'fiscal_year_id', 	
        'campaign_id', 	
        'divisional_office_id', 	
		'quantity',
		'start_date',
		'end_date',
		'facilator_1',
		'facilator_2',
		'status',
		'created_by',
		'updated_by'
    ];
}
