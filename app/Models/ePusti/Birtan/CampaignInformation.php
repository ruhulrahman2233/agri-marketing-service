<?php

namespace App\Models\EPusti\Birtan;

use Illuminate\Database\Eloquent\Model;

class CampaignInformation extends Model
{
    protected $table = "pusti_campaign_informations";

    protected $fillable = [	
        'campaign_id', 	
        'divisional_office_id', 	
		'division_id',
		'district_id',
		'document_name',
		'document_name_bn',
		'campaign_date',
		'attachment',
		'created_by',
		'updated_by'
    ];
}
