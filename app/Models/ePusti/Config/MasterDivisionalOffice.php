<?php

namespace App\Models\EPusti\Config;

use Illuminate\Database\Eloquent\Model;

class MasterDivisionalOffice extends Model
{
    protected $table ="master_divisional_offices";
 	
	protected $fillable = [
		'office_name', 	
		'office_name_bn', 	
		'division_id', 	
		'district_id',
		'name',
		'name_bn',
		'email',
		'mobile_no',
		'address',
		'address_bn',
		'designation',
		'designation_bn',
		'remarks',
		'remarks_bn',
		'status',
		'created_by',
		'updated_by'
    ];
}
