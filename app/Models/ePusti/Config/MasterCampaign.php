<?php

namespace App\Models\EPusti\Config;

use Illuminate\Database\Eloquent\Model;

class MasterCampaign extends Model
{
    protected $table ="master_campaigns";
 	
	protected $fillable = [
		'campaign_name', 	
		'campaign_name_bn',
		'fiscal_year_id',
		'divisional_office_id',
		'status',
		'created_by',
		'updated_by'
    ];
}
