<?php

namespace App\Models\Cotton\GinnerGrower;

use Illuminate\Database\Eloquent\Model;

class GinnerSchedule extends Model
{
    protected $table = 'ginner_schedules';
    
    protected $fillable = [
        'fiscal_year_id', 'seasons_id','schedule_date', 'applicant_id', 'cotton_variety_id', 'hatt_id', 'cotton_id' ,'quantity', 'status'
    ];
}
