<?php
namespace app\Models\Cotton\GinnerGrower\GrowerStock;

use Illuminate\Database\Eloquent\Model;

class GrowerCentralStocks extends Model
{
	protected $table ="grower_central_stocks";
	
	protected $fillable = [
		'type', 	
		'applicant_id', 	
		'org_id', 	
		'fiscal_year_id', 	
		'seasons_id', 	
		'cotton_variety_id', 	
		'cotton_id', 	
		'quantity'
    ];
}

