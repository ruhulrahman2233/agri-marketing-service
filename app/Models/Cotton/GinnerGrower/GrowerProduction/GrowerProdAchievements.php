<?php

namespace App\Models\Cotton\GinnerGrower\GrowerProduction;

use Illuminate\Database\Eloquent\Model;

class GrowerProdAchievements extends Model
{
    protected $table ="grower_prod_achievements";

	protected $fillable = [
		'fiscal_year_id',
		'achievement_date',
		'applicant_id',
		'cotton_variety_id',
		'cotton_id',
        'seasons_id',
        'quantity',
		'remarks',
		'remarks_bn',
		'status',
		'created_by',
		'updated_by'
    ];
}
