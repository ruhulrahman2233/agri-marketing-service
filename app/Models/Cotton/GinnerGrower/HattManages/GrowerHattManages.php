<?php

namespace App\Models\Cotton\GinnerGrower\HattManages;

use Illuminate\Database\Eloquent\Model;

class GrowerHattManages extends Model
{
	protected $table ="grower_hatt_manages";
	
	protected $fillable = [
		'fiscal_year_id', 	
		'seasons_id',		
		'division_id', 	 	
		'district_id',
		'upazilla_id', 	 	
		'hatt_id', 	 	
		'region_id', 	 	
		'zone_id', 	 	
		'unit_id', 	 	
		'hatt_date', 
		'remarks', 	
		'remarks_bn',
		'address',
		'address_bn',
        'status'
    ];
}
