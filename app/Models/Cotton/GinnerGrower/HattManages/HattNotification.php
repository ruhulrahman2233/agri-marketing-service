<?php

namespace App\Models\Cotton\GinnerGrower\HattManages;

use Illuminate\Database\Eloquent\Model;

class HattNotification extends Model
{
    protected $table ="hatt_notifications";
	
	protected $fillable = [
		'hatt_id', 	
		'fiscal_year_id', 	
		'seasons_id',		
		'ginner_grower_id', 	 	
		'seen_status ',
		'message'
    ];
}
