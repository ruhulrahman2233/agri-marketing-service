<?php

namespace App\Models\Cotton\GinnerGrower\GrowerSell;

use Illuminate\Database\Eloquent\Model;

class GrowerSellEntry extends Model
{
	protected $table ="grower_sell_entry";
	
	protected $fillable = [
		'fiscal_year_id', 	
		'seasons_id', 	
		'applicant_id', 	
		'hatt_id', 	
		'cotton_variety_id', 	
		'cotton_id', 	
		'hatt_date', 	
		'quantity', 	
		'unit_id',	
		'price',
		'status',
		'created_by',
		'updated_by'
    ];
}
