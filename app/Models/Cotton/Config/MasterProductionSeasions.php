<?php

namespace App\Models\Cotton\Config;

use Illuminate\Database\Eloquent\Model;

class MasterProductionSeasions extends Model
{
    protected $table ="master_seasons";
	protected $fillable = [
		'season_name',
		'season_name_bn',
        'status',
		'created_by',
		'updated_by'
    ];
}
