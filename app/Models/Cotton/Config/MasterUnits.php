<?php

namespace App\Models\Cotton\Config;

use Illuminate\Database\Eloquent\Model;

class MasterUnits extends Model
{
    protected $table ="master_units";

    protected $fillable = [	
		'region_id', 	
		'zone_id',	
		'district_id', 	
		'upazilla_id',	
		'unit_name', 	
		'unit_name_bn', 	
		'status', 
		'created_by', 	
		'updated_by' 
    ];
}
