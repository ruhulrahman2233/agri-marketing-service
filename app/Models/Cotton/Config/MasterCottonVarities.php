<?php

namespace App\Models\Cotton\Config;

use Illuminate\Database\Eloquent\Model;

class MasterCottonVarities extends Model
{
    protected $table ="master_cotton_varities";

	 protected $fillable = [
		'cotton_variety', 	
		'cotton_variety_bn', 	
		'status', 
		'created_by', 	
		'updated_by' 
    ];
}
