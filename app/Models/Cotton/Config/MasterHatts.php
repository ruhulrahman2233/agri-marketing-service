<?php

namespace App\Models\Cotton\Config;

use Illuminate\Database\Eloquent\Model;

class MasterHatts extends Model
{
    
 	protected $table ="master_hatts";
	
	protected $fillable = [
		'division_id',
		'district_id',
		'upazilla_id',
		'hatt_name',
		'hatt_name_bn',
        'status',
		'created_by',
		'updated_by'
    ];
}

	