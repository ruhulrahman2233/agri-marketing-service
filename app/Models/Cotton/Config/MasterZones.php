<?php

namespace App\Models\Cotton\Config;

use Illuminate\Database\Eloquent\Model;

class MasterZones extends Model
{
    protected $table ="master_zones";

	protected $fillable = [
		'region_id',	
		'zone_name', 	
		'zone_name_bn', 	
		'status', 
		'created_by', 	
		'updated_by' 
    ];
}

