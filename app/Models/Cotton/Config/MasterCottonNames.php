<?php

namespace App\Models\Cotton\Config;

use Illuminate\Database\Eloquent\Model;

class MasterCottonNames extends Model
{
 	protected $table ="master_cotton_names";
 	
	protected $fillable = [
		'cotton_variety_id',
		'cotton_name',
		'cotton_name_bn',
		'created_by', 	
		'updated_by', 
        'status'
    ];

}

