<?php

namespace App\Models\Cotton\Config;

use Illuminate\Database\Eloquent\Model;

class MasterRegions extends Model
{
 	protected $table ="master_regions";
	protected $fillable = [
		'region_name',
		'region_name_bn',
        'status',
		'created_by',
		'updated_by'
    ];

}
