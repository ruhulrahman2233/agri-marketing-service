<?php

namespace App\Models\Cotton\Config;

use Illuminate\Database\Eloquent\Model;

class GinnerGrowerProfile extends Model
{
    protected $table = 'ginner_grower_profiles';
    protected $fillable = [
        'type', 'applicant_id', 'region_id' ,'district_id', 'upazilla_id', 'unit_id', 'zone_id', 'name', 'name_bn', 'father_name',
        'father_name_bn', 'land_area','address','address_bn','mobile_no','nid','password','status','created_by','updated_by'
    ];

    protected $guarded = ['view_password'];
}
