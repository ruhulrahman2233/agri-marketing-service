<?php

namespace App\Models\PriceCollectorProfile\Config;

use Illuminate\Database\Eloquent\Model;

class CpiPriceCollectorProfiles extends Model
{
    protected $table ="cpi_price_collector_profiles";
    
	protected $fillable = [
		'division_id', 	
		'district_id', 	
		'upazilla_id', 	
		'union_id', 	
		'name', 	
		'name_bn', 	
		'father_name', 	
		'father_name_bn', 	
		'designation',	
		'designation_bn', 	
		'address', 	
		'address_bn', 	
		'mobile_no', 	
		'nid', 	
		'latitude', 	
		'longitude', 	
		'remarks', 	
		'remarks_bn', 	
		'attachment', 	
		'signature',	
		'app_rej_reason', 	
		'app_rej_reason_bn', 	
		'is_approved', 
		'status',
		'created_by', 	
		'updated_by' 
    ];
}
