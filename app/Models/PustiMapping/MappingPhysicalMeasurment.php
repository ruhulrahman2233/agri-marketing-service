<?php

namespace App\Models\PustiMapping;

use Illuminate\Database\Eloquent\Model;

class MappingPhysicalMeasurment extends Model
{
    protected $table = 'pm_physical_measurement';
    protected $fillable = [
        'socio_economic_info_id','height','weight','head_circumference','chest_circumference','muac'
    ];

    public function social_info()
    {
        return $this->belongsTo('App\Models\PustiMapping\MappingPhysicalMeasurment','socio_economic_info_id','id');       
    }

    public static function createPhysicalInfo($requestData){
        try{
            $data = static::create($requestData);
            return $data;

        }catch(\Exception $e){   

            throw new \Exception($e->getMessage(), 1);               
        }
    }
}
