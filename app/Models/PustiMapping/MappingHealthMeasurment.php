<?php

namespace App\Models\PustiMapping;

use Illuminate\Database\Eloquent\Model;

class MappingHealthMeasurment extends Model
{
    protected $table = 'pm_health_measurement';
    protected $fillable = [
        'socio_economic_info_id','question_no_1','question_no_2','question_no_3','question_no_4','question_no_5','question_no_6','question_no_7','question_no_8','question_no_9','question_no_10','question_no_11','question_no_12','question_no_13','question_no_14','question_no_15','question_no_16','question_no_17','question_no_18'
    ];

    public function social_info()
    {
        return $this->belongsTo('App\Models\PustiMapping\MappingPhysicalMeasurment','socio_economic_info_id','id');       
    }
}
