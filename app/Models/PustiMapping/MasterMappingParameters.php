<?php

namespace App\Models\PustiMapping;

use Illuminate\Database\Eloquent\Model;

class MasterMappingParameters extends Model
{
    protected $table = 'master_mapping_parameters';
    protected $fillable = [
        'month','gender','range_from','range_to','z_score_type','z_score','status','created_by','updated_by'
    ];
}
