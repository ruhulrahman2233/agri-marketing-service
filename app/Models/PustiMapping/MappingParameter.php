<?php

namespace App\Models\PustiMapping;

use Illuminate\Database\Eloquent\Model;

class MappingParameter extends Model
{
    protected $table = 'master_mapping_parameters';
}
