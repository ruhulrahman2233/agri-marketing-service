<?php

namespace App\Models\PustiMapping;

use Illuminate\Database\Eloquent\Model;

class MappingSocioEconomicInfo extends Model
{
    protected $table = 'pm_socio_economic_info';
    protected $fillable = [
        'pusti_mapping_id','pusti_mapping_date','name_en','name_bn','father_name_en','father_name_bn','mother_name_en','mother_name_bn','age','gender','father_education_status','mother_education_status','earning_member','monthly_family_income','income_source','family_category','no_of_family_member','religion','area_type_id','division_id','district_id','city_corporation_id','ward_id','pauroshoba_id','upazila_id','union_id','village_en','village_bn','mobile_no','status','created_by','updated_by'
    ];

    public function physical_info()
    {
        return $this->hasOne('App\Models\PustiMapping\MappingPhysicalMeasurment','socio_economic_info_id','id');       
    }

    public function health_info()
    {
        return $this->hasOne('App\Models\PustiMapping\MappingHealthMeasurment','socio_economic_info_id','id');       
    }

    public static function createSocioInfo($requestData){
        try{
            $data = static::create($requestData);
            return $data->id;
        }catch(\Exception $e){   

            throw new \Exception($e->getMessage(), 1);               
        }
    }
}
