<?php
namespace app\Http\Validations\Cotton\Config;
use Illuminate\Validation\Rule;
use Validator;

class MasterUnitsValidations
{
    /**
     * Master Report Heading Validation
    */
    public static function validate ($request , $id = null)
    { 
        $region_id 		= $request->region_id;
        $zone_id 		= $request->zone_id;
        $unit_name 	    = $request->unit_name;
        $unit_name_bn	= $request->unit_name_bn;

        $validator = Validator::make($request->all(), [
            'region_id' 	=> 'required',
            'zone_id' 		=> 'required',
            'district_id' 	=> 'required',
            'upazilla_id' 	=> 'required',

            'unit_name' => [
                'required',
                Rule::unique('master_units')->where(function ($query) use($region_id,$zone_id, $unit_name, $id) {
                    $query->where('region_id', $region_id);
                    $query->where('zone_id', $zone_id);
                    $query->where('unit_name', $unit_name);
                    if ($id) {
                        $query =$query->where('id', '!=' ,$id);
                    }
                    return $query;             
                }),
            ],

            'unit_name_bn' => [
                'required',
                Rule::unique('master_units')->where(function ($query) use($region_id,$zone_id, $unit_name_bn, $id) {
                    $query->where('region_id', $region_id);
                    $query->where('zone_id', $zone_id);                    
                    $query->where('unit_name_bn', $unit_name_bn);
                    if ($id) {
                        $query =$query->where('id', '!=' ,$id);
                    }
                    return $query;             
                }),
            ]

        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


