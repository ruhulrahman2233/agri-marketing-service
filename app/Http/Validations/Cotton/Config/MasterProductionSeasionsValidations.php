<?php
namespace app\Http\Validations\Cotton\Config;
use Validator;

class MasterProductionSeasionsValidations
{
    /**
     * Master Seasion Validation
    */
    public static function validate ($request , $id = null)
    { 
        $validator = Validator::make($request->all(), [
            'season_name'    => 'required|unique:master_seasons,season_name,'.$id,
            'season_name_bn' => 'required|unique:master_seasons,season_name_bn,'.$id,
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


