<?php
namespace app\Http\Validations\Cotton\Config;
use Illuminate\Validation\Rule;
use Validator;

class MasterHattsValidations
{
    /**
     * Master hats Validation
    */
    public static function validate ($request , $id = null)
    { 
        $division_id 	= $request->division_id;
        $district_id 	= $request->district_id;
        $upazilla_id 	= $request->upazilla_id;
        $hatt_name 	    = $request->hatt_name;
        $hatt_name_bn	= $request->hatt_name_bn;

        $validator = Validator::make($request->all(), [
            'division_id' 	=> 'required',
            'district_id' 	=> 'required',
            'district_id' 	=> 'required',
            'upazilla_id' 	=> 'required',
            'hatt_name' => [
                'required',
                Rule::unique('master_hatts')->where(function ($query) use($division_id,$district_id, $upazilla_id, $hatt_name, $id) {
                    $query->where('division_id', $division_id);
                    $query->where('district_id', $district_id);
                    $query->where('upazilla_id', $upazilla_id);
                    $query->where('hatt_name', $hatt_name);
                    if ($id) {
                        $query =$query->where('id', '!=' ,$id);
                    }
                    return $query;             
                }),
            ],

            'hatt_name_bn' => [
                'required',
                Rule::unique('master_hatts')->where(function ($query) use($division_id,$district_id, $upazilla_id, $hatt_name_bn, $id) {
                    $query->where('division_id', $division_id);
                    $query->where('district_id', $district_id);  
                    $query->where('upazilla_id', $upazilla_id);                  
                    $query->where('hatt_name_bn', $hatt_name_bn);
                    if ($id) {
                        $query =$query->where('id', '!=' ,$id);
                    }
                    return $query;             
                }),
            ]

        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


