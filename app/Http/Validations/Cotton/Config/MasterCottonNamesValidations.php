<?php
namespace app\Http\Validations\Cotton\Config;
use Illuminate\Validation\Rule;
use Validator;

class MasterCottonNamesValidations
{
    /**
     * Master Report Heading Validation
    */
    public static function validate ($request , $id = null)
    { 
        $cotton_variety_id 	= $request->cotton_variety_id;
        $cotton_name 		= $request->cotton_name;
        $cotton_name_bn		= $request->cotton_name_bn;

        $validator = Validator::make($request->all(), [
            'cotton_variety_id' 	=> 'required',
            'cotton_name' => [
                'required',
                Rule::unique('master_cotton_names')->where(function ($query) use($cotton_variety_id, $cotton_name, $id) {
                    $query->where('cotton_variety_id', $cotton_variety_id);
                    $query->where('cotton_name', $cotton_name);
                    if ($id) {
                        $query =$query->where('id', '!=' ,$id);
                    }
                    return $query;             
                }),
            ],

            'cotton_name_bn' => [
                'required',
                Rule::unique('master_cotton_names')->where(function ($query) use($cotton_variety_id, $cotton_name_bn, $id) {
                    $query->where('cotton_variety_id', $cotton_variety_id);
                    $query->where('cotton_name_bn', $cotton_name_bn);
                    if ($id) {
                        $query =$query->where('id', '!=' ,$id);
                    }
                    return $query;             
                }),
            ]

        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


