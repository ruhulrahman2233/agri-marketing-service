<?php
namespace app\Http\Validations\Cotton\Config;
use Illuminate\Validation\Rule;
use Validator;

class MasterRegionsValidations
{
    /**
     * Master Report Heading Validation
    */
    public static function validate ($request , $id = null)
    { 
        $validator = Validator::make($request->all(), [
            'region_name'    => 'required|unique:master_regions,region_name,'.$id,
            'region_name_bn' => 'required|unique:master_regions,region_name_bn,'.$id,
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


