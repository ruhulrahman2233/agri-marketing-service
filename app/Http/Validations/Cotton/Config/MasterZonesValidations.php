<?php
namespace app\Http\Validations\Cotton\Config;
use Illuminate\Validation\Rule;
use Validator;

class MasterZonesValidations
{
    /**
     * Master Report Heading Validation
    */
    public static function validate ($request , $id = null)
    { 
        $region_id 		= $request->region_id;
        $zone_name 		= $request->zone_name;
        $zone_name_bn	= $request->zone_name_bn;

        $validator = Validator::make($request->all(), [
            'region_id' 	=> 'required',
            'zone_name' => [
                'required',
                Rule::unique('master_zones')->where(function ($query) use($region_id, $zone_name, $id) {
                    $query->where('region_id', $region_id);
                    $query->where('zone_name', $zone_name);
                    if ($id) {
                        $query =$query->where('id', '!=' ,$id);
                    }
                    return $query;             
                }),
            ],

            'zone_name_bn' => [
                'required',
                Rule::unique('master_zones')->where(function ($query) use($region_id, $zone_name_bn, $id) {
                    $query->where('region_id', $region_id);
                    $query->where('zone_name_bn', $zone_name_bn);
                    if ($id) {
                        $query = $query->where('id', '!=' ,$id);
                    }
                    return $query;             
                }),
            ]

        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


