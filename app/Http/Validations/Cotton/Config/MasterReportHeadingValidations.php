<?php
namespace app\Http\Validations\Cotton\Config;

use Illuminate\Support\Facades\Validator;

class  MasterReportHeadingValidations 
{
  /**
   * Report heading Validation
   */
  public static function validate($request, $id = 0)
  {
      $validator = Validator::make($request->all(), [
          'heading' => 'required|string',
          'heading_bn' => 'nullable|string',
          'address'  =>  'nullable|string',
          'address_bn'  =>  'nullable|string'
      ]);

      if ($validator->fails()) {
          return([
              'success' => false,
              'errors'  => $validator->errors()
          ]);
      }
      return ['success'=>true];
  }


}