<?php
namespace App\Http\Validations\Cotton\Config;
use Illuminate\Validation\Rule;
use Validator;

class GinnerGrowerProfileValidations
{
    /**
     * GinnerGrowerProfileValidations Validation
    */
    public static function validate ($request , $id = null)
    {
        $validator = Validator::make($request->all(), [
            'district_id' 	        => 'required',
            'father_name' 	        => 'required',
            'father_name_bn' 	    => 'required',
            'mobile_no' 	        => 'required|numeric|regex:/(01)[0-9]{9}/',
            'name' 	                => 'required',
            'name_bn' 	            => 'required',
            'region_id' 	        => 'required',
            'password_confirm' 	    => 'required|min:6',
            'password' 	            => 'min:6|required_with:password_confirm|same:password_confirm',
            'type' 	                => 'required',
            'unit_id' 	            => 'required',
            'upazilla_id' 	        => 'required',
            'zone_id' 	            => 'required',
        ]);


        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }

    /**
     * GinnerGrowerProfileValidations update Validation
    */
    public static function updateValidate ($request , $id = null)
    {
        $validator = Validator::make($request->all(), [
            'org_id' 				=> 'required',
            'address' 	            => 'required',
            'address_bn' 	        => 'required',
            'district_id' 	        => 'required',
            'father_name' 	        => 'required',
            'father_name_bn' 	    => 'required',
            'mobile_no' 	        => 'required',
            'name' 	                => 'required',
            'name_bn' 	            => 'required',
            'region_id' 	        => 'required',
            'password' 	            => 'min:6|required',
            'type' 	                => 'required',
            'unit_id' 	            => 'required',
            'upazilla_id' 	        => 'required',
            'zone_id' 	            => 'required',
        ]);


        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


