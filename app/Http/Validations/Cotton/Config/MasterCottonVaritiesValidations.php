<?php
namespace app\Http\Validations\Cotton\Config;
use Illuminate\Validation\Rule;
use Validator;

class MasterCottonVaritiesValidations
{
    /**
     * Master Report Heading Validation
    */
    public static function validate ($request , $id = null)
    { 

        $validator = Validator::make($request->all(), [
            'cotton_variety'    => 'required|unique:master_cotton_varities,cotton_variety,'.$id,
            'cotton_variety_bn' => 'required|unique:master_cotton_varities,cotton_variety_bn,'.$id
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


