<?php
namespace App\Http\Validations\Cotton\GinnerGrower\GrowerProduction;

use Illuminate\Support\Facades\Validator;

class GrowerProdAchievementsValidations
{
    /**
     * Hat Manage Validation
    */
    public static function validate ($request, $id = null)
    {
        //dd($request);

        $validator = Validator::make($request->all(), [
            'fiscal_year_id'        => 'required',
            'applicant_id'          => 'required',
            'seasons_id'            => 'required',
            'cotton_variety_id'     => 'required',
            'cotton_id'             => 'required',
            'achievement_date'      => 'required',
            'quantity'              => 'required'
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}