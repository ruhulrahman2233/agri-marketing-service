<?php
namespace app\Http\Validations\Cotton\GinnerGrower\HattManages;
use Illuminate\Support\Facades\Validator;

class GrowerHattManagesValidations
{
    /**
     * Hat Manage Validation
    */
    public static function validate ($request , $id = null)
    {
        $validator = Validator::make($request->all(), [
            'fiscal_year_id' 	=> 'required',
            'seasons_id' 	    => 'required',
            'hatt_id' 	        => 'required',
            'region_id' 	    => 'required',
            'zone_id' 	        => 'required',
            'unit_id' 	        => 'required',
            'hatt_date' 	    => 'required',
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


