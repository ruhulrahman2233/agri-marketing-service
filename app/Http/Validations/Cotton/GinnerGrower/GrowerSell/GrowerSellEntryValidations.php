<?php

namespace App\Http\Validations\Cotton\GinnerGrower\GrowerSell;

use Illuminate\Support\Facades\Validator;

class GrowerSellEntryValidations
{
    /**
     * Hat Manage Validation
    */
    public static function validate ($request , $id = null)
    {
        $validator = Validator::make($request->all(), [
            'fiscal_year_id' 		=> 'required',
            'seasons_id'            => 'required',
            'applicant_id' 	    	=> 'required',
            'hatt_id' 	        	=> 'required',
            'cotton_variety_id' 	=> 'required',
            'cotton_id' 	        => 'required',
            'hatt_date' 	    	=> 'required',
            'quantity' 	    		=> 'required',
            'unit_id' 	    		=> 'required',
            'price' 	    		=> 'required'
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}



