<?php
namespace App\Http\Validations\Cotton\GinnerGrower;
use Validator;

class ScheduleValidation
{
    /**
     * Master Report Heading Validation
    */
    public static function validate ($request , $id = null)
    {
        $validator = Validator::make($request->all(), [
            'fiscal_year_id'=> 'required',
            'schedule_date' => 'required',
            'seasons_id' 	=> 'required',
            'applicant_id' 	=> 'required',
            'cotton_variety_id' => 'required',
            'hatt_id' 	    => 'required',
            'cotton_id' 	=> 'required',
            'quantity' 	    => 'required',
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}
