<?php

namespace App\Http\Validations\MarketLinkage\Config;

use Illuminate\Support\Facades\Validator;

class  CommodityTypeValidations 
{
  /**
   * Report heading Validation
   */
  public static function validate($request, $id = 0)
  {
      $validator = Validator::make($request->all(), [
          'type_name' => 'required|string|unique:master_commodity_types,type_name,'.$id
      ]);

      if ($validator->fails()) {
          return([
              'success' => false,
              'errors'  => $validator->errors()
          ]);
      }
      return ['success'=>true];
  }


}