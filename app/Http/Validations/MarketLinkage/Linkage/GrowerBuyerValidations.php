<?php
namespace App\Http\Validations\MarketLinkage\Linkage;

use Validator;

class GrowerBuyerValidations
{
    /**
     *validations
     */
    public static function validate($request, $id = 0)
    {  
        $validator = Validator::make($request->all(), [
            'commodity_type_id' =>'required',
            'name'              =>'required',
            'father_name'       =>'required',
            'land_area'         =>'required',
            'division_id'       =>'required',
            'district_id'       =>'required',
            'upazilla_id'       =>'required',
            'union_id'          =>'required',
            'address'           =>'required',
            'mobile_no'         =>'required',
            'nid'               =>'required',
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }

}
