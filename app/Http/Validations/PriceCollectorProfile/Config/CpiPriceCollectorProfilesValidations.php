<?php

namespace App\Http\Validations\PriceCollectorProfile\Config;

use Illuminate\Support\Facades\Validator;

class  CpiPriceCollectorProfilesValidations 
{
  /**
   * Cpi Price Collector Profiles Validations
   */
  public static function validate($request, $id = 0)
  {
      $validator = Validator::make($request->all(), [
        'division_id'  		=> 'required', 	
        'district_id'  		=> 'required', 	
        'upazilla_id'  		=> 'required', 	
        'union_id'  		=> 'required', 	
        'name'  			=> 'required', 	
        'name_bn'  			=> 'required', 		
        'mobile_no'  		=> 'required', 	
        'latitude'  		=> 'required', 	
        'longitude'  		=> 'required'
      ]);

      if ($validator->fails()) {
          return([
              'success' => false,
              'errors'  => $validator->errors()
          ]);
      }
      return ['success'=>true];
  }


}
