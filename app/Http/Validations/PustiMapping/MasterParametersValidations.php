<?php
namespace app\Http\Validations\PustiMapping;

use Validator;

class MasterParametersValidations
{
    /**
     * Master Report Heading Validation
    */
    public static function validate ($request , $id = null)
    { 

        $validator = Validator::make($request->all(), [
            'month'             => 'required',
            'gender'            => 'required',
            'weight_range_from' => 'required',
            'weight_range_to'   => 'required',
            'height_range_from' => 'required',
            'height_range_to'   => 'required',
            'z_score'           => 'required'
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


