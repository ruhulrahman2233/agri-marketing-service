<?php
namespace app\Http\Validations\PustiMapping;

use Validator;

class MappingPhysicalValidations
{
    /**
     * Master Report Heading Validation
    */
    public static function validate ($request , $id = null)
    { 

        $validator = Validator::make($request->all(), [
            'socio_economic_info_id'=> 'required',
            'height' 	=> 'required',
            'weight'=> 'required',
            'head_circumference'=> 'required',
            'chest_circumference' 	    => 'required',
            'muac' 	    => 'required'
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


