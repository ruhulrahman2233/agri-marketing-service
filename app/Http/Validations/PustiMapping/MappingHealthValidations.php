<?php
namespace app\Http\Validations\PustiMapping;

use Validator;

class MappingHealthValidations
{
    /**
     * Master Report Heading Validation
    */
    public static function validate ($request , $id = null)
    { 

        $validator = Validator::make($request->all(), [
            'socio_economic_info_id'=> 'required',
            'question_no_1' 	=> 'required',
            'question_no_2'=> 'required',
            'question_no_3'=> 'required',
            'question_no_4' 	    => 'required',
            'question_no_5' 	    => 'required',
            'question_no_6'       => 'required',
            'question_no_8'       => 'required',
            'question_no_9'       => 'required',
            'question_no_10'       => 'required',
            'question_no_11'       => 'required',
            'question_no_12'       => 'required',
            'question_no_13'       => 'required',
            'question_no_14'       => 'required',
            'question_no_15'       => 'required',
            'question_no_16'       => 'required',
            'question_no_17'       => 'required',
            'question_no_18'       => 'required'
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


