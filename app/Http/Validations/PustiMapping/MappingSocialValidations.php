<?php
namespace app\Http\Validations\PustiMapping;

use Validator;

class MappingSocialValidations
{
    /**
     * Master Report Heading Validation
    */
    public static function validate ($request , $id = null)
    { 

        $validator = Validator::make($request->all(), [
            'pusti_mapping_id'=> 'required',
            'pusti_mapping_date' 	=> 'required',
            'name_en'=> 'required',
            'father_name_en'=> 'required',
            'mother_name_en' 	    => 'required',
            'age' 	    => 'required',
            'gender'       => 'required',
            'father_education_status'       => 'required',
            'mother_education_status'       => 'required',
            'earning_member'       => 'required',
            'monthly_family_income'       => 'required',
            'income_source'       => 'required',
            'family_category'       => 'required',
            'no_of_family_member'       => 'required',
            'religion'       => 'required',
            'division_id'       => 'required',
            'district_id'       => 'required',
            'mobile_no'       => 'required|max:11|min:11'
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


