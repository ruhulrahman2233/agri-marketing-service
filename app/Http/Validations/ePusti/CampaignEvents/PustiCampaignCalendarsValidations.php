<?php

namespace App\Http\Validations\EPusti\CampaignEvents;

use Validator;

class PustiCampaignCalendarsValidations
{
    /**
     * Pusti Campaign Calendars Validations
    */
    public static function validate ($request , $id = null)
    { 
        $validator = Validator::make($request->all(), [
             	'fiscal_year_id'         => 'required', 	
			 	'campaign_type_id'       => 'required', 	
			 	'campaign_id'            => 'required', 	
			 	'divisional_office_id'   => 'required', 	
			 	'division_id'            => 'required',	
			 	'district_id'            => 'required', 	
			 	'quantity'           	 => 'required', 	
			 	'start_date'             => 'required', 	
			 	'end_date'           	 => 'required', 	
			 	'budget'           		 => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


