<?php

namespace App\Http\Validations\EPusti\CampaignEvents;

use Validator;

class PustiCampaignRequestsValidations
{
    /**
     * Pusti Campaign Calendars Validations
    */
    public static function validate ($request , $id = null)
    { 
        $validator = Validator::make($request->all(), [
             	'mobile_no'         => 'required|numeric|digits_between:11,11',
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


