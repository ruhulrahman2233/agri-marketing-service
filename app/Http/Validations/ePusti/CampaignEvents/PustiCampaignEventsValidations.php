<?php
namespace app\Http\Validations\EPusti\CampaignEvents;

use Validator;

class PustiCampaignEventsValidations
{
    /**
     * Master Report Heading Validation
    */
    public static function validate ($request , $id = null)
    { 

        $validator = Validator::make($request->all(), [
            'office_name' 	=> 'required',
            'division_id' 	=> 'required',
            'district_id' 	=> 'required',
            'name' 	        => 'required',
            'name_bn' 	    => 'required',
            'email' 	    => 'required|email',
            'mobile_no' 	=> 'required',

        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


