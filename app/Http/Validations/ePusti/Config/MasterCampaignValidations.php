<?php
namespace App\Http\Validations\EPusti\Config;

use Validator;

class MasterCampaignValidations
{
    /**
     * Master Campaign Validation
    */
    public static function validate ($request , $id = null)
    { 
        $validator = Validator::make($request->all(), [
            'campaign_name' 	    => 'required',
            'campaign_name_bn' 	    => 'required',
            'type_id'               => 'required',
            'fiscal_year_id'        => 'required'
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


