<?php
namespace app\Http\Validations\EPusti\Config;

use Validator;

class MasterDivisionalOfficeValidations
{
    /**
     * Master Report Heading Validation
    */
    public static function validate ($request , $id = null)
    { 
        $validator = Validator::make($request->all(), [
            'office_name' 	=> 'required',
            'division_id' 	=> 'required',
            'district_id' 	=> 'required',
            'name' 	        => 'required',
            'name_bn' 	    => 'required',
            'email' 	    => 'email|email:rfc,dns|unique:master_divisional_offices,email,'.$id,
            'mobile_no' 	=> 'required|numeric|digits_between:11,11|unique:master_divisional_offices,mobile_no,'.$id,
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


