<?php
namespace app\Http\Validations\EPusti\DivisionalHead;

use Validator;

class CampaignFeedbackValidations
{
    /**
     * Pusti Campaign Calendars Validations
    */
    public static function validate ($request , $id = null)
    { 
        $validator = Validator::make($request->all(), [
            'campaign_type_id'      => 'required', 	
            'campaign_id'         	=> 'required', 	
            'divisional_office_id'  => 'required', 	
            'school_name'         	=> 'required', 	
            'upload_date'       	=> 'required'
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }
        
        return ['success'=> 'true'];
    }
}


