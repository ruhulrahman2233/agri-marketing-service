<?php
namespace app\Http\Validations\EPusti\DivisionalHead;

use Validator;

class CampaignMaterialValidations
{
    /**
     * Master Report Heading Validation
    */
    public static function validate ($request , $id = null)
    { 

        $validator = Validator::make($request->all(), [
            'fiscal_year_id'=> 'required',
            'campaign_id' 	=> 'required',
            'campaign_id'   => 'required|unique:pusti_campaign_materials,campaign_id,'.$id,
            'divisional_office_id'  => 'required',
            'campaign_date' 	    => 'required'
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


