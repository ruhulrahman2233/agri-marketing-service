<?php
namespace app\Http\Validations\EPusti\Birtan;

use Validator;

class CampaignInformationValidations
{
    /**
     * Master Report Heading Validation
    */
    public static function validate ($request , $id = null)
    { 
        $validator = Validator::make($request->all(), [
            'campaign_name' 	=> 'required',
            'campaign_name_bn' 	=> 'required',
            'divisional_office_id'  => 'required',
            'division_id' 	=> 'required',
            'district_id'   => 'required',
            'document_name' => 'required',
            'campaign_date' => 'required'
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


