<?php
namespace app\Http\Validations\EPusti\Birtan;

use Validator;

class CampaignScheduleValidations
{
    /**
     * Master Report Heading Validation
    */
    public static function validate ($request , $id = null)
    { 

        $validator = Validator::make($request->all(), [
            'fiscal_year_id'=> 'required',
            'campaign_id' 	=> 'required',
            'quantity' 	    => 'required',
            'start_date' 	=> 'required',
            'end_date' 	    => 'required'
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


