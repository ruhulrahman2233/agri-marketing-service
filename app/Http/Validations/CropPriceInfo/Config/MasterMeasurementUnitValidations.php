<?php
namespace App\Http\Validations\CropPriceInfo\Config;

use Validator;

class MasterMeasurementUnitValidations
{
    /**
     * Maser Measurement Units Validation 
     */
    public static function validate($request, $id = 0)
    {
        $validator = Validator::make($request->all(), [
            'unit_name'         =>'required',
            'unit_name_bn'      =>'required',
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }

}
