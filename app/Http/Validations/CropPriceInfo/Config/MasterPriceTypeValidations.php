<?php
namespace app\Http\Validations\CropPriceInfo\Config;
use Illuminate\Validation\Rule;
use Validator;

class MasterPriceTypeValidations
{
    /**
     * Master Report Heading Validation
    */
    public static function validate ($request , $id = null)
    { 
        $org_id 		= $request->org_id;
        $type_name 	= $request->type_name;
        $type_name_bn	= $request->type_name_bn;

        $validator = Validator::make($request->all(), [
            'org_id' 		=> 'required',
            'type_name' => [
                'required',
                Rule::unique('master_price_types')->where(function ($query) use($org_id, $type_name, $id) {
                    $query->where('org_id', $org_id);
                    $query->where('type_name', $type_name);
                    if ($id) {
                        $query =$query->where('id', '!=' ,$id);
                    }
                    return $query;             
                }),
            ],

            'type_name_bn' => [
                'required',
                Rule::unique('master_price_types')->where(function ($query) use($org_id, $type_name_bn, $id) {
                    $query->where('org_id', $org_id);
                    $query->where('type_name_bn', $type_name_bn);
                    if ($id) {
                        $query =$query->where('id', '!=' ,$id);
                    }
                    return $query;             
                }),
            ]

        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


