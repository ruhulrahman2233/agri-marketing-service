<?php

namespace App\Http\Validations\CropPriceInfo\Config;

use Validator;
use Illuminate\Validation\Rule;

class MasterAlertPercentagesValidations
{
    /**
     * Master alert percentages validation 
     */
    public static function validate ($request , $id = null)
    { 
        $fiscal_year_id 	= $request->fiscal_year_id;
        $alert_percentage 	= $request->alert_percentage;

        $validator = Validator::make($request->all(), [
            'alert_percentage' 		=> 'required',
            'fiscal_year_id' => [
                'required',
                Rule::unique('master_alert_percentages')->where(function ($query) use($fiscal_year_id, $alert_percentage, $id) {
                    $query->where('fiscal_year_id', $fiscal_year_id);
                    if ($id) {
                        $query =$query->where('id', '!=' ,$id);
                    }
                    return $query;             
                }),
            ]           

        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }

}
