<?php
namespace app\Http\Validations\CropPriceInfo\Config;

use Validator;

class CommoditySubGroupValidations
{
    /**
     * Add Document Validation 
     */
    public static function validate($request, $id = 0)
    {
        $validator = Validator::make($request->all(), [
            'commodity_group_id'       =>'required',
            'sub_group_name'         =>'required',
            'sub_group_name_bn'      =>'required',
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }

}
