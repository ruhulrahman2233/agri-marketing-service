
<?php
namespace app\Http\Validations\CropPriceInfo\Config;
use Illuminate\Validation\Rule;
use Validator;

class MasterCottonNamesValidations
{
    /**
     * Master Report Heading Validation
    */
    public static function validate ($request , $id = null)
    { 
        $org_id 			= $request->org_id;
        $group_name 		= $request->group_name;
        $group_name_bn		= $request->group_name_bn;
        $validator = Validator::make($request->all(), [
            'org_id' 				=> 'required',
            'group_name' => [
                'required',
                Rule::unique('master_commodity_groups')->where(function ($query) use($org_id, $group_name, $id) {
                    $query->where('org_id', $org_id);
                    $query->where('group_name', $group_name);
                    if ($id) {
                        $query =$query->where('id', '!=' ,$id);
                    }
                    return $query;             
                }),
            ],

            'group_name_bn' => [
                'required',
                Rule::unique('master_commodity_groups')->where(function ($query) use($org_id, $group_name_bn, $id) {
                    $query->where('org_id', $org_id);
                    $query->where('group_name_bn', $group_name_bny);
                    if ($id) {
                        $query =$query->where('id', '!=' ,$id);
                    }
                    return $query;             
                }),
            ]

        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}




