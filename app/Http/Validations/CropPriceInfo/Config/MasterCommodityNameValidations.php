<?php
namespace app\Http\Validations\CropPriceInfo\Config;
use Illuminate\Validation\Rule;
use Validator;

class MasterCommodityNameValidations
{
    /**
     * Master Commodity Name Validation
    */
    public static function validate ($request , $id = null)
    { 
        $validator = Validator::make($request->all(), [
            'commodity_group_id' => 'required|integer|exists:master_commodity_groups,id',
            'commodity_name'  => [
                'required',
                Rule::unique('master_commodity_names')
                        ->ignore($id)
                        ->where(function ($query) use($request) {
                            return $query->where('commodity_group_id', $request->commodity_group_id);
                        })
               ],
            'commodity_name_bn'  => [
                'required',
                Rule::unique('master_commodity_names')
                        ->ignore($id)
                        ->where(function ($query) use($request) {
                            return $query->where('commodity_group_id', $request->commodity_group_id);
                        })
               ]
        ]);

        if ($validator->fails()) {
            return([
                'success' => false,
                'errors'  => $validator->errors()
            ]);
        }
        return ['success'=>true];
    }

}


