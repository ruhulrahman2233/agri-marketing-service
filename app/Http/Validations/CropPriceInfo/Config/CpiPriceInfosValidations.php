<?php

namespace App\Http\Validations\CropPriceInfo\Config;

use Validator;

class CpiPriceInfosValidations
{
    /**
     * Maser Measurement Units Validation 
     */
    public static function validate($request, $id = 0)
    {
        // return $request;
        $validator = Validator::make($request->all(), [
            'PriceDate'           	=>'required', 	
			'LatitudeRecorded'      =>'required', 	
			'LongitudeRecorded'    =>'required',	
			'HighestPrice'          =>'required', 	
			'LowestPrice'           =>'required', 	
			'AveragePrice'          =>'required',  	
			'PriceType_id'          =>'required', 	
			'Market_id'           	=>'required', 		
			'District_id'           =>'required', 	
			'Division_id'           =>'required', 	
			'Commodity_id'          =>'required',
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }

}

