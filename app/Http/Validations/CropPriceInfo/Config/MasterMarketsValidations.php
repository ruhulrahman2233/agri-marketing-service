<?php

namespace App\Http\Validations\CropPriceInfo\Config;

use Validator;
use Illuminate\Validation\Rule;

class MasterMarketsValidations
{
    /**
     * Maser markets  Validation 
     */
    public static function validate ($request , $id = null)
    { 
        $validator = Validator::make($request->all(), [
            'division_id' 		=> 'nullable|integer',
            'district_id' 		=> 'nullable|integer',
            'upazila_id' 		=> 'nullable|integer',
            'market_name' 		=> 'nullable|string|max:255',
            'market_name_bn'    => 'nullable|string|max:255',
            'foundation_year' 	=> 'nullable',
            'village' 		    => 'nullable|string',
            'post_office' 		=> 'nullable|string',
            'union' 		    => 'nullable|string',
            'market_type' 		=> 'nullable|integer',
            'govt_covered' 		=> 'nullable|integer',
            'govt_open' 		=> 'nullable|integer',
            'private_covered'   => 'nullable|integer',
            'private_open'      => 'nullable|integer',
            'total_market_area' => 'nullable|integer',
            'shed_no'           => 'nullable|integer',
            'shed_area'         => 'nullable|integer',
            'stall_no_agri'     => 'nullable|integer',
            'stall_no_nonagri'  => 'nullable|integer',
            'stall_no_nonagri'  => 'nullable|integer',
            'hat_days'          => 'nullable|array', 
            'market_time_from'  => 'nullable', 
            'market_time_to'    => 'nullable', 
            'infrastructure_id' => 'nullable|array', 
            'communication_linkage_id' => 'nullable|array', 
            'number_of_buyers'          => 'nullable|integer', 
            'number_of_sellers'         => 'nullable|integer', 
            'farmer_share'              => 'nullable', 
            'trader_share'              => 'nullable', 
            'other_product_destination' => 'nullable|string', 
            'product_destination'       => 'nullable|array', 
            'vehicle_id'                => 'nullable|array', 
            'avg_distance'              => 'nullable|integer', 
            'data_collection_year'      => 'nullable', 
            'mobile_market_committee'    => 'nullable|string', 
            'market_representative_name' => 'nullable|string', 
            'market_representative_mobile' => 'nullable|string', 
            'latitude'                     => 'nullable', 
            'longitude'                     => 'nullable', 
            'approved_date'                 => 'nullable'
        ]); 

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }

}
