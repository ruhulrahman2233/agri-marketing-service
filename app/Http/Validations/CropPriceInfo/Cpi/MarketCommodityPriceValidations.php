<?php

namespace App\Http\Validations\CropPriceInfo\Cpi;

use Validator;

class MarketCommodityPriceValidations
{
    /**
     * Maser Measurement Units Validation 
     */
    public static function validate($request, $id = 0)
    {

        $validator = Validator::make($request->all(), [
            'division_id'          =>'required', 	
			'district_id'          =>'required', 	
			'upazila_id'           =>'required', 	
			'price_entry_type'     =>'required', 	
			'price_type_id'        =>'required',
			'select_type'          =>'required',
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }

}

