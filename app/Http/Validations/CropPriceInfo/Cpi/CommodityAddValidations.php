<?php

namespace App\Http\Validations\CropPriceInfo\Cpi;

use Illuminate\Validation\Rule;
use Validator;

class CommodityAddValidations
{
    /**
     * Maser Measurement Units Validation 
     */
    public static function validate($request, $id = 0)
    {

        $year = $request->year;
        $month_id = $request->month_id;
        $week_id = $request->week_id;
        $market_id = $request->market_id;

    
        if ($request->select_type === 'Daily' && $request->price_entry_type === 'Market') {

            $validator = Validator::make($request->all(), [
                'division_id'          =>'required', 	
                'district_id'          =>'required', 	
                'upazila_id'           =>'required', 	
                'price_entry_type'     =>'required', 	
                'price_type_id'        =>'required',
                'select_type'          =>'required',
                'market_id'            =>'required',
                'price_date' => [
                    'required',
                    Rule::unique('cpi_market_commodity_prices')->where(function ($query) use($market_id, $id, $request) {
                        $query->where('market_id', $market_id);
                        $query->where('price_date', $request->price_date);
                        if ($id) {
                            $query =$query->where('id', '!=' ,$id);
                        }
                        return $query;
                    }),
                ],
            ]);

        } else if ($request->select_type === 'Daily' && $request->price_entry_type === 'Grower') {

            $validator = Validator::make($request->all(), [
                'division_id'          =>'required', 	
                'district_id'          =>'required', 	
                'upazila_id'           =>'required', 	
                'price_entry_type'     =>'required', 	
                'price_type_id'        =>'required',
                'select_type'          =>'required',
                'market_id'            =>'required',
                'price_date' => [
                    'required',
                    Rule::unique('cpi_market_commodity_growers_prices')->where(function ($query) use($market_id, $id, $request) {
                        $query->where('market_id', $market_id);
                        $query->where('price_date', $request->price_date);
                        if ($id) {
                            $query =$query->where('id', '!=' ,$id);
                        }
                        return $query;
                    }),
                ],
            ]);

        }else {

            $validator = Validator::make($request->all(), [
                'division_id'          =>'required', 	
                'district_id'          =>'required', 	
                'upazila_id'           =>'required', 	
                'price_entry_type'     =>'required', 	
                'price_type_id'        =>'required',
                'select_type'          =>'required',
                'market_id'            =>'required',
                'week_id' => [
                    'required',
                    Rule::unique('cpi_market_commodity_weekly_prices')->where(function ($query) use($market_id, $year, $month_id, $week_id, $id, $request) {
                        $query->where('market_id', $market_id)
                        ->where('price_entry_type', $request->price_entry_type)
                        ->where('week_id', $request->week_id)
                        ->where('year', $year)
                        ->where('month_id', $month_id);
                        if ($id) {
                            $query = $query->where('id', '!=' ,$id);
                        }
                        return $query;
                    }),
                ],
            ]);
        }

       

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }

}

