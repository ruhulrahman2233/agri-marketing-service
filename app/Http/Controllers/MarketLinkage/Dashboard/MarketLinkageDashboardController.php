<?php

namespace App\Http\Controllers\MarketLinkage\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class MarketLinkageDashboardController extends Controller
{
    public function index(Request $request)
    {
    	//dd('ok');


    	 			


    	 $query = DB::table('cpi_price_infos')
    	 		->leftJoin('master_price_types','cpi_price_infos.PriceType_id','master_price_types.id')
    	 		  ->select('cpi_price_infos.*','master_price_types.type_name','master_price_types.type_name_bn')
    	 		  ->addSelect(DB::raw('AVG(cpi_price_infos.HighestPrice) as hight_price'))
    	 		  ->addSelect(DB::raw('AVG(cpi_price_infos.LowestPrice) as lowest_price'))
    	 		  ->addSelect(DB::raw('AVG(cpi_price_infos.AveragePrice) as market_price'));

    	$retailQuery = DB::table('cpi_price_infos')
    	 		  ->select(DB::raw('AVG(cpi_price_infos.AveragePrice) as retai_price'))
    	 		  ->where('cpi_price_infos.PriceType_id', 2);
    	 		  
    	$wholesaleQuery = DB::table('cpi_price_infos')
    	 		  ->select(DB::raw('AVG(cpi_price_infos.AveragePrice) as wholesale_price'))
    	 		  ->where('cpi_price_infos.PriceType_id', 1);
                    
       if ($request->Market_id) {
            $query = $query->where('cpi_price_infos.Market_id', $request->Market_id);
            $retailQuery = $retailQuery->where('cpi_price_infos.Market_id', $request->Market_id);
            $wholesaleQuery = $wholesaleQuery->where('cpi_price_infos.Market_id', $request->Market_id);
        }

         if ($request->Division_id) {
            $query = $query->where('cpi_price_infos.Division_id', $request->Division_id);
            $retailQuery = $retailQuery->where('cpi_price_infos.Division_id', $request->Division_id);
            $wholesaleQuery = $wholesaleQuery->where('cpi_price_infos.Division_id', $request->Division_id);
        }

        if ($request->PriceType_id) {
            $query = $query->where('cpi_price_infos.PriceType_id', $request->PriceType_id);
            $retailQuery = $retailQuery->where('cpi_price_infos.PriceType_id', $request->PriceType_id);
            $wholesaleQuery = $wholesaleQuery->where('cpi_price_infos.PriceType_id', $request->PriceType_id);
        }
                    
        if ($request->District_id) {
            $query = $query->where('cpi_price_infos.District_id', $request->District_id);
            $retailQuery = $retailQuery->where('cpi_price_infos.District_id', $request->District_id);
            $wholesaleQuery = $wholesaleQuery->where('cpi_price_infos.District_id', $request->District_id);
        }
                    
        if ($request->SubDistrict_id) {
            $query = $query->where('cpi_price_infos.SubDistrict_id', $request->SubDistrict_id);
            $retailQuery = $retailQuery->where('cpi_price_infos.SubDistrict_id', $request->SubDistrict_id);
            $wholesaleQuery = $wholesaleQuery->where('cpi_price_infos.SubDistrict_id', $request->SubDistrict_id);
        }
                    
        if ($request->Commodity_id) {
            $query = $query->where('cpi_price_infos.Commodity_id', $request->Commodity_id);
            $retailQuery = $retailQuery->where('cpi_price_infos.Commodity_id', $request->Commodity_id);
            $wholesaleQuery = $wholesaleQuery->where('cpi_price_infos.Commodity_id', $request->Commodity_id);
        } 


        if ($request->from_PriceDate && $request->to_PriceDate) 
		{
		    $startDate   = date('Y-m-d', strtotime($request->from_PriceDate));
		    $endDate     = date('Y-m-d', strtotime($request->to_PriceDate));
		    $query       = $query->whereBetween('PriceDate', [$startDate, $endDate]);
		}

		if (isset($request->from_PriceDate) && !isset($request->to_PriceDate))
		{
		    $query = $query->whereDate('PriceDate', '>=', date('Y-m-d', strtotime($request->from_PriceDate)));
		}

		if (!isset($request->from_PriceDate) && isset($request->to_PriceDate))
		{
		    $query = $query->whereDate('PriceDate', '<=', date('Y-m-d', strtotime($request->to_PriceDate)));
		}  
               
        $retailQuery = $retailQuery->get();
        $wholesaleQuery = $wholesaleQuery->get();
        $list = $query->get();
                               

        if(count($list)>0){
        	return response([
	            'success' => true,
	            'message' => 'Linkage Grower Buyer Profile List',
	            'data'    => $list,
	            'retai_price'    => $retailQuery[0]->retai_price??0,
	            'wholesale_price'    => $wholesaleQuery[0]->wholesale_price??0
	        ]);
        }
        else
        {
        	return response([
	            'success' => false,
	            'message' => 'Data not found!!',
	        ]);
        }
        
    }
}
