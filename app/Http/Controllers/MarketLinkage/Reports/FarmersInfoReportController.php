<?php

namespace App\Http\Controllers\MarketLinkage\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MarketLinkage\Linkage\GrowerBuyerProfile;
use App\Models\MarketLinkage\Linkage\GrowerBuyerProfileDetails;

class FarmersInfoReportController extends Controller
{
    public function index(Request $request)
    {
        $query = GrowerBuyerProfile::leftjoin('master_commodity_types','linkage_gro_buy_profiles.commodity_type_id','master_commodity_types.id')
                    ->with('details')
                    ->select('linkage_gro_buy_profiles.*','master_commodity_types.type_name','master_commodity_types.type_name_bn')
                    ->where('linkage_gro_buy_profiles.type', 1)
                    ->orderBy('linkage_gro_buy_profiles.id','DESC');       
                    
        if ($request->division_id) {
            $query = $query->where('linkage_gro_buy_profiles.division_id', $request->division_id);
        }
                    
        if ($request->district_id) {
            $query = $query->where('linkage_gro_buy_profiles.district_id', $request->district_id);
        }
                    
        if ($request->upazilla_id) {
            $query = $query->where('linkage_gro_buy_profiles.upazilla_id', $request->upazilla_id);
        }
                    
        if ($request->union_id) {
            $query = $query->where('linkage_gro_buy_profiles.union_id', $request->union_id);
        }

        $list = $query->get();

        if(count($list)>0){
        	return response([
	            'success' => true,
	            'message' => 'Linkage Grower Buyer Profile List',
	            'data'    => $list
	        ]);
        }
        else
        {
        	return response([
	            'success' => false,
	            'message' => 'Data not found!!',
	        ]);
        }
        
    }
}
