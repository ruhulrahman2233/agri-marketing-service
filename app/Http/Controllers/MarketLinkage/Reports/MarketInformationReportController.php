<?php

namespace App\Http\Controllers\MarketLinkage\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class MarketInformationReportController extends Controller
{
    
    public function index(Request $request)
    {
    	$query = DB::table('master_markets')
    	 		->leftJoin('linkage_gro_buy_profiles','master_markets.Subdistrict_id','linkage_gro_buy_profiles.upazilla_id')
    	 		  ->select('master_markets.*','linkage_gro_buy_profiles.*');
                    
        if ($request->type) {
            $query = $query->where('linkage_gro_buy_profiles.type', $request->type);
        }

         if ($request->division_id) {
            $query = $query->where('linkage_gro_buy_profiles.division_id', $request->division_id);
        }

        
                    
        if ($request->district_id) {
            $query = $query->where('linkage_gro_buy_profiles.district_id', $request->district_id);
        }
                    
        if ($request->upazilla_id) {
            $query = $query->where('linkage_gro_buy_profiles.upazilla_id', $request->upazilla_id);
        }
                    
        if ($request->union_id) {
            $query = $query->where('linkage_gro_buy_profiles.union_id', $request->union_id);
        }

        $list = $query->get();

        if(count($list)>0){
        	return response([
	            'success' => true,
	            'message' => 'Linkage Grower Buyer Profile List',
	            'data'    => $list
	        ]);
        }
        else
        {
        	return response([
	            'success' => false,
	            'message' => 'Data not found!!',
	        ]);
        }
        
    }
}
