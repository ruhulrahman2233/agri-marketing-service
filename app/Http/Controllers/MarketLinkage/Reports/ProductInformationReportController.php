<?php

namespace App\Http\Controllers\MarketLinkage\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ProductInformationReportController extends Controller
{
    public function index(Request $request)
    {
    	 $query = DB::table('linkage_gro_buy_profiles')
    	 		->leftJoin('linkage_gro_buy_pro_details','linkage_gro_buy_profiles.id','linkage_gro_buy_pro_details.gro_buy_profiles_id')
                ->select('linkage_gro_buy_profiles.*','linkage_gro_buy_pro_details.commodity_id')
                ->addSelect(DB::raw('sum(linkage_gro_buy_pro_details.quantity) as total_quantity'))
                ->groupBy('linkage_gro_buy_profiles.commodity_type_id')       
                ->groupBy('linkage_gro_buy_pro_details.commodity_group_id')       
                ->groupBy('linkage_gro_buy_pro_details.commodity_sub_group_id')      
                ->groupBy('linkage_gro_buy_pro_details.commodity_id');    

        if ($request->division_id) {
            $query = $query->where('linkage_gro_buy_profiles.division_id', $request->division_id);
        }
                    
        if ($request->district_id) {
            $query = $query->where('linkage_gro_buy_profiles.district_id', $request->district_id);
        }
                    
        if ($request->upazilla_id) {
            $query = $query->where('linkage_gro_buy_profiles.upazilla_id', $request->upazilla_id);
        }
                    
        if ($request->union_id) {
            $query = $query->where('linkage_gro_buy_profiles.union_id', $request->union_id);
        }

        $list = $query->get();

        if (count($list) > 0) {
        	return response([
	            'success' => true,
	            'message' => 'Product information',
	            'data'    => $list
	        ]);
        } else {
        	return response([
	            'success' => false,
	            'message' => 'Data not found!!',
	        ]);
        }
        
    }
}
