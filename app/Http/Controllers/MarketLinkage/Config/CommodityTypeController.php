<?php

namespace App\Http\Controllers\MarketLinkage\Config;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MarketLinkage\Config\CommodityType;
use App\Http\Validations\MarketLinkage\Config\CommodityTypeValidations;

class CommodityTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new CommodityType();

        if ($request->type_name) 
        {
            $query = $query->where('type_name', 'like', "{$request->type_name}%")
                           ->orWhere('type_name_bn', 'like', "{$request->type_name}%");
        }       

        if ($request->status) {
            $query->where('status', $request->status);
        }

        $list = $query->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Commodity type list',
            'data' =>$list
        ]);
    }


    /*+++++++show+++++++++*/
    public function show($id)
    {
        $commodityType = CommodityType::find($id);

        if (!$commodityType) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Commodity type details',
            'data'    => $commodityType
        ]);
    }

   
    /*+++++++store+++++++++*/
    public function store(Request $request)
    {      
        $validationResult = CommodityTypeValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $postData = $request->all();
            $postData['created_by'] = (int)user_id()??null;
            $postData['updated_by'] = (int)user_id()??null;

            $commodityType = new CommodityType();
            $commodityType->create($postData);

            /*save_log([
                'data_id'    => $commodityType->id,
                'table_name' => 'master_commodity_types'
            ]);*/

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $commodityType
        ]);
    }

  

     /*++++++update+++++++*/
    public function update(Request $request, $id)
    {
        $validationResult = CommodityTypeValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $commodityType = CommodityType::find($id);

        if (!$commodityType) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {

            $putData = $request->all();
            $putData['updated_by'] = (int)user_id()??null;

            $commodityType->update($putData);

            /*save_log([
                'data_id'       => $commodityType->id,
                'table_name'    => 'master_commodity_types',
                'execution_type'=> 1
            ]);*/

           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $commodityType
        ]);
    }

   /*+++++status update++++++*/
    public function toggleStatus($id)
    {
        $commodityType = CommodityType::find($id);

        if (!$commodityType) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $commodityType->status = $commodityType->status == 1 ? 2 : 1;
        $commodityType->update();


        /*save_log([
            'data_id'       => $commodityType->id,
            'table_name'    => 'master_commodity_types',
            'execution_type'=> 2
        ]);*/


        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $commodityType
        ]);
    }

    
   /*+++++sdestroy++++++*/
    public function destroy($id)
    {
        $commodityType = CommodityType::find($id);

        if (!$commodityType) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $commodityType->delete();

        /*save_log([
            'data_id'       => $id,
            'table_name'    => 'master_commodity_types',
            'execution_type'=> 2
        ]);*/

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}