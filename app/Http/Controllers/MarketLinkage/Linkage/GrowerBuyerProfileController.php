<?php

namespace App\Http\Controllers\MarketLinkage\Linkage;

use App\Http\Controllers\Controller;
use App\Http\Validations\MarketLinkage\Linkage\GrowerBuyerValidations;
use App\Models\MarketLinkage\Linkage\GrowerBuyerProfile;
use App\Models\MarketLinkage\Linkage\GrowerBuyerProfileDetails;
use Illuminate\Http\Request;
use DB;

class GrowerBuyerProfileController extends Controller
{
    /**
     * get all
     */
    public function index(Request $request)
    {
        $query = GrowerBuyerProfile::leftjoin('master_commodity_types','linkage_gro_buy_profiles.commodity_type_id','master_commodity_types.id')
                    ->with('details')
                    ->select('linkage_gro_buy_profiles.*','master_commodity_types.type_name','master_commodity_types.type_name_bn')
                    ->orderBy('linkage_gro_buy_profiles.id','DESC');

        if ($request->type_name) {
            $query = $query->where('linkage_gro_buy_profiles.name', 'like', "{$request->name}%")
                            ->orWhere('linkage_gro_buy_profiles.name_bn', 'like', "{$request->name}%");
        } 
                    
        if ($request->applicant_id) {
            $query = $query->where('linkage_gro_buy_profiles.applicant_id', $request->applicant_id);
        }
                    
        if ($request->type) {
            $query = $query->where('linkage_gro_buy_profiles.type', $request->type);
        }
                    
        if ($request->mobile_no) {
            $query = $query->where('linkage_gro_buy_profiles.mobile_no', $request->mobile_no);
        }
                    
        if ($request->division_id) {
            $query = $query->where('linkage_gro_buy_profiles.division_id', $request->division_id);
        }
                    
        if ($request->district_id) {
            $query = $query->where('linkage_gro_buy_profiles.district_id', $request->district_id);
        }
                    
        if ($request->upazilla_id) {
            $query = $query->where('linkage_gro_buy_profiles.upazilla_id', $request->upazilla_id);
        }
                    
        if ($request->union_id) {
            $query = $query->where('linkage_gro_buy_profiles.union_id', $request->union_id);
        }

        $list = $query->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Linkage Grower Buyer Profile List',
            'data'    => $list
        ]);
    }

    /**
     * store
     */
    public function store(Request $request)
    {   
        $validationResult = GrowerBuyerValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $applicantIdGet = DB::table('linkage_gro_buy_profiles')
                            ->select('applicant_id')
                            ->orderBy('applicant_id','desc')
                            ->first();

        if($applicantIdGet){
            $applicant_id = $applicantIdGet->applicant_id;        
            if( $applicant_id != "" ){
                $applicant_id += 1;
            }
        } else {
            $applicant_id = 100000;
        }

        DB::beginTransaction();

        try {
            $postData                   = $request->all();
            $postData['applicant_id']   = $applicant_id;
            $postData['created_by']     = (int)user_id() ?? null;
            $postData['updated_by']     = (int)user_id() ?? null;

            $gbp = new GrowerBuyerProfile($postData);
            $gbp->save();    
            
            $profileDetails = $request->details;

            foreach ($profileDetails as $detail) {
                $proDetail = new GrowerBuyerProfileDetails();
                $proDetail->gro_buy_profiles_id     = $gbp->id;
                $proDetail->commodity_group_id      = $detail['commodity_group_id'];
                $proDetail->commodity_sub_group_id  = $detail['commodity_sub_group_id'];
                $proDetail->commodity_id            = $detail['commodity_id'];
                $proDetail->save();
            }

            DB::commit();

            save_log([
                'data_id'    => $gbp->id,
                'table_name' => 'linkage_gro_buy_profiles'
            ]);

        } catch (\Exception $ex) {

            DB::rollback();

            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $gbp
        ]);
    }

    /**
     * update
     */
    public function update(Request $request, $id)
    { 
        $validationResult = GrowerBuyerValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $gbp = GrowerBuyerProfile::find($id);

        if (!$gbp) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {

            $putData = $request->all();
            $putData['updated_by'] = (int)user_id()??null;

            $gbp->fill($putData);
            $gbp->update();

            GrowerBuyerProfileDetails::where('gro_buy_profiles_id', $id)->delete();

            $profileDetails = $request->details;

            foreach ($profileDetails as $detail) {
                $proDetail = new GrowerBuyerProfileDetails();
                $proDetail->gro_buy_profiles_id     = $gbp->id;
                $proDetail->commodity_group_id      = $detail['commodity_group_id'];
                $proDetail->commodity_sub_group_id  = $detail['commodity_sub_group_id'];
                $proDetail->commodity_id            = $detail['commodity_id'];
                $proDetail->save();
            }

            save_log([
                'data_id'       => $gbp->id,
                'table_name'    => 'linkage_gro_buy_profiles',
                'execution_type'=> 1
            ]);           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $gbp
        ]);
    }

    /**
     * approve
     */
    public function approve(Request $request, $id)
    { 
        $gbp = GrowerBuyerProfile::find($id);

        if (!$gbp) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {

            $gbp->status = 2; // mean approve
            $gbp->app_rej_reason = $request->app_rej_reason;
            $gbp->update();

            save_log([
                'data_id'       => $gbp->id,
                'table_name'    => 'linkage_gro_buy_profiles',
                'execution_type'=> 1
            ]);           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $gbp
        ]);
    }

    /**
     * reject
     */
    public function reject(Request $request, $id)
    { 
        $gbp = GrowerBuyerProfile::find($id);

        if (!$gbp) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {

            $gbp->status = 3; // mean reject
            $gbp->app_rej_reason = $request->app_rej_reason;
            $gbp->update();

            save_log([
                'data_id'       => $gbp->id,
                'table_name'    => 'linkage_gro_buy_profiles',
                'execution_type'=> 1
            ]);           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $gbp
        ]);
    }

    /**
     * destroy
     */
    public function destroy($id)
    {
        $gbp = GrowerBuyerProfile::find($id);

        if (!$gbp) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $gbp->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'linkage_gro_buy_profiles',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
