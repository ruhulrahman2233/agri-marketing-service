<?php

namespace App\Http\Controllers\MarketLinkage\Linkage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\GlobalFileUploadFunctoin;
use App\Models\MarketLinkage\Linkage\GrowerBuyerProfile;
use App\Models\MarketLinkage\Linkage\GrowerBuyerProfileDetails;
use DB;
class ProductInformationController extends Controller
{
    /**
     * get all
     */
    public function show($id)
    {  	

        $query = GrowerBuyerProfile::with('details')
                    ->leftjoin('master_commodity_types','linkage_gro_buy_profiles.commodity_type_id','master_commodity_types.id')
                    ->select('linkage_gro_buy_profiles.*',
                    	'master_commodity_types.type_name',
                    	'master_commodity_types.type_name_bn')
                    ->where('linkage_gro_buy_profiles.id',$id)
                    ->first();
        if(!$query){
         	return response([
	            'success' => false,
	            'message' => 'Data not found!!',
	            'data'    => $query
	        ]);

        }           

        return response([
            'success' => true,
            'message' => 'Linkage grower buyer profile details',
            'data'    => $query
        ]);
    }

    /**
     * store
     */
    public function store(Request $request)
    {           
    	$groBuyProfilesId = $request->gro_buy_profiles_id;

    	$growerBuyerProfile = GrowerBuyerProfile::find($groBuyProfilesId);

    	if(!$growerBuyerProfile){
    		return response([
	            'success' => false,
	            'message' => 'Data not found!!',
	            'data'    => $grorByrPrfilDtils
	        ]);
    	}


    	//$file_path      = 'grower-buyer-profile-details';     

        try {            
            
            $profileDetails = $request->details;
            $i = 1;

            foreach ($profileDetails as $detail) {
                /*$attachment_string 	= 'attachment'.$i;
            	$attachment     	=  $request->file($attachment_string);*/

            	$hDetailsId 	= $detail['id'];
                $proDetail 		= GrowerBuyerProfileDetails::find($hDetailsId);

                $old_file_name 	= $proDetail->attachment;

                $proDetail->gro_buy_profiles_id     = $groBuyProfilesId;
                $proDetail->commodity_group_id      = $detail['commodity_group_id'];
                $proDetail->commodity_sub_group_id  = $detail['commodity_sub_group_id'];
                $proDetail->commodity_id            = $detail['commodity_id'];
                $proDetail->price_type_id           = $detail['price_type_id'];
                $proDetail->unit_id            		= $detail['unit_id'];
                $proDetail->price            		= $detail['price'];
                $proDetail->quantity            	= $detail['quantity'];
                $proDetail->origin            		= $detail['origin'];
                $proDetail->origin_bn            	= $detail['origin_bn'];
                $proDetail->remarks            		= $detail['remarks'];
                $proDetail->remarks_bn            	= $detail['remarks_bn'];

                $proDetail->save();

                /*    $attachment_name = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request, $file_path, $attachment_string);

                    $proDetail->attachment            	= $i.$attachment_name;

                    if($proDetail->save()){
                    	GlobalFileUploadFunctoin::file_upload($request, $file_path, $attachment_string, $attachment_name, $old_file_name);  
                    }*/
                

	            save_log([
	                'data_id'    => $proDetail->id,
	                'table_name' => 'linkage_gro_buy_pro_details'
	            ]);

	           // $i++;
            }          


        } catch (\Exception $ex) {            

            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $proDetail
        ]);
    }

    

    /**
     * destroy
     */
    public function destroy($id)
    {
        $grorByrPrfilDtils = GrowerBuyerProfileDetails::find($id);

        if (!$grorByrPrfilDtils) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $file_path      	= 'uploads/grower-buyer-profile-details/original';        
        $target_file_name 	= $grorByrPrfilDtils->attachment;

        $delete = $grorByrPrfilDtils->delete();

        if($delete){
        	GlobalFileUploadFunctoin::file_image_permanently_delete($file_path, $target_file_name);
        }

        save_log([
            'data_id'       => $id,
            'table_name'    => 'linkage_gro_buy_profiles',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}