<?php

namespace App\Http\Controllers\Cotton\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class FiscalYearWiseCottonSellReportController extends Controller
{
    /**
     * Get all Seeds Stock
     */
    public function index(Request $request)
    {
        $query = DB::table('grower_sell_entry')
                    ->join('ginner_grower_profiles','grower_sell_entry.applicant_id','ginner_grower_profiles.id')
                    ->select('ginner_grower_profiles.region_id',
                        'ginner_grower_profiles.zone_id',
                        'ginner_grower_profiles.district_id',
                        'ginner_grower_profiles.upazilla_id',
                        'ginner_grower_profiles.unit_id',
                        'ginner_grower_profiles.name',
                        'ginner_grower_profiles.name_bn',
                        'grower_sell_entry.fiscal_year_id',
                        'grower_sell_entry.seasons_id',
                        'grower_sell_entry.hatt_date',
                        'grower_sell_entry.cotton_variety_id',
                        'grower_sell_entry.cotton_id',
                        'grower_sell_entry.quantity'
                    );
        
        if ($request->region_id) {
            $query = $query->where('ginner_grower_profiles.region_id', $request->region_id);
        }

        if ($request->district_id) {
            $query = $query->where('ginner_grower_profiles.district_id', $request->district_id);
        }

        if ($request->upazilla_id) {
            $query = $query->where('ginner_grower_profiles.upazilla_id', $request->upazilla_id);
        }      

        if ($request->zone_id) {
            $query = $query->where('ginner_grower_profiles.zone_id', $request->zone_id);
        }

        if ($request->fiscal_year_id) {
            $query = $query->where('grower_sell_entry.fiscal_year_id', $request->fiscal_year_id);
        }

        if ($request->seasons_id) {
            $query = $query->where('grower_sell_entry.seasons_id', $request->seasons_id);
        }

        if ($request->unit_id) {
            $query = $query->where('grower_sell_entry.unit_id', $request->unit_id);
        }

        if ($request->cotton_variety_id) {
            $query = $query->where('grower_sell_entry.cotton_variety_id', $request->cotton_variety_id);
        }

        if ($request->cotton_id) {
            $query = $query->where('grower_sell_entry.cotton_id', $request->cotton_id);
        }
       
        $list = $query->get();

        if (count($list) >0 ) {
            return response([
                'success' => true,
                'message' => 'Seeds stock list',
                'data'    => $list
            ]);
        } else {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }
    }

}
