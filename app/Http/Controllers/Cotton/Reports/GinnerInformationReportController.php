<?php

namespace App\Http\Controllers\Cotton\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class GinnerInformationReportController extends Controller
{
    /**
     * Ginner information Report
     */
    public function index(Request $request)
    {
        $query = DB::table('ginner_grower_profiles')
                ->select('district_id','upazilla_id',
                        'unit_id','zone_id',
                        'name', 'name_bn',
                        'father_name','father_name_bn',
                        'address','address_bn',
                        'mobile_no','land_area',
                        'nid')->where('type', 1);

        if ($request->region_id) {
            $query = $query->where('region_id', $request->region_id);
        }
        
        if ($request->zone_id) {
            $query = $query->where('zone_id', $request->zone_id);
        } 

        if ($request->unit_id) {
            $query = $query->where('unit_id', $request->unit_id);
        }

        if ($request->district_id) {
            $query = $query->where('district_id', $request->district_id);
        }

        if ($request->upazilla_id) {
            $query = $query->where('upazilla_id', $request->upazilla_id);
        }

        $list = $query->get();
        
        if (!$list) {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);

        } else {
            return response([
                'success' => true,
                'message' => 'ginner Information Data',
                'data'    => $list
            ]);
        }                          
    }
}
