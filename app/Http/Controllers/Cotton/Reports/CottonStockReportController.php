<?php

namespace App\Http\Controllers\Cotton\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class CottonStockReportController extends Controller
{
    public function index(Request $request)
    {     
        $query = DB::table('grower_central_stocks')
                    ->join('ginner_grower_profiles','grower_central_stocks.applicant_id','ginner_grower_profiles.id')
                    ->join('master_cotton_varities','grower_central_stocks.cotton_variety_id','master_cotton_varities.id')
                    ->leftjoin('master_cotton_names','grower_central_stocks.cotton_id','master_cotton_names.id')
                    ->leftjoin('grower_sell_entry','master_cotton_names.id','grower_sell_entry.cotton_id')
                    ->select('grower_central_stocks.applicant_id','grower_central_stocks.cotton_variety_id',
                        'ginner_grower_profiles.name','ginner_grower_profiles.name_bn',
                        'master_cotton_varities.cotton_variety','master_cotton_varities.cotton_variety_bn',
                        'master_cotton_names.cotton_name','master_cotton_names.cotton_name_bn',
                    )
                    ->addSelect(
                        DB::raw('sum(grower_central_stocks.quantity) as production_quantity'),
                        DB::raw('sum(grower_sell_entry.quantity) as sell_quantity')
                    )
                    ->groupBy('grower_central_stocks.seasons_id')
                    ->groupBy('grower_central_stocks.cotton_variety_id')
                    ->groupBy('grower_central_stocks.cotton_id')
                    ->where('grower_central_stocks.type', 2);

        if ($request->fiscal_year_id) {
            $query = $query->where('grower_central_stocks.fiscal_year_id', $request->fiscal_year_id);
        }

        if ($request->seasons_id) {
            $query = $query->where('grower_central_stocks.seasons_id', $request->seasons_id);
        }

        if ($request->region_id) {
            $query = $query->where('ginner_grower_profiles.region_id', $request->region_id);
        }

        if ($request->zone_id) {
            $query = $query->where('ginner_grower_profiles.zone_id', $request->zone_id);
        }

        if ($request->unit_id) {
            $query = $query->where('ginner_grower_profiles.unit_id', $request->unit_id);
        }

        if ($request->district_id) {
            $query = $query->where('ginner_grower_profiles.district_id', $request->district_id);
        }

        if ($request->upazilla_id) {
            $query = $query->where('ginner_grower_profiles.upazilla_id', $request->upazilla_id);
        }

        if ($request->applicant_id) {
            $query = $query->where('grower_central_stocks.applicant_id', $request->applicant_id);
        }

        if ($request->cotton_id) {
            $query = $query->where('grower_central_stocks.cotton_id', $request->cotton_id);
        }

        if ($request->cotton_variety_id) {
            $query = $query->where('grower_central_stocks.cotton_variety_id', $request->cotton_variety_id);
        }

        $items = $query->get();
        
        if (count($items) > 0) {
            return response([
                'success'   => true,
                'message'   => 'Cotton Stock Report',
                'data'      => $items
            ]);
        } else {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }
    }
}
