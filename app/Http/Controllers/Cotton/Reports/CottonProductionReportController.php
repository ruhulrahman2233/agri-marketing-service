<?php

namespace App\Http\Controllers\Cotton\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class CottonProductionReportController extends Controller
{
    /**
     * cotton production dashboard
     */
    public function dashboard(Request $request)
    {       
        $query = DB::table('grower_central_stocks')
                    ->join('ginner_grower_profiles','grower_central_stocks.applicant_id','ginner_grower_profiles.id')
                    ->leftjoin('master_cotton_names','grower_central_stocks.cotton_id','master_cotton_names.id')
                    ->leftjoin('grower_sell_entry','master_cotton_names.id','grower_sell_entry.cotton_id')
                    ->select(
                        DB::raw('sum(grower_central_stocks.quantity) as production_quantity'),
                        DB::raw('sum(grower_sell_entry.quantity) as sell_quantity'),
                        DB::raw('(sum(grower_central_stocks.quantity) - sum(grower_sell_entry.quantity)) as remaining')
                    )
                    ->groupBy('grower_central_stocks.seasons_id')
                    ->groupBy('grower_central_stocks.cotton_variety_id')
                    ->groupBy('grower_central_stocks.cotton_id')
                    ->where('grower_central_stocks.type', 2);

        if ($request->region_id) {
            $query = $query->where('ginner_grower_profiles.region_id', $request->region_id);
        }

        if ($request->zone_id) {
            $query = $query->where('ginner_grower_profiles.zone_id', $request->zone_id);
        }

        if ($request->district_id) {
            $query = $query->where('ginner_grower_profiles.district_id', $request->district_id);
        }

        if ($request->upazilla_id) {
            $query = $query->where('ginner_grower_profiles.upazilla_id', $request->upazilla_id);
        }

        if ($request->cotton_id) {
            $query = $query->where('grower_central_stocks.cotton_id', $request->cotton_id);
        }

        if ($request->cotton_variety_id) {
            $query = $query->where('grower_central_stocks.cotton_variety_id', $request->cotton_variety_id);
        }

        $items = $query->get();

        $total_production   = 0;
        $total_sell         = 0;
        $total_remaining    = 0;

        foreach ($items as $item) {
            $total_production   += $item->production_quantity;
            $total_sell         += $item->sell_quantity;
            $total_remaining    += $item->remaining;
        }

        $data = [
            number_format((float)$total_production, 2, '.', ''), 
            number_format((float)$total_sell, 2, '.', ''), 
            number_format((float)$total_remaining, 2, '.', ''),
        ];

        if (count($items) > 0) {
            return response([
                'success'   => true,
                'message'   => 'Cotton Production list',
                'data'      => $data
            ]);
        } else {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }
    }

    /**
     * cotton production report
     */
    public function index(Request $request)
    {
       
        $query = DB::table('grower_prod_achievements')
                    ->join('ginner_grower_profiles','grower_prod_achievements.applicant_id','ginner_grower_profiles.id')
                    ->select('ginner_grower_profiles.region_id',
                        'ginner_grower_profiles.district_id',
                        'ginner_grower_profiles.upazilla_id',
                        'ginner_grower_profiles.zone_id',
                        'ginner_grower_profiles.unit_id',
                        'ginner_grower_profiles.name',
                        'ginner_grower_profiles.name_bn',
                        'ginner_grower_profiles.mobile_no',
                        'ginner_grower_profiles.org_id',
                        'grower_prod_achievements.fiscal_year_id',
                        'grower_prod_achievements.seasons_id',
                        'grower_prod_achievements.cotton_variety_id',
                        'grower_prod_achievements.quantity',
                        'grower_prod_achievements.cotton_id'
                    )
                    ->addSelect(DB::raw('sum(grower_prod_achievements.quantity) as pro_achi_quantity'))
                    ->groupBy('ginner_grower_profiles.applicant_id')
                    ->groupBy('grower_prod_achievements.seasons_id')
                    ->groupBy('grower_prod_achievements.cotton_variety_id')
                    ->groupBy('grower_prod_achievements.cotton_id');

        if ($request->applicant_id) {
            $query = $query->where('ginner_grower_profiles.applicant_id', $request->applicant_id);
        }

        if ($request->region_id) {
            $query = $query->where('ginner_grower_profiles.region_id', $request->region_id);
        }

        if ($request->cotton_id) {
            $query = $query->where('grower_prod_achievements.cotton_id', $request->cotton_id);
        }

        if ($request->cotton_variety_id) {
            $query = $query->where('grower_prod_achievements.cotton_variety_id', $request->cotton_variety_id);
        }

        if ($request->org_id) {
            $query = $query->where('ginner_grower_profiles.org_id', $request->org_id);
        }
        if ($request->district_id) {
            $query = $query->where('ginner_grower_profiles.district_id', $request->district_id);
        }

        if ($request->upazilla_id) {
            $query = $query->where('ginner_grower_profiles.upazilla_id', $request->upazilla_id);
        }

        if ($request->unit_id) {
            $query = $query->where('ginner_grower_profiles.unit_id', $request->unit_id);
        }

        if ($request->zone_id) {
            $query = $query->where('ginner_grower_profiles.zone_id', $request->zone_id);
        }

        if ($request->name) {
            $query = $query->where('ginner_grower_profiles.name', 'like', "{$request->name}%")
                            ->orWhere('ginner_grower_profiles.name_bn', 'like', "{$request->name}%");
        }
        
        if ($request->fiscal_year_id) {
            $query = $query->where('grower_prod_achievements.fiscal_year_id', $request->fiscal_year_id);
        }

        if ($request->seasons_id) {
            $query = $query->where('grower_prod_achievements.seasons_id', $request->seasons_id);
        }

        if ($request->from_date && $request->to_date){
                $startDate   = date('Y-m-d', strtotime($request->from_date));
                $endDate     = date('Y-m-d', strtotime($request->to_date));
                $query       = $query->whereBetween('grower_prod_achievements.achievement_date', [$startDate, $endDate]);
            }

        if (isset($request->from_date) && !isset($request->to_date)){
                $query = $query->whereDate('grower_prod_achievements.achievement_date', '>=', date('Y-m-d', strtotime($request->from_date)));
            }

        if (!isset($request->from_date) && isset($request->to_date)){
                $query = $query->whereDate('grower_prod_achievements.achievement_date', '<=', date('Y-m-d', strtotime($request->to_date)));
            } 

        $list = $query->get();


        if(count($list)>0){
            return response([
                'success' => true,
                'message' => 'Production achievements list',
                'data' =>$list
            ]);

        }else{
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }

    }
}
