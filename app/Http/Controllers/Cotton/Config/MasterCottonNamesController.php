<?php

namespace App\Http\Controllers\Cotton\Config;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\Cotton\Config\MasterCottonNamesValidations;
use App\Models\Cotton\Config\MasterCottonNames;
use DB;

class MasterCottonNamesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new MasterCottonNames();

        if ($request->cotton_name) 
        {
            $query = $query->where('cotton_name', 'like', "{$request->cotton_name}%")
                           ->orWhere('cotton_name_bn', 'like', "{$request->cotton_name}%");
        }  

        if ($request->cotton_variety_id) {
            $query = $query->where('cotton_variety_id', $request->cotton_variety_id);
        }        

        if ($request->status) {
            $query->where('status', $request->status);
        }

        $list = $query->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Cotton names list',
            'data' =>$list
        ]);
    }

    /*+++++++show+++++++++*/
    public function show($id)
    {
        $masterCottonNames = MasterCottonNames::find($id);

        if (!$masterCottonNames) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Cotton name details',
            'data'    => $masterCottonNames
        ]);
    }

   
    /*+++++++store+++++++++*/
    public function store(Request $request)
    {
        $validationResult = MasterCottonNamesValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $postData = $request->all();
            $postData['created_by'] = (int)user_id()??null;
            $postData['updated_by'] = (int)user_id()??null;

            $masterCottonNames = new MasterCottonNames();
            $masterCottonNames->create($postData);

            save_log([
                'data_id'    => $masterCottonNames->id,
                'table_name' => 'master_cotton_names'
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $masterCottonNames
        ]);
    }  

    /*++++++update+++++++*/
    public function update(Request $request, $id)
    {
        $validationResult = MasterCottonNamesValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $masterCottonNames = MasterCottonNames::find($id);

        if (!$masterCottonNames) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {

            $putData = $request->all();
            $putData['updated_by'] = (int)user_id()??null;

            $masterCottonNames->update($putData);

            save_log([
                'data_id'       => $masterCottonNames->id,
                'table_name'    => 'master_cotton_names',
                'execution_type'=> 1
            ]);           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $masterCottonNames
        ]);
    }

   /*+++++status update++++++*/
    public function toggleStatus($id)
    {
        $masterCottonNames = MasterCottonNames::find($id);

        if (!$masterCottonNames) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterCottonNames->status = $masterCottonNames->status == 1 ? 2 : 1;
        $masterCottonNames->update();

        save_log([
            'data_id'       => $masterCottonNames->id,
            'table_name'    => 'master_cotton_names',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $masterCottonNames
        ]);
    }
    
    /*+++++sdestroy++++++*/
    public function destroy($id)
    {
        $masterCottonNames = MasterCottonNames::find($id);

        if (!$masterCottonNames) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterCottonNames->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'master_cotton_names',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
