<?php
namespace App\Http\Controllers\Cotton\Config;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\Cotton\Config\MasterZonesValidations;
use App\Models\Cotton\Config\MasterZones;
use DB;

class MasterZonesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new MasterZones();

        if ($request->zone_name) 
        {
            $query = $query->where('zone_name', 'like', "{$request->zone_name}%")
                           ->orWhere('zone_name_bn', 'like', "{$request->zone_name}%");
        }  

        if ($request->region_id) {
            $query = $query->where('region_id', $request->region_id);
        }

        if ($request->status) {
            $query->where('status', $request->status);
        }

        $list = $query->paginate($request->per_page??10); 

        return response([
            'success' => true,
            'message' => 'Zones list',
            'data' =>$list
        ]);
    }

    /*+++++++show+++++++++*/
    public function show($id)
    {
         $masterZones = MasterZones::find($id);

        if (!$masterZones) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Zones details',
            'data'    => $masterZones
        ]);
    }
   
    /*+++++++store+++++++++*/
    public function store(Request $request)
    {
        $validationResult = MasterZonesValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $postData = $request->all();
            $postData['created_by'] = (int)user_id()??null;
            $postData['updated_by'] = (int)user_id()??null;

            $masterZones = new MasterZones();
            $masterZones->create($postData);

            save_log([
                'data_id'    => $masterZones->id,
                'table_name' => 'master_zones'
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $masterZones
        ]);
    }

    /*++++++update+++++++*/
    public function update(Request $request, $id)
    {
        $validationResult = MasterZonesValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $masterZones = MasterZones::find($id);

        if (!$masterZones) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {

            $putData = $request->all();
            $putData['updated_by'] = (int)user_id()??null;

            $masterZones->update($putData);

            save_log([
                'data_id'       => $masterZones->id,
                'table_name'    => 'master_zones',
                'execution_type'=> 1
            ]);           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $masterZones
        ]);
    }

    /*+++++status update++++++*/
    public function toggleStatus($id)
    {
        $masterZones = MasterZones::find($id);

        if (!$masterZones) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterZones->status = $masterZones->status == 1 ? 2 : 1;
        $masterZones->update();

         save_log([
            'data_id'       => $id,
            'table_name'    => 'master_zones',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $masterZones
        ]);
    }
    
    /*+++++sdestroy++++++*/
    public function destroy($id)
    {
        $masterZones = MasterZones::find($id);

        if (!$masterZones) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterZones->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'master_zones',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
