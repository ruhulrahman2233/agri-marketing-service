<?php

namespace App\Http\Controllers\Cotton\Config;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\Cotton\Config\MasterHattsValidations;
use App\Models\Cotton\Config\MasterHatts;
use DB;

class MasterHattsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new MasterHatts();

        if ($request->hatt_name) 
        {
            $query = $query->where('hatt_name', 'like', "{$request->hatt_name}%")
                           ->orWhere('hatt_name_bn', 'like', "{$request->hatt_name}%");
        }  

        if ($request->division_id) {
            $query = $query->where('division_id', $request->division_id);
        }

        if ($request->district_id) {
            $query = $query->where('district_id', $request->district_id);
        }   

        if ($request->upazilla_id) {
            $query = $query->where('upazilla_id', $request->upazilla_id);
        } 

        if ($request->status) {
            $query->where('status', $request->status);
        }

        $list = $query->paginate($request->per_page??10); 

        return response([
            'success' => true,
            'message' => 'Hats list',
            'data'    => $list
        ]);
    }

    /*+++++++show+++++++++*/
    public function show($id)
    {
        $masterHatts = MasterHatts::find($id);

        if (!$masterHatts) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Hats details',
            'data'    => $masterHatts
        ]);
    }
   
    /*+++++++store+++++++++*/
    public function store(Request $request)
    {
        $validationResult = MasterHattsValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $postData = $request->all();
            $postData['created_by'] = (int)user_id()??null;
            $postData['updated_by'] = (int)user_id()??null;

            $masterHatts = new MasterHatts();
            $masterHatts->create($postData);

            /*save_log([
                'data_id'    => $masterHatts->id,
                'table_name' => 'master_hatts'
            ]);*/

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $masterHatts
        ]);
    }

  

     /*++++++update+++++++*/
    public function update(Request $request, $id)
    {
        $validationResult = MasterHattsValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $masterHatts = MasterHatts::find($id);

        if (!$masterHatts) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {

            $putData = $request->all();
            $putData['updated_by'] = (int)user_id()??null;

            $masterHatts->update($putData);

            /*save_log([
                'data_id'       => $masterHatts->id,
                'table_name'    => 'master_hatts',
                'execution_type'=> 1
            ]);*/

           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $masterHatts
        ]);
    }

   /*+++++status update++++++*/
    public function toggleStatus($id)
    {
        $masterHatts = MasterHatts::find($id);

        if (!$masterHatts) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterHatts->status = $masterHatts->status == 1 ? 2 : 1;
        $masterHatts->update();

        /*save_log([
            'data_id'       => $masterHatts->id,
            'table_name'    => 'master_hatts',
            'execution_type'=> 2
        ]);*/

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $masterHatts
        ]);
    }

    
   /*+++++destroy++++++*/
    public function destroy($id)
    {
        $masterHatts = MasterHatts::find($id);

        if (!$masterHatts) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterHatts->delete();

      /*  save_log([
            'data_id'       => $id,
            'table_name'    => 'master_hatts',
            'execution_type'=> 2
        ]);*/

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
