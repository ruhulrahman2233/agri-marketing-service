<?php

namespace App\Http\Controllers\Cotton\Config;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\Cotton\Config\MasterCottonVaritiesValidations;
use App\Models\Cotton\Config\MasterCottonVarities;
use DB;

class MasterCottonVaritiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new MasterCottonVarities();

        if ($request->cotton_variety) 
        {
            $query = $query->where('cotton_variety', 'like', "{$request->cotton_variety}%")
                           ->orWhere('cotton_variety_bn', 'like', "{$request->cotton_variety}%");
        }  

        if ($request->status) {
            $query->where('status', $request->status);
        }

        $list = $query->paginate(request('per_page', config('app.per_page'))); 

        return response([
            'success' => true,
            'message' => 'Regions list',
            'data' =>$list
        ]);
    }

    /*+++++++show+++++++++*/
    public function show($id)
    {
        $masterCottonVarities = MasterCottonVarities::find($id);

        if (!$masterCottonVarities) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Regions details',
            'data'    => $masterCottonVarities
        ]);
    }
   
    /*+++++++store+++++++++*/
    public function store(Request $request)
    {
        $validationResult = MasterCottonVaritiesValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $postData = $request->all();
            $postData['created_by'] = (int)user_id()??null;
            $postData['updated_by'] = (int)user_id()??null;

            $masterCottonVarities = new MasterCottonVarities();
            $masterCottonVarities->create($postData);

            save_log([
                'data_id'    => $masterCottonVarities->id,
                'table_name' => 'master_cotton_varities'
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $masterCottonVarities
        ]);
    }  

    /*++++++update+++++++*/
    public function update(Request $request, $id)
    {
        $validationResult = MasterCottonVaritiesValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $masterCottonVarities = MasterCottonVarities::find($id);

        if (!$masterCottonVarities) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {

            $putData = $request->all();
            $putData['updated_by'] = (int)user_id()??null;

            $masterCottonVarities->update($putData);

            save_log([
                'data_id'       => $masterCottonVarities->id,
                'table_name'    => 'master_cotton_varities',
                'execution_type'=> 1
            ]);           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $masterCottonVarities
        ]);
    }

   /*+++++status update++++++*/
    public function toggleStatus($id)
    {
        $masterCottonVarities = MasterCottonVarities::find($id);

        if (!$masterCottonVarities) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterCottonVarities->status = $masterCottonVarities->status == 1 ? 2 : 1;
        $masterCottonVarities->update();

        save_log([
            'data_id'       => $masterCottonVarities->id,
            'table_name'    => 'master_cotton_varities',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $masterCottonVarities
        ]);
    }
    
    /*+++++sdestroy++++++*/
    public function destroy($id)
    {
        $masterCottonVarities = MasterCottonVarities::find($id);

        if (!$masterCottonVarities) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterCottonVarities->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'master_cotton_varities',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}