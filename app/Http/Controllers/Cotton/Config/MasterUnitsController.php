<?php

namespace App\Http\Controllers\Cotton\Config;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\Cotton\Config\MasterUnitsValidations;
use App\Models\Cotton\Config\MasterUnits;
use DB;


class MasterUnitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new MasterUnits();

        if ($request->unit_name) 
        {
            $query = $query->where('unit_name', 'like', "{$request->unit_name}%")
                           ->orWhere('unit_name_bn', 'like', "{$request->unit_name}%");
        }  

        if ($request->org_id) {
            $query = $query->where('org_id', $request->org_id);
        }

        if ($request->region_id) {
            $query = $query->where('region_id', $request->region_id);
        }  

        if ($request->zone_id) {
            $query = $query->where('zone_id', $request->zone_id);
        }  

        if ($request->district_id) {
            $query = $query->where('district_id', $request->district_id);
        }   

        if ($request->upazilla_id) {
            $query = $query->where('upazilla_id', $request->upazilla_id);
        } 

        if ($request->status) {
            $query->where('status', $request->status);
        }

        $list = $query->paginate($request->per_page??10); 

        return response([
            'success' => true,
            'message' => 'Cotton names list',
            'data' =>$list
        ]);
    }


    /*+++++++show+++++++++*/
    public function show($id)
    {
        $masterUnits = MasterUnits::find($id);

        if (!$masterUnits) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Cotton name details',
            'data'    => $masterUnits
        ]);
    }

   
    /*+++++++store+++++++++*/
    public function store(Request $request)
    {
        // return $request;
        $validationResult = MasterUnitsValidations::validate($request);
       // return "ok";
        if (!$validationResult['success']) {
            return response($validationResult);
        }
       

        try {
           
            $postData = $request->all();
            $postData['created_by'] = (int)user_id()??null;
            $postData['updated_by'] = (int)user_id()??null;

            $masterUnits = new MasterUnits();
            $masterUnits->create($postData);

            /*save_log([
                'data_id'    => $masterUnits->id,
                'table_name' => 'master_units'
            ]);*/

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $masterUnits
        ]);
    }

  

     /*++++++update+++++++*/
    public function update(Request $request, $id)
    {
        $validationResult = MasterUnitsValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $masterUnits = MasterUnits::find($id);

        if (!$masterUnits) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {

            $putData = $request->all();
            $putData['updated_by'] = (int)user_id()??null;

            $masterUnits->update($putData);

            /*save_log([
                'data_id'       => $masterUnits->id,
                'table_name'    => 'master_units',
                'execution_type'=> 1
            ]);*/

           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $masterUnits
        ]);
    }

   /*+++++status update++++++*/
    public function toggleStatus($id)
    {
        $masterUnits = MasterUnits::find($id);

        if (!$masterUnits) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterUnits->status = $masterUnits->status == 1 ? 2 : 1;
        $masterUnits->update();

        /*save_log([
            'data_id'       => $masterUnits->id,
            'table_name'    => 'master_units',
            'execution_type'=> 2
        ]);*/

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $masterUnits
        ]);
    }

    
   /*+++++sdestroy++++++*/
    public function destroy($id)
    {
        $masterUnits = MasterUnits::find($id);

        if (!$masterUnits) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterUnits->delete();

      /*  save_log([
            'data_id'       => $id,
            'table_name'    => 'master_units',
            'execution_type'=> 2
        ]);*/

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
