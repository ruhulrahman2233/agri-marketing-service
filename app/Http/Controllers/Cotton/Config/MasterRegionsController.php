<?php
namespace App\Http\Controllers\Cotton\Config;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\Cotton\Config\MasterRegionsValidations;
use App\Models\Cotton\Config\MasterRegions;
use DB;

class MasterRegionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new MasterRegions();

        if ($request->region_name) 
        {
            $query = $query->where('region_name', 'like', "{$request->region_name}%")
                           ->orWhere('region_name_bn', 'like', "{$request->region_name}%");
        }  

        if ($request->status) {
            $query->where('status', $request->status);
        }

        $list = $query->paginate(request('per_page', config('app.per_page'))); 

        return response([
            'success' => true,
            'message' => 'Regions list',
            'data' =>$list
        ]);
    }

    /*+++++++show+++++++++*/
    public function show($id)
    {
        $masterRegions = MasterRegions::find($id);

        if (!$masterRegions) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Regions details',
            'data'    => $masterRegions
        ]);
    }
   
    /*+++++++store+++++++++*/
    public function store(Request $request)
    {
        $validationResult = MasterRegionsValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $postData = $request->all();
            $postData['created_by'] = (int)user_id()??null;
            $postData['updated_by'] = (int)user_id()??null;

            $masterRegions = new MasterRegions();
            $masterRegions->create($postData);

            save_log([
                'data_id'    => $masterRegions->id,
                'table_name' => 'master_regions'
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $masterRegions
        ]);
    } 

    /*++++++update+++++++*/
    public function update(Request $request, $id)
    {
        $validationResult = MasterRegionsValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $masterRegions = MasterRegions::find($id);

        if (!$masterRegions) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }
        try {

            $putData = $request->all();
            $putData['updated_by'] = (int)user_id()??null;

            $masterRegions->update($putData);

            save_log([
                'data_id'       => $masterRegions->id,
                'table_name'    => 'master_regions',
                'execution_type'=> 1
            ]);           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $masterRegions
        ]);
    }

   /*+++++status update++++++*/
    public function toggleStatus($id)
    {
        $masterRegions = MasterRegions::find($id);

        if (!$masterRegions) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterRegions->status = $masterRegions->status == 1 ? 2 : 1;
        $masterRegions->update();

        save_log([
            'data_id'       => $masterRegions->id,
            'table_name'    => 'master_regions',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $masterRegions
        ]);
    }
    
   /*+++++sdestroy++++++*/
    public function destroy($id)
    {
        $masterRegions = MasterRegions::find($id);

        if (!$masterRegions) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterRegions->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'master_regions',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
