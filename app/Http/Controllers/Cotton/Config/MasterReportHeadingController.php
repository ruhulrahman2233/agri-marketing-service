<?php

namespace App\Http\Controllers\Cotton\Config;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\GlobalFileUploadFunctoin;
use App\Library\PdfImage;
use App\Http\Validations\Cotton\Config\MasterReportHeadingValidations;
use App\Models\Cotton\Config\MasterReportHeading;

class MasterReportHeadingController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all Master Report Heading Type
     */
    public function index(Request $request)
    {
        $query = MasterReportHeading::select("*");

        if ($request->type_name) {
            $query = $query->where('heading', 'like', "{$request->heading}%")
                        ->orWhere('heading_bn', 'like', "{$request->heading}%");
        }

        if ($request->address) {
            $query = $query->where('address', 'like', "{$request->address}%")
                            ->orWhere('address_bn', 'like', "{$request->address}%");
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }

        $list = $query->paginate($request->per_page);

        return response([
            'success' => true,
            'message' => 'Report heading data list',
            'data'    => $list
        ]);
    }

    /**
     * Get detail data by id
     */
    public function detail(Request $request, $id)
    {
        $organizationId = $id;
        if (!$organizationId) {
            $organizationId = request()->org_id;
        }
        $model = MasterReportHeading::where('org_id', $organizationId)->first();

        $result = [
            'success' => false,
            'data' => [
                'left_logo' => '',
                'right_logo' => ''
            ],
            'message' => ''

        ];

        if (!$model) {
            $result['message'] = 'Report heading data not found.';
            return response($result);
        }

        $leftPath = $request->leftPath ?? '';
        $rightPath = $request->rightPath ?? '';

        $leftLogo = PdfImage::convertImage($leftPath . $model->left_logo);
        $rightLogo = PdfImage::convertImage($rightPath . $model->right_logo);

        if ($leftLogo['success']) {
            $result['data']['left_logo'] = $leftLogo['data'];
        } else {
            $result['message'] = $leftLogo['message'];
        }

        if ($rightLogo['success']) {
            $result['data']['right_logo'] = $rightLogo['data'];
        } else {
            $result['message'] .= $rightLogo['message'];
        }

        $result['success'] = true;
        $result['data']['address'] = $model->address;
        $result['data']['address_bn'] = $model->address_bn;
        return response($result);
    }

    /**
     * Master Report Heading  store
    */
    public function store(Request $request)
    {
        $validationResult =MasterReportHeadingValidations:: validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $leftLogoPath       = 'config/left-logo';
        $leftLogo           =  $request->file('left_logo');

        $rightLogoPath      = 'config/right-logo';
        $righttLogo         =  $request->file('right_logo');

        try {
            $report                    = new MasterReportHeading();
            $report->heading           = $request->heading;
            $report->heading_bn	       = $request->heading_bn;
            $report->address		   = $request->address;
            $report->address_bn		   = $request->address_bn;
            $report->org_id            = (int)$request->org_id;
            $report->created_by        = (int)user_id();
            $report->updated_by        = (int)user_id();

            if($leftLogo !=null && $leftLogo !=""){
                $leftLogoName = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request, $leftLogoPath,'left_logo');
            }

            $report->left_logo  = $leftLogoName ?? null;

            if($righttLogo !=null && $righttLogo !=""){
                $rightLogoName = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request, $rightLogoPath,'right_logo');
            }

            $report->right_logo  = $rightLogoName ?? null;

            if($report->save()){
                 if($leftLogo !=null && $leftLogo !=""){
                    GlobalFileUploadFunctoin::file_upload($request, $leftLogoPath, 'left_logo', $leftLogoName, $old_file=null);
                 }

                if($righttLogo !=null && $righttLogo !=""){
                    GlobalFileUploadFunctoin::file_upload($request, $rightLogoPath,'right_logo', $rightLogoName, $old_file=null);
                }
            }

            save_log([
                'data_id'   => $report->id,
                'table_name'=> 'master_report_headers'
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Report Heading Data save successfully',
            'data'    => $report
        ]);
    }

    /**
     * Master Report heading update
     */
    public function update(Request $request, $id)
    {
        $validationResult =MasterReportHeadingValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $report = MasterReportHeading::find($id);
        $oldLeftLogoName    = $report->left_logo;
        $oldRightLogoName   = $report->right_logo;

        $leftLogoPath       = 'config/left-logo';
        $leftLogo           =  $request->file('left_logo');

        $rightLogoPath      = 'config/right-logo';
        $righttLogo         =  $request->file('right_logo');

        if (!$report) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $report->heading           = $request->heading;
            $report->heading_bn	       = $request->heading_bn;
            $report->address		   = $request->address;
            $report->address_bn		   = $request->address_bn;
            $report->org_id            = (int)$request->org_id;

            if($leftLogo !=null && $leftLogo !=""){
                $leftLogoName = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request, $leftLogoPath,'left_logo');
            }

            $report->left_logo  = $leftLogoName ?? $oldLeftLogoName;

            if($righttLogo !=null && $righttLogo !=""){
                $rightLogoName = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request, $rightLogoPath,'right_logo');
            }

            $report->right_logo  = $rightLogoName ?? $oldRightLogoName;

            $report->updated_by      = (int)user_id();

            if($report->save()){
                 if($leftLogo !=null && $leftLogo !=""){
                    GlobalFileUploadFunctoin::file_upload($request, $leftLogoPath, 'left_logo', $leftLogoName, $oldLeftLogoName=null);
                 }

                if($righttLogo !=null && $righttLogo !=""){
                    GlobalFileUploadFunctoin::file_upload($request, $rightLogoPath,'right_logo', $rightLogoName, $oldRightLogoName);
                }
            }

            save_log([
                'data_id'       => $report->id,
                'table_name'    => 'master_report_headers',
                'execution_type'=> 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : ""
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Report Heading Data update successfully',
            'data'    => $report
        ]);
    }

    /**
     * Master Report heading  toggle status
     */
    public function toggleStatus($id)
    {
        $report = MasterReportHeading::find($id);

        if (!$report) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $report->status = $report->status ? 0 : 1;
        $report->save();

        save_log([
            'data_id'       => $report->id,
            'table_name'    => 'master_report_headers',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Report Heading Data updated successfully',
            'data'    => $report
        ]);
    }

    /**
     * Master Report Heading destroy
     */
    public function destroy($id)
    {
        $report = MasterReportHeading::find($id);

        if (!$report) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $report->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'master_report_headers',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Report Heading Data deleted successfully'
        ]);
    }
}
