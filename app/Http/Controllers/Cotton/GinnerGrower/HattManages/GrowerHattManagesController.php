<?php

namespace App\Http\Controllers\Cotton\GinnerGrower\HattManages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\Cotton\GinnerGrower\HattManages\GrowerHattManagesValidations;
use App\Library\SmsLibrary;
use App\Models\Cotton\Config\GinnerGrowerProfile;
use App\Models\Cotton\GinnerGrower\HattManages\GrowerHattManages;
use App\Models\Cotton\GinnerGrower\HattManages\HattNotification;
use DB;

class GrowerHattManagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function userProfile(Request $request)
    {        
        $query = DB::table('ginner_grower_profiles')->where('applicant_id', $request->applicant_id)->get(); 

        return response([
            'success' => true,
            'message' => 'Grower profiles details',
            'data'    => $query
        ]);
    } 

    /**
     * show all hatt list
     */
    public function index(Request $request)
    {
        $query = DB::table('grower_hatt_manages')
                    ->leftjoin('master_seasons','grower_hatt_manages.seasons_id','master_seasons.id')
                    ->leftjoin('master_regions','grower_hatt_manages.region_id','master_regions.id')
                    ->leftjoin('master_zones','grower_hatt_manages.zone_id','master_zones.id')
                    ->leftjoin('master_units','grower_hatt_manages.unit_id','master_units.id')
                    ->leftjoin('master_hatts','grower_hatt_manages.hatt_id','master_hatts.id')
                    ->select('grower_hatt_manages.*',
                        'master_seasons.season_name','master_seasons.season_name_bn',
                        'master_regions.region_name','master_regions.region_name_bn',
                        'master_zones.zone_name','master_zones.zone_name_bn',
                        'master_units.unit_name','master_units.unit_name_bn',
                        'master_hatts.hatt_name','master_hatts.hatt_name_bn',
                    )
                    ->orderBy('grower_hatt_manages.id','DESC');

        if ($request->fiscal_year_id) {
            $query = $query->where('grower_hatt_manages.fiscal_year_id', $request->fiscal_year_id);
        }

        if ($request->seasons_id) {
            $query = $query->where('grower_hatt_manages.seasons_id', $request->seasons_id);
        }

        if ($request->region_id) {
            $query = $query->where('grower_hatt_manages.region_id', $request->region_id);
        }

        if ($request->zone_id) {
            $query = $query->where('grower_hatt_manages.zone_id', $request->zone_id);
        }

        if ($request->unit_id) {
            $query = $query->where('grower_hatt_manages.unit_id', $request->unit_id);
        }

        if ($request->hatt_id) {
            $query = $query->where('grower_hatt_manages.hatt_id', $request->hatt_id);
        }         

        if ($request->status) {
            $query->where('status', $request->status);
        }

        if ($request->from_date && $request->to_date){
            $startDate   = date('Y-m-d', strtotime($request->from_date));
            $endDate     = date('Y-m-d', strtotime($request->to_date));
            $query       = $query->whereBetween('hatt_date', [$startDate, $endDate]);
        }

        if (isset($request->from_date) && !isset($request->to_date)){
            $query = $query->whereDate('hatt_date', '>=', date('Y-m-d', strtotime($request->from_date)));
        }

        if (!isset($request->from_date) && isset($request->to_date)){
            $query = $query->whereDate('hatt_date', '<=', date('Y-m-d', strtotime($request->to_date)));
        } 

        $list = $query->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Hats list',
            'data' =>$list
        ]);
    }

    /*
    * single show
    */
    public function show($id)
    {
        $growerHattManages = GrowerHattManages::find($id);

        if (!$growerHattManages) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Hats details',
            'data'    => $growerHattManages
        ]);
    }
   
    /**
     * store
     */
    public function store(Request $request)
    {   
        $validationResult = GrowerHattManagesValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        DB::beginTransaction();

        try {
            $postData = $request->all();
            $postData['created_by'] = (int)user_id() ?? null;
            $postData['updated_by'] = (int)user_id() ?? null;

            $growerHattManages = new GrowerHattManages($postData);
            $growerHattManages->save();   

            save_log([
                'data_id'    => $growerHattManages->id,
                'table_name' => 'grower_hatt_manages'
            ]);

            $ginnerGrowers = GinnerGrowerProfile::select('id','type','name','mobile_no')
                                                    ->where('district_id', $request->district_id)
                                                    ->where('upazilla_id', $request->upazilla_id)
                                                    ->where('region_id', $request->region_id)
                                                    ->where('zone_id', $request->zone_id)
                                                    ->where('unit_id', $request->unit_id)
                                                    ->get();

            foreach ($ginnerGrowers as $profile) { 
                //sms send 
                if ($profile->type == 1) {
                    $msg = "Dear ". $profile->name . ', you can purchase cotton from '.$request->address." in ". date('d M, Y', strtotime($request->hatt_date));
                } else {
                    $msg = "Dear ". $profile->name . ', you can sell cotton to '.$request->address." in ". date('d M, Y', strtotime($request->hatt_date));
                }

                $smsData['mobile']  = $profile->mobile_no;
                $smsData['message'] = $msg;
                $sms = new SmsLibrary();
                $sms->sms_helper($smsData);                       
                                       
                //notification add
                $hatNotification = new HattNotification(); 
                $hatNotification->hatt_id           = $request->hatt_id;
                $hatNotification->fiscal_year_id    = $request->fiscal_year_id;
                $hatNotification->seasons_id        = $request->seasons_id;
                $hatNotification->ginner_grower_id  = $profile->id;
                $hatNotification->message           = $msg;
                $hatNotification->save();
            }

            DB::commit();

        } catch (\Exception $ex) {

            DB::rollback();

            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $growerHattManages
        ]);
    }  

    /**
     * update
     */
    public function update(Request $request, $id)
    {
        $validationResult = GrowerHattManagesValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $growerHattManages = GrowerHattManages::find($id);

        if (!$growerHattManages) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {

            $putData = $request->all();
            $putData['updated_by'] = (int)user_id()??null;

            $growerHattManages->fill($putData);
            $growerHattManages->update();

            save_log([
                'data_id'       => $growerHattManages->id,
                'table_name'    => 'grower_hatt_manages',
                'execution_type'=> 1
            ]);          

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $growerHattManages
        ]);
    }

    /**
     * status update
    */
    public function toggleStatus($id)
    {
        $growerHattManages = GrowerHattManages::find($id);

        if (!$growerHattManages) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $growerHattManages->status = $growerHattManages->status == 1 ? 2 : 1;
        $growerHattManages->update();

        save_log([
            'data_id'       => $growerHattManages->id,
            'table_name'    => 'grower_hatt_manages',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $growerHattManages
        ]);
    }

    
    /**
     * destroy
    */
    public function destroy($id)
    {
        $growerHattManages = GrowerHattManages::find($id);

        if (!$growerHattManages) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $growerHattManages->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'grower_hatt_manages',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}