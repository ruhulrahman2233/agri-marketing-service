<?php

namespace App\Http\Controllers\Cotton\GinnerGrower;

use App\Http\Controllers\Controller;
use App\Http\Validations\Cotton\Config\GinnerGrowerProfileValidations;
use App\Models\Cotton\Config\GinnerGrowerProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;

class ProfileController extends Controller
{
    public function index(Request $request)
    { 
        $query = DB::table('ginner_grower_profiles')->select('*');

        if ($request->region_id) {
            $query = $query->where('region_id', $request->region_id);
        }

        if ($request->district_id) {
            $query = $query->where('district_id', $request->district_id);
        }

        if ($request->upazilla_id) {
            $query = $query->where('upazilla_id', $request->upazilla_id);
        }

        if ($request->unit_id) {
            $query = $query->where('unit_id', $request->unit_id);
        }

        $list =  $query->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Ginner/Grower Profile list',
            'data'    => $list
        ]);
    }

    public function store(Request $request)
    {
        $validationResult = GinnerGrowerProfileValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            DB::beginTransaction();

            $baseUrl = config('app.base_url.auth_service');
            $uri = '/external/user/check-user-existence';
            $params = ['username' => $request->mobile_no];
            $response = \App\Library\RestService::getData($baseUrl, $uri, $params);
            $userExists = json_decode($response)->success;

            if ($userExists) {
                $v = Validator::make([], []);
                $v->errors()->add('mobile_no', 'User exists, please enter another number.');

                DB::rollBack();

                return response([
                    'success' => false,
                    'message' => 'Failed to save data.',
                    'errors'  => $v->errors()
                ]);

            } else {
                $model = new GinnerGrowerProfile();

                $applicantId = $this->getRandom($model);

                $postData = $request->all();
                $postData['applicant_id'] = $applicantId;
                $postData['created_by'] = (int)user_id()??null;
                $postData['updated_by'] = (int)user_id()??null;
                $postData['password'] = Hash::make($postData['password']);

                $model = $model->create($postData);
                DB::commit();
            }

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'type' => 'create',
            'message' => 'Data save successfully',
            'data'    => $model
        ]);
    }

    public function update(Request $request, $id)
    {
        $validationResult = GinnerGrowerProfileValidations::updateValidate($request);
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }
        
        if($request->password_confirm !== '') {
            if ($request->password !== $request->password_confirm) {
                return response([
                    'success' => false,
                    'message' => 'Confirm Password Not Match',
                ]);
            }
        }

        $hasError = false;

        try {
            DB::beginTransaction();
            $model = GinnerGrowerProfile::find($id);
            $existingMobile = $model->mobile_no;

            if ($request->mobile_no == $model->mobile_no) {
                $userExists = false;
            } else {
                $baseUrl = config('app.base_url.auth_service');
                $uri = '/external/user/check-user-existence';
                $params = ['username' => $request->mobile_no];
                $response = \App\Library\RestService::getData($baseUrl, $uri, $params);
                $userExists = json_decode($response)->success;
            }
            if ($userExists) {
                $v = Validator::make([], []);
                $v->errors()->add('mobile_no', 'User exists, please enter another number.');
                return response([
                    'success' => false,
                    'message' => 'Failed to save data.',
                    'errors'  => $v->errors()
                ]);

            } else {
                $postData = $request->all();
                $postData['updated_by'] = (int)user_id()??null;

                if ($request->filled('password')) {
                    $postData['password'] = Hash::make($postData['password']);
                }

                if ($model->update($postData)) {
                    $baseUrl = config('app.base_url.auth_service');
                    $uri = '/external/user/user-update';
                    $params = ['username' => $request->mobile_no, 'mobile_no' => $existingMobile];

                    if ($request->filled('password')) {
                        $params = array_merge($params, ['password' => $postData['password']]);
                    }

                    $response = \App\Library\RestService::postData($baseUrl, $uri, $params);
                    $successful = json_decode($response)->success;

                    if ($successful) {
                        $hasError = true;
                    }
                } else {
                    $hasError = true;
                }
            }

            if ($hasError) {
                DB::commit();
            } else {
                DB::rollBack();
            }

        } catch (\Exception $ex) {
            DB::rollBack();

            return response([
                'success' => false,
                'message' => 'Failed to updated data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'type' => 'update',
            'message' => 'Data updated successfully',
            'data'    => $model
        ]);
    }    

    // Get Grower List
    public function getGrowerList (Request $request) {
        $list = [];
        if ($request->filled('unit_id')) {
            $list = DB::table('ginner_grower_profiles')
                ->where('unit_id', $request->unit_id)
                ->where('status', 1)
                ->select('id as applicant_id', 'type', 'nid', 'mobile_no', 'status', 'address_bn', 'address', 'land_area', 'father_name_bn',
                    'father_name', 'name_bn', 'name', 'zone_id', 'unit_id', 'upazilla_id', 'district_id', 'region_id', 'org_id')
                ->get();
        }

        return response([
            'success' => true,
            'message' => 'Grower Name list',
            'data' => $list
        ]);
    }

    public function toggleStatus($id)
    {
        $model = GinnerGrowerProfile::find($id);

        if (!$model) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $model->status = $model->status == 1 ? 2 : 1;
        $model->save();

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $model
        ]);
    }

    protected function getRandom ($model): int
    {
        $number = $this->random();
        while (true) {
            $randNum = $this->random();
            if ($model->where('applicant_id', $randNum)->exists()) {
                $number = $randNum;
            } else {
                break;
            }
        }

        return $number;
    }

    protected function random (): int
    {
        return mt_rand(10, 99999999);
    }

    public function destroy ($id) {
        $model = GinnerGrowerProfile::find($id);
        $mobile = $model ? $model->mobile_no : '';
        $destroyed = $model->delete();

        if ($destroyed) {
            return response([
                'success' => true,
                'message' => 'Data deleted successfully',
                'data'    => $model,
                'mobile_no'    => $mobile
            ]);
        } else {
            return response([
                'success' => false,
                'message' => 'Data deletion failed.'
            ]);
        }
    }
}
