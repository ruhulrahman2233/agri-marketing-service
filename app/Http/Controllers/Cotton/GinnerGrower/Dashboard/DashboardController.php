<?php

namespace App\Http\Controllers\Cotton\GinnerGrower\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class DashboardController extends Controller
{
     public function index(Request $request)
    {
        $query = DB::table('grower_prod_achievements')                    
                    ->leftJoin('grower_sell_entry','grower_prod_achievements.applicant_id','grower_sell_entry.applicant_id')
                    ->leftJoin('grower_central_stocks','grower_prod_achievements.applicant_id','grower_central_stocks.applicant_id')
                    ->leftJoin('ginner_grower_profiles','grower_prod_achievements.applicant_id','ginner_grower_profiles.id')
                    ->select('ginner_grower_profiles.id',
                        'ginner_grower_profiles.region_id',
                        'ginner_grower_profiles.district_id',
                        'ginner_grower_profiles.upazilla_id',
                        'ginner_grower_profiles.zone_id',
                        'ginner_grower_profiles.name',
                        'ginner_grower_profiles.name_bn',
                        'ginner_grower_profiles.mobile_no',
                        'grower_prod_achievements.seasons_id',
                        'grower_prod_achievements.cotton_variety_id',
                        'grower_prod_achievements.cotton_id',
                        'grower_central_stocks.quantity as remain_quantity')
                    ->addSelect(DB::raw('sum(grower_prod_achievements.quantity) as pro_achi_quantity'))
                    ->addSelect(DB::raw('sum(grower_sell_entry.quantity) as sell_quantity'))
                        ->groupBy('grower_prod_achievements.fiscal_year_id') 
                        ->groupBy('grower_prod_achievements.seasons_id') 
                        ->groupBy('grower_prod_achievements.cotton_variety_id')
                        ->groupBy('grower_prod_achievements.cotton_id');

        if ($request->applicant_id) {
            $query = $query->where('ginner_grower_profiles.applicant_id', $request->applicant_id);
        }

        if ($request->region_id) {
            $query = $query->where('ginner_grower_profiles.region_id', $request->region_id);
        }

        if ($request->district_id) {
            $query = $query->where('ginner_grower_profiles.district_id', $request->district_id);
        }

        if ($request->upazilla_id) {
            $query = $query->where('ginner_grower_profiles.upazilla_id', $request->upazilla_id);
        }

        if ($request->unit_id) {
            $query = $query->where('ginner_grower_profiles.unit_id', $request->unit_id);
        }

        if ($request->zone_id) {
            $query = $query->where('ginner_grower_profiles.zone_id', $request->zone_id);
        }

        if ($request->name) {
            $query = $query->where('ginner_grower_profiles.name', 'like', "{$request->name}%")
                            ->orWhere('ginner_grower_profiles.name_bn', 'like', "{$request->name}%");
        }
        
        if ($request->fiscal_year_id) {
            $query = $query->where('grower_prod_achievements.fiscal_year_id', $request->fiscal_year_id);
        }
         	 

        if ($request->seasons_id) {
            $query = $query->where('grower_prod_achievements.seasons_id', $request->seasons_id);
        }

         if ($request->cotton_variety_id) {
            $query = $query->where('grower_prod_achievements.cotton_variety_id', $request->cotton_variety_id);
        }

        if ($request->cotton_id) {
            $query = $query->where('grower_prod_achievements.cotton_id', $request->cotton_id);
        }

        if ($request->from_date && $request->to_date){
                $startDate   = date('Y-m-d', strtotime($request->from_date));
                $endDate     = date('Y-m-d', strtotime($request->to_date));
                $query       = $query->whereBetween('grower_prod_achievements.achievement_date', [$startDate, $endDate]);
            }

        if (isset($request->from_date) && !isset($request->to_date)){
                $query = $query->whereDate('grower_prod_achievements.achievement_date', '>=', date('Y-m-d', strtotime($request->from_date)));
            }

        if (!isset($request->from_date) && isset($request->to_date)){
                $query = $query->whereDate('grower_prod_achievements.achievement_date', '<=', date('Y-m-d', strtotime($request->to_date)));
            } 

         $list = $query->get();


        if(count($list)>0){
            return response([
                'success' => true,
                'message' => 'Production achievements list',
                'data' =>$list
            ]);

        }else{
            return response([
                'success' => true,
                'message' => 'Data not found',
                'data' =>$list
            ]);
        }
    }
}
