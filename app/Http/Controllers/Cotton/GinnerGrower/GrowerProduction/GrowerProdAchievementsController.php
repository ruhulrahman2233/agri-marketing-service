<?php

namespace App\Http\Controllers\Cotton\GinnerGrower\GrowerProduction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cotton\GinnerGrower\GrowerProduction\GrowerProdAchievements;
Use App\Http\Validations\Cotton\GinnerGrower\GrowerProduction\GrowerProdAchievementsValidations;
use App\Models\Cotton\GinnerGrower\GrowerStock\GrowerCentralStocks;
use DB;

class GrowerProdAchievementsController extends Controller
{
    public function index(Request $request)
    {
        $query = DB::table('grower_prod_achievements')
                    ->join('ginner_grower_profiles','grower_prod_achievements.applicant_id','ginner_grower_profiles.id')
                    ->select('ginner_grower_profiles.region_id',
                        'ginner_grower_profiles.district_id',
                        'ginner_grower_profiles.upazilla_id',
                        'ginner_grower_profiles.zone_id',
                        'ginner_grower_profiles.unit_id',
                        'ginner_grower_profiles.name',
                        'ginner_grower_profiles.name_bn',
                        'ginner_grower_profiles.mobile_no',
                        'ginner_grower_profiles.father_name',
                        'ginner_grower_profiles.father_name_bn',
                        'ginner_grower_profiles.address',
                        'ginner_grower_profiles.address_bn',
                        'grower_prod_achievements.seasons_id',
                        'grower_prod_achievements.cotton_variety_id',
                        'grower_prod_achievements.achievement_date',
                        'grower_prod_achievements.applicant_id',
                        'grower_prod_achievements.applicant_id as grower_id',
                        'grower_prod_achievements.fiscal_year_id',
                        'grower_prod_achievements.remarks',
                        'grower_prod_achievements.remarks_bn',
                        'grower_prod_achievements.quantity',
                        'grower_prod_achievements.cotton_id',
                        'grower_prod_achievements.id',
                        'grower_prod_achievements.status'
                    )->orderBy('grower_prod_achievements.created_at', 'DESC')
                    ->orderBy('grower_prod_achievements.status', 'ASC');;

        if ($request->applicant_id) {
            $query = $query->where('grower_prod_achievements.applicant_id', $request->applicant_id);
        }

        if ($request->region_id) {
            $query = $query->where('ginner_grower_profiles.region_id', $request->region_id);
        }

        if ($request->district_id) {
            $query = $query->where('ginner_grower_profiles.district_id', $request->district_id);
        }

        if ($request->upazilla_id) {
            $query = $query->where('ginner_grower_profiles.upazilla_id', $request->upazilla_id);
        }

        if ($request->unit_id) {
            $query = $query->where('ginner_grower_profiles.unit_id', $request->unit_id);
        }

        if ($request->zone_id) {
            $query = $query->where('ginner_grower_profiles.zone_id', $request->zone_id);
        }

        if ($request->name) {
            $query = $query->where('ginner_grower_profiles.name', 'like', "{$request->name}%")
                            ->orWhere('ginner_grower_profiles.name_bn', 'like', "{$request->name}%");
        }

        if ($request->fiscal_year_id) {
            $query = $query->where('grower_prod_achievements.fiscal_year_id', $request->fiscal_year_id);
        }

        if ($request->seasons_id) {
            $query = $query->where('grower_prod_achievements.seasons_id', $request->seasons_id);
        }

        $list = $query->paginate(request('per_page', config('app.per_page')));

        if (count($list) > 0) {
            return response([
                'success' => true,
                'message' => 'Production achievements list',
                'data' =>$list
            ]);

        } else {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }
    }

    /*+++++++show+++++++++*/
    public function show($id)
    {
        $growerProdAchievements = GrowerProdAchievements::find($id);

        if (!$growerProdAchievements) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Hats details',
            'data'    => $growerProdAchievements
        ]);
    }

    /*+++++++store+++++++++*/
    public function store(Request $request)
    {
        $validationResult = GrowerProdAchievementsValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $oldGrowerCentralStocks = GrowerCentralStocks::where('type',(int)$request->type)
                                ->where('fiscal_year_id', (int)$request->fiscal_year_id)
                                ->where('seasons_id', (int)$request->seasons_id)
                                ->where('applicant_id', (int)$request->applicant_id)
                                ->where('cotton_variety_id', (int)$request->cotton_variety_id)
                                ->where('cotton_id', (int)$request->cotton_id)
                                ->first();

        if($oldGrowerCentralStocks){
            $previousStockQuantity = $oldGrowerCentralStocks->quantity;
            $currentStockQuantaty  = (float)$previousStockQuantity + (float)$request->quantity;
        }else{
            $currentStockQuantaty  = (float)$request->quantity;
        }

        DB::beginTransaction();

        try {
            $growerProdAchievements = new GrowerProdAchievements();

            $growerProdAchievements->fiscal_year_id     = (int)$request->fiscal_year_id;
            $growerProdAchievements->achievement_date   = $request->achievement_date;
            $growerProdAchievements->applicant_id       = $request->applicant_id;
            $growerProdAchievements->cotton_variety_id  = (int)$request->cotton_variety_id;
            $growerProdAchievements->cotton_id          = (int)$request->cotton_id;
            $growerProdAchievements->seasons_id         = (int)$request->seasons_id;
            $growerProdAchievements->quantity           = $request->quantity;
            $growerProdAchievements->remarks            = $request->remarks??null;
            $growerProdAchievements->remarks_bn         = $request->remarks_bn??null;
            $growerProdAchievements->created_by         = (int)user_id()??null;
            $growerProdAchievements->updated_by         = (int)user_id()??null;
            $growerProdAchievements->save();

            if ($oldGrowerCentralStocks) {
                $oldGrowerCentralStocks->quantity       = $currentStockQuantaty;
                $oldGrowerCentralStocks->update();
            } else {
                $growerCentralStocks = new GrowerCentralStocks();
                $growerCentralStocks->type                  = 2;
                $growerCentralStocks->applicant_id          = (int)$request->applicant_id;
                $growerCentralStocks->org_id                = (int)$request->org_id;
                $growerCentralStocks->fiscal_year_id        = (int)$request->fiscal_year_id;
                $growerCentralStocks->seasons_id            = (int)$request->seasons_id;
                $growerCentralStocks->cotton_variety_id     = (int)$request->cotton_variety_id;
                $growerCentralStocks->cotton_id             = (int)$request->cotton_id;
                $growerCentralStocks->quantity              = $currentStockQuantaty;
                $growerCentralStocks->save();
            }

            save_log([
                'data_id'    => $growerCentralStocks->id,
                'table_name' => 'grower_prod_achievements'
            ]);

            DB::commit();

        } catch (\Exception $ex) {

            DB::rollback();

            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $growerProdAchievements
        ]);
    }

    /*+++++++update+++++++++*/
    public function update(Request $request, $id)
    {  
        $validationResult = GrowerProdAchievementsValidations::validate($request,$id);

        $growerProdAchievements = GrowerProdAchievements::find($id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $oldGrowerCentralStocks = GrowerCentralStocks::where('type',(int)$request->type)
                                ->where('fiscal_year_id', (int)$request->fiscal_year_id)
                                ->where('seasons_id', (int)$request->seasons_id)
                                ->where('applicant_id', (int)$request->applicant_id)
                                ->where('cotton_variety_id', (int)$request->cotton_variety_id)
                                ->where('cotton_id', (int)$request->cotton_id)
                                ->first();

        if ($growerProdAchievements && $oldGrowerCentralStocks != null) { 
            $previousQuantity       = $growerProdAchievements->quantity;
            $previousStockQuantity  = $oldGrowerCentralStocks->quantity;
            $requestNewQuantity     = $request->quantity;
            $currentStockQuantaty   = $previousStockQuantity;

            if ($previousQuantity > $requestNewQuantity) {
                $reduceQuantity = $previousQuantity - $requestNewQuantity;

                if ($reduceQuantity) {
                    $currentStockQuantaty  = $previousStockQuantity - $reduceQuantity;
                }
            }

            if ($previousQuantity < $requestNewQuantity) {
                $increaseQuantity = $requestNewQuantity - $previousQuantity;

                if ($increaseQuantity) {
                    $currentStockQuantaty  = $previousStockQuantity + $increaseQuantity;
                }
            }

        } else {
            $currentStockQuantaty  = (float)$request->quantity;
        }

        DB::beginTransaction();

        try {

            $growerProdAchievements->fiscal_year_id     = (int)$request->fiscal_year_id;
            $growerProdAchievements->achievement_date   = $request->achievement_date;
            $growerProdAchievements->applicant_id       = $request->applicant_id;
            $growerProdAchievements->cotton_variety_id  = (int)$request->cotton_variety_id;
            $growerProdAchievements->cotton_id          = (int)$request->cotton_id;
            $growerProdAchievements->seasons_id         = (int)$request->seasons_id;
            $growerProdAchievements->quantity           = $request->quantity;
            $growerProdAchievements->remarks            = $request->remarks??null;
            $growerProdAchievements->remarks_bn         = $request->remarks_bn??null;
            $growerProdAchievements->updated_by         = (int)user_id()??null;
            $growerProdAchievements->update();

            if ($oldGrowerCentralStocks != null) { 
                $oldGrowerCentralStocks->quantity       = $currentStockQuantaty;
                $oldGrowerCentralStocks->update();
            } else { 
                $growerCentralStocks = new GrowerCentralStocks();
                $growerCentralStocks->type                  = 2;
                $growerCentralStocks->applicant_id          = (int)$request->applicant_id;
                $growerCentralStocks->org_id                = (int)$request->org_id;
                $growerCentralStocks->fiscal_year_id        = (int)$request->fiscal_year_id;
                $growerCentralStocks->seasons_id            = (int)$request->seasons_id;
                $growerCentralStocks->cotton_variety_id     = (int)$request->cotton_variety_id;
                $growerCentralStocks->cotton_id             = (int)$request->cotton_id;
                $growerCentralStocks->quantity              = $currentStockQuantaty;
                $growerCentralStocks->save();
            }

            /*save_log([
                'data_id'    => $growerSellEntry->id,
                'table_name' => 'sd_stock_infos'
            ]);*/

            DB::commit();


        } catch (\Exception $ex) {

            DB::rollback();

            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $growerProdAchievements
        ]);
    }

    /*+++++++toggleStatus+++++++++*/
    public function toggleStatus($id)
    {
        $growerProdAchievements = GrowerProdAchievements::find($id);

        if (!$growerProdAchievements) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }


        $fiscalYearId       = $growerProdAchievements->fiscal_year_id;
        $seasonsId          = $growerProdAchievements->seasons_id;
        $applicantId        = $growerProdAchievements->applicant_id;
        $cottonVarietyId    = $growerProdAchievements->cotton_variety_id;
        $cottonId           = $growerProdAchievements->cotton_id;
        $quantity           = (int)$growerProdAchievements->quantity;


        $oldGrowerCentralStocks = GrowerCentralStocks::where('fiscal_year_id', $fiscalYearId)
                                ->where('seasons_id', $seasonsId)
                                ->where('applicant_id', $applicantId)
                                ->where('cotton_variety_id', $cottonVarietyId)
                                ->where('cotton_id', $cottonId)
                                ->first();

        $oldCentralStocksQuantity = (int)$oldGrowerCentralStocks->quantity;


        if($growerProdAchievements->status == 1){
            $currentQuantity = $oldCentralStocksQuantity - $quantity;
        }
        else
        {
            $currentQuantity = $oldCentralStocksQuantity + $quantity;
        }

        $growerProdAchievements->status = $growerProdAchievements->status == 1 ? 2 : 1;

        if($growerProdAchievements->save()){
            $oldGrowerCentralStocks->quantity = $currentQuantity;
            $oldGrowerCentralStocks->save();
        }

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $growerProdAchievements
        ]);
    }

    /*-----Close Prod Achievement-----*/
    public function close ($id) {
        $growerProdAchievements = GrowerProdAchievements::find($id);

        if (!$growerProdAchievements) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $growerProdAchievements->status = $growerProdAchievements->status == 1 ? 2 : 1;
        $growerProdAchievements->save();

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $growerProdAchievements
        ]);
    }


}
