<?php

namespace App\Http\Controllers\Cotton\GinnerGrower;

use App\Http\Controllers\Controller;
use App\Http\Validations\Cotton\GinnerGrower\ScheduleValidation;
use App\Models\Cotton\GinnerGrower\GinnerSchedule;
use Illuminate\Http\Request;
use DB;

class ScheduleController extends Controller
{
    /**
     * get all shchedule list
     */
    public function index (Request $request) 
    {   
        $query = DB::table('ginner_schedules')
                    ->leftjoin('ginner_grower_profiles','ginner_schedules.applicant_id','ginner_grower_profiles.id')
                    ->leftjoin('master_regions','ginner_grower_profiles.region_id','master_regions.id')
                    ->leftjoin('master_cotton_varities','ginner_schedules.cotton_variety_id','master_cotton_varities.id')
                    ->leftjoin('master_zones','ginner_grower_profiles.zone_id','master_zones.id')
                    ->leftjoin('master_units','ginner_grower_profiles.unit_id','master_units.id')
                    ->leftjoin('master_hatts','ginner_schedules.hatt_id','master_hatts.id')
                    ->leftjoin('master_seasons','ginner_schedules.seasons_id','master_seasons.id')
                    ->leftjoin('master_cotton_names','ginner_schedules.cotton_id','master_cotton_names.id')
                    ->select('ginner_schedules.*',
                        'ginner_grower_profiles.applicant_id as application_id',
                        'ginner_grower_profiles.zone_id',
                        'ginner_grower_profiles.org_id',
                        'ginner_grower_profiles.region_id','ginner_grower_profiles.district_id',
                        'ginner_grower_profiles.upazilla_id','ginner_grower_profiles.unit_id',
                        'ginner_grower_profiles.name','ginner_grower_profiles.name_bn','ginner_grower_profiles.father_name','ginner_grower_profiles.father_name_bn',                        
                        'master_regions.region_name','master_regions.region_name_bn',
                        'master_zones.zone_name','master_zones.zone_name_bn',
                        'master_units.unit_name','master_units.unit_name_bn',
                        'master_cotton_varities.cotton_variety','master_cotton_varities.cotton_variety_bn',
                        'master_hatts.hatt_name','master_hatts.hatt_name_bn',
                        'master_seasons.season_name','master_seasons.season_name_bn',
                        'master_cotton_names.cotton_name','master_cotton_names.cotton_name_bn'
                    )
                    ->orderBy('ginner_schedules.id','DESC');

        if ($request->fiscal_year_id) {
            $query = $query->where('ginner_schedules.fiscal_year_id', $request->fiscal_year_id);
        }

        if ($request->org_id) {
            $query = $query->where('ginner_grower_profiles.org_id', $request->org_id);
        }

        if ($request->from_date && $request->to_date) {
            $query = $query->whereBetween('ginner_schedules.schedule_date', array($request->from_date, $request->to_date));
        } else if ($request->from_date) {
            $query = $query->where('ginner_schedules.schedule_date', '=', $request->from_date);
        }

        $list = $query->paginate(request('per_page', config('app.per_page'))); 

        return response([
            'success'   => true,
            'message'   => 'Schedule list',
            'data'      => $list
        ]);
    }

    /**
     * get all approved shchedule list
     */
    public function approvedList (Request $request) 
    {   
        $query = DB::table('ginner_schedules')
                    ->leftjoin('ginner_grower_profiles','ginner_schedules.applicant_id','ginner_grower_profiles.id')
                    ->leftjoin('master_regions','ginner_grower_profiles.region_id','master_regions.id')
                    ->leftjoin('master_cotton_varities','ginner_schedules.cotton_variety_id','master_cotton_varities.id')
                    ->leftjoin('master_zones','ginner_grower_profiles.zone_id','master_zones.id')
                    ->leftjoin('master_units','ginner_grower_profiles.unit_id','master_units.id')
                    ->leftjoin('master_hatts','ginner_schedules.hatt_id','master_hatts.id')
                    ->leftjoin('master_seasons','ginner_schedules.seasons_id','master_seasons.id')
                    ->leftjoin('master_cotton_names','ginner_schedules.cotton_id','master_cotton_names.id')
                    ->select('ginner_schedules.*',
                        'ginner_grower_profiles.applicant_id as application_id',
                        'ginner_grower_profiles.zone_id',
                        'ginner_grower_profiles.org_id','ginner_grower_profiles.region_id','ginner_grower_profiles.district_id',
                        'ginner_grower_profiles.upazilla_id','ginner_grower_profiles.unit_id',
                        'ginner_grower_profiles.name','ginner_grower_profiles.name_bn','ginner_grower_profiles.father_name','ginner_grower_profiles.father_name_bn',                        
                        'master_regions.region_name','master_regions.region_name_bn',
                        'master_zones.zone_name','master_zones.zone_name_bn',
                        'master_units.unit_name','master_units.unit_name_bn',
                        'master_cotton_varities.cotton_variety','master_cotton_varities.cotton_variety_bn',
                        'master_hatts.hatt_name','master_hatts.hatt_name_bn',
                        'master_seasons.season_name','master_seasons.season_name_bn',
                        'master_cotton_names.cotton_name','master_cotton_names.cotton_name_bn'
                    )
                    ->where('ginner_schedules.status', 1) //1 mean only approve list shows
                    ->orderBy('ginner_schedules.id','DESC');

        if ($request->fiscal_year_id) {
            $query = $query->where('ginner_schedules.fiscal_year_id', $request->fiscal_year_id);
        }

        if ($request->org_id) {
            $query = $query->where('ginner_grower_profiles.org_id', $request->org_id);
        }
        
        if ($request->from_date && $request->to_date) {
            $query = $query->whereBetween('ginner_schedules.schedule_date', array($request->from_date, $request->to_date));
        } else if ($request->from_date) {
            $query = $query->where('ginner_schedules.schedule_date', '=', $request->from_date);
        }
        // if ($request->start_date) {
        //     $query = $query->where('ginner_schedules.schedule_date', date('Y-m-d',strtotime($request->start_date)));
        // }

        // if ($request->end_date) {
        //     $query = $query->where('ginner_schedules.schedule_date', date('Y-m-d',strtotime($request->end_date)));
        // }

        $list = $query->paginate(request('per_page', config('app.per_page'))); 

        return response([
            'success'   => true,
            'message'   => 'Schedule list',
            'data'      => $list
        ]);
    }

    /**
     * store ginner schedule
     */
    public function store(Request $request)
    {
        // return $request;
        $validationResult = ScheduleValidation::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            
            $schedule = new GinnerSchedule();
            $schedule->fiscal_year_id   = $request->fiscal_year_id;
            $schedule->schedule_date    = date('Y-m-d', strtotime($request->schedule_date));
            $schedule->applicant_id     = (int)$request->applicant_id;
            $schedule->cotton_variety_id= (int)$request->cotton_variety_id;
            $schedule->hatt_id          = (int)$request->hatt_id;
            $schedule->seasons_id       = (int)$request->seasons_id;
            $schedule->cotton_id        = (int)$request->cotton_id;
            $schedule->quantity         = $request->quantity;
            $schedule->remarks          = $request->remarks;
            $schedule->remarks_bn       = $request->remarks_bn;
            $schedule->created_by       = (int)user_id() ?? null;
            $schedule->updated_by       = (int)user_id() ?? null;
            $schedule->save();

            /*save_log([
                'data_id'    => $schedule->id,
                'table_name' => 'master_cotton_varities'
            ]);*/

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $schedule
        ]);
    }
    
    /**
     * update ginner schedule
     */
    public function update(Request $request, $id)
    {
        $validationResult = ScheduleValidation::validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            
            $schedule = GinnerSchedule::find($id);
            $schedule->fiscal_year_id   = $request->fiscal_year_id;
            $schedule->schedule_date    = date('Y-m-d', strtotime($request->schedule_date));
            $schedule->applicant_id     = (int)$request->applicant_id;
            $schedule->cotton_variety_id= (int)$request->cotton_variety_id;
            $schedule->hatt_id          = (int)$request->hatt_id;
            $schedule->seasons_id       = (int)$request->seasons_id;
            $schedule->cotton_id        = (int)$request->cotton_id;
            $schedule->quantity         = $request->quantity;
            $schedule->remarks          = $request->remarks;
            $schedule->remarks_bn       = $request->remarks_bn;
            $schedule->created_by       = (int)user_id() ?? null;
            $schedule->updated_by       = (int)user_id() ?? null;
            $schedule->update();

            /*save_log([
                'data_id'    => $schedule->id,
                'table_name' => 'master_cotton_varities'
            ]);*/

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $schedule
        ]);
    }

    /**
     * approve ginner schedule
     */
    public function approve($id)
    {
        $schedule = GinnerSchedule::find($id);

        if (!$schedule) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $schedule->status = 2; //2 mean approve
        $schedule->update();

      /*  save_log([
            'data_id'       => $id,
            'table_name'    => 'master_units',
            'execution_type'=> 2
        ]);*/

        return response([
            'success' => true,
            'message' => 'Data approved successfully'
        ]);
    }

    /**
     * reject ginner schedule
     */
    public function reject($id)
    {
        $schedule = GinnerSchedule::find($id);

        if (!$schedule) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $schedule->status = 3; //3 mean reject
        $schedule->update();

      /*  save_log([
            'data_id'       => $id,
            'table_name'    => 'master_units',
            'execution_type'=> 2
        ]);*/

        return response([
            'success' => true,
            'message' => 'Data reject successfully'
        ]);
    }
}
