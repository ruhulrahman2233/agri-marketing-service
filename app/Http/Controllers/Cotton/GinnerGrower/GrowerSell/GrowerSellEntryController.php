<?php

namespace App\Http\Controllers\Cotton\GinnerGrower\GrowerSell;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cotton\GinnerGrower\GrowerSell\GrowerSellEntry;
use App\Models\Cotton\GinnerGrower\GrowerStock\GrowerCentralStocks;
Use App\Http\Validations\Cotton\GinnerGrower\GrowerSell\GrowerSellEntryValidations;
use DB;

class GrowerSellEntryController extends Controller
{	

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Get all Seeds Stock
     */
    public function index(Request $request)
    {
        $query = DB::table('grower_sell_entry')->select('grower_sell_entry.*');

        if ($request->fiscal_year_id) {
            $query = $query->where('fiscal_year_id', $request->fiscal_year_id);
        }
        
        if ($request->applicant_id) {
            $query = $query->where('applicant_id', $request->applicant_id);
        }

        if ($request->hatt_id) {
            $query = $query->where('hatt_id', $request->hatt_id);
        }

        if ($request->cotton_variety_id) {
            $query = $query->where('cotton_variety_id', $request->cotton_variety_id);
        }

        if ($request->cotton_id) {
            $query = $query->where('cotton_id', $request->cotton_id);
        }

        if ($request->unit_id) {
            $query = $query->where('unit_id', $request->unit_id);
        }

        if ($request->from_date && $request->to_date) 
        {
            $startDate   = date('Y-m-d', strtotime($request->from_date));
            $endDate     = date('Y-m-d', strtotime($request->to_date));
            $query       = $query->whereBetween('hatt_date', [$startDate, $endDate]);
        }

        if (isset($request->from_date) && !isset($request->to_date))
        {
            $query = $query->whereDate('hatt_date', '>=', date('Y-m-d', strtotime($request->from_date)));
        }

        if (!isset($request->from_date) && isset($request->to_date))
        {
            $query = $query->whereDate('hatt_date', '<=', date('Y-m-d', strtotime($request->to_date)));
        }       

        $list = $query->paginate(request('per_page', env('PER_PAGE', 10)));

        if( count($list)>0){
            return response([
                'success' => true,
                'message' => 'Seeds stock list',
                'data' => $list
            ]);
        }
        else
        {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }
    }

    /**
     * Seeds Stock details
     */
    public function show($id)
    {
        $query = GrowerSellEntry::where('id', $id); 
        $list = $query->get();

        if( count($list)>0){
            return response([
                'success' => true,
                'message' => 'Seeds stock list',
                'data' => $list
            ]);
        }
        else
        {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }        
    }
    
    /**
     * Seeds Stock store
     */
    public function store(Request $request)
    { 
        $validationResult = GrowerSellEntryValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }  

        $oldGrowerCentralStocks = GrowerCentralStocks::select('quantity')
         						->where('type',2)
         						->where('applicant_id', $request->applicant_id)
         						->where('fiscal_year_id', $request->fiscal_year_id)
                                ->where('seasons_id', $request->seasons_id)
         						->where('cotton_variety_id', $request->cotton_variety_id)
         						->where('cotton_id', $request->cotton_id)->first();                      

        if($oldGrowerCentralStocks){
         	$previousStockQuantity = $oldGrowerCentralStocks->quantity;
        }else{
        	return response([
	            'success' => false,
	            'message' => 'Do not have any stock!!'
        	]);
        }

        $requestQuantity = (int)$request->quantity;
	
    	if($previousStockQuantity >= $requestQuantity){
    		$currentStockQuantaty  = (int)$previousStockQuantity - (int)$request->quantity;
    	}else{
    		 return response([
	            'success' => false,
	            'message' => 'Do not have enough quantity!!'
        	]);
    	}

        DB::beginTransaction();

        try {

        	$growerSellEntry                  			=  new GrowerSellEntry();  
			$growerSellEntry->fiscal_year_id       		=  (int)$request->fiscal_year_id;   
			$growerSellEntry->applicant_id     			=  $request->applicant_id;
            $growerSellEntry->seasons_id                =  (int)$request->seasons_id;
			$growerSellEntry->hatt_id     				=  (int)$request->hatt_id;     
			$growerSellEntry->cotton_variety_id       	=  (int)$request->cotton_variety_id;  
			$growerSellEntry->cotton_id       			=  (int)$request->cotton_id;    
			$growerSellEntry->hatt_date       			=  $request->hatt_date; 
			$growerSellEntry->quantity       			=  $requestQuantity;    
			$growerSellEntry->unit_id       			=  (int)$request->unit_id;     
			$growerSellEntry->price       				=  (int)$request->price;	

			if($growerSellEntry->save()){
					$oldGrowerCentralStocks->quantity     	= $currentStockQuantaty;
					$oldGrowerCentralStocks->update();
				
			}            

            /*save_log([
                'data_id'    => $growerSellEntry->id,
                'table_name' => 'sd_stock_infos'
            ]);*/

            DB::commit();

        } catch (\Exception $ex) {

        	DB::rollback();

            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $growerSellEntry
        ]);
    }

    /**
     * All application List
     */
    public function  getApplicationList()
    {
        $list = DB::table('ginner_grower_profiles')->select('id as value','applicant_id','status')->get(); 

        if (!$list) {

            return response([
                'success' => false,
                'message' => 'Data Not Found'
            ]);

        } else {

            return response([
                'success' => true,
                'message' => 'All Application List',
                'data'    => $list
            ]); 
        }

    }
    
}
