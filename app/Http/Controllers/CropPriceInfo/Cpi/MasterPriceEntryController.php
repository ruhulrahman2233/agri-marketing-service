<?php
namespace App\Http\Controllers\CropPriceInfo\Cpi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\CropPriceInfo\Cpi\MarketCommodityPriceValidations;
use App\Http\Validations\CropPriceInfo\Cpi\CommodityAddValidations;
use App\Models\CropPriceInfo\Config\{MasterCommodityName, MasterAlertPercentages};

use App\Models\CropPriceInfo\Cpi\
{ MarketCommodityPrice, MarketCommodityPriceDetail, MarketCommodityWeeklyPrice, MarketCommodityWeeklyPriceDetail,
    MarketCommodityGrowerPrice, MarketCommodityGrowerPriceDetail
};

use DB;

class MasterPriceEntryController extends Controller
{
    
    public function getCommodityList ($id =null, Request $request) {

        if (empty($id)) {

            $validationResult = CommodityAddValidations:: validate($request);
            if (!$validationResult['success']) {
                return response($validationResult);
            }

        }
        $list = MasterCommodityName::where('status', 1)
        ->select(
            'commodity_group_id as com_grp_id',
            'commodity_name',
            'commodity_name_bn',
            'commodity_sub_group_id as com_subgrp_id',
            'id as commodity_id',
            'status',
            'unit_grower',
            'unit_retail',
            'unit_whole_sale as unit_wholesale'
        )
        ->get();

        $data = [];
        foreach ($list as $k => $l) {
            $data[$k] = $l;
            $data[$k]['w_lowestPrice'] = 0;
            $data[$k]['w_highestPrice'] = 0;
            $data[$k]['r_lowestPrice'] = 0;
            $data[$k]['r_highestPrice'] = 0;
            $data[$k]['g_lowestPrice'] = 0;
            $data[$k]['g_highestPrice'] = 0;
            $data[$k]['w_avgPrice'] = '';
            $data[$k]['r_avgPrice'] = '';
            $data[$k]['g_avgPrice'] = '';
            $data[$k]['w_percentage_LtoH'] = '';
            $data[$k]['r_percentage_LtoH'] = '';
            $data[$k]['g_percentage_LtoH'] = '';
            $data[$k]['old_w_percentage_LtoH'] = '';
            $data[$k]['old_r_percentage_LtoH'] = '';
            $data[$k]['old_g_percentage_LtoH'] = '';
            $data[$k]['w_last_entry'] = '';
            $data[$k]['r_last_entry'] = '';
            $data[$k]['g_last_entry'] = '';
            $data[$k]['r_per_message'] = '';
            $data[$k]['w_per_message'] = '';
            $data[$k]['g_per_message'] = '';
        }

        if ($request->select_type === 'Daily' && $request->price_entry_type === 'Market') {
            $query = MarketCommodityPrice::with('commodity_list');
        } else if ($request->select_type === 'Daily' && $request->price_entry_type === 'Grower') {
            $query = MarketCommodityGrowerPrice::with('commodity_list');
        }else {
            $query = MarketCommodityWeeklyPrice::with('commodity_list');
            $query->where('price_entry_type', $request->price_entry_type);
        }
        $query->whereMarketId($request->market_id);
        if(!empty($request->id)) {
            $query->where('id', '!=', $request->id);
        }
        $old_data = $query->orderBy('id', 'desc')->first();
        return response([
            'success' => true,
            'message' => 'Price Type list',
            'data' =>$data,
            'old_data' =>$old_data
        ]);
    }
    public function getPercentage()
    {
        return response([
            'success' => true,
            'message' => 'Alert Percentage List',
            'prcentage'=>MasterAlertPercentages::whereFiscalYearId(1)->first()
        ]);
    }
    public function getMarketPriceList(Request $request)
    {
        $query = MarketCommodityPrice::with('commodity_list');
		
        if ($request->market_id) {
            $query->where('cpi_market_commodity_prices.market_id', $request->market_id);
        }
        if ($request->division_id) {
            $query->where('cpi_market_commodity_prices.division_id', $request->division_id);
        }
        if ($request->district_id) {
            $query->where('cpi_market_commodity_prices.district_id', $request->district_id);
        }
        if ($request->upazila_id) {
            $query->where('cpi_market_commodity_prices.upazila_id', $request->upazila_id);
        }
        if ($request->price_date) {
            $query->whereDate('cpi_market_commodity_prices.price_date', $request->price_date);
        }
       
        $list = $query->paginate(request('per_page', config('app.per_page'))); 

        return response([
            'success' => true,
            'message' => 'Price list',
            'data' =>$list
        ]);
    }
    public function getGrowerPriceList(Request $request)
    {
        $query = MarketCommodityGrowerPrice::with('commodity_list');
		
        if ($request->market_id) {
            $query->where('cpi_market_commodity_growers_prices.market_id', $request->market_id);
        }
        if ($request->division_id) {
            $query->where('cpi_market_commodity_growers_prices.division_id', $request->division_id);
        }
        if ($request->district_id) {
            $query->where('cpi_market_commodity_growers_prices.district_id', $request->district_id);
        }
        if ($request->upazila_id) {
            $query->where('cpi_market_commodity_growers_prices.upazila_id', $request->upazila_id);
        }
        if ($request->price_date) {
            $query->whereDate('cpi_market_commodity_growers_prices.price_date', $request->price_date);
        }
       
        $list = $query->paginate(request('per_page', config('app.per_page'))); 

        return response([
            'success' => true,
            'message' => 'Price list',
            'data' =>$list
        ]);
    }
    public function getWeeklyPriceList(Request $request)
    {
        $query = MarketCommodityWeeklyPrice::with('commodity_list');
		
        if ($request->market_id) {
            $query->where('cpi_market_commodity_weekly_prices.market_id', $request->market_id);
        }
        if ($request->division_id) {
            $query->where('cpi_market_commodity_weekly_prices.division_id', $request->division_id);
        }
        if ($request->district_id) {
            $query->where('cpi_market_commodity_weekly_prices.district_id', $request->district_id);
        }
        if ($request->upazila_id) {
            $query->where('cpi_market_commodity_weekly_prices.upazila_id', $request->upazila_id);
        }
        if ($request->week_id) {
            $query->where('cpi_market_commodity_weekly_prices.week_id', $request->week_id);
        }
        if ($request->month_id) {
            $query->where('cpi_market_commodity_weekly_prices.month_id', $request->month_id);
        }
        if ($request->year) {
            $query->where('cpi_market_commodity_weekly_prices.year', $request->year);
        }
        if ($request->price_entry_type) {
            $query->where('cpi_market_commodity_weekly_prices.price_entry_type', $request->price_entry_type);
        }
        if ($request->price_date) {
            $query->whereDate('cpi_market_commodity_weekly_prices.price_date', $request->price_date);
        }
       
        $list = $query->paginate(request('per_page', config('app.per_page'))); 

        return response([
            'success' => true,
            'message' => 'Price list',
            'data' =>$list
        ]);
    }
   
    /*+++++++store+++++++++*/
    public function store(Request $request)
    {

        $validationResult = MarketCommodityPriceValidations:: validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }
       
            
            // DB::beginTransaction();
            if ($request->select_type === 'Daily' && $request->price_entry_type === 'Market') {

                $MarketCommodityPrice            = new MarketCommodityPrice;
                $MarketCommodityPrice->division_id = $request->division_id;
                $MarketCommodityPrice->district_id = $request->district_id;
                $MarketCommodityPrice->upazila_id = $request->upazila_id;
                $MarketCommodityPrice->market_id = $request->market_id;
                $MarketCommodityPrice->price_date = $request->price_date;
                $MarketCommodityPrice->price_entry_type = $request->price_entry_type;
                $MarketCommodityPrice->price_type_id = json_encode($request->price_type_id);
                $MarketCommodityPrice->select_type = $request->select_type;

                if ($MarketCommodityPrice->save()) {
                    foreach ($request->commodity_list as $value) {
                        if(($value['w_lowestPrice'] >0 && $value['w_highestPrice'] >0) 
                            || ($value['r_lowestPrice'] > 0 && $value['r_highestPrice'] >0)) {
                                $MarketCommodityPriceDetail                     = new MarketCommodityPriceDetail;
                                $MarketCommodityPriceDetail->price_table_id     = $MarketCommodityPrice->id;
                                $MarketCommodityPriceDetail->price_type_id      = $request->price_type_id;
                                $MarketCommodityPriceDetail->com_grp_id         = $value['com_grp_id'];
                                $MarketCommodityPriceDetail->com_subgrp_id      = $value['com_subgrp_id'];
                                $MarketCommodityPriceDetail->commodity_id       = $value['commodity_id'];
                                $MarketCommodityPriceDetail->w_lowestPrice      = $value['w_lowestPrice'];
                                $MarketCommodityPriceDetail->w_highestPrice     = $value['w_highestPrice'];
                                $MarketCommodityPriceDetail->r_lowestPrice      = $value['r_lowestPrice'];
                                $MarketCommodityPriceDetail->r_highestPrice     = $value['r_highestPrice'];
                                $MarketCommodityPriceDetail->w_avgPrice         = $value['w_avgPrice'];
                                $MarketCommodityPriceDetail->r_avgPrice         = $value['r_avgPrice'];
                                $MarketCommodityPriceDetail->w_percentage_LtoH  = $value['w_percentage_LtoH'];
                                $MarketCommodityPriceDetail->r_percentage_LtoH  = $value['r_percentage_LtoH'];
                                $MarketCommodityPriceDetail->w_last_entry       = $value['w_last_entry'];
                                $MarketCommodityPriceDetail->r_last_entry       = $value['r_last_entry'];
                                $MarketCommodityPriceDetail->comparison_date    = $request->price_date;
                                $MarketCommodityPriceDetail->unit_retail        = $value['unit_retail'];
                                $MarketCommodityPriceDetail->unit_wholesale     = $value['unit_wholesale'];
                                $MarketCommodityPriceDetail->submit_status      = 1;
                                $MarketCommodityPriceDetail->status             = 0;
                                $MarketCommodityPriceDetail->save();
                        }
                    }
                }

         
            } else if ($request->select_type === 'Daily' && $request->price_entry_type === 'Grower') {

                $MarketCommodityGrowerPrice            = new MarketCommodityGrowerPrice;
                $MarketCommodityGrowerPrice->division_id = $request->division_id;
                $MarketCommodityGrowerPrice->district_id = $request->district_id;
                $MarketCommodityGrowerPrice->upazila_id = $request->upazila_id;
                $MarketCommodityGrowerPrice->market_id = $request->market_id;
                $MarketCommodityGrowerPrice->price_date = $request->price_date;
                $MarketCommodityGrowerPrice->price_entry_type = $request->price_entry_type;
                $MarketCommodityGrowerPrice->price_type_id = json_encode($request->price_type_id);
                $MarketCommodityGrowerPrice->select_type = $request->select_type;

                if ($MarketCommodityGrowerPrice->save()) {
                    $commodity_list = $request->commodity_list;
                    foreach ($request->commodity_list as $key=>$value) {
                        if ($value['g_lowestPrice'] > 0 && $value['g_highestPrice'] >0) {
                            $commodity_list[$key]['status'] = 0;
                            $commodity_list[$key]['price_type'] = json_encode($request->price_type_id);
                        } else {
                            unset($commodity_list[$key]);
                        }
                    }
                    $MarketCommodityGrowerPrice->commodity_list()->delete();
                    $MarketCommodityGrowerPrice->commodity_list()->createMany($commodity_list);
                }

            } else {
                
                $MarketCommodityWeeklyPrice            = new MarketCommodityWeeklyPrice;
                $MarketCommodityWeeklyPrice->division_id = $request->division_id;
                $MarketCommodityWeeklyPrice->district_id = $request->district_id;
                $MarketCommodityWeeklyPrice->upazila_id = $request->upazila_id;
                $MarketCommodityWeeklyPrice->market_id = $request->market_id;
                $MarketCommodityWeeklyPrice->year = $request->year;
                $MarketCommodityWeeklyPrice->month_id = $request->month_id;
                $MarketCommodityWeeklyPrice->week_id = $request->week_id;
                $MarketCommodityWeeklyPrice->week_table_id = $request->week_id;
                $MarketCommodityWeeklyPrice->price_entry_type = $request->price_entry_type;
                $MarketCommodityWeeklyPrice->price_type_id = json_encode($request->price_type_id);
                $MarketCommodityWeeklyPrice->select_type = $request->select_type;
                
                if ($MarketCommodityWeeklyPrice->save()) {
                    foreach ($request->commodity_list as $value) {
                        if(($value['w_lowestPrice'] >0 && $value['w_highestPrice'] >0) 
                            || ($value['r_lowestPrice'] > 0 && $value['r_highestPrice'] >0) 
                            || ($value['g_lowestPrice'] > 0 && $value['g_highestPrice'] >0)) {
                            $MarketCommodityWeeklyPriceDetail                     = new MarketCommodityWeeklyPriceDetail;
                            $MarketCommodityWeeklyPriceDetail->price_table_id     = $MarketCommodityWeeklyPrice->id;
                            $MarketCommodityWeeklyPriceDetail->price_type_id      = $request->price_type_id;
                            $MarketCommodityWeeklyPriceDetail->com_grp_id         = $value['com_grp_id'];
                            $MarketCommodityWeeklyPriceDetail->com_subgrp_id      = $value['com_subgrp_id'];
                            $MarketCommodityWeeklyPriceDetail->commodity_id       = $value['commodity_id'];

                            $MarketCommodityWeeklyPriceDetail->w_lowestPrice      = $value['w_lowestPrice'];
                            $MarketCommodityWeeklyPriceDetail->w_highestPrice     = $value['w_highestPrice'];
                            
                            $MarketCommodityWeeklyPriceDetail->r_lowestPrice      = $value['r_lowestPrice'];
                            $MarketCommodityWeeklyPriceDetail->r_highestPrice     = $value['r_highestPrice'];

                            $MarketCommodityWeeklyPriceDetail->g_lowestPrice     = $value['g_lowestPrice'];
                            $MarketCommodityWeeklyPriceDetail->g_highestPrice     = $value['g_highestPrice'];

                            $MarketCommodityWeeklyPriceDetail->w_avgPrice         = $value['w_avgPrice'];
                            $MarketCommodityWeeklyPriceDetail->r_avgPrice         = $value['r_avgPrice'];
                            $MarketCommodityWeeklyPriceDetail->g_avgPrice         = $value['g_avgPrice'];
                            

                            $MarketCommodityWeeklyPriceDetail->w_percentage_LtoH  = $value['w_percentage_LtoH'];
                            $MarketCommodityWeeklyPriceDetail->r_percentage_LtoH  = $value['r_percentage_LtoH'];
                            $MarketCommodityWeeklyPriceDetail->g_percentage_LtoH  = $value['g_percentage_LtoH'];
                            
                            $MarketCommodityWeeklyPriceDetail->w_last_entry       = $value['w_last_entry'];
                            $MarketCommodityWeeklyPriceDetail->r_last_entry       = $value['r_last_entry'];
                            $MarketCommodityWeeklyPriceDetail->g_last_entry       = $value['g_last_entry'];
                            $MarketCommodityWeeklyPriceDetail->unit_retail        = $value['unit_retail'];
                            $MarketCommodityWeeklyPriceDetail->unit_wholesale     = $value['unit_wholesale'];
                            $MarketCommodityWeeklyPriceDetail->unit_grower        = $value['unit_grower'];
                            $MarketCommodityWeeklyPriceDetail->submitted_by       = (int) user_id();
                            $MarketCommodityWeeklyPriceDetail->status             = 0;
                            $MarketCommodityWeeklyPriceDetail->save();
                        }
                    }
                }
            }
            // DB::commit();

        
        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => []
        ]);
    }

  

     /*++++++update+++++++*/
    public function update(Request $request, $id)
    {
        $validationResult = MarketCommodityPriceValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }
        try {
            
            DB::beginTransaction();
            if ($request->select_type === 'Daily' && $request->price_entry_type === 'Market') {
    
                $MarketCommodityPrice = MarketCommodityPrice::find($id);
    
                if (!$MarketCommodityPrice) {
                    return response([
                        'success' => false,
                        'message' => 'Data not found.'
                    ]);
                }
    
                $MarketCommodityPrice->division_id = $request->division_id;
                $MarketCommodityPrice->district_id = $request->district_id;
                $MarketCommodityPrice->upazila_id = $request->upazila_id;
                $MarketCommodityPrice->market_id = $request->market_id;
                $MarketCommodityPrice->price_date = $request->price_date;
                $MarketCommodityPrice->price_entry_type = $request->price_entry_type;
                $MarketCommodityPrice->price_type_id = json_encode($request->price_type_id);
                $MarketCommodityPrice->select_type = $request->select_type;
                $MarketCommodityPrice->save();
    
                MarketCommodityPriceDetail::where('price_table_id', $MarketCommodityPrice->id)->delete();
                if ($MarketCommodityPrice->save()) {
                    foreach ($request->commodity_list as $value) {
                        if(($value['w_lowestPrice'] >0 && $value['w_highestPrice'] >0) 
                            || ($value['r_lowestPrice'] > 0 && $value['r_highestPrice'] >0)) {
                            $MarketCommodityPriceDetail                     = new MarketCommodityPriceDetail;
                            $MarketCommodityPriceDetail->price_table_id     = $MarketCommodityPrice->id;
                            $MarketCommodityPriceDetail->price_type_id      = $request->price_type_id;
                            $MarketCommodityPriceDetail->com_grp_id         = $value['com_grp_id'];
                            $MarketCommodityPriceDetail->com_subgrp_id      = $value['com_subgrp_id'];
                            $MarketCommodityPriceDetail->commodity_id       = $value['commodity_id'];
                            $MarketCommodityPriceDetail->w_lowestPrice      = $value['w_lowestPrice'];
                            $MarketCommodityPriceDetail->w_highestPrice     = $value['w_highestPrice'];
                            $MarketCommodityPriceDetail->r_lowestPrice      = $value['r_lowestPrice'];
                            $MarketCommodityPriceDetail->r_highestPrice     = $value['r_highestPrice'];
                            $MarketCommodityPriceDetail->w_avgPrice         = $value['w_avgPrice'];
                            $MarketCommodityPriceDetail->r_avgPrice         = $value['r_avgPrice'];
                            $MarketCommodityPriceDetail->w_percentage_LtoH  = $value['w_percentage_LtoH'];
                            $MarketCommodityPriceDetail->r_percentage_LtoH  = $value['r_percentage_LtoH'];
                            $MarketCommodityPriceDetail->w_last_entry       = $value['w_last_entry'];
                            $MarketCommodityPriceDetail->r_last_entry       = $value['r_last_entry'];
                            $MarketCommodityPriceDetail->comparison_date    = $request->price_date;
                            $MarketCommodityPriceDetail->unit_retail        = $value['unit_retail'];
                            $MarketCommodityPriceDetail->unit_wholesale     = $value['unit_wholesale'];
                            $MarketCommodityPriceDetail->submit_status      = 1;
                            $MarketCommodityPriceDetail->status             = 0;
                            $MarketCommodityPriceDetail->save();
                        }
                    }
                }
    
         
            } else if ($request->select_type === 'Daily' && $request->price_entry_type === 'Grower') {
    
                $MarketCommodityGrowerPrice = MarketCommodityGrowerPrice::find($id);
    
                if (!$MarketCommodityGrowerPrice) {
                    return response([
                        'success' => false,
                        'message' => 'Data not found.'
                    ]);
                }
    
                $MarketCommodityGrowerPrice->division_id = $request->division_id;
                $MarketCommodityGrowerPrice->district_id = $request->district_id;
                $MarketCommodityGrowerPrice->upazila_id = $request->upazila_id;
                $MarketCommodityGrowerPrice->market_id = $request->market_id;
                $MarketCommodityGrowerPrice->price_date = $request->price_date;
                $MarketCommodityGrowerPrice->price_entry_type = $request->price_entry_type;
                $MarketCommodityGrowerPrice->price_type_id = $request->price_type_id;
                $MarketCommodityGrowerPrice->select_type = $request->select_type;
    
    
                MarketCommodityGrowerPriceDetail::where('price_table_id', $MarketCommodityGrowerPrice->id)->delete();
                if ($MarketCommodityGrowerPrice->save()) {
                    foreach ($request->commodity_list as $value) {
                        if ($value['g_lowestPrice'] > 0 && $value['g_highestPrice'] >0) {
                            $MarketCommodityGrowerPriceDetail                     = new MarketCommodityGrowerPriceDetail;
                            $MarketCommodityGrowerPriceDetail->price_table_id     = $MarketCommodityGrowerPrice->id;
                            $MarketCommodityGrowerPriceDetail->price_type         = json_encode($request->price_type_id);;
                            $MarketCommodityGrowerPriceDetail->com_grp_id         = $value['com_grp_id'];
                            $MarketCommodityGrowerPriceDetail->com_subgrp_id      = $value['com_subgrp_id'];
                            $MarketCommodityGrowerPriceDetail->commodity_id       = $value['commodity_id'];
                            $MarketCommodityGrowerPriceDetail->g_lowestPrice      = $value['g_lowestPrice'];
                            $MarketCommodityGrowerPriceDetail->g_highestPrice     = $value['g_highestPrice'];
                            $MarketCommodityGrowerPriceDetail->g_avgPrice         = $value['g_avgPrice'];
                            $MarketCommodityGrowerPriceDetail->g_percentage_LtoH  = $value['g_percentage_LtoH'];
                            $MarketCommodityGrowerPriceDetail->g_last_entry       = $value['g_last_entry'];
                            $MarketCommodityGrowerPriceDetail->price_date         = $request->price_date;
                            $MarketCommodityGrowerPriceDetail->submitted_by       = (int) user_id();
                            $MarketCommodityGrowerPriceDetail->status             = 0;
                            $MarketCommodityGrowerPriceDetail->unit_grower              = $value['unit_grower'];
                            $MarketCommodityGrowerPriceDetail->save();
                        }
                    }
                }
    
            } else {
    
                $MarketCommodityWeeklyPrice = MarketCommodityWeeklyPrice::find($id);
    
                if (!$MarketCommodityWeeklyPrice) {
                    return response([
                        'success' => false,
                        'message' => 'Data not found.'
                    ]);
                }
    
                $MarketCommodityWeeklyPrice->division_id = $request->division_id;
                $MarketCommodityWeeklyPrice->district_id = $request->district_id;
                $MarketCommodityWeeklyPrice->upazila_id = $request->upazila_id;
                $MarketCommodityWeeklyPrice->market_id = $request->market_id;
                $MarketCommodityWeeklyPrice->year = $request->year;
                $MarketCommodityWeeklyPrice->month_id = $request->month_id;
                $MarketCommodityWeeklyPrice->week_id = $request->week_id;
                $MarketCommodityWeeklyPrice->week_table_id = $request->week_id;
                $MarketCommodityWeeklyPrice->price_entry_type = $request->price_entry_type;
                $MarketCommodityWeeklyPrice->price_type_id = $request->price_type_id;
                $MarketCommodityWeeklyPrice->select_type = $request->select_type;
                
                MarketCommodityWeeklyPriceDetail::where('price_table_id', $MarketCommodityWeeklyPrice->id)->delete();
                if ($MarketCommodityWeeklyPrice->save()) {
                    foreach ($request->commodity_list as $value) {
                        if(($value['w_lowestPrice'] >0 && $value['w_highestPrice'] >0) 
                        || ($value['r_lowestPrice'] > 0 && $value['r_highestPrice'] >0) 
                        || ($value['g_lowestPrice'] > 0 && $value['g_highestPrice'] >0)) {
                            $MarketCommodityWeeklyPriceDetail                     = new MarketCommodityWeeklyPriceDetail;
                            $MarketCommodityWeeklyPriceDetail->price_table_id     = $MarketCommodityWeeklyPrice->id;
                            $MarketCommodityWeeklyPriceDetail->price_type_id      = $request->price_type_id;
                            $MarketCommodityWeeklyPriceDetail->com_grp_id         = $value['com_grp_id'];
                            $MarketCommodityWeeklyPriceDetail->com_subgrp_id      = $value['com_subgrp_id'];
                            $MarketCommodityWeeklyPriceDetail->commodity_id       = $value['commodity_id'];
        
                            $MarketCommodityWeeklyPriceDetail->w_lowestPrice      = $value['w_lowestPrice'];
                            $MarketCommodityWeeklyPriceDetail->w_highestPrice     = $value['w_highestPrice'];
                            
                            $MarketCommodityWeeklyPriceDetail->r_lowestPrice      = $value['r_lowestPrice'];
                            $MarketCommodityWeeklyPriceDetail->r_highestPrice     = $value['r_highestPrice'];
        
                            $MarketCommodityWeeklyPriceDetail->g_lowestPrice      = $value['g_lowestPrice'];
                            $MarketCommodityWeeklyPriceDetail->g_highestPrice     = $value['g_highestPrice'];
        
                            $MarketCommodityWeeklyPriceDetail->w_avgPrice         = $value['w_avgPrice'];
                            $MarketCommodityWeeklyPriceDetail->r_avgPrice         = $value['r_avgPrice'];
                            $MarketCommodityWeeklyPriceDetail->g_avgPrice         = $value['g_avgPrice'];
                            
        
                            $MarketCommodityWeeklyPriceDetail->w_percentage_LtoH  = $value['w_percentage_LtoH'];
                            $MarketCommodityWeeklyPriceDetail->r_percentage_LtoH  = $value['r_percentage_LtoH'];
                            $MarketCommodityWeeklyPriceDetail->g_percentage_LtoH  = $value['g_percentage_LtoH'];
                            
                            $MarketCommodityWeeklyPriceDetail->w_last_entry       = $value['w_last_entry'];
                            $MarketCommodityWeeklyPriceDetail->r_last_entry       = $value['r_last_entry'];
                            $MarketCommodityWeeklyPriceDetail->g_last_entry       = $value['g_last_entry'];
                            $MarketCommodityWeeklyPriceDetail->submitted_by       = (int) user_id();
                            $MarketCommodityWeeklyPriceDetail->status             = 0;
                            $MarketCommodityWeeklyPriceDetail->unit_retail        = $value['unit_retail'] ?? 0;
                            $MarketCommodityWeeklyPriceDetail->unit_wholesale     = $value['unit_wholesale'] ?? 0;
                            $MarketCommodityWeeklyPriceDetail->unit_grower        = $value['unit_grower'] ?? 0;
                            $MarketCommodityWeeklyPriceDetail->save();
                        }
                    }
                }
            }
            DB::commit();

        } catch (\Exception $ex) {
            DB::rollback();
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => []
        ]);
    }
     /*++++++approve+++++++*/
    public function approve(Request $request)
    {
        try {
            
            DB::beginTransaction();

            if ($request->select_type === 'Daily' && $request->price_entry_type === 'Market') {

                foreach ($request->commodity_list as $value) {
                    DB::table('cpi_market_commodity_price_details')
                    ->where('id', $value['id'])
                    ->update(array(
                        'status'=>$value['status']
                    ));
                }
         
            } else if ($request->select_type === 'Daily' && $request->price_entry_type === 'Grower') {
    
                foreach ($request->commodity_list as $value) {
                    DB::table('cpi_market_commodity_growers_price_details')
                    ->where('id', $value['id'])
                    ->update(array(
                        'status'=>$value['status']
                    ));
                }
    
            } else {
                foreach ($request->commodity_list as $value) {
                    DB::table('cpi_market_commodity_weekly_price_details')
                    ->where('id', $value['id'])
                    ->update(array(
                        'status'=>$value['status']
                    ));
                }

            }

            DB::commit();

        } catch (\Exception $ex) {
            DB::rollback();
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data approve successfully',
            'data'    => []
        ]);
    }
    
}
