<?php

namespace App\Http\Controllers\CropPriceInfo\Reports\PriceReport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CropPriceInfo\Cpi\{ MarketCommodityWeeklyPriceDetail };

class WeeklyMarketWisePriceReportController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $query = MarketCommodityWeeklyPriceDetail::active()->latest();
        $query = $query->with([
            'weekly_price:id,market_id,division_id,district_id,upazila_id,year,month_id,week_id',
            'commodity:id,commodity_name,commodity_name_bn,unit_grower,unit_whole_sale,unit_retail,price_type_id',
            'commodity.unit_grower:id,unit_name,unit_name_bn'
        ]);
        $query = $query->whereHas('weekly_price', function ($q) use($request) {
            $q->where('price_entry_type', 'Grower');
        });
        if ($request->market_id) {
            $query = $query->whereHas('weekly_price', function ($q) use($request) {
                $q->where('market_id', $request->market_id);
            });
        }
        if ($request->year) {
            $query = $query->whereHas('weekly_price', function ($q) use($request) {
                $q->where('year', $request->year);
            });
        }
        if ($request->month_id) {
            $query = $query->whereHas('weekly_price', function ($q) use($request) {
                $q->where('month_id', $request->month_id);
            });
        }
        if ($request->week_id) {
            $query = $query->whereHas('weekly_price', function ($q) use($request) {
                $q->where('week_id', $request->week_id);
            });
        }
        $query->groupBy(['commodity_id']);
        $datas = $query->get();

        if(count($datas) == 0) {

            return response([
                'success' => false,
                'message' => 'data Not found!'
            ]);

        } else {

            return response([
                'success' => true,
                'message' => 'data found!',
                'data' => $datas
            ]);
        }
    }
}
