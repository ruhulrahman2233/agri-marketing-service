<?php

namespace App\Http\Controllers\CropPriceInfo\Reports\PriceReport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\CropPriceInfo\Cpi\{ MarketCommodityPriceDetail };

class MarketMonthlyPriceReportController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // return $request;
        $query = MarketCommodityPriceDetail::active()->latest();
        $query = $query->with([
            'commodity:id,unit_grower,unit_whole_sale,unit_retail,price_type_id',
            'commodity.unit_whole_sale:id,unit_name,unit_name_bn',
            'commodity.unit_retail:id,unit_name,unit_name_bn'
        ]);
        $query->leftjoin('cpi_market_commodity_prices', 'cpi_market_commodity_price_details.price_table_id', '=', 'cpi_market_commodity_prices.id');
        $query = $query->select(
            'cpi_market_commodity_price_details.id',
            'cpi_market_commodity_price_details.price_type_id',
            'cpi_market_commodity_price_details.com_grp_id',
            'cpi_market_commodity_price_details.com_subgrp_id',
            'cpi_market_commodity_price_details.commodity_id',
            'cpi_market_commodity_price_details.w_lowestPrice',
            'cpi_market_commodity_price_details.w_highestPrice',
            'cpi_market_commodity_price_details.w_supply',
            'cpi_market_commodity_price_details.r_lowestPrice',
            'cpi_market_commodity_price_details.r_highestPrice',
            'cpi_market_commodity_price_details.created_at',
            DB::raw('AVG(cpi_market_commodity_price_details.w_lowestPrice) as a_w_lowestPrice'),
            DB::raw('AVG(cpi_market_commodity_price_details.w_highestPrice) as a_w_highestPrice'),
            DB::raw('AVG(cpi_market_commodity_price_details.r_lowestPrice) as a_r_lowestPrice'),
            DB::raw('AVG(cpi_market_commodity_price_details.r_highestPrice) as a_r_howestPrice'),
            'cpi_market_commodity_prices.id as cpi_market_commodity_price_id',
            'cpi_market_commodity_prices.price_date'
        );
        if ($request->division_id) {
            $query = $query->where('cpi_market_commodity_prices.division_id', $request->division_id);
        }
        if ($request->district_id) {
           $query = $query->where('cpi_market_commodity_prices.district_id', $request->district_id);
        }
        if ($request->upazila_id) {
           $query = $query->where('cpi_market_commodity_prices.upazila_id', $request->upazila_id);
        }
        if ($request->market_id) {
            $query = $query->where('cpi_market_commodity_prices.market_id', $request->market_id);
        }
        if ($request->year_from && $request->year_to) {
            $query = $query->whereYear('cpi_market_commodity_prices.price_date', '>=', $request->year_from)
                            ->whereYear('cpi_market_commodity_prices.price_date', '<=', $request->year_to);
        }
        if ($request->commodity_group_id) {
            $query = $query->where('cpi_market_commodity_price_details.com_grp_id', $request->commodity_group_id);
        }
        if ($request->commodity_sub_group_id) {
           $query = $query->where('cpi_market_commodity_price_details.com_subgrp_id', $request->commodity_sub_group_id);
        }
        if ($request->commodity_id) {
            $query = $query->where('cpi_market_commodity_price_details.commodity_id', $request->commodity_id);
        }

        $query->groupBy(['cpi_market_commodity_price_details.commodity_id']);
        $query->groupBy(DB::raw('YEAR(cpi_market_commodity_prices.price_date)'), DB::raw('MONTH(cpi_market_commodity_prices.price_date)'));
        $datas = $query->get();

        if(count($datas) == 0) {
            return response([
                'success' => false,
                'message' => 'data Not found!'
            ]);
        } else {
            return response([
                'success' => true,
                'message' => 'data found!',
                'data' => $datas
            ]);
        }
    }
}
