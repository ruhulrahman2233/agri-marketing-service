<?php

namespace App\Http\Controllers\CropPriceInfo\Reports\PriceReport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CropPriceInfo\Cpi\{ MarketCommodityWeeklyPriceDetail };
use Illuminate\Support\Facades\DB;

class WeeklyAveragePriceReportController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $query = MarketCommodityWeeklyPriceDetail::active()->latest();
        $query->with([
            'commodity:id,unit_grower,unit_whole_sale,unit_retail,price_type_id',
            'commodity.unit_grower:id,unit_name,unit_name_bn',
            'commodity.unit_whole_sale:id,unit_name,unit_name_bn'
        ]);
        $query->leftjoin('cpi_market_commodity_weekly_prices', 'cpi_market_commodity_weekly_price_details.price_table_id', '=', 'cpi_market_commodity_weekly_prices.id');
        $query->select(
            'cpi_market_commodity_weekly_price_details.id',
            'cpi_market_commodity_weekly_price_details.price_type_id',
            'cpi_market_commodity_weekly_price_details.com_grp_id',
            'cpi_market_commodity_weekly_price_details.com_subgrp_id',
            'cpi_market_commodity_weekly_price_details.commodity_id',
            'cpi_market_commodity_weekly_price_details.w_lowestPrice',
            'cpi_market_commodity_weekly_price_details.w_highestPrice',
            'cpi_market_commodity_weekly_price_details.w_supply',
            'cpi_market_commodity_weekly_price_details.r_lowestPrice',
            'cpi_market_commodity_weekly_price_details.r_highestPrice',
            'cpi_market_commodity_weekly_price_details.created_at',
            DB::raw('AVG(cpi_market_commodity_weekly_price_details.w_lowestPrice) as a_w_lowestPrice'),
            DB::raw('AVG(cpi_market_commodity_weekly_price_details.w_highestPrice) as a_w_highestPrice'),
            DB::raw('AVG(cpi_market_commodity_weekly_price_details.r_lowestPrice) as a_r_lowestPrice'),
            DB::raw('AVG(cpi_market_commodity_weekly_price_details.r_highestPrice) as a_r_howestPrice'),
            'cpi_market_commodity_weekly_prices.id as cpi_market_commodity_price_id',
            'cpi_market_commodity_weekly_prices.year',
            'cpi_market_commodity_weekly_prices.month_id',
            'cpi_market_commodity_weekly_prices.week_id',
            'cpi_market_commodity_weekly_prices.market_id'
        );
        $query->where('cpi_market_commodity_weekly_prices.price_entry_type', 'Market');
        if ($request->market_id) {
            $query->where('cpi_market_commodity_weekly_prices.market_id', $request->market_id);
        }

        if ($request->year) {
            $query->where('cpi_market_commodity_weekly_prices.year', $request->year);
        }
        if ($request->month_from && $request->month_to) {
            $query->where('cpi_market_commodity_weekly_prices.month_id', '>=', $request->month_from)
                            ->where('cpi_market_commodity_weekly_prices.month_id', '<=', $request->month_to);
        }
        $query->groupBy('cpi_market_commodity_weekly_price_details.commodity_id');
        $query->groupBy('cpi_market_commodity_weekly_prices.week_id');
        $datas = $query->get();

        if(count($datas) == 0) {
            return response([
                'success' => false,
                'message' => 'data Not found!'
            ]);
        } else {
            return response([
                'success' => true,
                'message' => 'data found!',
                'data' => $datas
            ]);
        }
    }
}
