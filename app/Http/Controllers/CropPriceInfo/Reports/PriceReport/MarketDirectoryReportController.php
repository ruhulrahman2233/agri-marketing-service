<?php

namespace App\Http\Controllers\CropPriceInfo\Reports\PriceReport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CropPriceInfo\Config\{ MasterMarkets, MasterMarketDirectoryCommodities, MasterMarketDirectoryLeaseValueYear };
use DB;

class MarketDirectoryReportController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = MasterMarkets::with(['marketing_items', 'lease_value']);

        if ($request->market_name) {
            $query = $query->where('master_markets.market_name', 'like', "{$request->market_name}%")
                           ->orWhere('master_markets.market_name_bn', 'like', "{$request->market_name}%");
        }  

        if ($request->division_id) {
            $query->where('master_markets.division_id', $request->division_id);
        }

        if ($request->district_id) {
            $query->where('master_markets.district_id', $request->district_id);
        }

        if ($request->upazila_id) {
            $query->where('master_markets.upazila_id', $request->upazila_id);
        }

        $list = $query->get();

        if (sizeof($list) > 0) {
            return response([
                'success' => true,
                'data'    => $list
            ]);
        }

        return response([
            'success' => false,
            'message' => 'Sorry, Data not found!!'
        ]); 
    }   
}
