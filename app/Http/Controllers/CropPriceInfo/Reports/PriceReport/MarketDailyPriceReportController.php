<?php

namespace App\Http\Controllers\CropPriceInfo\Reports\PriceReport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\CropPriceInfo\Cpi\{ MarketCommodityPriceDetail, MarketCommodityWeeklyPriceDetail };

class MarketDailyPriceReportController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if ($request->select_type == 'Daily') {
            $datas = $this->commodityPriceDetail($request);
        } else {
            $datas = $this->commodityWeeklyPriceDetail($request);
        }
        if(count($datas) == 0) {
            return response([
                'success' => false,
                'message' => 'data Not found!'
            ]);
        } else {
            return response([
                'success' => true,
                'message' => 'data found!',
                'data' => $datas
            ]);
        }
    }
    public function commodityPriceDetail($request)
    {
        $query = MarketCommodityPriceDetail::active()->latest();
        $query = $query->with([
            'commodity_price:id,market_id,division_id,district_id,upazila_id,price_date',
            'group:id,group_name,group_name_bn',
            'sub_group:id,sub_group_name,sub_group_name_bn',
            'commodity:id,commodity_name,commodity_name_bn,unit_grower,unit_whole_sale,unit_retail,price_type_id',
            'commodity.unit_whole_sale:id,unit_name,unit_name_bn',
            'commodity.unit_retail:id,unit_name,unit_name_bn'
        ]);
        if ($request->market_id) {
            $query = $query->whereHas('commodity_price', function ($q) use($request) {
                $q->where('market_id', $request->market_id);
            });
        }
        if ($request->price_date) {
            $query = $query->whereHas('commodity_price', function ($q) use($request) {
                $q->where('price_date', $request->price_date);
            });
        }
        // $query->whereJsonContains('price_type_id',["Wholesale"]);
        //$query->where('price_type_id', 'like', '%"Retail"%');
        $query->groupBy(['commodity_id']);
        $datas = $query->get();
        return $datas;
    }
    public function commodityWeeklyPriceDetail($request)
    {
        $query = MarketCommodityWeeklyPriceDetail::active()->latest();
        $query = $query->with([
            'weekly_price:id,market_id,division_id,district_id,upazila_id,year,month_id,week_id',
            'group:id,group_name,group_name_bn',
            'sub_group:id,sub_group_name,sub_group_name_bn',
            'commodity:id,commodity_name,commodity_name_bn,unit_grower,unit_whole_sale,unit_retail,price_type_id',
            'commodity.unit_whole_sale:id,unit_name,unit_name_bn',
            'commodity.unit_retail:id,unit_name,unit_name_bn'
        ]);
        $query = $query->whereHas('weekly_price', function ($q) use($request) {
            $q->where('price_entry_type', 'Market');
        });
        if ($request->market_id) {
            $query = $query->whereHas('weekly_price', function ($q) use($request) {
                $q->where('market_id', $request->market_id);
            });
        }
        if ($request->year) {
            $query = $query->whereHas('weekly_price', function ($q) use($request) {
                $q->where('year', $request->year);
            });
        }
        if ($request->month_id) {
            $query = $query->whereHas('weekly_price', function ($q) use($request) {
                $q->where('month_id', $request->month_id);
            });
        }
        if ($request->week_id) {
            $query = $query->whereHas('weekly_price', function ($q) use($request) {
                $q->where('week_id', $request->week_id);
            });
        }
        // $query->whereJsonContains('price_type_id',["Wholesale"]);
        // $query->where('price_type_id', 'like', '%"Retail"%');
        $query->groupBy(['commodity_id']);
        $datas = $query->get();
        return $datas;
    }
}
