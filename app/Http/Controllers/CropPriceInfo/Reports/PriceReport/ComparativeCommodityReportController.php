<?php

namespace App\Http\Controllers\CropPriceInfo\Reports\PriceReport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\CropPriceInfo\Cpi\{ MarketCommodityPriceDetail };
use Carbon\Carbon;

class ComparativeCommodityReportController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $givenDateReports = $this->reportSearchCustomize($request , 1);
        $lastWeeks = $this->reportSearchCustomize($request , 2);
        $lastMonths = $this->reportSearchCustomize($request, 3);
        $lastYears = $this->reportSearchCustomize($request, 4);

        if(count($givenDateReports) == 0) {
            return response([
                'success' => false,
                'message' => 'data Not found!'
            ]);
        } else {
            return response([
                'success' => true,
                'message' => 'data found!',
                'data' =>[
                    'givenDateReports' => $givenDateReports,
                    'lastWeeks' => $lastWeeks,
                    'lastMonths' => $lastMonths,
                    'lastYears' => $lastYears
                ]
            ]);
        }
    }
    public function reportSearchCustomize ($request, $type) {
        $query = MarketCommodityPriceDetail::active()->latest('price_date');
        if($type == 1) {
            $query->with([
                'commodity:id,unit_grower,unit_whole_sale,unit_retail,price_type_id',
                'commodity.unit_whole_sale:id,unit_name,unit_name_bn',
                'commodity.unit_retail:id,unit_name,unit_name_bn'
            ]);
        }
        $query->leftjoin('cpi_market_commodity_prices', 'cpi_market_commodity_price_details.price_table_id', '=', 'cpi_market_commodity_prices.id');
        $query->select(
            'cpi_market_commodity_price_details.id',
            'cpi_market_commodity_price_details.price_type_id',
            'cpi_market_commodity_price_details.com_grp_id',
            'cpi_market_commodity_price_details.com_subgrp_id',
            'cpi_market_commodity_price_details.commodity_id',
            'cpi_market_commodity_price_details.w_avgPrice',
            'cpi_market_commodity_price_details.r_avgPrice',
            'cpi_market_commodity_prices.id as cpi_market_commodity_price_id',
            'cpi_market_commodity_prices.price_date'
        );
        if($type === 1) {
            $query->select(
                'cpi_market_commodity_price_details.id',    
                'cpi_market_commodity_price_details.com_grp_id',
                'cpi_market_commodity_price_details.com_subgrp_id',
                'cpi_market_commodity_price_details.commodity_id',
                'cpi_market_commodity_price_details.w_avgPrice',
                'cpi_market_commodity_price_details.r_avgPrice',
                'cpi_market_commodity_price_details.r_lowestPrice',
                'cpi_market_commodity_price_details.r_highestPrice',
                'cpi_market_commodity_price_details.w_lowestPrice',
                'cpi_market_commodity_price_details.w_highestPrice',
                'cpi_market_commodity_prices.id as cpi_market_commodity_price_id',
                'cpi_market_commodity_prices.price_date'
            );
        } else {
            $query->select(
                'cpi_market_commodity_price_details.id',
                'cpi_market_commodity_price_details.com_grp_id',
                'cpi_market_commodity_price_details.com_subgrp_id',
                'cpi_market_commodity_price_details.commodity_id',
                'cpi_market_commodity_price_details.w_avgPrice',
                'cpi_market_commodity_price_details.r_avgPrice',
                DB::raw('AVG(cpi_market_commodity_price_details.r_lowestPrice) as r_lowestPrice'),
                DB::raw('AVG(cpi_market_commodity_price_details.r_highestPrice) as r_highestPrice'),
                DB::raw('AVG(cpi_market_commodity_price_details.w_lowestPrice) as w_lowestPrice'),
                DB::raw('AVG(cpi_market_commodity_price_details.w_highestPrice) as w_highestPrice'),
                'cpi_market_commodity_prices.id as cpi_market_commodity_price_id',
                'cpi_market_commodity_prices.price_date',
                DB::raw('AVG(cpi_market_commodity_price_details.w_avgPrice) as w_avgPrice'),
                DB::raw('AVG(cpi_market_commodity_price_details.r_avgPrice) as r_avgPrice')
            );
        }
        if ($request->market_id) {
           $query->where('cpi_market_commodity_prices.market_id', $request->market_id);
        }
        if ($request->commodity_group_id) {
            $query->where('cpi_market_commodity_price_details.com_grp_id', $request->commodity_group_id);
        }
        if ($request->commodity_sub_group_id) {
           $query->where('cpi_market_commodity_price_details.com_subgrp_id', $request->commodity_sub_group_id);
        }
        if ($request->commodity_id) {
          $query->where('cpi_market_commodity_price_details.commodity_id', $request->commodity_id);
        }
        if($type == 1) {
           $query->where('price_date', $request->price_date);
        }
        if($type == 2) {
            $previous_week = strtotime("-1 week +1 day", strtotime($request->price_date));
            $start_week = strtotime("last saturday midnight", $previous_week);
            $end_week = strtotime("next friday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $query->whereBetween('price_date', [$start_week, $end_week]);
        }
        if($type == 3) {
            $query->whereMonth('price_date', '=', Carbon::now()->subMonth()->month);
        }
        if($type == 4) {
            $query->whereYear('price_date', '=', date('Y', strtotime('-1 year')));
        }
        if($type !== 1) {
            $query->groupBy('cpi_market_commodity_price_details.commodity_id');
        }
        $datas = $query->get();
        return  $datas;
    }
}
