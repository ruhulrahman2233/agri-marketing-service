<?php

namespace App\Http\Controllers\CropPriceInfo\Reports\PriceReport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CropPriceInfo\Cpi\MarketCommodityPriceDetail;
use DB;

class DivisionWiseAvgPriceController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    { 
        $start_date = date('Y-m-d', strtotime($request->start_date));
        $end_date   = date('Y-m-d', strtotime($request->end_date));

        $query = MarketCommodityPriceDetail::active()->latest();
        $query = $query->with([
                        'commodity:id,unit_grower,unit_whole_sale,unit_retail,commodity_name,commodity_name_bn,price_type_id',
                        'commodity.unit_whole_sale:id,unit_name,unit_name_bn',
                        'commodity.unit_retail:id,unit_name,unit_name_bn'
                    ])
                    ->whereBetween('cpi_market_commodity_prices.price_date',[$start_date, $end_date]);

        $query->leftjoin('cpi_market_commodity_prices', 'cpi_market_commodity_price_details.price_table_id', '=', 'cpi_market_commodity_prices.id');
        $query = $query->select(
            'cpi_market_commodity_price_details.com_grp_id',
            'cpi_market_commodity_price_details.com_subgrp_id',
            'cpi_market_commodity_price_details.commodity_id',
            'cpi_market_commodity_price_details.created_at',
            'cpi_market_commodity_prices.division_id',
            DB::raw('AVG(cpi_market_commodity_price_details.w_lowestPrice) as avg_wholesale_lowestPrice'),
            DB::raw('AVG(cpi_market_commodity_price_details.w_highestPrice) as avg_wholesale_highestPrice'),
            DB::raw('AVG(cpi_market_commodity_price_details.r_lowestPrice) as avg_retail_lowestPrice'),
            DB::raw('AVG(cpi_market_commodity_price_details.r_highestPrice) as avg_retail_howestPrice')
        );

        if ($request->division_id && $request->division_id != 0) {
            $query = $query->whereHas('commodity_price', function ($q) use($request) {
                $q->where('cpi_market_commodity_prices.division_id', $request->division_id);
            });
        }             

        if ($request->commodity_id) {
            $query = $query->where('cpi_market_commodity_price_details.commodity_id', $request->commodity_id);
        }             
        
        $query->groupBy(['commodity_id']);
        $datas = $query->get();

        if(count($datas) == 0) {

            return response([
                'success' => false,
                'message' => 'data Not found!'
            ]);

        } else {

            return response([
                'success' => true,
                'message' => 'data found!',
                'data' => $datas
            ]);
        }
    }
}
