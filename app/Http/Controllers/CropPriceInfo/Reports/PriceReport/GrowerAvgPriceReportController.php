<?php

namespace App\Http\Controllers\CropPriceInfo\Reports\PriceReport;

use App\Http\Controllers\Controller;
use App\Models\CropPriceInfo\Cpi\MarketCommodityGrowerPriceDetail;
use App\Models\CropPriceInfo\Cpi\MarketCommodityWeeklyPriceDetail;
use Illuminate\Http\Request;
use DB;

class GrowerAvgPriceReportController extends Controller
{
        /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function __invoke(Request $request)
    // {        
    //     // $query = DB::table('cpi_market_commodity_growers_prices as main_table')
    //     //         ->leftJoin('cpi_market_commodity_growers_price_details as details_table', 'main_table.id', '=', 'details_table.price_table_id')
    //     //         ->select('main_table.price_date', 'main_table.division_id', 'details_table.commodity_id')
    //     //         ->get();

    //     // return response(['reposne data' => $query]);

    //     $query = MarketCommodityGrowerPriceDetail::latest();

    //     $query = $query->with([
    //         'commodity_price:id,market_id,division_id,district_id,upazila_id,price_date',
    //         'commodity:id,unit_grower,unit_whole_sale,unit_retail,price_type_id,commodity_name,commodity_name_bn'
    //     ]);

    //     // if ($request->division_id) {
    //     //     $query = $query->whereHas('commodity_price', function ($q) use($request) {
    //     //         $q->where('division_id', $request->division_id);
    //     //     });
    //     // }
        
    //     // if ($request->price_date) { 
    //     //     $query = $query->whereHas('commodity_price', function ($q) use($request) {
    //     //         $q->where('price_date', $request->price_date);
    //     //     });
    //     // }              
        
    //     // if (request()->from_date) {
    //     //     $query->where('price_date', '>=',request()->from_date);
    //     // }

    //     // if (request()->to_date) {
    //     //     $query->where('price_date', '<=', request()->to_date);
    //     // }

    //     $query->groupBy(['commodity_id']);
    //     $datas = $query->get();

    //     return response([
    //         'success' => true,
    //         'message' => 'data found!',
    //         'data' => $datas
    //     ]);
    // }
    public function __invoke(Request $request)
    { 
        $start_date = date('Y-m-d', strtotime($request->start_date));
        $end_date   = date('Y-m-d', strtotime($request->end_date));

        $query = MarketCommodityGrowerPriceDetail::active()->latest();
        $query = $query->with([
                        'commodity:id,unit_grower,unit_whole_sale,unit_retail,commodity_name,commodity_name_bn,price_type_id',
                        'commodity.unit_whole_sale:id,unit_name,unit_name_bn',
                        'commodity.unit_retail:id,unit_name,unit_name_bn'
                    ])
                    ->whereBetween('cpi_market_commodity_growers_prices.price_date',[$start_date, $end_date]);

        $query->leftjoin('cpi_market_commodity_growers_prices', 'cpi_market_commodity_growers_price_details.price_table_id', '=', 'cpi_market_commodity_growers_prices.id');
        $query = $query->select(
            'cpi_market_commodity_growers_price_details.com_grp_id',
            'cpi_market_commodity_growers_price_details.com_subgrp_id',
            'cpi_market_commodity_growers_price_details.commodity_id',
            'cpi_market_commodity_growers_price_details.created_at',
            'cpi_market_commodity_growers_prices.division_id',
            DB::raw('AVG(cpi_market_commodity_growers_price_details.g_lowestPrice) as avg_wholesale_lowestPrice'),
            DB::raw('AVG(cpi_market_commodity_growers_price_details.g_highestPrice) as avg_wholesale_highestPrice')
            // DB::raw('AVG(cpi_market_commodity_growers_price_details.g_lowestPrice) as avg_retail_lowestPrice'),
            // DB::raw('AVG(cpi_market_commodity_growers_price_details.g_highestPrice) as avg_retail_howestPrice')
        );

        if ($request->division_id && $request->division_id != 0) {
            $query->where('cpi_market_commodity_growers_prices.division_id', $request->division_id);
            // $query->whereHas('commodity_price', function ($q) use($request) {
            //     $q->where('cpi_market_commodity_growers_prices.division_id', $request->division_id);
            // });
        }             

        if ($request->commodity_id) {
            $query->where('cpi_market_commodity_growers_price_details.commodity_id', $request->commodity_id);
        }             
        
        $query->groupBy('commodity_id');
        $datas = $query->get();

        if(count($datas) == 0) {

            return response([
                'success' => false,
                'message' => 'data Not found!'
            ]);

        } else {

            return response([
                'success' => true,
                'message' => 'data found!',
                'data' => $datas
            ]);
        }
    }
}
