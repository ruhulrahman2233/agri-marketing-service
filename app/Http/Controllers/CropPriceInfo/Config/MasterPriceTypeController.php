<?php
namespace App\Http\Controllers\CropPriceInfo\Config;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\CropPriceInfo\Config\MasterPriceTypeValidations;
use App\Models\CropPriceInfo\Config\MasterPriceType;
use DB;

class MasterPriceTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new MasterPriceType();

        if ($request->type_name) 
        {
            $query = $query->where('type_name', 'like', "{$request->type_name}%")
                           ->orWhere('type_name_bn', 'like', "{$request->type_name}%");
        }  

        if ($request->org_id) {
            $query = $query->where('org_id', $request->org_id);
        }

        if ($request->status) {
            $query->where('status', $request->status);
        }

        $list = $query->paginate(request('per_page', config('app.per_page'))); 

        return response([
            'success' => true,
            'message' => 'Price Type list',
            'data' =>$list
        ]);
    }


    /*+++++++show+++++++++*/
    public function show($id)
    {
        $masterPriceType = MasterPriceType::find($id);

        if (!$masterPriceType) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Price Type details',
            'data'    => $masterPriceType
        ]);
    }

   
    /*+++++++store+++++++++*/
    public function store(Request $request)
    {
        $validationResult = MasterPriceTypeValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $postData = $request->all();
            $postData['created_by'] = (int)user_id()??null;
            $postData['updated_by'] = (int)user_id()??null;

            $masterPriceType = new MasterPriceType();
            $masterPriceType->create($postData);

            /*save_log([
                'data_id'    => $masterPriceType->id,
                'table_name' => 'master_price_types'
            ]);*/

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $masterPriceType
        ]);
    }

  

     /*++++++update+++++++*/
    public function update(Request $request, $id)
    {
        $validationResult = MasterPriceTypeValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $masterPriceType = MasterPriceType::find($id);

        if (!$masterPriceType) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }
        try {

            $putData = $request->all();
            $putData['updated_by'] = (int)user_id()??null;

            $masterPriceType->update($putData);

            /*save_log([
                'data_id'       => $masterPriceType->id,
                'table_name'    => 'master_price_types',
                'execution_type'=> 1
            ]);*/

           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $masterPriceType
        ]);
    }

   /*+++++status update++++++*/
    public function toggleStatus($id)
    {
        $masterPriceType = MasterPriceType::find($id);

        if (!$masterPriceType) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterPriceType->status = $masterPriceType->status == 1 ? 2 : 1;
        $masterPriceType->update();

        // save_log([
        //     'data_id'       => $masterPriceType->id,
        //     'table_name'    => 'master_price_types',
        //     'execution_type'=> 2
        // ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $masterPriceType
        ]);
    }

    
   /*+++++sdestroy++++++*/
    public function destroy($id)
    {
        $masterPriceType = MasterPriceType::find($id);

        if (!$masterPriceType) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterPriceType->delete();

       /* save_log([
            'data_id'       => $id,
            'table_name'    => 'master_price_types',
            'execution_type'=> 2
        ]);*/

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
