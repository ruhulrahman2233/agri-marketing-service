<?php

namespace App\Http\Controllers\CropPriceInfo\Config;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\CropPriceInfo\Config\MasterMarketsValidations;
use App\Models\CropPriceInfo\Config\{ MasterMarkets, MasterMarketDirectoryCommodities, MasterMarketDirectoryLeaseValueYear };
use DB;

class MasterMarketsController extends Controller
{
	/**
     * Master markets list
    */
	public function index(Request $request)
    {
        
        $query = MasterMarkets::with(['marketing_items', 'lease_value'])->orderBy('id', 'DESC');

        if ($request->market_name) {
            $query = $query->where('market_name', 'like', "{$request->market_name}%")
                           ->orWhere('market_name_bn', 'like', "{$request->market_name}%");
        }  

        if ($request->division_id) {
            $query->where('division_id', $request->division_id);
        }

        if ($request->district_id) {
            $query->where('district_id', $request->district_id);
        }

        if ($request->district_id) {
            $query->where('district_id', $request->district_id);
        }

        $list = $query->paginate(request('per_page', config('app.per_page')));

        if (sizeof($list) > 0) {
            return response([
                'success' => true,
                'data'    => $list
            ]);
        }

        return response([
            'success' => false,
            'message' => 'Sorry, Data not found!!'
        ]); 
    }

    /**
     * Show
    */
    public function show($id)
    {
        $masterMarkets = MasterMarkets::where('id', $id)->with(['marketing_items', 'lease_value'])->first();
        // $masterMarkets = MasterMarkets::find($id);

        if (!$masterMarkets) {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Market details',
            'data'    => $masterMarkets
        ]);
    }

    /**
     * Store
    */
    public function store(Request $request)
    {
        $validationResult = MasterMarketsValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }
        DB::beginTransaction();
        try {

            $requestAll = $request->all();
            $marterMarket = MasterMarkets::create($requestAll);
            $marterMarket['created_by'] = (int)user_id();
            $marterMarket['updated_by'] = (int)user_id();
            $MasterMarketDirectoryCommodities = $marterMarket->market_directory_commodities()->createMany($requestAll['marketing_items']);  
            $MasterMarketDirectoryLeaseValueYear = $marterMarket->market_lease_year()->createMany($requestAll['lease_value']);

                save_log([
                    'data_id'    => $marterMarket->id,
                    'table_name' => 'master_markets'
                ]);



            DB::commit();

        } catch (\Exception $ex) {

            DB::rollback();
            
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $marterMarket
        ]);
    }  

    /**
     * Update
    */
    public function update(Request $request, $id)
    {
        $validationResult = MasterMarketsValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }
        DB::beginTransaction();

        $masterMarket = MasterMarkets::find($id);
        if (!$masterMarket) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {

            $requestAll    = $request->all();
            $masterMarket->update($requestAll);
            $masterMarket['updated_by']          = (int)user_id();
            $masterMarket->market_directory_commodities()->delete();
            $masterMarket->market_directory_commodities()->createMany($requestAll['marketing_items']);
            $masterMarket->market_lease_year()->delete();  
            $masterMarket->market_lease_year()->createMany($requestAll['lease_value']);

            save_log([
                'data_id'       => $masterMarket->id,
                'table_name'    => 'master_markets',
                'execution_type'=> 1
            ]);  

            DB::commit();

        } catch (\Exception $ex) {

            DB::rollback();
            
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $masterMarket
        ]);
    }

    /**
     * Status update
    */
    public function toggleStatus($id)
    {
        $masterMarkets = MasterMarkets::find($id);

        if (!$masterMarkets) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterMarkets->status = $masterMarkets->status == 1 ? 2 : 1;
        $masterMarkets->update();

        save_log([
            'data_id'       => $masterMarkets->id,
            'table_name'    => 'master_markets',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $masterMarkets
        ]);
    }
    
    /**
     * Destroy
    */
    public function destroy($id)
    {
        $masterMarkets = MasterMarkets::find($id);

        if (!$masterMarkets) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterMarkets->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'master_markets',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
