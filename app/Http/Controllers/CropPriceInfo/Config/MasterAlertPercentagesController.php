<?php

namespace App\Http\Controllers\CropPriceInfo\Config;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\CropPriceInfo\Config\MasterAlertPercentagesValidations;
use App\Models\CropPriceInfo\Config\MasterAlertPercentages;
use DB;


class MasterAlertPercentagesController extends Controller
{
	/*Master alert percentages list*/
	public function index(Request $request)
    {
        $query = new MasterAlertPercentages();         

        if ($request->fiscal_year_id) {
            $query = $query->where('fiscal_year_id', $request->fiscal_year_id);
        }

        if ($request->alert_percentage) {
            $query = $query->where('alert_percentage', $request->alert_percentage);
        }   

        if ($request->status) {
            $query->where('status', $request->status);
        }

        $list = $query->paginate(request('per_page', config('app.per_page')));

        if( count($list)<=0){
        	return response([
		            'success' => false,
		            'message' => 'Data not found!!',
		            'data' =>$list
		       ]);
        }

        if( count($list)>0){
        	return response([
	            'success' => true,
	            'message' => 'Markets list',
	            'data' =>$list
	        ]);	        	
        }        
    }

    /*+++++++Master alert percentages show+++++++++*/
    public function show($id)
    {
        $mstrAltPrcnts = MasterAlertPercentages::find($id);

        if (!$mstrAltPrcnts) {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Alert percentages details',
            'data'    => $mstrAltPrcnts
        ]);
    }

    /*+++++++Master alert percentages store+++++++++*/
    public function store(Request $request)
    {
        //dd('ok');
        $validationResult = MasterAlertPercentagesValidations::validate($request);
        if (!$validationResult['success']) {
            return response($validationResult);
        }


        try {
            $postData = $request->all();
            $postData['created_by'] = (int)user_id()??null;
            $postData['updated_by'] = (int)user_id()??null;

            $mstrAltPrcnts = new MasterAlertPercentages();
            $mstrAltPrcnts->create($postData);

            /*save_log([
                'data_id'    => $mstrAltPrcnts->id,
                'table_name' => 'master_alert_percentages'
            ]);*/

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $mstrAltPrcnts
        ]);
    }

  

    /*++++++Master alert percentages update+++++++*/
    public function update(Request $request, $id)
    {
        $validationResult = MasterAlertPercentagesValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $mstrAltPrcnts = MasterAlertPercentages::find($id);

        if (!$mstrAltPrcnts) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {

            $putData = $request->all();
            $putData['updated_by'] = (int)user_id()??null;

            $mstrAltPrcnts->update($putData);

            /*save_log([
                'data_id'       => $mstrAltPrcnts->id,
                'table_name'    => 'master_alert_percentages',
                'execution_type'=> 1
            ]);*/

           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $mstrAltPrcnts
        ]);
    }

   /*+++++Master alert percentages status update++++++*/
    public function toggleStatus($id)
    {
        $mstrAltPrcnts = MasterAlertPercentages::find($id);

        if (!$mstrAltPrcnts) {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }

        $mstrAltPrcnts->status = $mstrAltPrcnts->status == 1 ? 2 : 1;
        $mstrAltPrcnts->update();


        /*save_log([
            'data_id'       => $mstrAltPrcnts->id,
            'table_name'    => 'master_alert_percentages',
            'execution_type'=> 2
        ]);*/


        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $mstrAltPrcnts
        ]);
    }

    
   /*+++++Master alert percentages sdestroy++++++*/
    public function destroy($id)
    {
        $mstrAltPrcnts = MasterAlertPercentages::find($id);

        if (!$mstrAltPrcnts) {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }

        $mstrAltPrcnts->delete();

        /*save_log([
            'data_id'       => $id,
            'table_name'    => 'master_alert_percentages',
            'execution_type'=> 2
        ]);*/

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
