<?php

namespace App\Http\Controllers\CropPriceInfo\Config;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\CropPriceInfo\Config\CommoditySubGroupValidations;
use App\Models\CropPriceInfo\Config\MasterCommoditySubGroup;
use DB;

class CommoditySubGroupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all CommoditySubGroup
     */
    public function index(Request $request)
    {
        $query = MasterCommoditySubGroup::select("*");

        if ($request->sub_group_name) {
            $query = $query->where('sub_group_name', 'like', "{$request->sub_group_name}%")
                            ->orWhere('sub_group_name_bn', 'like', "{$request->sub_group_name}%");
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }

        $list = $query->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Commodity Sub Group list',
            'data' => $list
        ]);
    }

    /**
     * CommoditySubGroup store
     */
    public function store(Request $request)
    {
        $validationResult = CommoditySubGroupValidations:: validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $postData = $request->all();
            $postData['created_by'] = (int)user_id()??null;
            $postData['updated_by'] = (int)user_id()??null;

            $commodity_sub_group = new MasterCommoditySubGroup();
            $commodity_sub_group->create($postData);

            // save_log([
            //     'data_id'    => $commodity_sub_group->id,
            //     'table_name' => 'master_commodity_sub_groups',
            // ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $commodity_sub_group
        ]);
    }

    /**
     * CommoditySubGroup update
     */
    public function update(Request $request, $id)
    {
        $validationResult = CommoditySubGroupValidations:: validate($request ,$id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $commodity_sub_group = MasterCommoditySubGroup::find($id);

        if (!$commodity_sub_group) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $putData = $request->all();
            $putData['updated_by'] = (int)user_id()??null;

            $commodity_sub_group->update($putData);

            // save_log([
            //     'data_id'        => $commodity_sub_group->id,
            //     'table_name'     => 'master_commodity_sub_groups',
            //     'execution_type' => 1
            // ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $commodity_sub_group
        ]);
    }

    /**
     * CommoditySubGroup status update
     */
    public function toggleStatus($id)
    {
        $commodity_sub_group = MasterCommoditySubGroup::find($id);

        if (!$commodity_sub_group) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $commodity_sub_group->status = $commodity_sub_group->status ? 0 : 1;
        $commodity_sub_group->update();

        // save_log([
        //     'data_id'        => $commodity_sub_group->id,
        //     'table_name'     => 'master_commodity_sub_groups',
        //     'execution_type' => 2
        // ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $commodity_sub_group
        ]);
    }

    /**
     * CommoditySubGroup destroy
     */
    public function destroy($id)
    {
        $commodity_sub_group = MasterCommoditySubGroup::find($id);

        if (!$commodity_sub_group) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $commodity_sub_group->delete();
        
        // save_log([
        //     'data_id'        => $commodity_sub_group->id,
        //     'table_name'     => 'master_commodity_sub_groups',
        //     'execution_type' => 2
        // ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}

