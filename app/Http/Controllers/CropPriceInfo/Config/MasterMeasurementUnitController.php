<?php

namespace App\Http\Controllers\CropPriceInfo\Config;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\CropPriceInfo\Config\MasterMeasurementUnit;
use App\Http\Validations\CropPriceInfo\Config\MasterMeasurementUnitValidations;

class MasterMeasurementUnitController extends Controller
{
   /**
     * get all Master Measurement Unit
     */
    public function index(Request $request)
    {
        $query = DB::table('master_measurement_units')->select('*')->orderBy('id','DESC');

        if ($request->unit_name) 
        {
            $query = $query->where('unit_name', 'like', "{$request->unit_name}%")
                           ->orWhere('unit_name_bn', 'like', "{$request->unit_name}%");
        }  

        $list = $query->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Master Measurement Unit list',
            'data'    => $list
        ]);
    }

    /**
     * store
     */
    public function store(Request $request)
    {
        return $request;
        $validationResult = MasterMeasurementUnitValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $postData = $request->all();
            $postData['created_by'] = (int)user_id() ?? null;
            $postData['updated_by'] = (int)user_id() ?? null;

            $mdo = new MasterMeasurementUnit($postData);
            $mdo->save();   

            save_log([
                'data_id'    => $mdo->id,
                'table_name' => 'master_measurement_units'
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $mdo
        ]);
    }

    /**
     * update
     */
    public function update(Request $request, $id)
    { 
        $validationResult = MasterMeasurementUnitValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $mdo = MasterMeasurementUnit::find($id);

        if (!$mdo) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {

            $putData = $request->all();
            $putData['updated_by'] = (int)user_id()??null;

            $mdo->fill($putData);
            $mdo->update();

            save_log([
                'data_id'       => $mdo->id,
                'table_name'    => 'master_measurement_units',
                'execution_type'=> 1
            ]);           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $mdo
        ]);
    }

    /**
     * toggle status
     */
    public function toggleStatus($id)
    {
        $mdo = MasterMeasurementUnit::find($id);

        if (!$mdo) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $mdo->status = $mdo->status == 1 ? 2 : 1;
        $mdo->update();

        save_log([
            'data_id'       => $mdo->id,
            'table_name'    => 'master_measurement_units',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $mdo
        ]);
    }

    /**
     * destroy
     */
    public function destroy($id)
    {
        $mdo = MasterMeasurementUnit::find($id);

        if (!$mdo) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $mdo->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'master_measurement_units',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
