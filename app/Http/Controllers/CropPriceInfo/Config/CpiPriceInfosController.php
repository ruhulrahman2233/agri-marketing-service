<?php

namespace App\Http\Controllers\CropPriceInfo\Config;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\CropPriceInfo\Config\CpiPriceInfos;
use App\Http\Validations\CropPriceInfo\Config\CpiPriceInfosValidations;

class CpiPriceInfosController extends Controller
{
	/*Cpi price infos list*/
	public function index(Request $request)
    {
        $query = DB::table('cpi_price_infos')
                    ->leftjoin('master_commodity_names', 'cpi_price_infos.Commodity_id', 'master_commodity_names.id')
                    ->leftjoin('master_commodity_sub_groups', 'master_commodity_names.commodity_sub_group_id', 'master_commodity_sub_groups.id')
                    ->leftjoin('master_commodity_groups', 'master_commodity_sub_groups.commodity_group_id', 'master_commodity_groups.id')
                    ->select(
                        'cpi_price_infos.*',
                        'master_commodity_groups.id as Commodity_Group_id',
                        'master_commodity_groups.group_name_bn as commodity_group_name_bn',
                        'master_commodity_groups.group_name as commodity_group_name',
                        'master_commodity_sub_groups.id as Commodity_SubGroup_id',
                        'master_commodity_sub_groups.sub_group_name_bn as commodity_sub_group_name_bn',
                        'master_commodity_sub_groups.sub_group_name as commodity_sub_group_name'
                    );

        if ($request->LatitudeRecorded) {
            $query->where('LatitudeRecorded', $request->LatitudeRecorded);
        }

        if ($request->LongitudeRecorded) {
            $query->where('LongitudeRecorded', $request->LongitudeRecorded);
        }

        if ($request->HighestPrice) {
            $query->where('HighestPrice', $request->HighestPrice);
        }

        if ($request->LowestPrice) {
            $query->where('LowestPrice', $request->LowestPrice);
        }

        if ($request->AveragePrice) {
            $query->where('AveragePrice', $request->AveragePrice);
        }

        if ($request->PriceUSDRate) {
            $query->where('PriceUSDRate', $request->PriceUSDRate);
        }

        if ($request->PriceEuroRate) {
            $query->where('PriceEuroRate', $request->PriceEuroRate);
        }

        if ($request->PriceType_id) {
            $query->where('PriceType_id', $request->PriceType_id);
        }

        if ($request->Market_id) {
            $query->where('Market_id', $request->Market_id);
        }

        if ($request->Subdistrict_id) {
            $query->where('Subdistrict_id', $request->Subdistrict_id);
        }

        if ($request->District_id) {
            $query->where('District_id', $request->District_id);
        }

        if ($request->Division_id) {
            $query->where('Division_id', $request->Division_id);
        }
        
        if ($request->Commodity_id) {
            $query->where('Commodity_id', $request->Commodity_id);
        }

        if ($request->BuyerSellerType) {
            $query->where('BuyerSellerType', $request->BuyerSellerType);
        }

        if ($request->PriceDate) {
            $query->where('PriceDate', date('Y-m-d', strtotime($request->PriceDate)));
        }

        if ($request->VerificationDate) {
            $query->where('VerificationDate', date('Y-m-d', strtotime($request->VerificationDate)));
        }

        $list = $query->paginate(request('per_page', config('app.per_page')));

        if( count($list)<=0){
        	return response([
		            'success' => false,
		            'message' => 'Data not found!!',
		            'data' =>$list
		       ]);
        }

        if( count($list)>0){
        	return response([
	            'success' => true,
	            'message' => 'Cpi price infos list',
	            'data' =>$list
	        ]);	        	
        }        
    }

    /*+++++++Cpi price infos show+++++++++*/
    public function show($id)
    {
        $cpiPriceInfos = CpiPriceInfos::find($id);

        if (!$cpiPriceInfos) {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Cpi price infos details',
            'data'    => $cpiPriceInfos
        ]);
    }

    /*+++++++Cpi price infos store+++++++++*/
    public function store(Request $request)
    {      
        $validationResult = CpiPriceInfosValidations::validate($request);
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $postData = $request->all();
            $postData['MarketPriceEnteredBy'] = (int)user_id()??null;

            $cpiPriceInfos = new CpiPriceInfos();
            $cpiPriceInfos->create($postData);

            /*save_log([
                'data_id'    => $cpiPriceInfos->id,
                'table_name' => 'master_markets'
            ]);*/

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $cpiPriceInfos
        ]);
    }

    /*++++++Cpi price infos update+++++++*/
    public function update(Request $request, $id)
    {
        $validationResult = CpiPriceInfosValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $cpiPriceInfos = CpiPriceInfos::find($id);

        if (!$cpiPriceInfos) {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }

        try {

            $putData = $request->all();
            $putData['Verified_by'] = (int)user_id()??null;

            $cpiPriceInfos->update($putData);

            /*save_log([
                'data_id'       => $cpiPriceInfos->id,
                'table_name'    => 'master_markets',
                'execution_type'=> 1
            ]);*/

           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $cpiPriceInfos
        ]);
    }
    
    /*+++++Cpi price infos destroy++++++*/
    public function destroy($id)
    {
        $cpiPriceInfos = CpiPriceInfos::find($id);

        if (!$cpiPriceInfos) {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }

        $cpiPriceInfos->delete();

        /*save_log([
            'data_id'       => $id,
            'table_name'    => 'cpi_price_infos',
            'execution_type'=> 2
        ]);*/

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }

    /**
     * price collection verify list 
    */
    public function priceCollectionVerifyList(Request $request)
    {  
        $query = DB::table('cpi_price_infos')
                    ->orderBy('id','DESC'); 

        if ($request->Division_id) {
            $query->where('Division_id', $request->Division_id)
                    ->groupBy('Division_id');
        }

        if ($request->District_id) {
            $query->where('District_id', $request->District_id)
                    ->groupBy('District_id');
        }

        if ($request->PriceDate) {
            $query->where('PriceDate', date('Y-m-d', strtotime($request->PriceDate)))
                    ->groupBy('PriceDate');
        }

        $list = $query->paginate(request('per_page', config('app.per_page')));

        if (count($list) > 0) {
        	return response([
	            'success'   => true,
	            'message'   => 'Cpi price infos list',
	            'data'      => $list
	        ]);	        	
        } else {
            return response([
                'success'   => false,
                'message'   => 'Data not found!!',
                'data'      => []
            ]);
        }        
    }

    /**
     * price collection verify
    */
    public function priceCollectionVerify($id)
    {  
        $cpiPriceInfo = CpiPriceInfos::find($id);

        if (!$cpiPriceInfo) {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }     
        
        $cpiPriceInfo->VerificationDate = date('Y-m-d');
        $cpiPriceInfo->VerificationTime = date('H:i:s');
        $cpiPriceInfo->update();

        save_log([
            'data_id'       => $cpiPriceInfo->id,
            'table_name'    => 'cpi_price_infos',
            'execution_type'=> 1
        ]);

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $cpiPriceInfo
        ]);
    }

    /**
     * price collection price update
    */
    public function priceUpdate(Request $request, $id)
    { 
        $validationResult = CpiPriceInfosValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $day_before = date( 'Y-m-d', strtotime($request->PriceDate . ' -1 day')); 
        $previous   = CpiPriceInfos::where('Commodity_id', $request->Commodity_id)
                                    ->where('Market_id', $request->Market_id)
                                    ->where('PriceDate', $day_before)
                                    ->first();
        if ($previous != null) {
            $highest_price = ($request->HighestPrice - $previous->HighestPrice) / 100;
            if ($highest_price > $request->Percentage) {
                $highest_price_error = 1;
                $error_occur = 1;
            }

            $lowest_price = ($request->LowestPrice - $previous->LowestPrice) / 100;
            if ($lowest_price > $request->Percentage) {
                $lowest_price_error = 1;
                $error_occur = 1;
            }

            $average_price = ($request->AveragePrice - $previous->AveragePrice) / 100;
            if ($average_price > $request->Percentage) {
                $average_price_error = 1;
                $error_occur = 1;
            }

            $usd_rate = ($request->PriceUSDRate - $previous->PriceUSDRate) / 100;
            if ($usd_rate > $request->Percentage) {
                $usd_rate_error = 1;
                $error_occur = 1;
            }

            $euro_rate = ($request->PriceEuroRate - $previous->PriceEuroRate) / 100;
            if ($euro_rate > $request->Percentage) {
                $euro_rate_error = 1;
                $error_occur = 1;
            }

            $erros = [
                'errors' => [
                    'HighestPrice' =>  isset($highest_price_error) ? 'Highest price more difference from yesterday higest price' : Null,
                    'LowestPrice'  =>  isset($lowest_price_error) ? 'Lowest price more difference from yesterday lowest price' : Null,
                    'AveragePrice' =>  isset($average_price_error) ? 'Average price more difference from yesterday average price' : Null,
                    'PriceUSDRate' =>  isset($usd_rate_error) ? 'USD rate more difference from yesterday USD rate' : Null,
                    'PriceEuroRate'=>  isset($euro_rate_error) ? 'Euro rate more difference from yesterday Euro rate' : Null,
                ]
            ];
        }

        if (isset($error_occur)) {
            return response($erros);
        }

        $cpiPriceInfo = CpiPriceInfos::find($id);

        if (!$cpiPriceInfo) {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }

        try {

            $cpiPriceInfo->HighestPrice = $request->HighestPrice;
            $cpiPriceInfo->LowestPrice  = $request->LowestPrice;
            $cpiPriceInfo->AveragePrice = $request->AveragePrice;
            $cpiPriceInfo->PriceUSDRate = $request->PriceUSDRate;
            $cpiPriceInfo->PriceEuroRate= $request->PriceEuroRate;
            $cpiPriceInfo->update();

            save_log([
                'data_id'       => $cpiPriceInfo->id,
                'table_name'    => 'cpi_price_infos',
                'execution_type'=> 1
            ]);           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $cpiPriceInfo
        ]);
    }

    /**
     * price collection price update
    */
    public function getPriceInfoByPriceDate(Request $request)
    {   
        $day_before = date( 'Y-m-d', strtotime($request->PriceDate . ' -1 day'));
        return CpiPriceInfos::where('PriceDate', $day_before)->first();
    }

    /**
     * price view
    */
	public function priceView(Request $request)
    {  
        $date = empty(!$request->PriceDate) ? date('Y-m-d', strtotime($request->PriceDate)) : date('Y-m-d');
        $items = DB::table('cpi_price_infos')
                    ->leftjoin('master_price_types', 'cpi_price_infos.PriceType_id', 'master_price_types.id')
                    ->leftjoin('master_commodity_names', 'cpi_price_infos.Commodity_id', 'master_commodity_names.id')
                    ->leftjoin('master_units', 'master_commodity_names.unit_id', 'master_units.id')
                    ->select('cpi_price_infos.*',
                            'master_commodity_names.commodity_name','master_commodity_names.commodity_name_bn',
                            'master_price_types.type_name','master_price_types.type_name_bn',
                            'master_units.unit_name','master_units.unit_name_bn'
                    )
                    ->orderBy('cpi_price_infos.id', 'DESC')
                    ->where('cpi_price_infos.PriceDate', $date)
                    ->get();
        $list = [];
        foreach ($items as $item) { 
            $list[$item->commodity_name][$item->type_name]['unit_name_en']    = $item->unit_name;
            $list[$item->commodity_name][$item->type_name]['unit_name_bn']    = $item->unit_name_bn;
            $list[$item->commodity_name][$item->type_name]['dhaka']           = $this->getDvisionalPrice(6, $item->PriceDate);
            $list[$item->commodity_name][$item->type_name]['chattagram']      = $this->getDvisionalPrice(1, $item->PriceDate);
            $list[$item->commodity_name][$item->type_name]['khulna']          = $this->getDvisionalPrice(3, $item->PriceDate);
            $list[$item->commodity_name][$item->type_name]['rajshahi']        = $this->getDvisionalPrice(2, $item->PriceDate);
            $list[$item->commodity_name][$item->type_name]['rangpur']         = $this->getDvisionalPrice(7, $item->PriceDate);
            $list[$item->commodity_name][$item->type_name]['sylhet']          = $this->getDvisionalPrice(5, $item->PriceDate);
            $list[$item->commodity_name][$item->type_name]['barisal']         = $this->getDvisionalPrice(4, $item->PriceDate);
        }

        if (count($list) > 0) {
        	return response([
	            'success'   => true,
	            'message'   => 'Cpi price view list',
	            'data'      => $list
	        ]);	        	
        } else {
            return response([
                'success'   => false,
                'message'   => 'Data not found!!',
                'data'      => []
            ]);
        }        
    }

    /**
     * get price
     */
    public function getDvisionalPrice($division_id, $price_date)
    {
        $average_price = CpiPriceInfos::where('Division_id', $division_id)->where('PriceDate', $price_date)->sum('AveragePrice');
        $total_market  = CpiPriceInfos::where('Division_id', $division_id)->count('Market_id');
        return $total_market != 0 ? ($average_price / $total_market) : 0;
    }
}