<?php
namespace App\Http\Controllers\CropPriceInfo\Config;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\CropPriceInfo\Config\MasterCommodityGroupValidations;
use App\Models\CropPriceInfo\Config\MasterCommodityGroup;
use DB;

class MasterCommodityGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new MasterCommodityGroup();

        if ($request->group_name) 
        {
            $query = $query->where('group_name', 'like', "{$request->group_name}%")
                           ->orWhere('group_name_bn', 'like', "{$request->group_name}%");
        }  

        if ($request->org_id) {
            $query = $query->where('org_id', $request->org_id);
        }

        if ($request->status) {
            $query->where('status', $request->status);
        }

        $list = $query->paginate(request('per_page', config('app.per_page'))); 

        return response([
            'success' => true,
            'message' => 'Commodity Group list',
            'data' =>$list
        ]);
    }


    /*+++++++show+++++++++*/
    public function show($id)
    {
        $masterCommodityGroups = MasterCommodityGroup::find($id);

        if (!$masterCommodityGroups) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Commodity Group details',
            'data'    => $masterCommodityGroups
        ]);
    }

   
    /*+++++++store+++++++++*/
    public function store(Request $request)
    {
        $validationResult = MasterCommodityGroupValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $postData = $request->all();
            $postData['created_by'] = (int)user_id()??null;
            $postData['updated_by'] = (int)user_id()??null;

            $masterCommodityGroup = new MasterCommodityGroup();
            $masterCommodityGroup->create($postData);

            /*save_log([
                'data_id'    => $masterCommodityGroup->id,
                'table_name' => 'master_commidity_groups'
            ]);*/

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $masterCommodityGroup
        ]);
    }

  

     /*++++++update+++++++*/
    public function update(Request $request, $id)
    {
        $validationResult = MasterCommodityGroupValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $masterCommodityGroup = MasterCommodityGroup::find($id);

        if (!$masterCommodityGroup) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }
        try {

            $putData = $request->all();
            $putData['updated_by'] = (int)user_id()??null;

            $masterCommodityGroup->update($putData);

            /*save_log([
                'data_id'       => $masterCommodityGroup->id,
                'table_name'    => 'master_commidity_groups',
                'execution_type'=> 1
            ]);*/

           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $masterCommodityGroup
        ]);
    }

   /*+++++status update++++++*/
    public function toggleStatus($id)
    {
        $masterCommodityGroup = MasterCommodityGroup::find($id);

        if (!$masterCommodityGroup) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterCommodityGroup->status = $masterCommodityGroup->status == 1 ? 2 : 1;
        $masterCommodityGroup->update();

        // save_log([
        //     'data_id'       => $masterCommodityGroup->id,
        //     'table_name'    => 'master_commidity_groups',
        //     'execution_type'=> 2
        // ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $masterCommodityGroup
        ]);
    }

    
   /*+++++sdestroy++++++*/
    public function destroy($id)
    {
        $masterCommodityGroup = MasterCommodityGroup::find($id);

        if (!$masterCommodityGroup) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterCommodityGroup->delete();

       /* save_log([
            'data_id'       => $id,
            'table_name'    => 'master_commidity_groups',
            'execution_type'=> 2
        ]);*/

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
