<?php

namespace App\Http\Controllers\CropPriceInfo\Config;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\CropPriceInfo\Config\MasterCommodityNameValidations;
use App\Models\CropPriceInfo\Config\MasterCommodityName;
use DB;

class MasterCommodityNameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new MasterCommodityName();

        if ($request->commodity_name) 
        {
            $query = $query->where('commodity_name', 'like', "{$request->commodity_name}%")
                           ->orWhere('commodity_name_bn', 'like', "{$request->commodity_name}%");
        }  

        if ($request->org_id) {
            $query = $query->where('org_id', $request->org_id);
        }

        if ($request->group_id) {
            $query = $query->where('commodity_group_id', $request->commodity_group_id);
        }

        if ($request->_id) {
            $query = $query->where('commodity_sub_group_id', $request->commodity_sub_group_id);
        }   

        if ($request->status) {
            $query->where('status', $request->status);
        }

        $list = $query->paginate($request->per_page??10); 

        return response([
            'success' => true,
            'message' => 'Commodity Name list',
            'data' =>$list
        ]);
    }


    /*+++++++show+++++++++*/
    public function show($id)
    {
        $masterCommodityNames = MasterCommodityName::find($id);

        if (!$masterCommodityNames) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Commodity Name details',
            'data'    => $masterCommodityNames
        ]);
    }

   
    /*+++++++store+++++++++*/
    public function store(Request $request)
    {
        $validationResult = MasterCommodityNameValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }


        try {
            $postData = $request->all();
            $postData['created_by'] = (int)user_id()??null;
            $postData['updated_by'] = (int)user_id()??null;

            $masterCommodityNames = new MasterCommodityName();
            $masterCommodityNames->create($postData);

            /*save_log([
                'data_id'    => $masterCommodityNames->id,
                'table_name' => 'master_commodity_names'
            ]);*/

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $masterCommodityNames
        ]);
    }

  

     /*++++++update+++++++*/
    public function update(Request $request, $id)
    {
        $validationResult = MasterCommodityNameValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $masterCommodityNames = MasterCommodityName::find($id);

        if (!$masterCommodityNames) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {

            $putData = $request->all();
            $putData['updated_by'] = (int)user_id()??null;

            $masterCommodityNames->update($putData);

            /*save_log([
                'data_id'       => $masterCommodityNames->id,
                'table_name'    => 'master_commodity_names',
                'execution_type'=> 1
            ]);*/

           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $masterCommodityNames
        ]);
    }

   /*+++++status update++++++*/
    public function toggleStatus($id)
    {
        $masterCommodityNames = MasterCommodityName::find($id);

        if (!$masterCommodityNames) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterCommodityNames->status = $masterCommodityNames->status == 1 ? 2 : 1;
        $masterCommodityNames->update();

        /*save_log([
            'data_id'       => $masterCommodityNames->id,
            'table_name'    => 'master_commodity_names',
            'execution_type'=> 2
        ]);*/

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $masterCommodityNames
        ]);
    }

    
   /*+++++destroy++++++*/
    public function destroy($id)
    {
        $masterCommodityNames = MasterCommodityName::find($id);

        if (!$masterCommodityNames) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterCommodityNames->delete();

      /*  save_log([
            'data_id'       => $id,
            'table_name'    => 'master_commodity_names',
            'execution_type'=> 2
        ]);*/

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
