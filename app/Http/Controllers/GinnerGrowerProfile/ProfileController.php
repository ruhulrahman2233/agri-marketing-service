<?php

namespace App\Http\Controllers\GinnerGrowerProfile;

use App\Http\Controllers\Controller;
use App\Http\Validations\Cotton\Config\GinnerGrowerProfileModifyValidations;
use App\Models\Cotton\Config\GinnerGrowerProfile;
use Illuminate\Http\Request;
use DB;

class ProfileController extends Controller
{
    /**
     * get profile details by mobile_no
     */
    public function details ($mobile_no)
    {
        $details = GinnerGrowerProfile::where('mobile_no', $mobile_no)->first();

        if (!$details) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Profile details',
            'data'    => $details
        ]);
    }

    /**
     * grower profile update
     */
    public function update(Request $request, $id)
    {
        $validationResult = GinnerGrowerProfileModifyValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $ggp = GinnerGrowerProfile::find($id);
            $ggp->name          = $request->name;
            $ggp->name_bn       = $request->name_bn;
            $ggp->father_name   = $request->father_name;
            $ggp->father_name_bn= $request->father_name_bn;
            $ggp->nid           = $request->nid;
            $ggp->mobile_no     = $request->mobile_no;
            $ggp->district_id   = $request->district_id;
            $ggp->upazilla_id   = $request->upazilla_id;
            $ggp->org_id        = $request->org_id;
            $ggp->region_id     = $request->region_id;
            $ggp->unit_id       = $request->unit_id;
            $ggp->zone_id       = $request->zone_id;
            $ggp->address       = $request->address;
            $ggp->address_bn    = $request->address_bn;
            $ggp->land_area     = $request->land_area;
            $ggp->is_draft      = $request->is_draft;
            $ggp->update();

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to updated data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success'   => true,
            'message'   => 'Data updated successfully',
            'data'       => $ggp
        ]);
    }

    /***
     * grower or grower hatt notification
     */
    public function hattNotification(Request $request) 
    {   
        $mobile_no = $request->mobile_no;
        $query = DB::table('hatt_notifications')                  
                    ->leftjoin('master_hatts','hatt_notifications.hatt_id','master_hatts.id')
                    ->leftjoin('ginner_grower_profiles','hatt_notifications.ginner_grower_id','ginner_grower_profiles.id')
                    ->leftjoin('master_seasons','hatt_notifications.seasons_id','master_seasons.id')
                    ->select('hatt_notifications.*',
                        'master_seasons.season_name','master_seasons.season_name_bn',
                        'master_hatts.hatt_name','master_hatts.hatt_name_bn'
                    )
                    ->where('ginner_grower_profiles.mobile_no', $mobile_no)
                    ->orderBy('hatt_notifications.id', 'DESC');

        if ($request->fiscal_year_id) {
            $query = $query->where('hatt_notifications.fiscal_year_id', $request->fiscal_year_id);
        }

        if ($request->seasons_id) {
            $query = $query->where('hatt_notifications.seasons_id', $request->seasons_id);
        }

        if ($request->hatt_id) {
            $query = $query->where('hatt_notifications.hatt_id', $request->hatt_id);
        }

        $list = $query->paginate(request('per_page', config('app.per_page')));

        if(count($list) > 0){
            return response([
                'success'   => true,
                'message'   => 'Notification list',
                'data'      => $list
            ]);
        }else{
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }
    }
}
