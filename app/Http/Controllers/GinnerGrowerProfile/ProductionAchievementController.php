<?php

namespace App\Http\Controllers\GinnerGrowerProfile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ProductionAchievementController extends Controller
{
    /**
     * show all production achievement by grower id
     */
    public function index (Request $request) 
    { 
        $query = DB::table('grower_prod_achievements')
                    ->join('ginner_grower_profiles','grower_prod_achievements.applicant_id','ginner_grower_profiles.id')
                    ->leftjoin('master_regions','ginner_grower_profiles.region_id','master_regions.id')                    
                    ->leftjoin('master_zones','ginner_grower_profiles.zone_id','master_zones.id')
                    ->leftjoin('master_units','ginner_grower_profiles.unit_id','master_units.id')
                    ->select('grower_prod_achievements.*',
                        'ginner_grower_profiles.region_id',
                        'ginner_grower_profiles.district_id',
                        'ginner_grower_profiles.upazilla_id',
                        'ginner_grower_profiles.zone_id',
                        'ginner_grower_profiles.unit_id',
                        'ginner_grower_profiles.name','ginner_grower_profiles.name_bn',
                        'ginner_grower_profiles.mobile_no','ginner_grower_profiles.nid',
                        'ginner_grower_profiles.father_name','ginner_grower_profiles.father_name_bn',
                        'ginner_grower_profiles.address','ginner_grower_profiles.address_bn',
                        'ginner_grower_profiles.land_area',
                        'master_regions.region_name','master_regions.region_name_bn',
                        'master_zones.zone_name','master_zones.zone_name_bn',
                        'master_units.unit_name','master_units.unit_name_bn'
                    )
                    ->where('grower_prod_achievements.applicant_id', $request->id);

        if ($request->region_id) {
            $query = $query->where('ginner_grower_profiles.region_id', $request->region_id);
        }

        if ($request->district_id) {
            $query = $query->where('ginner_grower_profiles.district_id', $request->district_id);
        }

        if ($request->upazilla_id) {
            $query = $query->where('ginner_grower_profiles.upazilla_id', $request->upazilla_id);
        }

        if ($request->unit_id) {
            $query = $query->where('ginner_grower_profiles.unit_id', $request->unit_id);
        }

        if ($request->zone_id) {
            $query = $query->where('ginner_grower_profiles.zone_id', $request->zone_id);
        }

        if ($request->fiscal_year_id) {
            $query = $query->where('grower_prod_achievements.fiscal_year_id', $request->fiscal_year_id);
        }

        if ($request->seasons_id) {
            $query = $query->where('grower_prod_achievements.seasons_id', $request->seasons_id);
        }

        $list= $query->get();

        if(count($list) > 0){
            return response([
                'success'   => true,
                'message'   => 'Production achievements list',
                'data'      => $list
            ]);
        }else{
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }
    }
}
