<?php

namespace App\Http\Controllers\PriceCollectorProfile\Config;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Helpers\GlobalFileUploadFunctoin;
use App\Models\PriceCollectorProfile\Config\CpiPriceCollectorProfiles;
use App\Http\Validations\PriceCollectorProfile\Config\CpiPriceCollectorProfilesValidations;

class CpiPriceCollectorProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = DB::table('cpi_price_collector_profiles');

        if ($request->division_id) {
            $query->where('division_id', $request->division_id);
        }

        if ($request->district_id) {
            $query->where('district_id', $request->district_id);
        }

        if ($request->upazilla_id) {
            $query->where('upazilla_id', $request->upazilla_id);
        }


        if ($request->union_id) {
            $query->where('union_id', $request->union_id);
        }

        if ($request->name) 
        {
            $query = $query->where('name', 'like', "{$request->name}%")
                           ->orWhere('name_bn', 'like', "{$request->name}%");
        } 

        if ($request->father_name) 
        {
            $query = $query->where('father_name', 'like', "{$request->father_name}%")
                           ->orWhere('father_name_bn', 'like', "{$request->father_name}%");
        }        

        if ($request->designation) 
        {
            $query = $query->where('designation', 'like', "{$request->designation}%")
                           ->orWhere('designation_bn', 'like', "{$request->designation}%");
        }

        if ($request->mobile_no) {
            $query->where('mobile_no', $request->mobile_no);
        }

        if ($request->nid) {
            $query->where('nid', $request->nid);
        }


        if ($request->is_approved) {
            $query->where('is_approved', $request->is_approved);
        }


        if ($request->status) {
            $query->where('status', $request->status);
        }

        $list = $query->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Commodity type list',
            'data' =>$list
        ]);
    }	 	

    /*+++++++show+++++++++*/
    public function show($id)
    {
        $cpiPriceCollectorProfiles = CpiPriceCollectorProfiles::find($id);

        if (!$cpiPriceCollectorProfiles) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Commodity type details',
            'data'    => $cpiPriceCollectorProfiles
        ]);
    }

   
    /*+++++++store+++++++++*/
    public function store(Request $request)
    {      
        $validationResult = CpiPriceCollectorProfilesValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $attachment_path      	= 'price-collector-profiles/attachment';
        $signature_path      	= 'price-collector-profiles/signature';
        $attachment     		=  $request->file('attachment'); 
        $signature     			=  $request->file('signature'); 

        if($attachment !=""){
            $attachment_name = GlobalFileUploadFunctoin::image_validation_and_return_image_name($request, 'attachment');
        } 

        if($signature !=""){
            $signature_name = GlobalFileUploadFunctoin::image_validation_and_return_image_name($request,'signature');
        } 

        try {

            $cpiPriceCollectorProfiles 					= new CpiPriceCollectorProfiles();
            $cpiPriceCollectorProfiles->division_id     = (int)$request->division_id;
            $cpiPriceCollectorProfiles->district_id     = (int)$request->district_id;
            $cpiPriceCollectorProfiles->upazilla_id     = (int)$request->upazilla_id;
            $cpiPriceCollectorProfiles->union_id     	= (int)$request->union_id;
            $cpiPriceCollectorProfiles->name     		= $request->name;
            $cpiPriceCollectorProfiles->name_bn     	= $request->name_bn;
            $cpiPriceCollectorProfiles->father_name    	= $request->father_name;
            $cpiPriceCollectorProfiles->father_name_bn  = $request->father_name_bn;
            $cpiPriceCollectorProfiles->designation    	= $request->designation;
            $cpiPriceCollectorProfiles->designation_bn  = $request->designation_bn;
            $cpiPriceCollectorProfiles->address    		= $request->address;
            $cpiPriceCollectorProfiles->address_bn  	= $request->address_bn;
            $cpiPriceCollectorProfiles->mobile_no     	= $request->mobile_no;
            $cpiPriceCollectorProfiles->nid     		= $request->nid;
            $cpiPriceCollectorProfiles->latitude     	= $request->latitude;
            $cpiPriceCollectorProfiles->longitude     	= $request->longitude;
            $cpiPriceCollectorProfiles->remarks     	= $request->remarks;
            $cpiPriceCollectorProfiles->remarks_bn     	= $request->remarks_bn;
            $cpiPriceCollectorProfiles->attachment     	= $attachment_name??null;
            $cpiPriceCollectorProfiles->signature     	= $signature_name??null;
            $cpiPriceCollectorProfiles->created_by     	= (int)user_id()??null;
            $cpiPriceCollectorProfiles->updated_by     	= (int)user_id()??null;

            if($cpiPriceCollectorProfiles->save()){
                if($attachment !="" && $attachment !=null){
                    GlobalFileUploadFunctoin::image_uploader($request, $attachment_path,'attachment', $attachment_name);
                }

                if($signature !="" && $signature !=null){
                    GlobalFileUploadFunctoin::image_uploader($request, $signature_path,'signature', $signature_name);
                }
            }

            /*save_log([
                'data_id'    => $cpiPriceCollectorProfiles->id,
                'table_name' => 'cpi_price_collector_profiles'
            ]);*/

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $cpiPriceCollectorProfiles
        ]);
    }

  

     /*++++++update+++++++*/
    public function update(Request $request, $id)
    {
        $validationResult = CpiPriceCollectorProfilesValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $cpiPriceCollectorProfiles = CpiPriceCollectorProfiles::find($id);

        if (!$cpiPriceCollectorProfiles) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $attachment_path      	= 'price-collector-profiles/attachment';
        $signature_path      	= 'price-collector-profiles/signature';
        $attachment     		=  $request->file('attachment'); 
        $signature     			=  $request->file('signature'); 

        $oldAttachmentName = $cpiPriceCollectorProfiles->attachment;
        $oldSignatureName = $cpiPriceCollectorProfiles->signature;

        if($attachment !="" && $attachment !=null){
            $attachment_name = GlobalFileUploadFunctoin::image_validation_and_return_image_name($request, 'attachment');
        } 

        if($signature !="" && $signature !=null){
            $signature_name = GlobalFileUploadFunctoin::image_validation_and_return_image_name($request, 'signature');
        } 


        try {
            $cpiPriceCollectorProfiles->division_id     = (int)$request->division_id;
            $cpiPriceCollectorProfiles->district_id     = (int)$request->district_id;
            $cpiPriceCollectorProfiles->upazilla_id     = (int)$request->upazilla_id;
            $cpiPriceCollectorProfiles->union_id     	= (int)$request->union_id;
            $cpiPriceCollectorProfiles->name     		= $request->name;
            $cpiPriceCollectorProfiles->name_bn     	= $request->name_bn;
            $cpiPriceCollectorProfiles->father_name    	= $request->father_name;
            $cpiPriceCollectorProfiles->father_name_bn  = $request->father_name_bn;
            $cpiPriceCollectorProfiles->designation    	= $request->designation;
            $cpiPriceCollectorProfiles->designation_bn  = $request->designation_bn;
            $cpiPriceCollectorProfiles->address    		= $request->address;
            $cpiPriceCollectorProfiles->address_bn  	= $request->address_bn;
            $cpiPriceCollectorProfiles->mobile_no     	= $request->mobile_no;
            $cpiPriceCollectorProfiles->nid     		= $request->nid;
            $cpiPriceCollectorProfiles->latitude     	= $request->latitude;
            $cpiPriceCollectorProfiles->longitude     	= $request->longitude;
            $cpiPriceCollectorProfiles->remarks     	= $request->remarks;
            $cpiPriceCollectorProfiles->remarks_bn     	= $request->remarks_bn;
            $cpiPriceCollectorProfiles->attachment     	= $attachment_name??$oldAttachmentName;
            $cpiPriceCollectorProfiles->signature     	= $signature_name??$oldSignatureName;
            $cpiPriceCollectorProfiles->updated_by     	= (int)user_id()??null;


            if($cpiPriceCollectorProfiles->save()){
                if($attachment !="" && $attachment !=null){
                    GlobalFileUploadFunctoin::image_uploader($request, $attachment_path, 'attachment', $attachment_name, $oldAttachmentName);
                }

                if($signature !="" && $signature !=null){
                    GlobalFileUploadFunctoin::image_uploader($request, $signature_path, 'signature', $signature_name, $oldSignatureName);
                }
            } 
            /*save_log([
                'data_id'       => $cpiPriceCollectorProfiles->id,
                'table_name'    => 'cpi_price_collector_profiles',
                'execution_type'=> 1
            ]);*/

           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $cpiPriceCollectorProfiles
        ]);
    }

   /*+++++status update++++++*/
    public function toggleStatus($id)
    {
        $cpiPriceCollectorProfiles = CpiPriceCollectorProfiles::find($id);

        if (!$cpiPriceCollectorProfiles) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $cpiPriceCollectorProfiles->status = $cpiPriceCollectorProfiles->status == 1 ? 2 : 1;
        $cpiPriceCollectorProfiles->update();


        /*save_log([
            'data_id'       => $cpiPriceCollectorProfiles->id,
            'table_name'    => 'cpi_price_collector_profiles',
            'execution_type'=> 2
        ]);*/


        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $cpiPriceCollectorProfiles
        ]);
    }


    /*+++++status approved++++++*/
    public function isApproved(Request $request, $id)
    {
        // return $request;
        $cpiPriceCollectorProfiles = CpiPriceCollectorProfiles::find($id);

        if (!$cpiPriceCollectorProfiles) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

       $cpiPriceCollectorProfiles->status = 2;
        $cpiPriceCollectorProfiles->app_rej_reason = $request->app_rej_reason??null;
        $cpiPriceCollectorProfiles->app_rej_reason_bn = $request->app_rej_reason_bn??null;
        $cpiPriceCollectorProfiles->update();


        /*save_log([
            'data_id'       => $cpiPriceCollectorProfiles->id,
            'table_name'    => 'cpi_price_collector_profiles',
            'execution_type'=> 2
        ]);*/


        return response([
            'success' => true,
            'message' => 'Profile successfully approved',
            'data'    => $cpiPriceCollectorProfiles
        ]);
    }



     /*+++++status reject++++++*/
    public function isReject(Request $request, $id)
    {
        $cpiPriceCollectorProfiles = CpiPriceCollectorProfiles::find($id);

        if (!$cpiPriceCollectorProfiles) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $cpiPriceCollectorProfiles->status = 3;
        $cpiPriceCollectorProfiles->app_rej_reason = $request->app_rej_reason??null;
        $cpiPriceCollectorProfiles->app_rej_reason_bn = $request->app_rej_reason_bn??null;
        $cpiPriceCollectorProfiles->update();

        /*save_log([
            'data_id'       => $cpiPriceCollectorProfiles->id,
            'table_name'    => 'cpi_price_collector_profiles',
            'execution_type'=> 2
        ]);*/


        return response([
            'success' => true,
            'message' => 'Profile successfully rejected',
            'data'    => $cpiPriceCollectorProfiles
        ]);
    }


     

    
   /*+++++sdestroy++++++*/
    public function destroy($id)
    {
        $cpiPriceCollectorProfiles = CpiPriceCollectorProfiles::find($id);

        if (!$cpiPriceCollectorProfiles) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $cpiPriceCollectorProfiles->delete();

        /*save_log([
            'data_id'       => $id,
            'table_name'    => 'cpi_price_collector_profiles',
            'execution_type'=> 2
        ]);*/

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
