<?php
namespace App\Http\Controllers\PustiMapping\MappingParameter;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\PustiMapping\MasterParametersValidations;
use App\Models\PustiMapping\MasterMappingParameters;
use DB;

class MasterParametersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = MasterMappingParameters::select("*");

        if ($request->gender) {
            $query = $query->where('gender',$request->gender);
        }
        if ($request->month) {
            $query = $query->where('month',$request->month);
        }
        if ($request->weight_range_from) {
            $query = $query->where('weight_range_from',$request->weight_range_from);
        }
        if ($request->weight_range_to) {
            $query = $query->where('weight_range_to',$request->weight_range_to);
        }
        if ($request->height_range_from) {
            $query = $query->where('height_range_from',$request->height_range_to);
        }
        if ($request->z_score) {
            $query = $query->where('z_score',$request->z_score);
        }
        if ($request->gender && $request->month && $request->weight_range_from && $request->weight_range_to && $request->height_range_from && $request->height_range_to && $request->z_score) {
            $query = $query->where('gender',$request->gender)->where('month',$request->month)->where('weight_range_from',$request->weight_range_from)->where('weight_range_to',$request->weight_range_to)->where('height_range_from',$request->height_range_from)->where('height_range_to',$request->height_range_to)->where('z_score',$request->z_score);
        }
        if ($request->gender && $request->month && $request->z_score) {
            $query = $query->where('gender',$request->gender)->where('month',$request->month)->where('z_score',$request->z_score);
        }
        if ($request->gender && $request->month) {
            $query = $query->where('gender',$request->gender)->where('month',$request->month);
        }
        $list = $query->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Mapping Parameter list',
            'data' =>$list
        ]);
    }


    /*+++++++Get Data Of the parameter Id+++++++++*/
    public function infoby_id($id)
    {
        $masterMappingParameters = MasterMappingParameters::find($id);

        if (!$masterMappingParameters) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Mapping Perameters details',
            'data'    => $masterMappingParameters
        ]);
    }
   
    /*+++++++store+++++++++*/
    public function store(Request $request)
    {
        $validationResult = MasterParametersValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $postData = $request->all();
            $postData['created_by'] = (int)user_id()??null;
            $postData['updated_by'] = (int)user_id()??null;
            foreach ($postData['weight_range_from'] as $key => $value) {
                if (!empty($value)) {
                    $find_exist = MasterMappingParameters::where('month',$postData['month'])
                                    ->where('gender',$postData['gender'])
                                    ->where('weight_range_from',$value['value'])
                                    ->where('weight_range_to',$postData['weight_range_to'][$key]['value'])
                                    ->where('height_range_from',$postData['height_range_from'][$key]['value'])
                                    ->where('height_range_to',$postData['height_range_to'][$key]['value'])
                                    ->where('z_score',$postData['z_score'][$key]['value'])
                                    ->get();
                    if (!$find_exist->count()  > 0) {
                        $masterMappingParameters = new MasterMappingParameters;
                        $masterMappingParameters->month = $postData['month'];
                        $masterMappingParameters->gender = $postData['gender'];
                        $masterMappingParameters->weight_range_from = $value['value'];
                        $masterMappingParameters->weight_range_to = $postData['weight_range_to'][$key]['value'];
                        $masterMappingParameters->height_range_from = $postData['height_range_from'][$key]['value'];
                        $masterMappingParameters->height_range_to = $postData['height_range_to'][$key]['value'];
                        $masterMappingParameters->z_score = $postData['z_score'][$key]['value'];
                        $masterMappingParameters['created_by'] = (int)user_id()??null;
                        $masterMappingParameters['updated_by'] = (int)user_id()??null;
                        $masterMappingParameters->save();
                    } else {
                        return response([
                            'success' => false,
                            'message' => 'Data Already Exist',
                            'errors'  => 'Data Already Exist'
                        ]);
                    }
                }
            }

            /*save_log([
                'data_id'    => $masterMappingParameters->id,
                'table_name' => 'master_mapping_parameters'
            ]);*/

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $masterMappingParameters
        ]);
    }

  

     /*++++++update+++++++*/
    public function update(Request $request, $id)
    {
        $validationResult = MasterParametersValidations:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $masterMappingParameters = MasterMappingParameters::find($id);

        if (!$masterMappingParameters) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }
        try {

            $getData = $request->all();

            $putData['weight_range_from'] = $getData['weight_range_from']['0']['value'];
            $putData['weight_range_to'] = $getData['weight_range_to']['0']['value'];
            $putData['height_range_from'] = $getData['height_range_from']['0']['value'];
            $putData['height_range_to'] = $getData['height_range_to']['0']['value'];
            $putData['z_score'] = $getData['z_score']['0']['value'];
            $putData['gender'] = $getData['gender'];
            $putData['month'] = $getData['month'];
            $putData['updated_by'] = (int)user_id()??null;

            $masterMappingParameters->update($putData);

            /*save_log([
                'data_id'       => $masterMappingParameters->id,
                'table_name'    => 'master_mapping_parameters',
                'execution_type'=> 1
            ]);*/

           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $masterMappingParameters
        ]);
    }

   /*+++++status update++++++*/
    public function toggleStatus($id)
    {
        $masterMappingParameters = MasterMappingParameters::find($id);

        if (!$masterMappingParameters) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterMappingParameters->status = $masterMappingParameters->status == 1 ? 2 : 1;
        $masterMappingParameters->update();

        // save_log([
        //     'data_id'       => $masterMappingParameters->id,
        //     'table_name'    => 'master_mapping_parameters',
        //     'execution_type'=> 2
        // ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $masterMappingParameters
        ]);
    }

    
   /*+++++sdestroy++++++*/
    public function destroy($id)
    {
        $masterMappingParameters = MasterMappingParameters::find($id);

        if (!$masterMappingParameters) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterMappingParameters->delete();

       /* save_log([
            'data_id'       => $id,
            'table_name'    => 'master_mapping_parameters',
            'execution_type'=> 2
        ]);*/

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
