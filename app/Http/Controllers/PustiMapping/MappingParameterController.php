<?php

namespace App\Http\Controllers\PustiMapping;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PustiMapping\MappingParameter;

class MappingParameterController extends Controller
{
    //mapping parameter Pdf Data
    public function mappingParameterPdf(Request $request)
    {
        if($request->gender && $request->month_from && $request->month_to){
            $records = MappingParameter::where('gender',$request->gender)->whereBetween('month', [$request->month_from, $request->month_to])->orderBy('month', 'ASC')->get()->groupBy(['month','gender']);
        }else if ($request->gender) {
            $records = MappingParameter::where('gender',$request->gender)->orderBy('month', 'ASC')->get()->groupBy(['month','gender']);
        }else {
            $records = MappingParameter::orderBy('month', 'ASC')->get()->groupBy(['month','gender']);
        }
        foreach($records as $key => $record)
        {
            $list[][$key] = $record;
        }
        return response([
            'success' => true,
            'data' => $list,
            'message' => 'Mapping Parameter Pdf data'
        ]);
    }
}
