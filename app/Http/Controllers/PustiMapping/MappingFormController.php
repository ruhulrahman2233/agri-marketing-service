<?php
namespace App\Http\Controllers\PustiMapping;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\PustiMapping\MappingSocialValidations;
use App\Http\Validations\PustiMapping\MappingPhysicalValidations;
use App\Http\Validations\PustiMapping\MappingHealthValidations;
use App\Models\PustiMapping\MappingSocioEconomicInfo;
use App\Models\PustiMapping\MappingPhysicalMeasurment;
use App\Models\PustiMapping\MappingHealthMeasurment;
use DB;

class MappingFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = MappingSocioEconomicInfo::select("*");

        if ($request->gender) {
            $query = $query->where('gender',$request->gender);
        }
        if ($request->division_id) {
            $query = $query->where('division_id',$request->division_id);
        }
        if ($request->district_id) {
            $query = $query->where('district_id',$request->district_id);
        }
        if ($request->upazila_id) {
            $query = $query->where('upazila_id',$request->upazila_id);
        }
        if ($request->gender && $request->division_id) {
            $query = $query->where('gender',$request->gender)->where('division_id',$request->division_id);
        }
        if ($request->gender && $request->division_id && $request->district_id) {
            $query = $query->where('gender',$request->gender)->where('division_id',$request->division_id)->where('district_id',$request->district_id);
        }
        $list = $query->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Pusti Mapping Form list',
            'data' =>$list
        ]);
    }


    /*+++++++Get Form Submitted Data By ID+++++++++*/
    public function infoby_id($id)
    {
        $mappingFormData = MappingSocioEconomicInfo::with('physical_info','health_info')->where('id',$id)->first();

        if (!$mappingFormData) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Mapping Form Data',
            'data'    => $mappingFormData
        ]);
    }

   
    /*+++++++Socio Economic Form Data Save Or Update+++++++++*/
    public function social_info(Request $request)
    {
        $validationResult = MappingSocialValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $postData = $request->all();
            $mappingIdData = MappingSocioEconomicInfo::where('pusti_mapping_id',$request->pusti_mapping_id)->first();
            if (!$mappingIdData) {
                $postData['created_by'] = (int)user_id()??null;
                $postData['updated_by'] = (int)user_id()??null;
                $postData['status'] = 2;
                $mappingSocioData = MappingSocioEconomicInfo::createSocioInfo($postData);
                return response([
                'success' => true,
                'message' => 'Data save successfully',
                'data'    => $mappingSocioData
                ]);
            }else{
                $putData = $request->all();
                $putData['updated_by'] = (int)user_id()??null;
                $mappingIdData->update($putData);

                return response([
                'success' => true,
                'message' => 'Data updated successfully',
                'data'    => $mappingIdData->id
                ]);
            }


            /*save_log([
                'data_id'    => $mappingSocioData->id,
                'table_name' => 'pm_socio_economic_info'
            ]);*/

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

    }

    /*+++++++Physical Measurment Form Data Save Or Update+++++++++*/
    public function physical_info(Request $request)
    {
        $validationResult = MappingPhysicalValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $postData = $request->all();
            $mappingIdData = MappingPhysicalMeasurment::where('socio_economic_info_id',$request->socio_economic_info_id)->first();
            if (!$mappingIdData) {
                $postData['created_by'] = (int)user_id()??null;
                $postData['updated_by'] = (int)user_id()??null;
                $mappingPhysicalData = MappingPhysicalMeasurment::createPhysicalInfo($postData);

                return response([
                'success' => true,
                'message' => 'Data save successfully',
                'data'    => $mappingPhysicalData
                ]);
                /*save_log([
                    'data_id'    => $mappingPhysicalData->id,
                    'table_name' => 'pm_physical_measurement'
                ]);*/
            }else{
                $postData['created_by'] = $mappingIdData->created_by;
                $mappingIdData->delete();
                $postData = $request->all();
                $postData['updated_by'] = (int)user_id()??null;
                $mappingPhysicalData = MappingPhysicalMeasurment::createPhysicalInfo($postData);

                return response([
                'success' => true,
                'message' => 'Data updated successfully',
                'data'    => $mappingIdData
                ]);

                /*save_log([
                    'data_id'    => $mappingIdData->id,
                    'table_name' => 'pm_physical_measurement'
                ]);*/
            }

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

    }

    /*+++++++Health Measurment Form Data Save Or Update+++++++++*/
    public function health_info(Request $request)
    {
        $validationResult = MappingHealthValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $postData = $request->all();
            $mappingIdData = MappingHealthMeasurment::where('socio_economic_info_id',$request->socio_economic_info_id)->first();
            if (!$mappingIdData) {
                $postData['created_by'] = (int)user_id()??null;
                $postData['updated_by'] = (int)user_id()??null;
                $mappingHealthData = new MappingHealthMeasurment();
                $mappingHealthData->create($postData);

                return response([
                'success' => true,
                'message' => 'Data save successfully',
                'data'    => $mappingHealthData
                ]);
                /*save_log([
                    'data_id'    => $mappingHealthData->id,
                    'table_name' => 'pm_health_measurement'
                ]);*/
            }else{
                $postData['created_by'] = $mappingIdData->created_by;
                $mappingIdData->delete();
                $postData = $request->all();
                $postData['updated_by'] = (int)user_id()??null;
                $mappingHealthData = new MappingHealthMeasurment();
                $mappingHealthData->create($postData);

                return response([
                'success' => true,
                'message' => 'Data updated successfully',
                'data'    => $mappingIdData
                ]);

            /*save_log([
                'data_id'    => $mappingIdData->id,
                'table_name' => 'pm_health_measurement'
            ]);*/
            }

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

    }
   /*+++++status update++++++*/
    public function toggleStatus($id)
    {
        $mappingIdData = MappingSocioEconomicInfo::whereId($id)->first();

        if (!$mappingIdData) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $mappingIdData->status = $mappingIdData->status == 1 ? 2 : 1;
        $mappingIdData->update();

        // save_log([
        //     'data_id'       => $mappingIdData->id,
        //     'table_name'    => 'pm_socio_economic_info',
        //     'execution_type'=> 2
        // ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $mappingIdData
        ]);
    }

    //mapping parameter report Data
    public function mappingParameterReport(Request $request)
    {
        $query = MappingSocioEconomicInfo::with('physical_info','health_info');
        if ($request->area_type_id) {
            $query = $query->where('area_type_id',$request->area_type_id);
        }
        if ($request->gender) {
            $query = $query->where('gender',$request->gender);
        }
        if ($request->division_id) {
            $query = $query->where('division_id',$request->division_id);
        }
        if ($request->district_id) {
            $query = $query->where('district_id',$request->district_id);
        }
        if ($request->division_id && $request->district_id) {
            $query = $query->where('division_id',$request->division_id)->where('district_id',$request->district_id);
        }
        if ($request->division_id && $request->district_id && $request->gender) {
            $query = $query->where('division_id',$request->division_id)->where('district_id',$request->district_id)->where('gender',$request->gender);
        }
        $records = $query->get()->groupBy(['division_id','district_id']);
        if ($records->count() > 0) {
            foreach($records as $key => $record)
            {
                $list[][$key] = $record;
            }
        }else{
            $list = [];
        }
        return response([
            'success' => true,
            'data' => $list,
            'message' => 'Mapping Parameter Report data'
        ]);
    }
}
