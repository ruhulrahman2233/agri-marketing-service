<?php

namespace App\Http\Controllers\Migration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MigrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateDamDb ($data)
    {
        $isThere = DB::connection('mysql2')
            ->table('masters')
            ->where('item_id',$data->item_id)
            ->update([
                'status'=>1
            ]);
        return true;
    }
    public function deleteDamDb ($data)
    {
        DB::connection('mysql2')
            ->table('masters')
            ->where('item_id',$data->item_id)
            ->delete();
        return true;
    }
    public function createEditDelete ($migData, $data, $modelName)
    {
        if($data->action_type == 'delete') {
            $masterCommodityGroups = $modelName::whereDamId($data->item_id);
            if(!empty($masterCommodityGroups)) {
                if($masterCommodityGroups->delete()) {
                    $this->deleteDamDb($data);
                } else {
                    $this->updateDamDb($data);
                }
            }
            return true;
        }
        if($data->action_type == 'insert') {
            if($modelName::create($migData)) {
                $this->deleteDamDb($data);
            } else {
                $this->updateDamDb($data);
            }
        }
        if($data->action_type == 'update') {
            $masterCommodityGroups = $modelName::whereDamId($data->item_id)->first();
            if(!empty($masterCommodityGroups)) {
                if($masterCommodityGroups->update($migData)) {
                    $this->deleteDamDb($data);
                } else {
                    $this->updateDamDb($data);
                }
            } else {
                if($modelName::create($migData)) {
                    $this->deleteDamDb($data);
                } else {
                    $this->updateDamDb($data);
                }
            }
        } 
        return true;
    }
    public function index()
    {
        $tables = $this->tableList();
        
        //Get Data From Dam server masters table
        $datas = DB::connection('mysql2')->table("masters")
            ->where('status', 0)
            ->orderBy('order_type','asc')
            ->orderBy('created_at', 'ASC')
            ->limit(1)
            ->get();
        if(!empty($datas)) {
            foreach($datas as $key=>$value) {

                //Get Data From Dam server existing table data
                $saveData = DB::connection('mysql2')->table($value->table_name)->where('id',$value->item_id)->first();
                $ourTable = $tables[$value->table_name];
                $dataFunction = $ourTable['function'];
                $migData = [];
                if($value->action_type != 'delete') {
                    $migData = $this->$dataFunction($saveData, $value->table_name);
                }
                
                //Add/Edit/Delete to our server
                $check = $this->createEditDelete($migData, $value, $ourTable['model']);
            }
        }

        $isThere = DB::connection('mysql2')->table("masters")
            ->where('status', 0)
            ->count();
        if($isThere != 0) {
            $this->index();
        }

        return response([
            'success' => true,
            'message' => 'Migration Done',
        ]);
    }
    public function tableList() {
        $tables = array(
            'lease_value_years'	=> array(
                'model'=>"App\Models\CropPriceInfo\Config\MasterleaseValueYear",
                'function'=>"sameData"
            ),
            'commodity_groups'	=> array(
                'model'=>"App\Models\CropPriceInfo\Config\MasterCommodityGroups",
                'function'=>"sameData"
            ),
            'commodity_sub_groups'	=> array(
                'model'=>"App\Models\CropPriceInfo\Config\MasterCommoditySubGroup",
                'function'=>"sameData"
            ),
            'measurement_units'	=> array(
                'model'=>"App\Models\CropPriceInfo\Config\MasterMeasurementUnit",
                'function'=>"sameData"
            ),
            'price_types'	=> array(
                'model'=>"App\Models\CropPriceInfo\Config\MasterPriceType",
                'function'=>"sameData"
            ),
            'weeks'	=> array(
                'model'=>"App\Models\CropPriceInfo\Config\MasterWeek",
                'function'=>"sameData"
            ),
            'commodities'	=> array(
                'model'=>"App\Models\CropPriceInfo\Config\MasterCommodityName",
                'function'=>"commodities"
            ),
            'destination_of_produces'	=> array(
                'model'=>"App\Models\CropPriceInfo\Config\MasterDestinationOfProduce",
                'function'=>"basicData"
            ),
            'infrastructures'	=> array(
                'model'=>"App\Models\CropPriceInfo\Config\MasterInfrastructure",
                'function'=>"basicData"
            ),
            'vehicles'	=> array(
                'model'=>"App\Models\CropPriceInfo\Config\MasterVehicle",
                'function'=>"basicData"
            ),
            'communication_linkages'	=> array(
                'model'=>"App\Models\CropPriceInfo\Config\MasterCommunicationLinkage",
                'function'=>"basicData"
            ),
            'divisions'	=> array(
                'model'=>"App\Models\CropPriceInfo\Config\DamDivision",
                'function'=>"sameData"
            ),
            'districts'	=> array(
                'model'=>"App\Models\CropPriceInfo\Config\DamDistrict",
                'function'=>"sameData"
            ),
            'thanas'	=> array(
                'model'=>"App\Models\CropPriceInfo\Config\DamUpazila",
                'function'=>"sameData"
            ),
            'market_directories' => array(
                'model'=>"App\Models\CropPriceInfo\Config\MasterMarkets",
                'function'=>"sameData"
            ),
            'market_directory_lease_value_years' => array(
                'model'=>"App\Models\CropPriceInfo\Config\MasterMarketDirectoryLeaseValueYear",
                'function'=>"sameData"
            ),
            'market_directory_commodities' => array(
                'model'=>"App\Models\CropPriceInfo\Config\MasterMarketDirectoryCommodities",
                'function'=>"sameData"
            ),
            'market_commodity_prices' => array(
                'model'=>"App\Models\CropPriceInfo\Cpi\MarketCommodityPrice",
                'function'=>"sameData"
            ),
            'market_commodity_price_details' => array(
                'model'=>"App\Models\CropPriceInfo\Cpi\MarketCommodityPriceDetail",
                'function'=>"sameData"
            ),
            'market_commodity_growers_prices' => array(
                'model'=>"App\Models\CropPriceInfo\Cpi\MarketCommodityPrice",
                'function'=>"sameData"
            ),
            'market_commodity_growers_price_details' => array(
                'model'=>"App\Models\CropPriceInfo\Cpi\MarketCommodityPriceDetail",
                'function'=>"sameData"
            ),
            'market_commodity_weekly_prices' => array(
                'model'=>"App\Models\CropPriceInfo\Cpi\MarketCommodityWeeklyPrice",
                'function'=>"sameData"
            ),
            'market_commodity_weekly_price_details' => array(
                'model'=>"App\Models\CropPriceInfo\Cpi\MarketCommodityWeeklyPriceDetail",
                'function'=>"sameData"
            ),
            'market_commodity_weekly_price_details' => array(
                'model'=>"App\Models\CropPriceInfo\Cpi\MarketCommodityWeeklyPriceDetail",
                'function'=>"sameData"
            )
        );
        return $tables;
    }
    public function sameData ($saveData, $table_name)
    {
        $saveData->dam_id = $saveData->id;
        if(isset($saveData->com_grp_id)) {
            $saveData->com_grp_id = $this->dbCheck('master_commodity_groups',$saveData->com_grp_id);
        }
        if(isset($saveData->com_subgrp_id)) {
            $saveData->com_subgrp_id = $this->dbCheck('master_commodity_sub_groups',$saveData->com_subgrp_id);
        }
        if(isset($saveData->commodity_id)) {
            $saveData->commodity_id = $this->dbCheck('master_commodity_names',$saveData->commodity_id);
        }
        
        /*commodity group */
        if(isset($saveData->commodity_group_name)) {
            $saveData->group_name = $saveData->commodity_group_name;
            $saveData->org_id = 3;
        }
        if(isset($saveData->commodity_group_name_bangla)) {
            $saveData->group_name_bn = $saveData->commodity_group_name_bangla;
        }
        /*end commodity group */

        /*commodity sub group */
        if(isset($saveData->commodity_subgroup_name)) {
            $saveData->org_id = 3;
            $saveData->sub_group_name = $saveData->commodity_subgroup_name;
        }
        if(isset($saveData->commodity_subgroup_name_bangla)) {
            $saveData->sub_group_name_bn = $saveData->commodity_subgroup_name_bangla;
        }
        if(isset($saveData->commodity_group_id)) {
            $saveData->commodity_group_id = $this->dbCheck('master_commodity_groups',$saveData->commodity_group_id);
        }
        /*end commodity sub group */

        /*master unit, priceTypes and lease Value*/
        if(isset($saveData->name)) {
            $saveData->unit_name = $saveData->name;

            $saveData->type_name = $saveData->name;
            $saveData->type_name_bn = $saveData->name;

            $saveData->org_id = 3;
        }
        /*lease Value*/
        if(isset($saveData->name_english)) {
            $saveData->name_bn = $saveData->name ?? '';
            $saveData->name = $saveData->name_english;
        }
        if(isset($saveData->name_bangla)) {
            $saveData->unit_name_bn = $saveData->name_bangla;
        }
        /*end lease Value*/
        /*end master unit */

        /*Week Table */
        if(isset($saveData->year)) {
            $saveData->unit_name_bn = $saveData->year;
        }
        if(isset($saveData->week)) {
            $saveData->week = $saveData->week;
        }
        if(isset($saveData->month)) {
            $saveData->month = $saveData->month;
        }
        if(isset($saveData->starting_date)) {
            $saveData->starting_date = $saveData->starting_date;
        }
        if(isset($saveData->ending_date)) {
            $saveData->ending_date = $saveData->ending_date;
        }
        /*End Week Table */

        /*master market*/
        if(isset($saveData->thana_id)) {
            $saveData->upazila_id = $saveData->thana_id;
        }
        if(empty($saveData->status)) {
            $saveData->status = 1;
        }
        if(isset($saveData->market_name_bangla)) {
            $saveData->market_name_bn = $saveData->market_name_bangla;
        }
        /*end master market*/

        /*master master_market_directory_lease_value_years and market_directory_commodities*/
        if(isset($saveData->market_directory_id)) {
            $saveData->market_directory_id = $this->dbCheck('master_markets',$saveData->market_directory_id);
        }
        /*end master market*/
        //Add Prize Info
        if(isset($saveData->market_id)) {
            $saveData->market_id = $this->dbCheck('master_markets',$saveData->market_id);
        }
        //End Prize Info
        /*master market_commodity_prices and market_commodity_weekly_prices*/
        if(isset($saveData->price_table_id)) {
            if($table_name == 'market_commodity_weekly_price_details') {
                $saveData->price_table_id = $this->dbCheck('cpi_market_commodity_weekly_prices',$saveData->price_table_id);
            } else {
                $saveData->price_table_id = $this->dbCheck('cpi_market_commodity_prices',$saveData->price_table_id);
            }
        }
        /*end this*/
        /*master market_commodity_growers_price_details*/
        if(isset($saveData->growers_table_id)) {
            $saveData->price_table_id = $this->dbCheck('cpi_market_commodity_prices',$saveData->growers_table_id);
            $saveData->price_type_id = $saveData->price_type;
        }
        /*end this*/
        /*master market_commodity_weekly_prices*/
        if(isset($saveData->week_table_id)) {
            $saveData->week_table_id = $this->dbCheck('cpi_market_commodity_prices',$saveData->week_table_id);
            $saveData->week_id = $this->weekOfMonth(strtotime($saveData->price_date));
            $saveData->month_id = date("m", strtotime($saveData->price_date));
            $saveData->year = date("Y", strtotime($saveData->price_date));
        }
        if(isset($saveData->price_type_id)) {
            $saveData->price_type_id = json_decode($saveData->price_type_id);
        }
        /*end this*/
        return (array) $saveData;
    }
    public function weekOfMonth($date) {
        //Get the first day of the month.
        $firstOfMonth = strtotime(date("Y-m-01", $date));
        //Apply above formula.
        return $this->weekOfYear($date) - $this->weekOfYear($firstOfMonth) + 1;
    }
    
    public function weekOfYear($date) {
        $weekOfYear = intval(date("W", $date));
        if (date('n', $date) == "1" && $weekOfYear > 51) {
            // It's the last week of the previos year.
            $weekOfYear = 0;    
        }
        return $weekOfYear;
    }
    public function dbCheck ($table, $dam_id, $condField = 'dam_id' ) {
        $isThere = DB::table($table)
            ->select(['id'])
            ->where($condField, $dam_id)
            ->first();
        return $isThere->id ?? '';
    }
    public function commodities ($saveData)
    {
        return $migData = [
            'commodity_group_id' => $this->dbCheck('master_commodity_groups',$saveData->commodity_group_id),
            'commodity_sub_group_id' => $this->dbCheck('master_commodity_sub_groups',$saveData->commodity_subgroup_id),
            'commodity_name' => $saveData->commodity_name ?? '',
            'commodity_name_bn' => $saveData->commodity_name_bangla ?? '',
            'unit_grower' => $this->dbCheck('master_measurement_units',$saveData->MeasurementUnitGrowers,'unit_name'),
            'unit_whole_sale' => $this->dbCheck('master_measurement_units',$saveData->MeasurementUnitWholesale,'unit_name'),
            'unit_retail' => $this->dbCheck('master_measurement_units',$saveData->MeasurementUnitRetail,'unit_name'),
            'dam_id' => $saveData->id ?? '',
            'org_id' => 3
        ];
    }
    public function basicData ($saveData) {
        return $migData = [
            'id' => $saveData->id ?? '',
            'name' => $saveData->name ?? '',
            'name_bn' => $saveData->name ?? '',
            'dam_id' => $saveData->id ?? ''
        ];
    }
}