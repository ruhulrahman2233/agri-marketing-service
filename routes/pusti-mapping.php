<?php
use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/pusti-mapping', 'namespace' => 'PustiMapping'], function () {
    Route::get('mapping-parameter-pdf', 'MappingParameterController@mappingParameterPdf');
    Route::get('mapping-parameter-report', 'MappingFormController@mappingParameterReport');
    Route::group(['prefix'=>'/mapping-form'], function(){ 
        Route::get('list', 'MappingFormController@index');
        Route::delete('toggle-status/{id}', 'MappingFormController@toggleStatus');
        Route::delete('delete/{id}', 'MappingFormController@destroy');
        Route::post('social_info', 'MappingFormController@social_info');
        Route::post('physical_info', 'MappingFormController@physical_info');
        Route::post('health_info', 'MappingFormController@health_info');
        Route::get('get_data/{id}', 'MappingFormController@infoby_id');
        Route::delete('toggle-status/{id}', 'MappingFormController@toggleStatus');
        Route::get('get_data/{id}', 'MappingFormController@infoby_id');
    });
    Route::group(['prefix'=>'/mapping-parameter', 'namespace' => 'MappingParameter'], function(){ 
        Route::get('list', 'MasterParametersController@index');
        Route::post('store', 'MasterParametersController@store');
        Route::put('update/{id}', 'MasterParametersController@update');
        Route::delete('toggle-status/{id}', 'MasterParametersController@toggleStatus');
        Route::delete('delete/{id}', 'MasterParametersController@destroy');
        Route::get('get_data/{id}', 'MasterParametersController@infoby_id');
    });
});