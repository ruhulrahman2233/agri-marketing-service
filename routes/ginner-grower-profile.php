<?php
use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/ginner-grower-profile', 'namespace' => 'GinnerGrowerProfile'], function(){ 
    Route::get('/details/{mobile_no}', 'ProfileController@details');
    Route::put('/update/{id}', 'ProfileController@update');
    Route::get('/production-achievement', 'ProductionAchievementController@index');
    Route::get('/hatt-notification', 'ProfileController@hattNotification');

    Route::group(['prefix'=>'/manage-schedule'], function(){ 
        Route::get('/list', 'ScheduleController@index');
        Route::post('/store', 'ScheduleController@store');
        Route::put('/update/{id}', 'ScheduleController@update');
    });
});