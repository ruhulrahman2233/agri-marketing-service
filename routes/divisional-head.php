<?php
use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/e-pusti', 'namespace' => 'ePusti'], function(){ 
    Route::group(['prefix'=>'/divisional-head', 'namespace' => 'DivisionalHead'], function(){ 
        Route::group(['prefix'=>'/campaign-request'], function(){ 
            Route::get('/list', 'CampaignRequestController@index');
        });
        //campaign material
        Route::group(['prefix'=>'/campaign-material'], function(){ 
            Route::get('/list', 'CampaignMaterialController@index');
            Route::post('/store', 'CampaignMaterialController@store');
            Route::put('/update/{id}', 'CampaignMaterialController@update');
        });
        //campaign material details
        Route::group(['prefix'=>'/campaign-material-details'], function(){ 
            Route::get('/list/{campaign_material_id}', 'CampaignMaterialController@detailsList');
            Route::post('/store', 'CampaignMaterialController@detailsStore');
            Route::delete('/destroy/{id}', 'CampaignMaterialController@detailsDestroy');
        });
        //campaign information
        Route::group(['prefix'=>'/campaign-information'], function(){ 
            Route::get('/list', 'CampaignInformationController@index');
            Route::post('/store', 'CampaignInformationController@store');
            Route::put('/update/{id}', 'CampaignInformationController@update');
            Route::delete('/destroy/{id}', 'CampaignInformationController@destroy');
        });
        //campaign attendance
        Route::group(['prefix'=>'/campaign-attendance'], function(){ 
            Route::get('/list', 'CampaignAttendanceController@index');
            Route::post('/store', 'CampaignAttendanceController@store');
            Route::put('/update/{id}', 'CampaignAttendanceController@update');
            Route::delete('/destroy/{id}', 'CampaignAttendanceController@destroy');
        });
        //participant feedback
        Route::group(['prefix'=>'/campaign-feedback'], function(){ 
            Route::get('/list', 'CampaignFeedbackController@index');
            Route::post('/store', 'CampaignFeedbackController@store');
            Route::put('/update/{id}', 'CampaignFeedbackController@update');
            Route::delete('/destroy/{id}', 'CampaignFeedbackController@destroy');
        });

        //Campaign dashboard routes
        Route::group(['prefix'=>'/campaign-dashboard'], function(){
            Route::get('/list', 'DashboardController@index');            
        });
        
    });
});