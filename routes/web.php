<?php

/** @var \Laravel\Lumen\Routing\Router $router */
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


/******************** Data Archive Module *********************/
Route::group(['prefix'=>'/data-archive'], function() {
    Route::get('/database-backup', 'DataArchiveController@dumpDB');
    //download file path from storage
    Route::get('download-backup-db', 'DataArchiveController@downloadBackupDb');
    Route::get('db-backup-files', 'DataArchiveController@getDbBackupFiles');
    Route::delete('db-backup-delete', 'DataArchiveController@deleteDbBackupFile');
});

//download file path from storage
Route::get('download-attachment', 'DownloadController@downloadAttachment');

Route::group(['prefix'=>'/migration', 'namespace' => 'Migration'], function () {
    Route::get('/', 'MigrationController@index');
});
Route::get('common-dropdowns', function () {
    return response()->json([
        'success' => true,
        'data' => [
            'regionsList' 			=> \App\Library\CommonDropdown::regionsList(),
            'zonesList'   			=> \App\Library\CommonDropdown::zonesList(),
            'cottonVaritiesList'   	=> \App\Library\CommonDropdown::cottonVaritiesList(),
            'cottonNamesList'   	=> \App\Library\CommonDropdown::cottonNamesList(),
            'hatsList'   			=> \App\Library\CommonDropdown::hatList(),
            'unitList'   			=> \App\Library\CommonDropdown::unitList(),
            'commodityGroupList'    => \App\Library\CommonDropdown::CommodityGroupList(),
            'commoditySubGroupList' => \App\Library\CommonDropdown::CommoditySubGroupList(),
            'seasonsList'   		=> \App\Library\CommonDropdown::seasonsList(),
            'campaignNameList'   	=> \App\Library\CommonDropdown::campaignNameList(),
            'commodityGroupList'    => \App\Library\CommonDropdown::commodityGroupList(),
            'commoditySubGroupList' => \App\Library\CommonDropdown::commoditySubGroupList(),
            'seasonsList'   		=> \App\Library\CommonDropdown::seasonsList(),
            'divisionalOfficeList' 	=> \App\Library\CommonDropdown::divisionalOfficeList(),
            'commodityTypeList' 	=> \App\Library\CommonDropdown::commodityTypeList(),
            'commodityNameList' 	=> \App\Library\CommonDropdown::commodityNameList(),
            'marketList' 	        => \App\Library\CommonDropdown::marketList(),
            'alertPercentageList' 	=> \App\Library\CommonDropdown::alertPercentageList(),
            'ginnerGrowerList' 	    => \App\Library\CommonDropdown::ginnerGrowerList(),
            'leaseYearList' 	    => \App\Library\CommonDropdown::leaseYearList(),
            'measurementUnitList'   => \App\Library\CommonDropdown::measurementUnitList(),
            'infrastructureList' 	=> \App\Library\CommonDropdown::infrastructureList(),
            'communicationLinkageList' 	=> \App\Library\CommonDropdown::communicationLinkageList(),
            'designationOfProductList' 	=> \App\Library\CommonDropdown::designationOfProductList(),
            'vehicleList' 	            => \App\Library\CommonDropdown::vehicleList()
        ]
    ]);
});
Route::get('common-data-dam', function () {
    return response()->json([
        'success' => true,
        'data' => [
        'divisionList' => \App\Library\DamCommon::divisionList(),
        'districtList' => \App\Library\DamCommon::districtList(),
        'upazilaList' => \App\Library\DamCommon::upazilaList(),
        ]
    ]);
});

include('cotton.php');
include('crop-price-info.php');
include('ginner-grower-profile.php');
include('e-pusti.php');
include('divisional-head.php');
include('pusti-mapping.php');
include('market-linkage.php');
include('price-collector-profile.php');




