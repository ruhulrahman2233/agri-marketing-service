<?php
use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/market-linkage', 'namespace' => 'MarketLinkage'], function(){ 
  	//MarketLinkage config routes
    Route::group(['prefix'=>'/config', 'namespace' => 'Config'], function(){	  	  	
  		//Commodity Type crud routes
  		Route::group(['prefix'=>'/commodity-type'], function(){
		    Route::get('/list', 'CommodityTypeController@index');
		    Route::get('/details/{id}', 'CommodityTypeController@show');
		    Route::post('/store', 'CommodityTypeController@store');
		    Route::put('/update/{id}', 'CommodityTypeController@update');
		    Route::delete('/toggle-status/{id}', 'CommodityTypeController@toggleStatus');
		    Route::delete('/destroy/{id}', 'CommodityTypeController@destroy');
		});	
	});

    Route::group(['prefix'=>'/linkage', 'namespace' => 'Linkage'], function(){	
  		//grower buyer profile crud routes
  		Route::group(['prefix'=>'/grower-buyer-profile'], function(){
		    Route::get('/list', 'GrowerBuyerProfileController@index');
		    Route::post('/store', 'GrowerBuyerProfileController@store');
		    Route::put('/update/{id}', 'GrowerBuyerProfileController@update');
		    Route::put('/approve/{id}', 'GrowerBuyerProfileController@approve');
		    Route::put('/reject/{id}', 'GrowerBuyerProfileController@reject');
		    Route::delete('/destroy/{id}', 'GrowerBuyerProfileController@destroy');
		});	

		//product-infocrud routes
		Route::group(['prefix'=>'/product-info'], function(){
		    Route::get('/details/{id}', 'ProductInformationController@show');
		    Route::post('/store', 'ProductInformationController@store');
		    Route::delete('/destroy/{id}', 'ProductInformationController@destroy');
		});
	});

	//All reports routes
	Route::group(['prefix'=>'/reports', 'namespace' => 'Reports'], function(){	
		Route::get('/farmers-info', 'FarmersInfoReportController@index');			
		Route::get('/buyers-info', 'BuyersInfoReportController@index');			
		Route::get('/product-info', 'ProductInformationReportController@index');		
		Route::get('/market-info', 'MarketInformationReportController@index');			
	});

	//All reports routes
	Route::group(['prefix'=>'/dashboard', 'namespace' => 'Dashboard'], function(){	
		Route::get('/list', 'MarketLinkageDashboardController@index');			
	});


});