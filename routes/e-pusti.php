<?php
use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/e-pusti', 'namespace' => 'EPusti'], function(){ 

    Route::group(['prefix'=>'/config', 'namespace' => 'Config'], function(){ 

        Route::group(['prefix'=>'/divisional-office'], function(){ 
            Route::get('/list', 'DivisionalOfficeController@index');
            Route::post('/store', 'DivisionalOfficeController@store');
            Route::put('/update/{id}', 'DivisionalOfficeController@update');
            Route::delete('/toggle-status/{id}', 'DivisionalOfficeController@toggleStatus');
            Route::delete('/destroy/{id}', 'DivisionalOfficeController@destroy');
        });

        //Master Campaign
        Route::group(['prefix'=>'/master-campaign'], function(){ 
            Route::get('/list', 'MasterCampaignController@index');
            Route::post('/store', 'MasterCampaignController@store');
            Route::put('/update/{id}', 'MasterCampaignController@update');
            Route::delete('/toggle-status/{id}', 'MasterCampaignController@toggleStatus');
            Route::delete('/destroy/{id}', 'MasterCampaignController@destroy');
        });

    });

    Route::group(['prefix'=>'/birtan', 'namespace' => 'Birtan'], function(){ 

        Route::group(['prefix'=>'/campaign-schedule'], function(){ 
            Route::get('/list', 'CampaignScheduleController@index');
            Route::post('/store', 'CampaignScheduleController@store');
            Route::put('/update/{id}', 'CampaignScheduleController@update');
            Route::delete('/toggle-status/{id}', 'CampaignScheduleController@toggleStatus');
            Route::delete('/destroy/{id}', 'CampaignScheduleController@destroy');
        });

    });

    //Campaign events crud routes
    Route::group(['prefix'=>'/campaign-events', 'namespace' => 'CampaignEvents'], function(){
        Route::get('/list', 'PustiCampaignEventsController@index');
        Route::get('/details/{id}', 'PustiCampaignEventsController@show');
        Route::post('/store', 'PustiCampaignEventsController@store');
        Route::post('/update/{id}', 'PustiCampaignEventsController@update');
        // Route::put('/update/{id}', 'PustiCampaignEventsController@update');
        Route::delete('/toggle-status/{id}', 'PustiCampaignEventsController@toggleStatus');
        Route::delete('/destroy/{id}', 'PustiCampaignEventsController@destroy');
    });

    //Campaign requests crud routes
    Route::group(['prefix'=>'/campaign-requests', 'namespace' => 'CampaignEvents'], function(){
        Route::get('/list', 'PustiCampaignRequestsController@index');
        Route::get('/details/{id}', 'PustiCampaignRequestsController@show');
        Route::post('/store', 'PustiCampaignRequestsController@store');
        Route::put('/update/{id}', 'PustiCampaignRequestsController@update');
        Route::delete('/toggle-status/{id}', 'PustiCampaignRequestsController@toggleStatus');
        Route::delete('/destroy/{id}', 'PustiCampaignRequestsController@destroy');
    });

    //Campaign calendars crud routes
    Route::group(['prefix'=>'/campaign-calendars', 'namespace' => 'CampaignEvents'], function(){
        Route::get('/list', 'PustiCampaignCalendarsController@index');
        Route::get('/details/{id}', 'PustiCampaignCalendarsController@show');
        Route::post('/store', 'PustiCampaignCalendarsController@store');
        Route::put('/update/{id}', 'PustiCampaignCalendarsController@update');
        Route::delete('/destroy/{id}', 'PustiCampaignCalendarsController@destroy');
    });

    //Campaign attendances crud routes
    Route::group(['prefix'=>'/campaign-attendances', 'namespace' => 'CampaignEvents'], function(){
        Route::get('/list', 'PustiCampaignAttendancesController@index');
        Route::get('/details/{id}', 'PustiCampaignAttendancesController@show');
        Route::post('/store', 'PustiCampaignAttendancesController@store');
        Route::put('/update/{id}', 'PustiCampaignAttendancesController@update');
        Route::delete('/destroy/{id}', 'PustiCampaignAttendancesController@destroy');
    });

    //Campaign feedbacks crud routes
    Route::group(['prefix'=>'/campaign-feedbacks', 'namespace' => 'CampaignEvents'], function(){
        Route::get('/list', 'PustiCampaignFeedbacksController@index');
        Route::get('/details/{id}', 'PustiCampaignFeedbacksController@show');
        Route::post('/store', 'PustiCampaignFeedbacksController@store');
        Route::put('/update/{id}', 'PustiCampaignFeedbacksController@update');
        Route::delete('/destroy/{id}', 'PustiCampaignFeedbacksController@destroy');
    });

         
    Route::group(['prefix'=>'/admin-campaign-material', 'namespace' => 'CampaignEvents'], function(){ 
        Route::get('/list', 'PustiCampaignMaterialController@index');
        Route::post('/store', 'PustiCampaignMaterialController@store');
        Route::put('/update/{id}', 'PustiCampaignMaterialController@update');
    });  
    
    Route::group(['prefix'=>'/admin-campaign-material-details', 'namespace' => 'CampaignEvents'], function(){
        Route::get('/list/{campaign_material_id}', 'PustiCampaignMaterialController@detailsList');
        Route::post('/store', 'PustiCampaignMaterialController@detailsStore');
        Route::delete('/destroy/{id}', 'PustiCampaignMaterialController@detailsDestroy');
    });

    //Campaign dashboard routes
    Route::group(['prefix'=>'/campaign-dashboard', 'namespace' => 'CampaignEvents'], function(){
        Route::get('/', 'DashboardController@index');
    });   

});