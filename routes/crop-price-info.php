<?php
use Illuminate\Support\Facades\Route;

$router->get('/test-crop-price-info', function () use ($router) {
    return "CropPriceInfo routes working porperly...";
});

Route::group(['prefix' => '/crop-price-info', 'namespace' => 'CropPriceInfo'], function () {
    // All Config routes...
    Route::group(['prefix' => '/config', 'namespace' => 'Config'], function () {
        //commodity group crud routes
        Route::group(['prefix'=>'/commodity-group'], function(){
            Route::get('/list', 'MasterCommodityGroupController@index');
            Route::get('/show/{id}', 'MasterCommodityGroupController@show');
            Route::post('/store', 'MasterCommodityGroupController@store');
            Route::put('/update/{id}', 'MasterCommodityGroupController@update');
            Route::delete('/toggle-status/{id}', 'MasterCommodityGroupController@toggleStatus');
            Route::delete('/destroy/{id}', 'MasterCommodityGroupController@destroy');
        });
    	//Commodity Sub Group Crud here
        Route::group(['prefix'=>'/commodity-sub-group'], function(){
            Route::get('/list', 'CommoditySubGroupController@index');
            Route::post('/store', 'CommoditySubGroupController@store');
            Route::put('/update/{id}', 'CommoditySubGroupController@update');
            Route::delete('/toggle-status/{id}', 'CommoditySubGroupController@toggleStatus');
            Route::delete('/destroy/{id}', 'CommoditySubGroupController@destroy');
        });
        //commodity name crud routes
        Route::group(['prefix'=>'/commodity-name'], function(){
            Route::get('/list', 'MasterCommodityNameController@index');
            Route::get('/show/{id}', 'MasterCommodityNameController@show');
            Route::post('/store', 'MasterCommodityNameController@store');
            Route::put('/update/{id}', 'MasterCommodityNameController@update');
            Route::delete('/toggle-status/{id}', 'MasterCommodityNameController@toggleStatus');
            Route::delete('/destroy/{id}', 'MasterCommodityNameController@destroy');
        });
        //Price Type crud routes
        Route::group(['prefix'=>'/price-type'], function(){
            Route::get('/list', 'MasterPriceTypeController@index');
            Route::get('/show/{id}', 'MasterPriceTypeController@show');
            Route::post('/store', 'MasterPriceTypeController@store');
            Route::put('/update/{id}', 'MasterPriceTypeController@update');
            Route::delete('/toggle-status/{id}', 'MasterPriceTypeController@toggleStatus');
            Route::delete('/destroy/{id}', 'MasterPriceTypeController@destroy');
        });

        //master-measurement-units crud routes
        Route::group(['prefix'=>'/master-measurement-units'], function(){
            Route::get('/list', 'MasterMeasurementUnitController@index');
            Route::get('/show/{id}', 'MasterMeasurementUnitController@show');
            Route::post('/store', 'MasterMeasurementUnitController@store');
            Route::put('/update/{id}', 'MasterMeasurementUnitController@update');
            Route::delete('/toggle-status/{id}', 'MasterMeasurementUnitController@toggleStatus');
            Route::delete('/destroy/{id}', 'MasterMeasurementUnitController@destroy');
        });

        //Master markets crud routes
        Route::group(['prefix'=>'/master-markets'], function(){
            Route::get('/list', 'MasterMarketsController@index');
            Route::get('/details/{id}', 'MasterMarketsController@show');
            Route::post('/store', 'MasterMarketsController@store');
            Route::put('/update/{id}', 'MasterMarketsController@update');
            Route::delete('/toggle-status/{id}', 'MasterMarketsController@toggleStatus');
            Route::delete('/destroy/{id}', 'MasterMarketsController@destroy');
        });

        //Master alert percentages crud routes
        Route::group(['prefix'=>'/master-alert-percentages'], function(){
            Route::get('/list', 'MasterAlertPercentagesController@index');
            Route::get('/details/{id}', 'MasterAlertPercentagesController@show');
            Route::post('/store', 'MasterAlertPercentagesController@store');
            Route::put('/update/{id}', 'MasterAlertPercentagesController@update');
            Route::delete('/toggle-status/{id}', 'MasterAlertPercentagesController@toggleStatus');
            Route::delete('/destroy/{id}', 'MasterAlertPercentagesController@destroy');
        });

        //Cpi price infos crud routes
        Route::group(['prefix'=>'/cpi-price-infos'], function(){
            Route::get('/list', 'CpiPriceInfosController@index');
            Route::get('/details/{id}', 'CpiPriceInfosController@show');
            Route::post('/store', 'CpiPriceInfosController@store');
            Route::put('/update/{id}', 'CpiPriceInfosController@update');
            Route::delete('/toggle-status/{id}', 'CpiPriceInfosController@toggleStatus');
            Route::delete('/destroy/{id}', 'CpiPriceInfosController@destroy');
            Route::get('/price-collection-verify-list', 'CpiPriceInfosController@priceCollectionVerifyList');
            Route::get('/get-price-info-by-price-date', 'CpiPriceInfosController@getPriceInfoByPriceDate');
            Route::delete('/price-collection-verify/{id}', 'CpiPriceInfosController@priceCollectionVerify');
            Route::put('/price-update/{id}', 'CpiPriceInfosController@priceUpdate');
            Route::get('/price-view', 'CpiPriceInfosController@priceView');
        });
    });
    Route::group(['prefix' => '/cpi', 'namespace' => 'Cpi'], function () {
        //Price entry crud routes
        Route::group(['prefix'=>'/price-entry'], function(){
            Route::get('/get-percentage', 'MasterPriceEntryController@getPercentage');
            Route::get('/get-commodity-list[/{id}]', 'MasterPriceEntryController@getCommodityList');
            Route::get('/market-price-list', 'MasterPriceEntryController@getMarketPriceList');
            Route::get('/market-grower-price-list', 'MasterPriceEntryController@getGrowerPriceList');
            Route::get('/market-weekly-price-list', 'MasterPriceEntryController@getWeeklyPriceList');
            Route::post('/store', 'MasterPriceEntryController@store');
            Route::put('/update/{id}', 'MasterPriceEntryController@update');
            Route::post('/approve', 'MasterPriceEntryController@approve');
        });
    });

    Route::group(['prefix' => '/reports', 'namespace' => 'Reports'], function () {
        Route::group(['prefix' => '/price-report', 'namespace' => 'PriceReport'], function () {
            Route::get('/market-daily-price-report', 'MarketDailyPriceReportController');
            Route::get('/market-weekly-price-report', 'MarketWeeklyPriceReportController');
            Route::get('/product-wise-price-report', 'ProductWisePriceReportController');
            Route::get('/weekly-market-wise-price-report', 'WeeklyMarketWisePriceReportController');
            Route::get('/marketing-directory-report', 'MarketDirectoryReportController');
            Route::get('/yearly-average-price-report', 'YearlyAveragePriceReportController');
            Route::get('/division-wise-average-price-report', 'DivisionWiseAvgPriceController');
            Route::get('/market-monthly-price-report', 'MarketMonthlyPriceReportController');
            Route::get('/grower-avg-price-report', 'GrowerAvgPriceReportController');
            Route::get('/weekly-average-price-report', 'WeeklyAveragePriceReportController');
            Route::get('/comparative-commodity-report', 'ComparativeCommodityReportController');
        });
    });
});
