<?php
use Illuminate\Support\Facades\Route;
/** @var \Laravel\Lumen\Routing\Router $router */

$router->get('/test-cotton', function () use ($router) {
    return "Cotton routes working porperly...";
});
//Route::group(['middleware'  =>  'token'], function () {

	Route::group(['prefix' => '/cotton', 'namespace' => 'Cotton'], function () {
		
        // All Config routes...
     	Route::group(['prefix' => '/config', 'namespace' => 'Config'], function () {
        	//regions crud routes
			Route::group(['prefix'=>'/regions'], function(){
			    Route::get('/list', 'MasterRegionsController@index');
			    Route::get('/show/{id}', 'MasterRegionsController@show');
			    Route::post('/store', 'MasterRegionsController@store');
			    Route::put('/update/{id}', 'MasterRegionsController@update');
			    Route::delete('/toggle-status/{id}', 'MasterRegionsController@toggleStatus');
			    Route::delete('/destroy/{id}', 'MasterRegionsController@destroy');
			});

			//seasons crud routes
			Route::group(['prefix'=>'/seasons'], function(){
			    Route::get('/list', 'MasterProductionSeasionsController@index');
			    Route::get('/show/{id}', 'MasterProductionSeasionsController@show');
			    Route::post('/store', 'MasterProductionSeasionsController@store');
			    Route::put('/update/{id}', 'MasterProductionSeasionsController@update');
			    Route::delete('/toggle-status/{id}', 'MasterProductionSeasionsController@toggleStatus');
			    Route::delete('/destroy/{id}', 'MasterProductionSeasionsController@destroy');
			});

			//zones crud routes
			Route::group(['prefix'=>'/zones'], function(){
			    Route::get('/list', 'MasterZonesController@index');
			    Route::get('/show/{id}', 'MasterZonesController@show');
			    Route::post('/store', 'MasterZonesController@store');
			    Route::put('/update/{id}', 'MasterZonesController@update');
			    Route::delete('/toggle-status/{id}', 'MasterZonesController@toggleStatus');
			    Route::delete('/destroy/{id}', 'MasterZonesController@destroy');
			});

			//cotton variety crud routes
			Route::group(['prefix'=>'/cotton-variety'], function(){
			    Route::get('/list', 'MasterCottonVaritiesController@index');
			    Route::get('/show/{id}', 'MasterCottonVaritiesController@show');
			    Route::post('/store', 'MasterCottonVaritiesController@store');
			    Route::put('/update/{id}', 'MasterCottonVaritiesController@update');
			    Route::delete('/toggle-status/{id}', 'MasterCottonVaritiesController@toggleStatus');
			    Route::delete('/destroy/{id}', 'MasterCottonVaritiesController@destroy');
			});

			//cotton names crud routes
			Route::group(['prefix'=>'/cotton-names'], function(){
			    Route::get('/list', 'MasterCottonNamesController@index');
			    Route::get('/show/{id}', 'MasterCottonNamesController@show');
			    Route::post('/store', 'MasterCottonNamesController@store');
			    Route::put('/update/{id}', 'MasterCottonNamesController@update');
			    Route::delete('/toggle-status/{id}', 'MasterCottonNamesController@toggleStatus');
			    Route::delete('/destroy/{id}', 'MasterCottonNamesController@destroy');
			});

			//units crud routes
			Route::group(['prefix'=>'/units'], function(){
			    Route::get('/list', 'MasterUnitsController@index');
			    Route::get('/show/{id}', 'MasterUnitsController@show');
			    Route::post('/store', 'MasterUnitsController@store');
			    Route::put('/update/{id}', 'MasterUnitsController@update');
			    Route::delete('/toggle-status/{id}', 'MasterUnitsController@toggleStatus');
			    Route::delete('/destroy/{id}', 'MasterUnitsController@destroy');
			});

			//seasions crud routes
			Route::group(['prefix'=>'/report-heading'], function(){
			    Route::get('/list', 'MasterReportHeadingController@index');
			    Route::get('/detail/{id}', 'MasterReportHeadingController@detail');
			    Route::post('/store', 'MasterReportHeadingController@store');
			    Route::put('/update/{id}', 'MasterReportHeadingController@update');
			    Route::delete('/toggle-status/{id}', 'MasterReportHeadingController@toggleStatus');
			    Route::delete('/destroy/{id}', 'MasterReportHeadingController@destroy');
			});

			//Hats crud routes
			Route::group(['prefix'=>'/hats'], function(){
			    Route::get('/list', 'MasterHattsController@index');
			    Route::get('/show/{id}', 'MasterHattsController@show');
			    Route::post('/store', 'MasterHattsController@store');
			    Route::put('/update/{id}', 'MasterHattsController@update');
			    Route::delete('/toggle-status/{id}', 'MasterHattsController@toggleStatus');
			    Route::delete('/destroy/{id}', 'MasterHattsController@destroy');
			});

			// Master Report heading Routes
			Route::group(['prefix'=>'/hats'], function(){
				Route::get('/list', 'MasterHattsController@index');
				Route::get('/show/{id}', 'MasterHattsController@show');
				Route::post('/store', 'MasterHattsController@store');
				Route::put('/update/{id}', 'MasterHattsController@update');
				Route::delete('/toggle-status/{id}', 'MasterHattsController@toggleStatus');
				Route::delete('/destroy/{id}', 'MasterHattsController@destroy');
			});

		});

		// All ginner grower routes...
		Route::group(['prefix' => '/ginner-grower', 'namespace' => 'GinnerGrower'], function () {

			//ginner schedules routes

			//manage schedules routes

			Route::group(['prefix'=>'/manage-schedule'], function(){
			    Route::get('/list', 'ScheduleController@index');
			    Route::get('/approved-list', 'ScheduleController@approvedList');
			    Route::post('/store', 'ScheduleController@store');
			    Route::put('/update/{id}', 'ScheduleController@update');
			    Route::delete('/approve/{id}', 'ScheduleController@approve');
			    Route::delete('/reject/{id}', 'ScheduleController@reject');
			});

			//hat manage  routes
			Route::group(['prefix' => '/hat-manages', 'namespace' => 'HattManages'], function () {
				Route::get('/list', 'GrowerHattManagesController@index');
			    Route::get('/show/{id}', 'GrowerHattManagesController@show');
			    Route::post('/store', 'GrowerHattManagesController@store');
			    Route::put('/update/{id}', 'GrowerHattManagesController@update');
			    Route::delete('/toggle-status/{id}', 'GrowerHattManagesController@toggleStatus');
			    Route::delete('/destroy/{id}', 'GrowerHattManagesController@destroy');
			});


            //ginner grower profiles routes
            Route::group(['prefix'=>'/profile'], function(){
                Route::get('/list', 'ProfileController@index');
                Route::get('/show/{id}', 'ProfileController@show');
                Route::post('/store', 'ProfileController@store');
                Route::put('/update/{id}', 'ProfileController@update');
                Route::delete('/toggle-status/{id}', 'ProfileController@toggleStatus');
                Route::delete('/destroy/{id}', 'ProfileController@destroy');
                Route::get('/grower-list', 'ProfileController@getGrowerList');
            });

            //grower-sell-entry  routes
			Route::group(['prefix' => '/grower-sell-entry', 'namespace' => 'GrowerSell'], function () {
				Route::get('/list', 'GrowerSellEntryController@index');
				Route::get('/show/{id}', 'GrowerSellEntryController@show');
				Route::post('/store', 'GrowerSellEntryController@store');
				Route::get('/applicantList','GrowerSellEntryController@getApplicationList');
			});

			//grower-production-achievements routes
			Route::group(['prefix' => '/grower-production-achievements', 'namespace' => 'GrowerProduction'], function () {
				Route::get('/list', 'GrowerProdAchievementsController@index');
				Route::get('/show/{id}', 'GrowerProdAchievementsController@show');
				Route::post('/store', 'GrowerProdAchievementsController@store');
                Route::put('/update/{id}', 'GrowerProdAchievementsController@update');
                Route::delete('/toggle-status/{id}', 'GrowerProdAchievementsController@toggleStatus');
                Route::delete('/close/{id}', 'GrowerProdAchievementsController@close');
			});

			//Dashboard routes
			Route::group(['prefix' => '/dashboard', 'namespace' => 'Dashboard'], function () {
				Route::get('/list', 'DashboardController@index');
			});

		});

		// All Reports routes...
		Route::group(['prefix' => '/reports', 'namespace' => 'Reports'], function () {
            //CottonProduction report routes
            Route::group(['prefix'=>'/cotton-production'], function(){
	            Route::get('/dashboard', 'CottonProductionReportController@dashboard');
                Route::get('/list', 'CottonProductionReportController@index');
            });
            //Grower Information report routes
            Route::group(['prefix'=>'/grower-information'], function(){
	            Route::get('/list', 'GrowerInformationReportController@index');
	        });

            //CottonProduction report routes
            Route::group(['prefix'=>'/fiscal-year-wise-cotton-sell'], function(){
                Route::get('/list', 'FiscalYearWiseCottonSellReportController@index');
            });

			//Ginner Information report routes
            Route::group(['prefix'=>'/ginner-information'], function(){
	            Route::get('/list', 'GinnerInformationReportController@index');
	        });

			//Cotton Stock report routes
            Route::group(['prefix'=>'/cotton-stock-report'], function(){
	            Route::get('/list', 'CottonStockReportController@index');
	        });
    	});

});

//});
