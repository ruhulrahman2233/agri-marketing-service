<?php
use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/price-collector', 'namespace' => 'PriceCollectorProfile'], function(){ 
  	//PriceCollectorProfile config routes
    Route::group(['prefix'=>'/config', 'namespace' => 'Config'], function(){	  	  	
  		//Commodity Type crud routes
  		Route::group(['prefix'=>'/profile'], function(){
		    Route::get('/list', 'CpiPriceCollectorProfilesController@index');
		    Route::get('/details/{id}', 'CpiPriceCollectorProfilesController@show');
		    Route::post('/store', 'CpiPriceCollectorProfilesController@store');
			Route::put('/update/{id}', 'CpiPriceCollectorProfilesController@update');
		    Route::put('/reject/{id}', 'CpiPriceCollectorProfilesController@isReject');
		    Route::put('/approve/{id}', 'CpiPriceCollectorProfilesController@isApproved');
		    Route::delete('/toggle-status/{id}', 'CpiPriceCollectorProfilesController@toggleStatus');
		    Route::delete('/destroy/{id}', 'CpiPriceCollectorProfilesController@destroy');
		});			
		/*++++++++*/

	
	});
});