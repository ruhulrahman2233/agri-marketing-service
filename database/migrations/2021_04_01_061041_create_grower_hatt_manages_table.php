<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrowerHattManagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grower_hatt_manages', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('fiscal_year_id');
            $table->integer('seasons_id');       
            $table->integer('division_id'); 
            $table->integer('district_id'); 
            $table->integer('upazilla_id'); 
            $table->integer('hatt_id'); 
            $table->integer('region_id'); 
            $table->integer('zone_id'); 
            $table->integer('unit_id'); 
            $table->date('hatt_date');              
            $table->string('remarks')->nullable();
            $table->string('remarks_bn')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1=not close, 2=closed');
            $table->string('address')->nullable();
            $table->string('address_bn')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();                      
            $table->foreign('seasons_id')->references('id')->on('master_seasons');
            $table->foreign('hatt_id')->references('id')->on('master_hatts');
            $table->foreign('region_id')->references('id')->on('master_regions');
            $table->foreign('zone_id')->references('id')->on('master_zones');
            $table->foreign('unit_id')->references('id')->on('master_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grower_hatt_manages');
    }
}
