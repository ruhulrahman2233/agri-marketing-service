<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHattNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hatt_notifications', function (Blueprint $table) {
            $table->id();      
            $table->integer('hatt_id'); 
            $table->integer('fiscal_year_id');
            $table->integer('seasons_id'); 
            $table->integer('ginner_grower_id'); 
            $table->tinyInteger('seen_status')->default(1)->comment('1=unseen, 2=seen');
            $table->text('message');
            $table->timestamps();
            $table->foreign('hatt_id')->references('id')->on('master_hatts');
            $table->foreign('seasons_id')->references('id')->on('master_seasons');
            $table->foreign('ginner_grower_id')->references('id')->on('ginner_grower_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hatt_notifications');
    }
}
