<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCpiMarketCommodityWeeklyPriceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpi_market_commodity_weekly_price_details', function (Blueprint $table) {
            $table->id();
            $table->integer('price_table_id')->nullable();
            $table->text('price_type_id')->nullable();
            $table->integer('com_grp_id')->nullable();
            $table->integer('com_subgrp_id')->nullable();
            $table->integer('commodity_id')->nullable();
            $table->decimal('w_lowestPrice',10,2)->nullable();
            $table->decimal('w_highestPrice',10,2)->nullable();
            $table->decimal('w_supply',10,2)->nullable();
            $table->decimal('r_lowestPrice',10,2)->nullable();
            $table->decimal('r_highestPrice',10,2)->nullable();
            $table->decimal('r_supply',10,2)->nullable();
            $table->decimal('w_avgPrice',10,2)->nullable();
            $table->decimal('r_avgPrice',10,2)->nullable();
            $table->decimal('w_percentage_LtoH',10,2)->nullable();
            $table->decimal('r_percentage_LtoH',10,2)->nullable();
            $table->decimal('w_last_entry',10,2)->nullable();
            $table->decimal('r_last_entry',10,2)->nullable();
            $table->decimal('g_lowestPrice',10,2)->nullable();
            $table->decimal('g_highestPrice',10,2)->nullable();
            $table->decimal('g_supply',10,2)->nullable();
            $table->decimal('g_last_entry',10,2)->nullable();
            $table->decimal('g_percentage_LtoH',10,2)->nullable();
            $table->unsignedBigInteger('unit_retail')->nullable();
            $table->unsignedBigInteger('unit_wholesale')->nullable();
            $table->unsignedBigInteger('unit_grower')->nullable();
            $table->unsignedBigInteger('submitted_by')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('verified_by')->nullable();
            $table->date('verification_date')->nullable();
            $table->unsignedBigInteger('comment_id')->nullable();
            $table->date('comparison_date')->nullable();
            $table->tinyInteger('submit_status')->default(0);
            $table->tinyInteger('status')->default(1)->comment('1=active, 2=inactive');
            $table->timestamps();
            $table->Integer('dam_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cpi_market_commodity_weekly_price_details');
    }
}
