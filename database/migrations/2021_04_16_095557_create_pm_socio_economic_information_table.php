<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePmSocioEconomicInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pm_socio_economic_info', function (Blueprint $table) {
            $table->id();
            $table->integer('pusti_mapping_id');
            $table->date('pusti_mapping_date');
            $table->string('name_en', 100);
            $table->string('name_bn', 255)->nullable();
            $table->string('father_name_en', 100);
            $table->string('father_name_bn', 255)->nullable();
            $table->string('mother_name_en', 100);
            $table->string('mother_name_bn', 255)->nullable();
            $table->date('age')->comment('Date of birth will be taken as input');
            $table->tinyInteger('gender')->comment('1 = Boy, 2 = Girl');
            $table->tinyInteger('father_education_status')->comment('1 = Never Goes to School, 2 = Primary, 3 = Secondary or Hiegher Secondary , 4 = Honours');
            $table->tinyInteger('mother_education_status')->comment('1 = Never Goes to School, 2 = Primary, 3 = Secondary/Hiegher Secondary , 4 = Honours');;
            $table->tinyInteger('earning_member')->comment('1 = Father, 2 = Mother, 3 = Father/Mother , 4 = Others');;
            $table->tinyInteger('monthly_family_income')->comment('1 = <5000, 2 = 5000 - 10000, 3 = 10000 - 20000, 4 = >20000');
            $table->tinyInteger('income_source')->comment('1 = Business/Hervest, 2 = Service, 3 = Teacher, 4 = Others');
            $table->tinyInteger('family_category')->comment('1 = Single, 2 = Join');
            $table->tinyInteger('no_of_family_member');
            $table->tinyInteger('religion')->comment('1 = Muslim, 2 = Hindu, 3 = Christian, 4  = Buddhist');
            $table->tinyInteger('area_type_id');
            $table->integer('division_id',11);
            $table->integer('district_id',11);
            $table->tinyInteger('city_corporation_id')->nullable();
            $table->integer('ward_id',11)->nullable();
            $table->integer('pauroshoba_id',11)->nullable();
            $table->integer('upazila_id',11)->nullable();
            $table->integer('union_id',11)->nullable();
            $table->string('village_en', 255)->nullable();
            $table->string('village_bn', 255)->nullable();
            $table->string('mobile_no', 11);
            $table->tinyInteger('status')->default(1)->comment('1 = active, 2 = inactive');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pm_socio_economic_info');
    }
}
