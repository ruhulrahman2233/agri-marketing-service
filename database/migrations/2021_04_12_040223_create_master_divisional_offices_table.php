<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterDivisionalOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_divisional_offices', function (Blueprint $table) {
            $table->id();
            $table->string('office_name');
            $table->string('office_name_bn')->nullable();
            $table->integer('division_id'); 
            $table->integer('district_id'); 
            $table->string('name',100);
            $table->string('name_bn',100)->nullable();
            $table->string('email',100);
            $table->string('mobile_no',100);
            $table->string('address')->nullable();
            $table->string('address_bn')->nullable();
            $table->string('designation')->nullable();
            $table->string('designation_bn')->nullable();
            $table->string('remarks')->nullable();
            $table->string('remarks_bn')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1=active, 2=inactive');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_divisional_offices');
    }
}
