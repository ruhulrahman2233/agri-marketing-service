<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDamDivisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dam_divisions', function (Blueprint $table) {
            $table->integer('id')->nullable();
            $table->string('division_title')->nullable();
            $table->string('division_title_bangla',100)->nullable();
            $table->tinyInteger('division_status')->default(1)->comment('1=active, 2=inactive');
            $table->integer('dam_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dam_divisions');
    }
}
