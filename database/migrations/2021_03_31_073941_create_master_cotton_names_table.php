<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterCottonNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_cotton_names', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('org_id')->nullable();
            $table->integer('cotton_variety_id'); 
            $table->string('cotton_name')->nullable();
            $table->string('cotton_name_bn')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1=active, 2=inactive');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->foreign('cotton_variety_id')->references('id')->on('master_cotton_varities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_cotton_names');
    }
}
