<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrowerCentralStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grower_central_stocks', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->tinyInteger('type')->comment('1=Ginner, 2=Grower');
            $table->integer('applicant_id');
            $table->integer('org_id');
            $table->integer('fiscal_year_id');
            $table->integer('seasons_id');
            $table->integer('cotton_variety_id');
            $table->integer('cotton_id');
            $table->float('quantity'); 
            $table->timestamps();
            $table->foreign('cotton_variety_id')->references('id')->on('master_cotton_varities');
            $table->foreign('cotton_id')->references('id')->on('master_cotton_names');
            $table->foreign('seasons_id')->references('id')->on('master_seasons');
            $table->foreign('applicant_id')->references('id')->on('ginner_grower_profiles');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grower_central_stocks');
    }
}
