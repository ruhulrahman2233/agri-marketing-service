<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGinnerGrowerProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ginner_grower_profiles', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->tinyInteger('type')->comment('1=Ginner, 2=Grower');
            $table->string('applicant_id')->unique();
            $table->integer('org_id');
            $table->integer('region_id');
            $table->integer('district_id');
            $table->integer('upazilla_id');
            $table->integer('unit_id');
            $table->integer('zone_id');
            $table->string('name',100)->nullable();
            $table->string('name_bn',100)->nullable();
            $table->string('father_name',100)->nullable();
            $table->string('father_name_bn',100)->nullable();
            $table->string('land_area',100)->nullable();
            $table->string('address')->nullable();
            $table->string('address_bn')->nullable();
            $table->string('mobile_no',100)->nullable();
            $table->string('nid',100)->nullable();
            $table->string('password')->nullable();
            $table->tinyInteger('is_draft')->default(1)->comment('1=Draft, 2=Final Save');
            $table->tinyInteger('status')->default(1)->comment('1=active, 2=inactive');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->foreign('region_id')->references('id')->on('master_regions');
            $table->foreign('zone_id')->references('id')->on('master_zones');
            $table->foreign('unit_id')->references('id')->on('master_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ginner_grower_profiles');
    }
}
