<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePmHealthMeasurementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pm_health_measurement', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('socio_economic_info_id');
            $table->tinyInteger('question_no_1')->comment('12 - 15 years = 1, 16 - 19 years = 2, 20 -23 years = 3, 24 - 27 years = 4, 28 - 31 years = 5, 32 - 35 years = 6, >36 years = 7');
            $table->tinyInteger('question_no_2')->comment('1 = 1, 2 = 2, 3 = 3, 4 = 4, 5 = 5, 6 = 6');
            $table->tinyInteger('question_no_3');
            $table->tinyInteger('question_no_4')->comment('<2 years = 1, 2 - 3 years = 2, 3 - 4 years = 3, >4 years = 4');
            $table->tinyInteger('question_no_5')->comment('2.5 kv = 1, 2.5kg = 2');
            $table->tinyInteger('question_no_6')->comment('1 = Yes, 2 = No');
            $table->tinyInteger('question_no_7')->comment('Superstition = 1, Illness = 2, Others = 3')->nullable();
            $table->tinyInteger('question_no_8')->comment('1 = Yes, 2 = No');
            $table->tinyInteger('question_no_9')->comment('1 = Yes, 2 = No');
            $table->tinyInteger('question_no_10')->comment('1 = Yes, 2 = No');
            $table->tinyInteger('question_no_11')->comment(' 1 Items = 1, 2 Items = 2, 3 Items = 3, 4 Items = 4, 5 Items = 5, 6 Items = 6');
            $table->tinyInteger('question_no_12')->comment('1 = Yes, 2 = No');
            $table->tinyInteger('question_no_13')->comment('1 = Yes, 2 = No');
            $table->tinyInteger('question_no_14')->comment('1 = Yes, 2 = No');
            $table->tinyInteger('question_no_15')->comment('1 = Yes, 2 = No');
            $table->tinyInteger('question_no_16')->comment('1 = Yes, 2 = No');
            $table->tinyInteger('question_no_17')->comment('1 = Yes, 2 = No');
            $table->tinyInteger('question_no_18')->comment('1 = Yes, 2 = No');
            $table->timestamps();

            $table->foreign('socio_economic_info_id')->references('id')->on('pm_socio_economic_info');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pm_health_measurement');
    }
}
