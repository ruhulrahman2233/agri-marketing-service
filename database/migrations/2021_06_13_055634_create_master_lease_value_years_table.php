<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterLeaseValueYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_lease_value_years', function (Blueprint $table) {
            $table->Integer('id')->nullable();
            $table->string('name',100)->nullable();
            $table->string('name_bn',100)->nullable();
            $table->year('name_english',100)->nullable();  
            $table->tinyInteger('status')->default(1)->comment('1=active, 2=inactive');
            $table->timestamps();
            $table->Integer('dam_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_lease_value_years');
    }
}
