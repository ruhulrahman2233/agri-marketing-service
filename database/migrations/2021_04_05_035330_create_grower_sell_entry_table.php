<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrowerSellEntryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grower_sell_entry', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('fiscal_year_id');            
            $table->integer('seasons_id');            
            $table->integer('applicant_id');
            $table->integer('hatt_id');
            $table->integer('cotton_variety_id');           
            $table->integer('cotton_id');
            $table->date('hatt_date');
            $table->float('quantity');
            $table->integer('unit_id');
            $table->float('price');           
            $table->tinyInteger('status')->default(1)->comment('1=not approved, 2=approved, 3=reject');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->foreign('cotton_variety_id')->references('id')->on('master_cotton_varities');
            $table->foreign('cotton_id')->references('id')->on('master_cotton_names');
            $table->foreign('hatt_id')->references('id')->on('master_hatts');
            $table->foreign('unit_id')->references('id')->on('master_units');
            $table->foreign('seasons_id')->references('id')->on('master_seasons');
            $table->foreign('applicant_id')->references('id')->on('ginner_grower_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grower_sell_entry');
    }
}
