<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterCommoditySubGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_commodity_sub_groups', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('org_id'); 
            $table->integer('commodity_group_id');           
            $table->string('sub_group_name')->nullable();
            $table->string('sub_group_name_bn')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1=active, 2=inactive');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->Integer('dam_id')->nullable();
            $table->foreign('commodity_group_id')->references('id')->on('master_commodity_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_commodity_sub_groups');
    }
}
