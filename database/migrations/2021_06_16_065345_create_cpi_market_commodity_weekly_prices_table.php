<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCpiMarketCommodityWeeklyPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpi_market_commodity_weekly_prices', function (Blueprint $table) {
            $table->id();
            $table->text('price_type_id')->nullable();
            $table->string('price_entry_type')->nullable();
            $table->string('select_type')->nullable();
            $table->integer('division_id')->nullable();
            $table->integer('district_id')->nullable();
            $table->integer('upazila_id')->nullable();
            $table->integer('market_id')->nullable();
            $table->integer('year')->nullable();
            $table->integer('month_id')->nullable();
            $table->integer('week_id')->nullable();
            $table->integer('week_table_id')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->Integer('dam_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cpi_market_commodity_weekly_prices');
    }
}
