<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePustiCampaignFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pusti_campaign_feedbacks', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('campaign_type_id');
            $table->unsignedBigInteger('campaign_id'); 
            $table->unsignedBigInteger('divisional_office_id'); 
            $table->string('school_name',100);
            $table->string('school_name_bn',100)->nullable();
            $table->string('attachment');
            $table->date('upload_date');            
            $table->timestamps();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->foreign('campaign_id')->references('id')->on('master_campaigns');
            $table->foreign('divisional_office_id')->references('id')->on('master_divisional_offices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pusti_campaign_feedbacks');
    }
}
