<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePustiCampaignInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pusti_campaign_informations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('campaign_id');
            $table->unsignedBigInteger('divisional_office_id');
            $table->integer('division_id');
            $table->integer('district_id');
            $table->string('document_name');
            $table->string('document_name_bn')->nullable();
            $table->date('campaign_date');
            $table->string('attachment');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->foreign('campaign_id')->references('id')->on('master_campaigns');
            $table->foreign('divisional_office_id')->references('id')->on('master_divisional_offices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pusti_campaign_informations');
    }
}
