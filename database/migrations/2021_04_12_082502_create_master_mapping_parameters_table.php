<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterMappingParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_mapping_parameters', function (Blueprint $table) {
            $table->id();
            $table->integer('month');
            $table->integer('gender')->comment('1 = Male, 2 = Female');
            $table->float('weight_range_from', 5, 2);
            $table->float('weight_range_to', 5, 2)->nullable();
            $table->float('height_range_from', 5, 2);
            $table->float('height_range_to', 5, 2)->nullable();
            $table->char('z_score', 3);
            $table->tinyInteger('status')->default(1)->comment('1 = active, 2 = inactive');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_mapping_parameters');
    }
}
