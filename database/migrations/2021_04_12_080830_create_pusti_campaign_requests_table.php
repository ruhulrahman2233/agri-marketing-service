<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePustiCampaignRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pusti_campaign_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('campaign_id');
            $table->integer('division_id');
            $table->integer('district_id');
            $table->string('school_name');
            $table->string('school_name_bn')->nullable();
            $table->string('name');
            $table->string('name_bn')->nullable();
            $table->string('designation');
            $table->string('designation_bn')->nullable();
            $table->string('email',100)->nullable();
            $table->string('mobile_no',100);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('campaign_reason')->nullable();
            $table->string('campaign_reason_bn')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1=active, 2=inactive');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->foreign('campaign_id')->references('id')->on('master_campaigns');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pusti_campaign_requests');
    }
}
