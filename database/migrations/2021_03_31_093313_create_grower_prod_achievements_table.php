<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrowerProdAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grower_prod_achievements', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('fiscal_year_id');
            $table->date('achievement_date');
            $table->integer('applicant_id');
            $table->integer('cotton_variety_id');
            $table->integer('cotton_id');
            $table->integer('seasons_id')->nullable();
            $table->float('quantity');
            $table->string('remarks')->nullable();
            $table->string('remarks_bn')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1=not close, 2=closed');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->foreign('cotton_variety_id')->references('id')->on('master_cotton_varities');
            $table->foreign('cotton_id')->references('id')->on('master_cotton_names');
            $table->foreign('seasons_id')->references('id')->on('master_seasons');
            $table->foreign('applicant_id')->references('id')->on('ginner_grower_profiles');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grower_prod_achievements');
    }
}
