<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterCommodityNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_commodity_names', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('org_id'); 
            $table->integer('commodity_group_id');
            $table->integer('commodity_sub_group_id');            
            $table->string('commodity_name')->nullable();
            $table->string('commodity_name_bn')->nullable();
            $table->integer('unit_id')->nullable();
            $table->integer('unit_grower')->nullable();
            $table->integer('unit_whole_sale')->nullable();
            $table->integer('unit_retail')->nullable();
            $table->integer('price_type_id')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1=active, 2=inactive');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->Integer('dam_id')->nullable();
            $table->timestamps();
            $table->foreign('commodity_group_id')->references('id')->on('master_commodity_groups');
            $table->foreign('commodity_sub_group_id')->references('id')->on('master_commodity_sub_groups');
            $table->foreign('unit_id')->references('id')->on('master_units');
            $table->foreign('price_type_id')->references('id')->on('master_price_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_commodity_names');
    }
}
