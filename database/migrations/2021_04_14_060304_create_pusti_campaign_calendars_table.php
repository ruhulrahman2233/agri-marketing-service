<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePustiCampaignCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pusti_campaign_calendars', function (Blueprint $table) {
            $table->id();
            $table->integer('fiscal_year_id');
            $table->tinyInteger('campaign_type_id'); 
            $table->unsignedBigInteger('campaign_id'); 
            $table->unsignedBigInteger('divisional_office_id')->nullable();
            $table->unsignedBigInteger('division_id')->nullable();
            $table->unsignedBigInteger('district_id')->nullable();
            $table->integer('quantity');
            $table->date('start_date');
            $table->date('end_date');
            $table->float('budget');
            $table->string('remarks')->nullable();
            $table->integer('remarks_bn')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->foreign('campaign_id')->references('id')->on('master_campaigns');
            $table->foreign('divisional_office_id')->references('id')->on('master_divisional_offices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pusti_campaign_calendars');
    }
}
