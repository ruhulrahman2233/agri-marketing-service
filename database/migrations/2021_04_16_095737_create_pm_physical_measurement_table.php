<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePmPhysicalMeasurementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pm_physical_measurement', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('socio_economic_info_id');
            $table->float('height', 5, 2)->comment('Unit is inch');
            $table->float('weight', 5, 2)->comment('Unit is Kg');
            $table->float('head_circumference', 5, 2)->comment('Unit is cm');
            $table->float('chest_circumference', 5, 2)->comment('Unit is cm');
            $table->tinyInteger('muac')->comment('SAM (11.5cm) = 1, MAM (11.5 - 12.5cm) = 2, Normal (>12.5cm) = 3');
            $table->timestamps();

            $table->foreign('socio_economic_info_id')->references('id')->on('pm_socio_economic_info');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pm_physical_measurement');
    }
}
