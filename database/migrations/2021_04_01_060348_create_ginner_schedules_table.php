<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGinnerSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ginner_schedules', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('fiscal_year_id');
            $table->date('schedule_date');
            $table->integer('applicant_id');
            $table->integer('cotton_variety_id');
            $table->integer('hatt_id');
            $table->integer('seasons_id');
            $table->integer('cotton_id');
            $table->float('quantity');
            $table->string('remarks')->nullable();
            $table->string('remarks_bn')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1=not approved, 2=approved, 3=reject');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->foreign('cotton_variety_id')->references('id')->on('master_cotton_varities');
            $table->foreign('cotton_id')->references('id')->on('master_cotton_names');
            $table->foreign('hatt_id')->references('id')->on('master_hatts');
            $table->foreign('seasons_id')->references('id')->on('master_seasons');
            $table->foreign('applicant_id')->references('id')->on('ginner_grower_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ginner_schedules');
    }
}
