<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterZonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_zones', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('org_id')->nullable();
            $table->string('zone_name',100)->nullable();
            $table->string('zone_name_bn',100)->nullable();
            $table->integer('region_id');
            $table->tinyInteger('status')->default(1)->comment('1=active, 2=inactive');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->foreign('region_id')->references('id')->on('master_regions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_zones');
    }
}
