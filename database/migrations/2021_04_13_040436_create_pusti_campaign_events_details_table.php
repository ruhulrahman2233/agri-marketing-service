<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePustiCampaignEventsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pusti_campaign_events_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pusti_campaign_event_id'); 
            $table->integer('division_id');
            $table->integer('district_id');
            $table->timestamps();
            $table->foreign('pusti_campaign_event_id')->references('id')->on('pusti_campaign_events');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pusti_campaign_events_details');
    }
}
