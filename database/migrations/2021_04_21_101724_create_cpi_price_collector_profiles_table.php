<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCpiPriceCollectorProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpi_price_collector_profiles', function (Blueprint $table) {
            $table->id(); 
            $table->unsignedBigInteger('division_id')->nullable();
            $table->unsignedBigInteger('district_id')->nullable();
            $table->unsignedBigInteger('upazilla_id')->nullable();
            $table->unsignedBigInteger('union_id')->nullable();                    
            $table->string('name',100)->nullable();
            $table->string('name_bn',100)->nullable();
            $table->string('father_name',100)->nullable();
            $table->string('father_name_bn',100)->nullable(); 
            $table->string('designation',100)->nullable();
            $table->string('designation_bn')->nullable();            
            $table->string('address')->nullable();
            $table->string('address_bn')->nullable();
            $table->string('mobile_no',100)->nullable();
            $table->string('nid')->nullable();
            $table->string('latitude',100)->nullable();
            $table->string('longitude',100)->nullable();
            $table->string('remarks')->nullable();
            $table->string('remarks_bn')->nullable();
            $table->string('attachment')->nullable();
            $table->string('signature')->nullable();            
            $table->string('app_rej_reason')->nullable();
            $table->string('app_rej_reason_bn')->nullable();            
            $table->tinyInteger('is_approved')->default(1)->comment('1=no, 2=approved,3=reject');
            $table->tinyInteger('status')->default(1)->comment('1=active, 2=inactive');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cpi_price_collector_profiles');
    }
}
