<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePustiCampaignMatDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pusti_campaign_mat_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('campaign_materials_id')->nullable(); 
            $table->string('material_name');
            $table->string('material_name_bn')->nullable();
            $table->string('attachment')->nullable();
            $table->integer('created_by');
            $table->timestamps();
            $table->foreign('campaign_materials_id')->references('id')->on('pusti_campaign_materials');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pusti_campaign_mat_details');
    }
}
