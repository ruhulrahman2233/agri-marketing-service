<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarketDirectoryCommoditiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_market_directory_commodities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('market_directory_id')->nullable();
            $table->text('commodity')->nullable();
            $table->string('unit',100)->nullable();
            $table->integer('quantity')->nullable();
            $table->timestamps();
            $table->Integer('dam_id')->nullable();
            $table->foreign('market_directory_id')->references('id')->on('master_markets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('market_directory_commodities');
    }
}
