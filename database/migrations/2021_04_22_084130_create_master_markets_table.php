<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterMarketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_markets', function (Blueprint $table) {
            $table->id();
            $table->integer('division_id')->nullable();
            $table->integer('district_id')->nullable();
            $table->integer('upazila_id')->nullable();
            $table->string('market_name',100)->nullable();
            $table->string('market_name_bn',100)->nullable();
            $table->year('foundation_year')->nullable();
            $table->string('village',100)->nullable();
            $table->string('post_office',100)->nullable();
            $table->string('union',100)->nullable();
            $table->string('market_type',100)->nullable();
            $table->unsignedBigInteger('govt_covered')->nullable();
            $table->unsignedBigInteger('govt_open')->nullable();
            $table->unsignedBigInteger('private_covered')->nullable();
            $table->unsignedBigInteger('private_open')->nullable();
            $table->unsignedBigInteger('total_market_area')->nullable();
            $table->unsignedBigInteger('shed_no')->nullable();
            $table->unsignedBigInteger('shed_area')->nullable();
            $table->unsignedBigInteger('stall_no_agri')->nullable();
            $table->unsignedBigInteger('stall_no_nonagri')->nullable();
            $table->text('hat_days')->nullable();
            $table->time('market_time_from')->nullable();
            $table->time('market_time_to')->nullable();
            $table->text('infrastructure_id')->nullable();
            $table->text('communication_linkage_id')->nullable();
            $table->tinyInteger('number_of_buyers')->nullable();
            $table->tinyInteger('number_of_sellers')->nullable();
            $table->string('farmer_share',100)->nullable();
            $table->string('trader_share',100)->nullable();
            $table->string('other_product_destination',100)->nullable();
            $table->text('product_destination',100)->nullable();
            $table->text('vehicle_id')->nullable();
            $table->unsignedBigInteger('avg_distance')->nullable();
            $table->year('data_collection_year')->nullable();
            $table->string('mobile_market_committee',100)->nullable();
            $table->string('market_representative_name',100)->nullable();
            $table->string('market_representative_mobile',100)->nullable();
            $table->decimal('latitude',10,2)->nullable();
            $table->decimal('longitude',10,2)->nullable();
            $table->tinyInteger('approval_status')->default(1)->comment('1=pending, 2=approved, 2=rejected');
            $table->tinyInteger('status')->default(1)->comment('1=active, 2=inactive');
            $table->date('approved_date');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->Integer('dam_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_markets');
    }
}
