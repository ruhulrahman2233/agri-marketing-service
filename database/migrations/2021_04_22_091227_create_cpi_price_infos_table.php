<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCpiPriceInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpi_price_infos', function (Blueprint $table) {
            $table->id();
            $table->date('PriceDate');
            $table->decimal('LatitudeRecorded',10,4)->nullable();
            $table->decimal('LongitudeRecorded',10,4)->nullable();
            $table->decimal('HighestPrice',10,2)->nullable();
            $table->decimal('LowestPrice',10,2)->nullable();
            $table->decimal('AveragePrice',10,2)->nullable();
            $table->decimal('PriceUSDRate',10,2)->nullable();
            $table->decimal('PriceEuroRate',10,2)->nullable();
            $table->date('VerificationDate')->nullable();
            $table->time('VerificationTime')->nullable();
            $table->integer('PriceType_id')->nullable();
            $table->integer('Market_id')->nullable();
            $table->integer('Subdistrict_id')->nullable();
            $table->integer('District_id')->nullable();
            $table->integer('Division_id')->nullable();
            $table->integer('Commodity_id')->nullable();
            $table->integer('BuyerSellerType')->nullable();
            $table->decimal('DailyVolume',10,2)->nullable();
            $table->string('MarketPriceEnteredBy')->nullable();
            $table->string('Verified_by')->nullable();
            $table->timestamps();
            $table->index('PriceDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cpi_price_infos');
    }
}
