<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkageGroBuyProDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linkage_gro_buy_pro_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('gro_buy_profiles_id');
            $table->Integer('commodity_group_id');
            $table->Integer('commodity_sub_group_id');
            $table->Integer('commodity_id');
            $table->Integer('price_type_id');
            $table->Integer('unit_id');
            $table->float('price')->default(0);
            $table->float('quantity')->default(0);
            $table->string('attachment')->nullable();
            $table->string('origin')->nullable();
            $table->string('origin_bn')->nullable();
            $table->string('remarks')->nullable();
            $table->string('remarks_bn')->nullable();
            $table->foreign('gro_buy_profiles_id')->references('id')->on('linkage_gro_buy_profiles');
            $table->foreign('commodity_group_id')->references('id')->on('master_commodity_groups');
            $table->foreign('commodity_sub_group_id')->references('id')->on('master_commodity_sub_groups');
            $table->foreign('commodity_id')->references('id')->on('master_commodity_names');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('linkage_gro_buy_pro_details');
    }
}
