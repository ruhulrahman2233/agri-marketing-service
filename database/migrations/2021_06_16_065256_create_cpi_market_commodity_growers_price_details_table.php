<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCpiMarketCommodityGrowersPriceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpi_market_commodity_growers_price_details', function (Blueprint $table) {
            $table->id();
            $table->integer('price_table_id')->nullable();
            $table->string('price_type',100)->nullable();
            $table->integer('com_grp_id')->nullable();
            $table->integer('com_subgrp_id')->nullable();
            $table->integer('commodity_id')->nullable();
            $table->integer('unit_wholesale')->nullable();
            $table->decimal('g_lowestPrice',10,2)->nullable();
            $table->decimal('g_highestPrice',10,2)->nullable();
            $table->decimal('g_supply',10,2)->nullable();
            $table->decimal('g_avgPrice',10,2)->nullable();
            $table->decimal('g_percentage_LtoH',10,2)->nullable();
            $table->decimal('g_last_entry',10,2)->nullable();
            $table->integer('unit_grower')->nullable();
            $table->unsignedBigInteger('submitted_by')->nullable();
            $table->unsignedBigInteger('verified_by')->nullable();
            $table->date('verification_date')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1=active, 2=inactive');
            $table->unsignedBigInteger('comment_id')->nullable();
            $table->date('price_date')->nullable();
            $table->timestamps();
            $table->Integer('dam_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cpi_market_commodity_growers_price_details');
    }
}
