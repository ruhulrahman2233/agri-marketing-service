<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarketMarketDirectoryLeaseValueYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_market_directory_lease_value_years', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('market_directory_id')->nullable();
            $table->unsignedBigInteger('lease_year_id')->nullable();
            $table->integer('value')->nullable();
            $table->timestamps();
            $table->Integer('dam_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('market_market_directory_lease_value_years');
    }
}
